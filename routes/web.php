<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/not-ready', 'HomeController@launchingSoon');
Route::get('/', 'HomeController@index')->name('landing');
Auth::routes(['verify' => true]);

Route::get('/home', function() {
    return redirect('/admin');
})->name('home');

require_once('Routes/admin.php');
require_once('Routes/about.php');
require_once('Routes/product.php');
require_once('Routes/marketing.php');
require_once('Routes/contact-us.php');


Route::get('/admin-run-migrations', function() {
    return Artisan::call('migrate', ["--force" => true ]);
})->middleware('auth');

Route::get('test-mail', function() {
    $order = \App\Models\Order::find(59);
    if ($order->product->type == "product") {
        $url = route('my-orders.index', ["tab" => "received"]);
    } else {
        $url = route('admin.activation.index');
    }

    \App\Http\Helpers\Helpers::notifyRecievedOrder($order, $url);

    return 'Email sent successfully.';
});


Route::get('fix-tree', function() {
    \App\Models\Membership::fixTree();
});

Route::get('switch-from-free/{user_id}', function($user_id) {
    \App\Http\Helpers\Helpers::earnReferralCommission($user_id, true);
});

Route::get('fix-commissions/{username}', function ($username) {

    $user = \App\User::whereUsername($username)->first();
    if (!$user && $user->role_id != 1) {
        return "Member not found.";
    }
    
    \DB::transaction(function () use ($user) {
        
        
        $earnings = \App\Http\Helpers\Helpers::computeEarnings($user->membership->id);
        $encashments = $user->membership->encashment_histories()->whereIn("status", ["pending", "complete"])->sum("amount");
        $available_commissions = $earnings - $encashments;
        $user->membership->update([
            "available_commissions" => $available_commissions
        ]);
    });

});


Route::group(['prefix' => 'contact-us', 'as' => 'contact-us.', 'namespace' => 'Admin'], function() {
    Route::post('/postMessage', ['as' => 'post-message', 'uses' => 'ContactUsController@postMessage']);
});

Route::get('admin-members-claim-bonus', function() {
    $now = \Carbon\Carbon::now();

    $secondDayOfThisMonth = $now->firstOfMonth()->addDays(1)->toDateString();
    $bonuses = \App\Models\Earning::whereNull("added_to_wallet_at")
                                ->whereIn("type", ["unilevel", "indirect referral"])
                                ->whereDate("valid_at", "<", $secondDayOfThisMonth)
                                ->get();

    $membersToClaimBonus = \App\Models\Membership::whereHas('earnings', function($q) use ($secondDayOfThisMonth) {
                                        $q->whereNull('added_to_wallet_at')
                                            ->whereIn('type', ['unilevel', 'indirect referral'])
                                            ->whereDate('valid_at', '<', $secondDayOfThisMonth);
                                    })->get();
                                    
    \DB::transaction(function () use ($membersToClaimBonus) {
        $membersToClaimBonus->each(function($member) {
            \App\Http\Helpers\TransactionsHelper::claimBonus($member, 2);
        });
    });
});

Route::get('fix-wrong-valid-at', function() {
    $earnings = \App\Models\Earning::whereNotNull('valid_at')
                                ->whereIn('type', ['unilevel', 'indirect referral'])
                                ->select('id', 'type', 'member_id', 'added_to_wallet_at', 'created_at', 'valid_at')
                                ->whereRaw("DATE_FORMAT(DATE_ADD(created_at, interval 1 MONTH), '%m') <> DATE_FORMAT(valid_at, '%m')")
                                ->get();

    \DB::beginTransaction();

    try {

        foreach ($earnings as $earning) {
            $valid_at = \Carbon\Carbon::parse($earning->created_at)->firstOfMonth()->addMonths(1);
            $earning->valid_at = $valid_at->toDateTimeString();
            $earning->save();
        };

        \DB::commit();
    } catch (\Throwable $th) {
        \DB::rollback();
        throw $th;
    }
});

Route::group(['prefix' => 'posts', 'as' => 'posts.'], function () {
    Route::get('', ['as' => 'index', 'uses' => 'PostController@index']);
    Route::get('create', ['as' => 'create', 'uses' => 'PostController@create']);
    Route::get('{post}', ['as' => 'edit', 'uses' => 'PostController@edit']);
    Route::post('', ['as' => 'store', 'uses' => 'PostController@store']);
    Route::post('{post}', ['as' => 'update', 'uses' => 'PostController@update']);
});

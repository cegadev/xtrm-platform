<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'products', 'as' => 'api-products.', 'namespace' => 'Api'], function () {
    Route::get('/', ['as' => 'products-list', 'uses' => 'ProductsController@list']);
    Route::get('/view-product/{productId}', ['as' => 'view', 'uses' => 'ProductsController@viewProduct']);
    Route::post('/review/{product}', ['as' => 'write-review', 'uses' => 'ProductsController@writeReview']);
});

Route::group(['prefix' => 'orders', 'as' => 'api-orders.', 'namespace' => 'Api'], function () {
    Route::get('/get-order', ['as' => 'get-order', 'uses' => 'OrdersController@getOrder']);
    Route::get('/pending-orders', ['as' => 'pending-orders', 'uses' => 'OrdersController@pendingOrders']);
    Route::get('/available-orders-for-activation', ['as' => 'available-orders-for-activation', 'uses' => 'OrdersController@availableOrders']);

    Route::post('/transfer-item-of-order', ['as' => 'transfer-item', 'uses' => 'OrdersController@transferItem']);
    Route::post('/activate-product', ['as' => 'activate-product', 'uses' => 'ActivationController@activateProduct']);
});

Route::group(['prefix' => 'members', 'as' => 'api-members.', 'namespace' => 'Api'], function () {
    Route::get('/', ['as' => 'members-list', 'uses' => 'MembersController@list']);
    Route::get('/member-earnings', ['as' => 'member-earnings', 'uses' => 'MembersController@memberEarnings']);
});

Route::group(['prefix' => 'activation', 'as' => 'api-activation.', 'namespace' => 'Api'], function () {
    Route::post('/validate-activation', ['as' => 'validate-activation', 'uses' => 'ActivationController@validateActivation']);
    Route::post('/validate-new-user', ['as' => 'validate-new-user', 'uses' => 'ActivationController@validateNewUser']);

    Route::get('/validate-product-code', ['as' => 'validate-product-code', 'uses' => 'ActivationController@validateActivationCode']);
    Route::get('/view-history', ['as' => 'view-history', 'uses' => 'ActivationController@viewHistory']);

    Route::post('/register-new-member', ['as' => 'register-new-member', 'uses' => 'ActivationController@registerNewMember']);
});

Route::group(['prefix' => 'membership', 'as' => 'api-memberships.', 'namespace' => 'Api'], function () {
    Route::get('/earnings', ['as' => 'earnings', 'uses' => 'MembershipController@earnings']);
    Route::get('/genealogy', ['as' => 'genealogy', 'uses' => 'MembershipController@genealogy']);
    Route::get('/earnings-type', ['as' => 'earnings-type', 'uses' => 'MembershipController@earningsType']);
});

Route::group(['prefix' => 'bank-infos', 'as' => 'api-bank-infos.', 'namespace' => 'Api'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'BankInfoController@index']);
    Route::get('/{member_bank_info}', ['as' => 'bank-info', 'uses' => 'BankInfoController@bankInfo']);
});

Route::group(['prefix' => 'encashment-history', 'as' => 'api-encashment.', 'namespace' => 'Api'], function () {
    Route::get('/pending-count', ['as' => 'pending-count', 'uses' => 'EncashmentController@pendingCount']);
    Route::get('/{encashmentHistory}', ['as' => 'encashment-info', 'uses' => 'EncashmentController@viewEncashment']);
});

Route::group(['prefix' => 'reports', 'as' => 'api-reports.', 'namespace' => 'Api'], function() {
    Route::get('encashment-chart', ['as' => 'encashment-chart', 'uses' => 'ReportsController@encashmentChart']);
    Route::get('encashment-data', ['as' => 'encashment-data', 'uses' => 'ReportsController@encashmentJsonData']);

    Route::get('sales-chart', ['as' => 'sales-chart', 'uses' => 'ReportsController@salesChart']);
    Route::get('sales-data', ['as' => 'sales-data', 'uses' => 'ReportsController@salesJsonData']);

});

Route::group(['prefix' => 'activation-code', 'as' => 'api-activation-code.', 'namespace' => 'Api'], function() {
    Route::post('validate-registration', ['as' => 'validate-registration', 'uses' => 'ActivationCodesController@validateRegistration']);
    Route::post('register-activation-code', ['as' => 'register-activation-code', 'uses' => 'ActivationCodesController@registerActivationCode']);
    Route::post('activate-item-of-order', ['as' => 'activate-item-of-order', 'uses' => 'ActivationCodesController@activateItemOfOrder']);

    Route::get('/my-codes', ['as' => 'my-codes', 'uses' => 'ActivationCodesController@myCodes']);
    Route::get('/pending-users', ['as' => 'pending-users', 'uses' => 'ActivationCodesController@pendingUsers']);
});

Route::group(['prefix' => 'transactions', 'as' => 'api-transactions', 'namespace' => 'Api'], function() {
    Route::get('/', ['as' => 'transactions', 'uses' => 'TransactionsController@transactions']);
});

Route::group(['prefix' => 'categories', 'as' => 'api-categories', 'namespace' => 'Api'], function() {
    Route::post('/save', ['as' => 'save-category', 'uses' => 'ProductsController@saveCategory']);
    Route::post('/update', ['as' => 'update-category', 'uses' => 'ProductsController@updateCategory']);
    Route::post('/delete', ['as' => 'delete-category', 'uses' => 'ProductsController@deleteCategory']);
});

<?php

Route::group(['prefix' => 'products', 'as' => 'site-products.'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'ProductController@index']);
    Route::get('/{productSlug}', ['as' => 'info', 'uses' => 'ProductController@viewProduct']);
});
<?php

Route::group(['prefix' => 'marketing', 'as' => 'marketing.'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'MarketingController@index']);
});
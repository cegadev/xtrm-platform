<?php

Route::group(['prefix' => 'contact-us', 'as' => 'contact-us.'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'ContactUsController@index']);
});
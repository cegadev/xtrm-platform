<?php 

Route::group(['prefix' => 'about-us', 'as' => 'about-us.'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'AboutUsController@index']);
});
<?php

Route::get('/verify-email', ['as' => 'verify-email', 'uses' => 'Admin\ProfileController@verifyEmail']);

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'as' => 'admin.', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    
    Route::group(['prefix' => 'activation', 'as' => 'activation.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ActivationController@index']);
        Route::post('/postActivation', ['as' => 'activate', 'uses' => 'ActivationController@activate']);
        Route::get('/transfer/{order}', ['as' => 'transfer', 'uses' => 'ActivationController@transfer']);
        Route::post('/transfer/{order}/send', ['as' => 'send-package', 'uses' => 'ActivationController@sendPackage']);
    });
    
    Route::group(['prefix' => 'members', 'as' => 'members.', 'middleware' => ['role:Admin|Manager']], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'MembersController@index', 'middleware' => 'permission:members.read']);
        
        Route::group(['middleware' => 'role:Admin'], function() {

            Route::get('/create', ['as' => 'member-create', 'uses' => 'MembersController@create', 'middleware' => 'permission:members.write']);
            Route::get('/edit/{user}', ['as' => 'member-edit', 'uses' => 'MembersController@edit', 'middleware' => 'permission:members.write']);
            Route::post('/save-member', ['as' => 'save-member', 'uses' => 'MembersController@saveMember', 'middleware' => 'permission:members.write']);
            Route::post('/update-member/{user}', ['as' => 'update-member', 'uses' => 'MembersController@updateMember', 'middleware' => 'permission:members.write']);

            Route::get('/change-password/{user}', ['as' => 'change-member-password', 'uses' => 'MembersController@changePassword']);
            Route::post('/update-password/{user}', ['as' => 'update-member-password', 'uses' => 'MembersController@updatePassword']);

            Route::get('earnings/{membership}', ['as' => 'member-earnings', 'uses' => 'MembersController@earnings']);
            Route::post('/upgrade/{member}', ['as' => 'member-upgrades', 'uses' => 'MembersController@upgradeAccount']);
        });

    });

    Route::group(['prefix' => 'encashment-requests', 'as' => 'encashment-requests.', 'middleware' => 'role:Admin'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'EncashmentController@requests']);
        Route::get('/{encashment}', ['as' => 'view-encashment', 'uses' => 'EncashmentController@view']);
        Route::post('/action-requests', ['as' => 'action-requests', 'uses' => 'EncashmentController@actionRequests']);
    });

    Route::group(['prefix' => 'genealogy', 'as' => 'genealogy.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'MembersController@genealogy']);
    });
    
    Route::group(['prefix' => 'encashment', 'as' => 'encashment.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'EncashmentController@index', 'middleware' => 'role:Member']);
        Route::post('/create-request', ['as' => 'create-request', 'uses' => 'EncashmentController@createRequest', 'middleware' => 'role:Member']);
    });

    Route::group(['prefix' => 'reports', 'as' => 'reports.', 'middleware' => 'role:Admin'], function() {
        Route::group(['prefix' => 'encashment', 'as' => 'encashment.'], function() {
            Route::get('/', ['as' => 'index', 'uses' => 'ReportsController@encashment']);
        });
        
        Route::group(['prefix' => 'sales', 'as' => 'sales.'], function() {
            Route::get('/', ['as' => 'index', 'uses' => 'ReportsController@sales']);
        });
        
    });

    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ProfileController@index']);
        Route::get('/edit', ['as' => 'edit', 'uses' => 'ProfileController@edit']);
        Route::put('/update', ['as' => 'update', 'uses' => 'ProfileController@update']);
        Route::get('/change-password', ['as' => 'change-password', 'uses' => 'ProfileController@changePassword']);
        Route::post('/update-password', ['as' => 'update-password', 'uses' => 'ProfileController@updatePassword']);

        Route::get('/change-photo', ['as' => 'change-photo', 'uses' => 'ProfileController@changePhoto']);
        Route::post('/update-photo', ['as' => 'update-photo', 'uses' => 'ProfileController@updatePhoto']);
        
        Route::post('/update-bank-info/{key}', ['as' => 'update-bank-info', 'uses' => 'ProfileController@updateBankInfo']);
        Route::post('/new-bank-info', ['as' => 'new-bank-info', 'uses' => 'ProfileController@newBank']);
        Route::post('/remove-bank', ['as' => 'remove-bank', 'uses' => 'ProfileController@removeBank']);

    });

});

Route::group(['prefix' => 'admin/products', 'middleware' => 'auth', 'as' => 'products.', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'ProductsController@index', 'middleware' => 'permission:products.read']);
    Route::get('/create', ['as' => 'create', 'uses' => 'ProductsController@create', 'middleware' => 'permission:products.write']);
    Route::get('/edit/{product}', ['as' => 'edit', 'uses' => 'ProductsController@edit', 'middleware' => 'permission:products.write']);
    Route::post('/save', ['as' => 'save-product', 'uses' => 'ProductsController@save', 'middleware' => 'permission:products.write']);
    Route::put('/update/{product}', ['as' => 'update-product', 'uses' => 'ProductsController@update', 'middleware' => 'permission:products.write']);
    
    Route::post('enable', ['as' => 'enable-product', 'uses' => 'ProductsController@enableItem']);
    Route::post('disable', ['as' => 'disable-product', 'uses' => 'ProductsController@disableItem']);
});

Route::group(['prefix' => 'admin/packages', 'middleware' => 'auth', 'as' => 'packages.', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'PackagesController@index', 'middleware' => 'permission:packages.read']);
    Route::get('/create', ['as' => 'create', 'uses' => 'PackagesController@create', 'middleware' => 'permission:packages.write']);
    Route::get('/edit/{package}', ['as' => 'edit', 'uses' => 'PackagesController@edit', 'middleware' => 'permission:packages.write']);
    Route::post('/save', ['as' => 'save-package', 'uses' => 'PackagesController@save', 'middleware' => 'permission:packages.write']);
    Route::put('/update/{package}', ['as' => 'update-package', 'uses' => 'PackagesController@update', 'middleware' => 'permission:packages.write']);

    Route::post('enable', ['as' => 'enable-package', 'uses' => 'PackagesController@enableItem']);
    Route::post('disable', ['as' => 'disable-package', 'uses' => 'PackagesController@disableItem']);
});

Route::group(['prefix' => 'admin/orders', 'middleware' => ['auth', 'role:Admin|Manager'], 'as' => 'orders.', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'OrdersController@index']);
    
    Route::group(['middleware' => 'role:Admin'], function() {
        Route::get('/create', ['as' => 'create', 'uses' => 'OrdersController@create', 'middleware' => 'permission:orders.write']);
        Route::post('/save', ['as' => 'save-order', 'uses' => 'OrdersController@save', 'middleware' => 'permission:orders.write']);
    });
    Route::post('/approve-request/{order}', ['as' => 'approve-request', 'uses' => 'OrdersController@approveRequest', 'middleware' => 'permission:orders.write']);
    Route::post('/cancel-request/{order}', ['as' => 'cancel-request', 'uses' => 'OrdersController@cancelRequest', 'middleware' => 'permission:orders.write']);
    Route::get('/{order}', ['as' => 'view', 'uses' => 'OrdersController@view', 'middleware' => 'permission:orders.read']);
    Route::post('approve-order', ['as' => 'approve-order', 'uses' => 'OrdersController@approveOrder', 'middleware' => 'permission:orders.write']);
});

Route::group(['prefix' => 'admin/my-orders', 'middleware' => 'auth', 'as' => 'my-orders.', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'OrdersController@myOrders']);
    Route::post('/saveOrderRequest', ['as' => 'save-order-request', 'uses' => 'OrdersController@saveOrderRequest']);
    Route::post('/mark-order-complete/{order}', ['as' => 'mark-order-complete', 'uses' => 'OrdersController@markOrderComplete']);
    Route::post('/update-order/{order}', ['as' => 'update-order-request', 'uses' => 'OrdersController@updateOrderRequest']);
    Route::post('/cancel-order', ['as' => 'cancel-order', 'uses' => 'OrdersController@cancelOrder']);


    Route::group(['prefix' => 'request', 'as' => 'request.', 'middleware' => 'role:Member'], function() {
        Route::get('/new', ['as' => 'new-order', 'uses' => 'OrdersController@memberNewRequest']);
        Route::get('/edit/{order}', ['as' => 'edit-order', 'uses' => 'OrdersController@memberEditRequest']);
    });

    Route::group(['prefix' => 'activate', 'as' => 'activate.'], function() {
        Route::get('/{order}', ['as' => 'index', 'uses' => 'OrdersController@activateOrder']);
        Route::post('/{order}/activate', ['as' => 'activate', 'uses' => 'OrdersController@postActivate']);

        Route::get('/activation/complete', ['as' => 'after-activation', 'uses' => 'OrdersController@afterActivation']);
    });

    Route::group(['prefix' => 'my-activation-codes', 'as' => 'my-activation-codes.'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'ActivationCodesController@assignedToMe']);
        Route::get('/after-registercode', ['as' => 'after-registercode', 'uses' => 'ActivationCodesController@afterRegisterCode']);
    });
    
    Route::group(['prefix' => 'transactions', 'as' => 'transactions.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'TransactionController@index']);
    });

    Route::group(['prefix' => 'activations', 'as' => 'activations.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'TransactionController@activations']);
    });
});

// Route::group(['prefix' => 'admin/branches', 'middleware' => ['auth', 'role:Admin'], 'as' => 'branches.', 'namespace' => 'Admin'], function () {
//     Route::get('/', ['as' => 'index', 'uses' => 'BranchController@index']);
//     Route::get('/create', ['as' => 'create', 'uses' => 'BranchController@create']);
//     Route::post('/save', ['as' => 'save', 'uses' => 'BranchController@save']);
//     Route::get('/{branch}', ['as' => 'view', 'uses' => 'BranchController@view']);
//     Route::get('/edit/{branch}', ['as' => 'edit', 'uses' => 'BranchController@edit']);
//     Route::post('/update/{branch}', ['as' => 'update', 'uses' => 'BranchController@update']);

//     Route::group(['prefix' => 'managers', 'as' => 'managers.'], function() {
//         Route::get('/{branch}/manager/create', ['as' => 'create', 'uses' => 'BranchManagerController@create']);
//         Route::post('/{branch}/manager/save', ['as' => 'save', 'uses' => 'BranchManagerController@save']);
//         Route::post('/{branch}/manager/remove', ['as' => 'remove', 'uses' => 'BranchManagerController@remove']);
//         Route::post('/{branch}/manager/enable', ['as' => 'enable', 'uses' => 'BranchManagerController@enable']);
//     });
// });

Route::group(['prefix' => 'admin/activation-codes', 'middleware' => ['auth', 'role:Admin'], 'as' => 'activation-codes.', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'ActivationCodesController@index']);
    Route::post('/generate', ['as' => 'generate', 'uses' => 'ActivationCodesController@generateCode']);
    Route::post('/send-activation-codes', ['as' => 'send-activation-codes', 'uses' => 'ActivationCodesController@sendActivationCodes']);
    Route::get('/after-send-activation-code', ['as' => 'after-send-activation-code', 'uses' => 'ActivationCodesController@afterSendActivationCode']);
    Route::get('histories', ['as' => 'histories', 'uses' => 'ActivationCodesController@histories']);
});

Route::group(['prefix' => 'admin/site-settings', 'middleware' => ['auth', 'role:Admin'], 'as' => 'site-settings.', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'SiteController@index']);

    Route::group(['prefix' => 'site-logo', 'as' => 'site-logo.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'SiteController@sitelogo']);
        Route::post('/update', ['as' => 'update', 'uses' => 'SiteController@updateSiteLogo']);

        Route::get('/icon', ['as' => 'icon', 'uses' => 'SiteController@siteIcon']);
        Route::post('/icon/update', ['as' => 'icon-update', 'uses' => 'SiteController@updateSiteIcon']);
    });

    Route::group(['prefix' => 'homepage-slider', 'as' => 'slider.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'SiteController@slider']);
        Route::post('/update', ['as' => 'update', 'uses' => 'SiteController@updateSlider']); 
    });

    Route::group(['prefix' => 'about-us', 'as' => 'about-us.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'SiteController@aboutUs']);
        Route::post('/update', ['as' => 'update', 'uses' => 'SiteController@updateAboutUs']);
    });

    Route::group(['prefix' => 'backgrounds', 'as' => 'backgrounds.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'SiteController@backgrounds']);
        Route::post('upload', ['as' => 'upload', 'uses' => 'SiteController@upload']);
    });

    Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'SiteController@products']);
        Route::post('save-products', ['as' => 'save-products', 'uses' => 'SiteController@saveProducts']);
    });

    Route::group(['prefix' => 'packages', 'as' => 'packages.'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'SiteController@packages']);
        Route::post('save-sorting', ['as' => 'save-sorting', 'uses' => 'SiteController@savePackageSorting']);
    });

    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {
        Route::group(['prefix' => 'about-us', 'as' => 'about-us.'], function () {
            Route::get('/edit', ['as' => 'index', 'uses' => 'AboutUsController@edit']);
            Route::post('/update', ['as' => 'update', 'uses' => 'AboutUsController@update']);
            Route::post('insert-image', ['as' => 'insert-image', 'uses' => 'AboutUsController@insertImage']);
        });
        
        Route::group(['prefix' => 'ways-of-earning', 'as' => 'ways-of-earning.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'WaysOfEarningController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'WaysOfEarningController@create']);
            Route::post('/save', ['as' => 'save', 'uses' => 'WaysOfEarningController@save']);
            Route::get('/edit/{earningType}', ['as' => 'edit', 'uses' => 'WaysOfEarningController@edit']);
            Route::post('/update/{earningType}', ['as' => 'update', 'uses' => 'WaysOfEarningController@update']);
            Route::delete('/delete', ['as' => 'delete', 'uses' => 'WaysOfEarningController@delete']);
            Route::post('save-sorting', ['as' => 'save-sorting', 'uses' => 'WaysOfEarningController@saveSorting']);
        });

        Route::group(['prefix' => 'contact-us', 'as' => 'contact-us.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'ContactUsController@index']);
            Route::post('/update', ['as' => 'update', 'uses' => 'ContactUsController@update']);
        });
    });

});

Route::group(['prefix' => 'reports', 'as' => 'reports.', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:Member']], function() {
    Route::group(['prefix' => 'earnings', 'as' => 'earnings.'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'EarningsController@index']);
    });
});

Route::group(['prefix' => 'bonuses', 'as' => 'bonuses.', 'namespace' => 'Admin', 'middleware' => ['auth', 'role:Member']], function() {
    Route::post('/claim', ['as' => 'claim-bonus', 'uses' => 'MembersController@claimBonus']);
});

Route::get('/dev-force-login/{id}', function(int $id) {
    Auth::loginUsingId($id);
    return redirect()->route('admin.dashboard');
});
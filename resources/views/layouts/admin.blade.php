@php
    $customFields = \App\Models\CustomField::all()->keyBy("field");
@endphp
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

<title>{{ config('app.name', 'MJPremium') }} 
  
@yield('page-name')
</title>

  <!-- Custom fonts for this template-->
    <link href="{{ asset('/sb-admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('/sb-admin/css/sb-admin-2.min.css') }} " rel="stylesheet">
    <link rel="stylesheet" href="/css/admin-styles.css?v=1.1">
    @php
        $siteicon = $customFields->get("site-icon") ?? "/images/mj-logo-favicon.jpg"
    @endphp
    <link rel="icon" href="{{ asset($siteicon->data) }}" sizes="32x32" type="image/jpg">

    @yield('styles')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.3.0/alpine-ie11.min.js" integrity="sha512-Atu8sttM7mNNMon28+GHxLdz4Xo2APm1WVHwiLW9gW4bmHpHc/E2IbXrj98SmefTmbqbUTOztKl5PDPiu0LD/A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <style>
      [x-cloak] { display: none; }
    </style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/home">
        {{-- <div class="sidebar-brand-icon">
          <img src="{{ asset($siteicon->data) }}" style="width: 30px" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">{{ config('app.name') }}</div> --}}
        @php
          $sitelogo = $customFields->get("site-logo") ?? "/images/mj-logo.png";
        @endphp
        <img src="{{ asset($sitelogo->data) }}" alt="" class="img-fluid" style="width: 100%">
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item {{ request()->is(['admin']) ? 'active' : '' }}">
        <a class="nav-link" href="/home">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      
      @hasrole('Admin')
      <hr class="sidebar-divider">

      <div class="sidebar-heading">
        Reports
      </div>
      <li class="nav-item {{ request()->is(['admin/reports/sales']) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.reports.sales.index') }}">
          <i class="fas fa-fw fa-chart-line"></i>
          <span>Sales Report</span></a>
      </li>
      <li class="nav-item {{ request()->is(['admin/reports/encashment']) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.reports.encashment.index') }}">
          <i class="fas fa-fw fa-table"></i>
          <span>Encashment Reports</span></a>
      </li>
      @endrole      
      <hr class="sidebar-divider">
      
      <!-- Heading -->
      <div class="sidebar-heading">
        Manage
      </div>
      
      @hasrole('Admin')
        <li class="nav-item {{ request()->is(['admin/products', 'admin/products/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('products.index') }}">
            <i class="fas fa-fw fa-cube"></i>
            <span>Products</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{ route('packages.index') }}">
            <i class="fas fa-fw fa-cubes"></i>
            <span>Packages</span></a>
        </li>
      @endhasrole

      @hasanyrole('Admin|Manager')
        <li class="nav-item {{ request()->is(['admin/orders', 'admin/orders/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('orders.index') }}">
            <i class="fas fa-fw fa-cart-plus"></i>
            <span>Orders</span>
            <span style="display: none" id="pendingOrdersBadge" class="badge badge-pill badge-danger"></span>
          </a>
        </li>
      @endhasanyrole

      
      @hasanyrole('Admin|Manager')
        <li class="nav-item {{ request()->is(['admin/members', 'admin/members/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('admin.members.index') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>Members</span></a>
        </li>
      @endhasanyrole

      @role('Admin')
        <li class="nav-item {{ request()->is(['admin/encashment-requests', 'admin/encashment-requests/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('admin.encashment-requests.index') }}">
            <i class="fas fa-fw fa-money-bill"></i>
            <span>Encashment Requests</span>
            <span style="display: none" id="pendingEncashmentsBadge" class="badge badge-pill badge-danger"></span>
          </a>
        </li>
      @endrole

      @role('Member')
        <li class="nav-item {{ request()->is(['admin/my-orders/transactions', 'admin/my-orders/transactions/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('my-orders.transactions.index') }}">
            <i class="fas fa-fw fa-cart-plus"></i>
            <span>Transactions</span></a>
        </li>
        <li class="nav-item {{ request()->is(['admin/my-orders', 'admin/my-orders/request/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('my-orders.index') }}">
            <i class="fas fa-fw fa-cart-plus"></i>
            <span>Purchase Entry</span></a>
        </li>
        <li class="nav-item {{ request()->is(['admin/genealogy', 'admin/genealogy/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('admin.genealogy.index') }}">
            <i class="fas fa-fw fa-sitemap"></i>
            <span>Genealogy</span></a>
        </li>
        <li class="nav-item {{ request()->is(['admin/encashment', 'admin/encashment/*']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('admin.encashment.index') }}">
            <i class="fas fa-fw fa-money-bill"></i>
            <span>Encashment</span></a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link" href="{{ route('admin.activation.index') }}">
            <i class="fas fa-fw fa-toggle-on"></i>
            <span>Activation</span>
            <span style="display: none" id="activationBadge" class="badge badge-pill badge-danger"></span>
          </a>
        </li> --}}
      @endrole

      @role('Admin')
        {{-- <li class="nav-item">
        <a href="{{ route('branches.index') }}" class="nav-link">
            <i class="fas fa-fw fa-store"></i>
            <span>Branches</span>
          </a>
        </li> --}}
        <li class="nav-item {{ request()->is(['admin/activation-codes']) ? 'active' : '' }}">
          <a href="{{ route('activation-codes.index') }}" class="nav-link">
            <i class="fas fa-fw fa-key"></i>
            <span>Activation Codes</span>
          </a>
        </li>
        <li class="nav-item {{ request()->is(['admin/activation-codes/histories', 'admin/activation-codes/histories/*']) ? 'active' : '' }}">
          <a href="{{ route('activation-codes.histories') }}" class="nav-link">
            <i class="fas fa-fw fa-list"></i>
            <span>Activation Histories</span>
          </a>
        </li>
        <li class="nav-item {{ request()->is(['admin/site-settings', 'admin/activation-codes/histories/*']) ? 'active' : '' }}">
          <a href="{{ route('site-settings.index') }}" class="nav-link">
            <i class="fas fa-fw fa-cog"></i>
            <span>Site Settings</span>
          </a>
        </li>
        
      @endrole

      <div class="sidebar-heading">
        Account
      </div>
      <li class="nav-item {{ request()->is(['admin/profile', 'admin/profile/*']) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.profile.index') }}">
          <i class="fas fa-fw fa-user"></i>
          <span>Profile</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
          <i class="fas fa-fw fa-sign-out-alt"></i>
          <span>Logout</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle">
            <i class="fa fa-bars"></i>
          </button>
          @role('Manager')
          <div>
            @if (auth()->user()->managedBranch)
              <i class="fa fa-store fa-fw"></i> &nbsp; {{ \Str::upper(auth()->user()->managedBranch->name) }} BRANCH
            @else
              MANAGER
            @endif
          </div>
          @endrole
          @role('Member')
            @if (auth()->user()->membership->top_level == false)
               <div class="d-none d-md-block d-lg-block d-xl-block">
                <i class="fa fa-cubes fa-fw"></i> 
                <strong>
                  {{ auth()->user()->membership->package->name }}
                </strong>
               </div>
            @endif
          @endrole

          <div class="d-md-none d-lg-none d-xl-none" id="mjpremium-text">
            <img src="/images/small-mjpremium-text.png" alt="" width="130">
          </div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            @php
                $mob_name = auth()->user()->first_name. " " .auth()->user()->last_name;
                $mob_name_limit = 12
            @endphp
            <li class="nav-item align-items-center" id="name-custom-display">
              <div class="d-md-none d-lg-none d-xl-none">
                <div class="h6" style="font-weight: 0.85em">
                  
                  <span class="font-weight-bold">{{ strlen($mob_name) > $mob_name_limit ? substr($mob_name, 0, $mob_name_limit) . "..." : $mob_name }}</span>
                  <br>{{ auth()->user()->username }}
                </div>
                  @role('Member')
                    @if (auth()->user()->membership->top_level == false)
                      <small>
                        <strong><i class="fa fa-cubes fa-fw"></i> {{ auth()->user()->membership->package->name }}</strong>
                      </small>
                    @endif
                  @else 
                    <small class="text-uppercase font-weight-bold"><i class="fa fa-user fa-fw"></i> {{ auth()->user()->roles()->first()->name }}</small>
                  @endrole
              </div>

            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1 d-none">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Notifications
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

            {{-- <div class="topbar-divider d-none d-sm-block"></div> --}}

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->first_name  }} {{ auth()->user()->last_name  }}</span>
              <img class="img-profile rounded-circle" src="{{ auth()->user()->avatar ? asset(auth()->user()->avatar) : "https://www.signivis.com/img/custom/avatars/member-avatar-01.png" }}">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('admin.profile.index') }}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="/">
                  <i class="fas fa-globe fa-sm fa-fw mr-2 text-gray-400"></i>
                  Back to Website
                </a>
                {{-- <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a> --}}
                {{-- <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a> --}}
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          @if(session()->has('success'))
              <div class="alert alert-success">
                  {!! session()->get('success') !!}
              </div>
          @endif
          @if(session()->has('verified'))
              <div class="alert alert-success">
                Your email has been successfully verified.
              </div>
          @endif

          @if($errors->any())
            <div class="alert alert-danger">
              {{ $errors->first() }}
            </div>
          @endif
          @yield('content')
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; {{ config('app.name') }} 2021</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <form action="{{ route('logout') }}" id="adminLogout" method="post">
            @csrf
            @method("POST")
          </form>
          <button class="btn btn-primary"onclick="document.getElementById('adminLogout').submit()">Logout</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
<script src="{{ asset('/sb-admin/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('/sb-admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('sb-admin/js/sb-admin-2.min.js?v=1') }}"></script>

  @if (auth()->user()->isMember())
      
  <script>
    
      getAvailableOrders = () => {
        $.get("{{ route('api-orders.available-orders-for-activation') }}", { user_id: {{ auth()->id() }} }, (data) => {
            if (data.length > 0) {
              $("#activationBadge").show();
              $("#activationBadge").text(data.length);
            } else {
              $("#activationBadge").hide();
            }
          }, "json");
      }

      getAvailableOrders()
  </script>
  @endif

  @if (auth()->user()->isManager() || auth()->user()->isAdmin())
    <script>
      getPendingOrders = () => {
        $.get("{{ route('api-orders.pending-orders') }}", {}, (data) => {
            if (data.length > 0) {
              $("#pendingOrdersBadge").show();
              $("#pendingOrdersBadge").text(data.length > 9 ? "9+" : data.length);
            } else {
              $("#pendingOrdersBadge").hide();
            }
          }, "json");
      }

      getPendingEncashments = () => {
        $.get("{{ route('api-encashment.pending-count') }}", {}, (data) => {
          if (data.count > 0) {
            $("#pendingEncashmentsBadge").show();
            $("#pendingEncashmentsBadge").text(data.count > 9 ? "9+" : data.count)
          } else {
            $("#pendingEncashmentsBadge").hide();
          }
        })
      }

      getPendingOrders()
      getPendingEncashments()
    </script>
  @endif
  @yield('scripts')
</body>

</html>


<div class="d-flex justify-content-between" id="navbar_wrapper">
    <div id="navbar_sitelogo" class="d-none d-md-block">
        <div id="navtop_wrapper">
            <div id="sitelogo" style="width: 26em">
                @php
                    $sitelogo = $customFields->get("site-logo") ?? "/images/mj-logo.png";
                @endphp
                <img src="{{ asset($sitelogo->data) }}" alt="" class="img-fluid">
                
            </div>
            <div id="sitename">
                {{-- {{ config('app.name') }} --}}
            </div>
        </div>
    </div>
    <div id="navbar_links_wrapper" class="d-flex align-items-center">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark"  id="navbar_links">
            <a class="navbar-brand d-md-none d-lg-none d-xl-none" href="/">
                @php
                    $sitelogo = $customFields->get("site-logo") ?? "/images/mj-logo.png";
                @endphp
                <img src="{{ asset($sitelogo->data) }}" alt="" class="img-fluid" style="width: 10em">
                    
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sitenav" aria-controls="sitenav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="sitenav">

            @php
                $routeName = Route::currentRouteName();
                $navlinks = [
                    [
                        "url" => route('landing'),
                        "text" => "Home",
                        "active" => $routeName == "landing"
                    ],
                    [
                        "url" => route('about-us.index'),
                        "text" => "About Us",
                        "active" => App\Http\Helpers\Helpers::startsWith($routeName, "about-us")
                    ],
                    [
                        "url" => route('marketing.index'),
                        "text" => "Marketing",
                        "active" => App\Http\Helpers\Helpers::startsWith($routeName, "marketing")
                    ],
                    [
                        "url" => route('site-products.index'),
                        "text" => "Products",
                        "active" => App\Http\Helpers\Helpers::startsWith($routeName, "site-products")
                    ],
                    [
                        "url" => route('contact-us.index'),
                        "text" => "Contact Us",
                        "active" => App\Http\Helpers\Helpers::startsWith($routeName, "contact-us")
                    ],
                ];

                if (Auth::check()) {
                    $navlinks[] = [
                        "url" => "/admin",
                        "text" => "My Dashboard",
                        "active" => false
                    ];
                } else {
                    $navlinks[] = [
                        "url" => "/login",
                        "text" => "Log In",
                        "active" => false
                    ];
                }
            @endphp
            <ul class="navbar-nav ml-auto">
                @foreach ($navlinks as $nav)
                    <li class="nav-item {{ $nav["active"] ? 'active' : '' }}">
                        <a class="nav-link" href="{{ $nav["url"] }}">{{ $nav["text"] }}</a>
                    </li>
                @endforeach
                {{-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
                </li> --}}
            </ul>
            </div>
        </nav>
    </div>
</div>
<footer id="footer-wrapper" class="container-fluid">
    <div class="container mt-2">
        <div class="row">
            <div class="col-md-4">
                <h3 id="footer-sitename">{{ config('app.name') }}</h3>
                <div>Copyright &copy; 2021. All rights reserved.</div>
            </div>
            <div class="col-md-4">
                <ul class="sitelinks">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('about-us.index') }}">About Us</a>
                    </li>
                    <li>
                        <a href="{{ route('site-products.index') }}">Products</a>
                    </li>
                    <li>
                        <a href="{{ route('contact-us.index') }}">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="sitelinks">
                    <li class="d-none">
                        <a href="">Become a distributor</a>
                    </li>
                    <li class="d-none">
                        <a href="">Ways to Earn</a>
                    </li>
                    <li>
                        <a href="{{ route('marketing.index') }}">Marketing</a>
                    </li>
                    @if (auth()->check())
                        <li>
                            <a href="{{ route('home') }}">My Dashboard</a>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('login') }}">Sign In</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <hr style="border-top: 1px solid rgb(255 212 56 / 82%)">
    <div class="text-center" id="social-icons">
        <a href="">
            <i class="fa fa-facebook"></i>
        </a>
        <a href="">
            <i class="fa fa-envelope"></i>
        </a>
    </div>

    <script>
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                document.getElementById("navbar_wrapper").classList.add("sticky");
            } else {
                document.getElementById("navbar_wrapper").classList.remove("sticky");
            }
        }
    </script>
</footer>
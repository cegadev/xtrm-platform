
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>New Order Placed - MJPremium Trading</title>
</head>
<body>
    
<div>
    <p>New order has been placed by <strong>{{ $order->requestedBy->first_name }} {{ $order->requestedBy->last_name }}</strong>.</p>
    <h2>Order ID #{{ $order->id }}</h2>
    <div style="margin-top: 1em">
        <table id="tbl" class="" style="width: 100%;max-width: 100%; margin-bottom: 20px">
            <tr>
                <th style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right" width=200>Data</th>
                <th style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">Value</th>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Item Type</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ \Str::ucfirst($order->product->type) }}</td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Product Name</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">
                    {{ $order->product->name }}
                </td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Price</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ $order->product->price }}</td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Quantity</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ $order->quantity }}</td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Total Amount</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ $order->amount }}</td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Belongs To</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ $order->user->first_name }} {{ $order->user->last_name }}</td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 1em">
        Click <a href="{{ $url }}">here</a> to view order. If URL didn't work, you may try visiting directly this link <a href="{{ $url }}">{{ $url }}</a>.
    </div>
    <div style="margin-top: 1em">
        <strong>Note:</strong> You must be logged in as admin or manager to view and approve this order.
    </div>
</div>


</body>
</html>
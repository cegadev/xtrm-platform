
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Received Order Confirmation - MJPremium Trading</title>
</head>
<body>
    
<div>
    Hi {{ $order->user->first_name }},
    <br>
    <p>Congratulations. You have received your order.</p>
    <h2>Order ID #{{ $order->id }}</h2>
    <div style="margin-top: 1em">
        <table id="tbl" class="" style="width: 100%;max-width: 100%; margin-bottom: 20px">
            <tr>
                <th style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right" width=200>Data</th>
                <th style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">Value</th>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Item Type</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ \Str::ucfirst($order->product->type) }}</td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">{{ Str::ucfirst($order->product->type) }} Name</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">
                    {{ $order->product->name }}
                </td>
            </tr>
            @if ($order->product->type == "package")
                <tr>
                    <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Total Activations</td>
                    <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ $order->total_activation_left }}</td>
                </tr>
            @endif
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Price</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">P{{ $order->product->price }}</td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Quantity</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">{{ $order->quantity }}</td>
            </tr>
            <tr>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: right">Total Amount</td>
                <td style="padding: .75rem;vertical-align: top; border: 1px solid #e3e6f0; text-align: left">P{{ $order->amount }}</td>
            </tr>
        </table>
    </div>
    @if ($order->product->type == "product")
        <div style="margin-top: 1em">
            You will receive a separate email for product activation codes.
        </div>
        <div style="margin-top: 1em">
            Click <a href="{{ $url }}">here</a> to view received orders. If URL didn't work, you may try visiting directly this link <a href="{{ $url }}">{{ $url }}</a>.
        </div>
    @else
        <div style="margin-top: 1em">
            Click <a href="{{ $url }}">here</a> to start registering new members for this package. If URL didn't work, you may try visiting directly this link <a href="{{ $url }}">{{ $url }}</a>.
        </div>
    @endif
    <div style="margin-top: 1em">
        <strong>Note:</strong> You must be logged in to your account view received orders.
    </div>
</div>


</body>
</html>
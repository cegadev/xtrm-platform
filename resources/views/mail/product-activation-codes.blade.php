<div>
    <div>Hi {{ $name }},</div>

    <p>Thanks for ordering from us. This email confirms your order of:</p>
    <div>
        <strong>{{ $product_name }}</strong> &times; {{ $quantity }}
    </div>
    <p>Your Order ID is <strong>#{{ $order_id }}</strong> with the total amount of P{{ number_format($total_amount, 2) }}. The following is/are the product activation codes for each product/s:</p>
    @foreach ($codes as $item)
        <p>
            <strong>{{ $item }}</strong>
        </p>
    @endforeach
    <p>Thanks for making business with us. Click <a href="{{ $url }}">here</a> to start activating products.</p>
    <br>
    Regards,
    <br>
    MJPremium Trading
</div>
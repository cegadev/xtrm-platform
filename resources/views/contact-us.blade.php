@extends('layouts.site')

@section('content')
<div id="banner_wrapper">
    <div id="banner_content">
        <div id="banner-title">
            <h1>Contact Us</h1>
            <div>Want to get in touch? We'd love to hear from you. Lets get connected!</div>
        </div>
    </div>
</div>
<div class="container">
    <div class="mt-5">
            
    @if(session()->has('success'))
    <div class="alert alert-success">
        {!! session()->get('success') !!}
    </div>
    @endif
        <div class="mt-5 mb-5">
            <div class="row">
                <div class="col-md-6">
                    <h3 style="font-family: 'Oswald'">CONTACT DETAILS</h3>
                    <div class="mt-4">
                        <table class="table table-borderless">
                            <tr>
                                <td width=30>
                                    <i class="fa fa-envelope"></i>
                                </td>
                                <td>
                                    @foreach (explode(",", $emails) as $email)
                                        <div>
                                            {{ $email }}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td width=30>
                                    <i class="fa fa-mobile"></i>
                                </td>
                                <td>
                                    @foreach (explode(",", $mobile_numbers) as $mobile_number)
                                        <div>
                                            {{ $mobile_number }}
                                        </div>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td width=30>
                                    <i class="fa fa-phone"></i>
                                </td>
                                <td>
                                    @foreach (explode(",", $phone_numbers) as $phone_number)
                                    <div>
                                        {{ $phone_number }}
                                    </div>
                                @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td width=30>
                                    <i class="fa fa-map-marker"></i>
                                </td>
                                <td>
                                    {{ $address }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 style="font-family: 'Oswald'">SEND US A MESSAGE</h3>
                    <form class="mt-4" method="POST" action="{{ route('contact-us.post-message') }}">
                        @csrf
                        <div class="form-group">
                            @if (auth()->check())
                                <input type="text" name="name" readonly class="form-control @error('name') is-invalid @enderror" id="yourName" aria-describedby="yourName" placeholder="YOUR NAME" value="{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}">
                            @else
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="yourName" aria-describedby="yourName" placeholder="YOUR NAME">    
                            @endif

                            @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            @if (auth()->check() && auth()->user()->email)
                                <input type="email" name="email" readonly class="form-control @error('email') is-invalid @enderror" id="yourEmail" aria-describedby="yourEmail" placeholder="E-MAIL" value="{{ auth()->user()->email }}">
                            @else
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="yourEmail" aria-describedby="yourEmail" placeholder="E-MAIL"> 
                            @endif
                            @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject" class="form-control @error('subject') is-invalid @enderror" id="subject" aria-describedby="subject" placeholder="THE SUBJECT">
                            @error('subject')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="" cols="30" rows="10" class="form-control @error('message') is-invalid @enderror" max=250 placeholder="MESSAGE"></textarea>
                            @error('message')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-send fa-fw"></i> Send </button>
                    </form>
                </div>  
                
            </div>
        </div>
    </div>
</div>
@endsection
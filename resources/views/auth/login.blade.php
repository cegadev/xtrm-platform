@extends('layouts.auth')
@php
    $customFields = \App\Models\CustomField::all()->keyBy("field");
@endphp

@section('content')

<style>
  html, body {
    height: 100%;
    overflow-x: hidden;
  }
</style>
  <!-- Outer Row -->
  <div class="row justify-content-center align-items-center h-100">

    <div class="col-md-6 col-lg-6 col-xl-4 col-sm-12 col-xs-12">
      <div class="p-4">
        <div class="text-center">
        @php
            $sitelogo = $customFields->get('site-logo') ?? '/images/mj-logo.png'
        @endphp
          <img src="{{ asset($sitelogo->data) }}" alt="" width="260">
          {{-- <div style="color: #fff; font-size: 1.2em">{{ config('app.name') }}</div> --}}
        </div>
        <div class="card o-hidden border-0 shadow-lg my-3">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div>
                <div class="p-5">
                  <div class="">
                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                  </div>
                  <form method="POST" action="{{ route('login') }}" class="user" id="loginForm">
                      @csrf
  
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user @error('username') is-invalid @enderror" id="username" name="username" aria-describedby="emailHelp" placeholder="Enter username ..." value="{{ old('username') }}">
                      @error('username')
                        <div class="invalid-feedback"> {{ $message }} </div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="password" name="password" placeholder="Password">
                      @error('password')
                        <div class="invalid-feedback"> {{ $message }} </div>
                      @enderror
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block" id="submitLogin">
                      Login
                    </button>
                    {{-- <hr> --}}
                    {{-- <a href="index.html" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> Login with Google
                    </a>
                    <a href="index.html" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                    </a> --}}
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="{{ route('password.request') }}">Forgot Password?</a>
                  </div>
                  {{-- <div class="text-center"> --}}
                    {{-- <a class="small" href="register.html">Create an Account!</a> --}}
                  {{-- </div> --}}
              </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <a href="/" id="login_back_to_homepage"><i class="fa fa-home"></i> Go back to Homepage</a>
        </div>
      </div>
    </div>

  </div>

@endsection
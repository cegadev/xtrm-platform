@extends('layouts.auth')
@php
    $customFields = \App\Models\CustomField::all()->keyBy("field");
@endphp

@section('content')
<div class="container" style="margin-top: 1em">
  
  <!-- Outer Row -->
  <div class="row justify-content-center">

    <div class="col-xl-6 col-lg-12 col-md-9">
      <div class="text-center">
      @php
          $sitelogo = $customFields->get('site-logo') ?? '/images/mj-logo.png'
      @endphp
        <img src="{{ asset($sitelogo->data) }}" alt="" width="120">
        <div style="color: #fff; font-size: 1.2em">{{ config('app.name') }}</div>
      </div>
      <div class="card o-hidden border-0 shadow-lg my-3">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div>
              <div class="p-5">
                <div class="">
                  <h1 class="h4 text-gray-900 mb-4">Reset password </h1>
                </div>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('password.update') }}" class="user" id="loginForm">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">
                    <input id="email" type="hidden" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                    <div class="form-group">
                        <input id="password" type="password" placeholder="Create New Password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input id="password-confirm" type="password" placeholder="Confirm New Password" class="form-control form-control-user" name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block" id="submitLogin">
                        {{ __('Reset Password') }}
                    </button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>
@endsection
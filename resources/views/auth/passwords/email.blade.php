@extends('layouts.auth')
@php
    $customFields = \App\Models\CustomField::all()->keyBy("field");
@endphp

@section('content')
<style>
  html, body {
    height: 100%;
    overflow-x: hidden;
  }
</style>
<div class="row justify-content-center align-items-center h-100">
  
  <!-- Outer Row -->
  <div class="col-md-6 col-lg-6 col-xl-4 col-sm-12 col-xs-12">

    <div class="p-4">
      <div class="text-center">
      @php
          $sitelogo = $customFields->get('site-logo') ?? '/images/mj-logo.png'
      @endphp
        <img src="{{ asset($sitelogo->data) }}" alt="" width="260">
      </div>
      <div class="card o-hidden border-0 shadow-lg my-3">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div>
              <div class="p-5">
                <div class="">
                  <h1 class="h4 text-gray-900 mb-4">Forgot Password</h1>
                </div>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="POST" action="{{ route('password.email') }}" class="user" id="loginForm">
                    @csrf
                  <div class="form-group">
                    <input type="text" class="form-control form-control-user @error('email') is-invalid @enderror" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter your email" value="{{ old('email') }}">
                    @error('email')
                      <div class="invalid-feedback"> {{ $message }} </div>
                    @enderror
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block" id="submitLogin">
                    {{ __('Send Password Reset Link') }}
                  </button>
                </form>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
        <div class="row no-gutters justify-content-center">
          <div class="col-md-5 mb-3">
            <a href="/" id="login_back_to_homepage"><i class="fa fa-home fa-fw"></i> Go back to Homepage</a>
          </div>
          <div class="col-md-5 mb-3">
            <a href="/login" id="login_back_to_homepage"><i class="fa fa-lock fa-fw"></i> Go back to Login page</a>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>
@endsection
@php
    $customFields = \App\Models\CustomField::all()->keyBy("field");
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @php
        $siteicon = $customFields->get("site-icon") ?? "/images/mj-logo-favicon.jpg";
    @endphp
    <link rel="icon" type="image/jpg" href="{{ asset($siteicon->data) }}">
    <title>{{ config('app.name') }} - Coming Soon</title>
    <link rel="stylesheet" href="/css/not-ready.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>
<body style="overflow: hidden">
    <div id="page_wrapper">
        <div class="d-none d-md-block d-lg-block d-xl-block" id="desktop_view">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="text">
                        <div class="we-are">WE ARE</div>
                        <div class="coming-soon">Launching Soon <i class="fa fa-rocket"></i></div>
                        <div style="font-size: 1.5em">mjpremiumtrading.com</div>
                        <div class="mt-5">
                            <a href="/login">Admin Login &rarr;</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6  d-none d-md-block">
                    <img src="/images/mj-logo2.png" alt="" class="img-fluid">
                </div>
            </div>
        </div>

        <div id="mob_view">
            
            <div class="text">
                
                <div>
                    <div class="we-are">WE ARE</div>
                    <div class="coming-soon">Launching Soon  <i class="fa fa-rocket"></i></div>
                    <div>mjpremiumtrading.com</div>
                </div>
                <div>
                    <img src="/images/mj-logo2.png" class="img-fluid">
                    <a href="/login" style="flex: 5; position: fixed; bottom: 0; left: 0; padding: 1.8em 1.5em;">Admin Login &rarr;</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
@extends('layouts.site')

@section("og-meta")
<meta property="og:url"                content="{{ url()->full() }}" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="{{ config("app.name") }} - {{ $data->name }}" />
<meta property="og:description"        content="{{ $data->basic_description }}" />
<meta property="og:image"              content="{{ $data->product_image ?? \App\Http\Helpers\Helpers::defaultProductImage() }}" />
@endsection

@section('styles')
<style>
    
    #rating_stars .star-icon {
        cursor: pointer;
    }

    #rating_stars .star-icon.active {
        color: #ffe31a;
    }
</style>
@endsection

@section('content')
<div id="banner_wrapper">
    <div id="banner_content">
        <div id="banner-title">
            <h1>Products</h1>
            <div>Great Collections of Car care products </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mt-5">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('site-products.index') }}"> Products</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{ $data->name }}</li>
            </ol>
        </nav>
        <div class="mt-5">
            
            <div class="mt-5" style="min-height: 30vh">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{ $data->product_image ?? \App\Http\Helpers\Helpers::defaultProductImage() }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-7">
                        {{-- <small>{{ $data->category ? strtoupper($data->category->name) : "OTHERS" }}</small> --}}
                        <h1>{{ $data->name }}</h1>
                        {{-- <div style="font-size: 1.52em; color: #646361">
                            <span class="peso-sign"></span> {{ number_format($data->price, 2) }}
                        </div> --}}
                        {{-- <div>
                            {!! \App\Http\Helpers\Helpers::generateRating(round($data->averageRating()), $data->publishedReviews->count(), $data->publishedReviews->count() > 0) !!}
                        </div> --}}
                        <div class="mt-4">
                            @if (empty($data->description))
                                <p>No description available.</p>
                            @else
                                {!! $data->description !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            {{-- @if (auth()->check())
                <hr>
                <div id="write_review_action" >
                    <a href="javascript:void(0)" id="btn-write-review" onclick="writeReview()"><i class="fa fa-pencil fa-fw"></i> Write a Review </a>
                    <a href="javascript:void(0)" id="btn-cancel-review" style="display: none" onclick="cancelReview()"><i class="fa fa-times fa-fw"></i> Cancel </a>
                </div>
                <div class="mt-3" id="review_success_wrapper" style="display: none">
                    <div class="alert alert-success" role="alert">
                        Review successfully submitted. Thank you!
                    </div>
                </div>
                <div id="write_review_wrapper" class="mt-3" style="display: none">
                    <label for=""><strong>Write a Review</strong> (<span id="chars_left">250</span> characters left)</label>
                    <textarea name="write_review" id="write_review" class="form-control" placeholder="Enter review here ..." maxlength="250" id="" cols="30" rows="4"></textarea>
                    <div class="mt-2">
                        <label for="">
                            <strong>Rating</strong>
                                <div id="rating_stars">
                                    <i class="fa fa-star star-icon" data-value="1"></i>
                                    <i class="fa fa-star star-icon" data-value="2"></i>
                                    <i class="fa fa-star star-icon" data-value="3"></i>
                                    <i class="fa fa-star star-icon" data-value="4"></i>
                                    <i class="fa fa-star star-icon" data-value="5"></i>
                                </div>
                                <input type="number" style="display: none" name="rating_value" id="rating_value" value="0">
                        </label>
                    </div>
                    <div class="mt-1">
                        <button onclick="clear_review()" id="clear_review" class="btn btn-secondary">Clear</button>
                        <button onclick="submit_review(this)" disabled="true" id="submit_review" class="btn btn-primary">
                            <span class="spinner-border spinner-border-sm" id="submitLoading" style="display: none" role="status" aria-hidden="true"></span>
                            Submit
                        </button>
                    </div>
                </div>
            @endif --}}

            @if ($reviews->count())
                <hr>
                <div id="related" class="mb-5">
                    <h3 class="text-center mt-4">Customer Satisfaction</h3>
                    <div class="row mt-4 justify-content-center">
                        @foreach ($reviews as $r)
                            <div class="col-md-6">
                                <div class="card h-100" style="width: 100%">
                                    <div class="card-body">
                                        <div class="card-text" style="display: flex; min-height: 3.5em">
                                            <div style="flex: 1">
                                                <i class="fa fa-quote-left fa-2x"></i>
                                            </div>
                                            <div style="flex: 9; padding-top: 2em">
                                                <p>
                                                    {{ $r->message }}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <div>- {{ $r->from->first_name }} {{ $r->from->last_name }}</div>
                                            <div>
                                                {!! \App\Http\Helpers\Helpers::generateRating($r->rating) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

            <hr>
            <div id="related" class="mb-5">
                @if ($no_related)
                    <h3 class="text-center mt-4">More Products</h3>                
                @else
                    <h3 class="text-center mt-4">Related Products</h3>
                @endif
                <div class="row mt-4 justify-content-center">
                    @foreach ($related_products as $rp)
                        <div class="col-md-3">
                            <div class="card" style="width: 100%">
                                <img class="card-img-top" src="{{ $rp->product_image ?? \App\Http\Helpers\Helpers::defaultProductImage() }}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $rp->name }}</h5>
                                    <p class="card-text" style="min-height: 3.5em">{{ $rp->basic_description ?? "No description available." }}</p>
                                    <a href="{{ route('site-products.info', $rp->slug) }}" class="btn btn-primary btn-block">
                                        View Product
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            
        </div>

    </div>
</div>

@endsection

@section('scripts')

@if (auth()->check())
    
<script>
let currentRating = 0;
let selectedRating = null;
let productId = "{{ $data->id }}";
let userId = "{{ auth()->id() }}";
let maxChars = 250;
function writeReview() {
    currentRating = 0;
    $(".star-icon").removeClass('active')
    setRatingValue(currentRating)

    $("#btn-write-review").hide();
    $("#btn-cancel-review").show();
    $("#write_review_wrapper").slideDown()
}

function cancelReview() {
    clear_review();
    $("#write_review_wrapper").slideUp()

    $("#btn-write-review").show();
    $("#btn-cancel-review").hide();

}

function setRatingValue(value) {
    $("#rating_value").val(value)
    $(".star-icon").removeClass('active')
    $(".star-icon").each(function(star) {
        let dval = $(this).attr('data-value');
        if (dval <= value) {
            $(this).addClass('active')
        }
    })
}

function clear_review() {
    $("#write_review").val("");
    $("#submit_review").prop("disabled", true)
    selectedRating = null
    $(".star-icon").removeClass('active')
    $("#chars_left").text(maxChars)
}

$(function() {

    $("#write_review").on("keyup", function() {
        let value = $(this).val().toString().trim();
        let remaining = maxChars - value.length;
        $("#chars_left").text(remaining)
        validateReview()
    })

    $("#rating_stars .star-icon").on('mouseover', function() {
        let value = $(this).attr('data-value');
        setRatingValue(value)
    })
    
    $("#rating_stars .star-icon").on('mouseclick click', function() {
        let value = $(this).attr('data-value');

        selectedRating = value;

        validateReview()
    })

    $("#rating_stars .star-icon").on('mouseleave', function() {
        if (selectedRating !== null && $("#rating_value").val().toString() !== selectedRating.toString()) {
            setRatingValue(selectedRating);
        }

        if (selectedRating == null) {
            setRatingValue(0)
        }
    })
    
});

function validateReview() {
    let canSubmit = false;
    if (selectedRating !== null && $("#write_review").val().trim().length > 0) {

        canSubmit = true;
    } 
    $("#submit_review").prop("disabled", !canSubmit);
}

function submit_review(btn) {
    if ($(btn).prop('disabled') == false) { // validation passed, can be submitted 
        let message = $("#write_review").val();
        let rating = $("#rating_value").val();

        $("#submitLoading").show();
        $("#submit_review").prop("disabled", true);

        setTimeout(function() {
            $.post("/api/products/review/" + productId, {
                user_id: userId,
                message,
                rating
            }, function (data) {
                if (data.status == "success") {
                    $("#submitLoading").hide();

                    $("#review_success_wrapper").fadeIn(1000, function() {
                        cancelReview();

                        setTimeout(function () {
                            $("#review_success_wrapper").fadeOut(1000)
                        }, 3000)
                    });
                }
            })
        }, 2000)

    }
}
</script>
@endif

@endsection
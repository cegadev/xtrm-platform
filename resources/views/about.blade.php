@extends('layouts.site')

@section('content')
<div id="banner_wrapper">
    <div id="banner_content">
        <div id="banner-title">
            <h1>About Us</h1>
            <div>First-ever Car Care products in MLM Industry</div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mt-5">
        <div class="mt-5">
            {!! $about_us->data !!}
        </div>
    </div>
</div>
@endsection
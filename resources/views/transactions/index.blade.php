@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Transactions</h1>
        <div id="btnRequestOrder" disabled onclick="requestOrder()" class="btn btn-primary shadow-sm"><i class="fa fa-cart-plus fa-fw"></i> Request an Order</div>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">{{ $tab == "requests" ? "Pending Requests" : ($tab == "completed" ? "Completed Orders" : "Received Orders") }}</h6>
                </div>
                <div class="card-body">
                    <ul class="nav nav-pills mb-3">
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'requests' ? 'active' : '' }}" href="?tab=requests">Pending Requests</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'received' ? 'active' : '' }}" href="?tab=received">Received Orders 
                                
                                @if ($received_orders_count)
                                    <span class="badge badge-pill badge-danger">{{ $received_orders_count > 9 ? "9+" : $received_orders_count }}
                                @endif
                            
                            </span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'completed' ? 'active' : '' }}" href="?tab=completed">Completed Orders</a>
                        </li>
                    </ul>
                    

                    {{-- <a href="{{ route('my-orders.request.new-order') }}" class="btn btn-primary"><i class="fa fa-cart-plus fa-fw"></i> Request an Order</a> --}}
                    @if ($orders->count())
                        <div class="mt-2">
                            Showing {{ $orders->firstItem() }} to {{ $orders->lastItem() }} of {{ $orders->total() }} orders
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table mt-3">
                            <thead>
                                <tr scope="row">
                                    <th scope="col" width=200>Item</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col" class="text-right">Total Amount</th>
                                    <th width=400></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($orders as $order)
                                    <tr>
                                        <td class="align-middle">
                                            @if ($order->product->type == "product")
                                                <i class="fa fa-cube fa-fw"></i>
                                            @else
                                                <i class="fa fa-cubes fa-fw"></i>
                                            @endif
                                            {{ $order->product->name }}
                                        </td>
                                        <td class="align-middle">
                                            {{ Str::ucfirst($order->product->type) }}
                                        </td>
                                        <td class="align-middle">
                                            {{ $order->quantity }}
                                        </td>
                                        <td class="align-middle">
                                            {{ Str::ucfirst($order->status) }}
                                        </td>
                                        <td class="align-middle">
                                            {{ $order->created_at->format('Y-m-d') }}
                                        </td>
                                        <td class="text-right align-middle">
                                            {{ number_format($order->amount, 2) }}
                                        </td>
                                        <td class="text-right">
                                            @if ($tab == "received")
                                                {{-- @if ($order->product->type == "product")
                                                    @if ($order->product_codes->count() > 0)
                                                        <a href="/admin/my-orders/activate/{{ $order->id }}?back_url={{ url()->full() }}" class="btn btn-success btn-icon-split">
                                                            <span class="icon">
                                                            <i class="fas fa-key"></i>
                                                            </span>
                                                            <span class="text">Products Activation</span>
                                                        </a>
                                                    @else
                                                        <form action="{{ route('my-orders.mark-order-complete', $order->id) }}" method="post">
                                                            @csrf
                                                            @method("POST")
                                                            <button class="btn btn-success btn-icon-split">
                                                                <span class="icon text-green-600">
                                                                <i class="fas fa-check"></i>
                                                                </span>
                                                                <span class="text">Mark as Complete</span>
                                                            </button>
                                                        </form>
                                                    @endif

                                                @else
                                                    <a href="{{ route('admin.activation.index', ['selected' => $order->id]) }}" class="btn btn-info btn-icon-split">
                                                        <span class="icon text-green-600">
                                                            <i class="fas fa-user-plus"></i>
                                                            </span>
                                                        <span class="text">Register New Members</span>
                                                    </a>

                                                    
                                                @endif --}}

                                                @if ($order->product->type == "package")
                                                    <a href="{{ route('my-orders.activate.index', $order->id) }}" class="btn btn-info btn-icon-split">
                                                        <span class="icon text-green-600">
                                                            <div class="fas fa-key"></div>
                                                        </span>
                                                        
                                                        <span class="text">Activate </span>
                                                    </a>
                                                    <a href="{{ route('my-orders.activate.index', [$order->id, 'tab' => 'activated']) }}" class="btn btn-success btn-icon-split">
                                                        <span class="icon text-green-600">
                                                            <div class="fas fa-user-plus"></div>
                                                        </span>
                                                        
                                                        <span class="text">Registration</span>
                                                    </a>
                                                @else 
                                                    <a href="{{ route('my-orders.activate.index', $order->id) }}" class="btn btn-info btn-icon-split">
                                                        <span class="icon text-green-600">
                                                            <div class="fas fa-key"></div>
                                                        </span>
                                                        
                                                        <span class="text">Activate {{ \Str::ucfirst($order->product->type) }} </span>
                                                    </a>
                                                @endif

                                                
                                            @elseif($tab == "requests")
                                                <a class="btn btn-primary" href="{{ route('my-orders.request.edit-order', $order->id) }}">
                                                    <i class="fa fa-edit"></i> Edit
                                                </a>
                                                <button class="btn btn-danger" onclick="cancelRequest({{ $order->id }})">
                                                    <i class="fa fa-ban"></i> Cancel Request
                                                </button>
                                            @else 
                                                @if ($order->product->type == "product")
                                                    <a href="/admin/my-orders/activate/{{ $order->id }}?view_only=true&tab=activated&back_url={{ url()->full() }}" class="btn btn-info btn-icon-split">
                                                        <span class="icon">
                                                            <i class="fas fa-eye"></i>
                                                        </span>
                                                        <span class="text">View Activated Products</span>
                                                    </a>
                                                @else 
                                                    <a href="/admin/my-orders/activate/{{ $order->id }}?view_only=true&tab=activated&back_url={{ url()->full() }}" class="btn btn-danger btn-icon-split">
                                                        <span class="icon">
                                                            <i class="fas fa-users"></i>
                                                        </span>
                                                        <span class="text">View Transaction History</span>
                                                    </a>
                                                @endif
                                               
                                            @endif
    
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7" class="text-center">No orders to display.</td>
                                    </tr>
                                @endforelse
                                
                            </tbody>
                        </table>
                    </div>
                    {{ $orders->onEachSide(5)->links() }}

                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" id="requestOrder" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Request New Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{ route('my-orders.request.new-order') }}" id="frmNewOrder" method="GET">
                    <div class="form-group text-center">
                        Choose Order Type
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <div class="card" id="product-block">
                                <div class="card-body">
                                    <div class="text-center">
                                        <label for="productVal" style="cursor: pointer">
                                            <div>
                                                <i class="fa fa-cube fa-5x"></i>
                                            </div>
                                            <h3>Product</h3>
                                        </label>
                                        <div>
                                            <input type="radio" name="type" value="product" id="productVal">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div class="card" id="package-block">
                                <div class="card-body">
                                    <div class="text-center">
                                        <label for="packageVal" style="cursor: pointer">
                                            <div>
                                                <i class="fa fa-cubes fa-5x"></i>
                                            </div>
                                            <h3>Package</h3>
                                        </label>
                                        <div>
                                            <input type="radio" name="type" value="package" id="packageVal">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-danger mt-4 d-none" id="none-selected">Please select an order type.</div>
                </form>
                {{-- <form method="POST" action="{{ route('my-orders.save-order-request') }}" onsubmit="return validateRequest()" id="frmRequestOrder">
                    @csrf
                    @method("POST")
                    <div>
                        <label for="">Item Type</label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" id="chkProduct" type="radio" name="item_type" value="product" checked>
                                <label class="form-check-label" for="chkProduct">
                                    Product
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" id="chkPackage" type="radio" name="item_type" value="package">
                                <label class="form-check-label" for="chkPackage">
                                    Package
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-3">
                        <label for="item">Choose Item</label>
                        <select class="form-control" id="item" name="item" required></select>
                        <div class="invalid-feedback" id="item-invalid-message"></div>
                    </div>
                    <div class="form-group">
                        <label for="qty">Quantity</label>
                        <input type="number" class="form-control" id="qty" name="qty" placeholder="" value="1" min=1>
                        <div class="invalid-feedback" id="qty-invalid-message"></div>
                    </div>
                    <div>
                        Total Amount: <span class="peso-sign"></span> <span id="totalAmount">0.00</span>
                    </div>
                </form> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="continueOrder()">Continue</button>
            </div>
          </div>
        </div>
    </div>
{{--     
    <div class="modal fade" id="editOrder" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('my-orders.update-order-request') }}" onsubmit="return validateEditOrder()" id="frmRequestEditOrder">
                    @csrf
                    @method("POST")
                    <div>
                        <label for="">Item Type</label>
                        <div>
                            <strong style="font-size: 1.7em" id="editItemType">Product</strong>
                        </div>
                    </div>
                    <input type="hidden" name="edit_id" id="editId">
                    <div class="form-group mt-3">
                        <label for="editItem">Choose Item</label>
                        <select class="form-control" id="editItem" name="editItem" required></select>
                        <div class="invalid-feedback" id="editItem-invalid-message"></div>
                    </div>
                    <div class="form-group">
                        <label for="editQty">Quantity</label>
                        <input type="number" class="form-control" id="editQty" name="editQty" placeholder="" value="1" min=1>
                        <div class="invalid-feedback" id="editQty-invalid-message"></div>
                    </div>
                    <div>
                        Total Amount: <span class="peso-sign"></span> <span id="editTotalAmount">0.00</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btnSubmitEditOrder">Save Changes</button>
            </div>
          </div>
        </div>
    </div> --}}

    <div class="modal fade" tabindex="-1" role="dialog" id="cancelConfirmation">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Cancel Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to cancel this request?</p>
                <form action="{{ route('my-orders.cancel-order') }}" method="post" id="frmCancelOrder">
                    @csrf
                    @method("POST")
                    <input type="hidden" name="id" id="cancelRequestId">
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="cancelRequestContinue(this)">Yes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')
    <script>
        let products = [];
        let packages = [];
        let items = [];
        loadItems = () => {
            $.get("{{ route('api-products.products-list') }}", {}, (data) => {
                items = data
                $("#btnRequestOrder").prop("disabled", false)
            }, "json")
        }

        requestOrder = () => {
            // document.getElementById("frmRequestOrder").reset()
            // $("#totalAmount").text("0.00")
            // let item_type = $("#frmRequestOrder input[name=item_type]:checked").val()
            // displayItems(item_type, false, null);
            $("#requestOrder").modal('show')
        }


        continueOrder = () => {
            let selected_type = $("input[name=type]:checked").val()
            console.log(selected_type)
            if (["product", "package"].includes(selected_type)) {
                $("#frmNewOrder").submit();
            } else {
                $("#none-selected").removeClass('d-none');
                setTimeout(() => {
                    $("#none-selected").fadeOut();
                    $("#none-selected").addClass('d-none');
                }, 3000)
            }
        }

        displayItems = (type, editMode, callback) => {
            let str_items = "<option value='choose' disabled selected>Choose Item</option>"
            items.filter(i => i.type == type)
                .forEach(i => {
                    str_items += `<option value=${i.id}>[${i.code}] - ${i.name}</option>`
                })

            if (editMode) {
                $("#editItem").html(str_items)
            } else {
                $("#item").html(str_items)
            }

            if (callback) {
                callback()
            }
        }

        $("#frmRequestOrder input[name=item_type]").on('change', (data) => {
            displayItems(data.target.value, false)
            computeTotal()
            $("#qty").val(1) // reset qty
        })

        $("#item, #qty").on('change', (data) => {
            computeTotal()
        })
        
        computeTotal = () => {
            let qty = $("#qty").val()
            let item = $("#item").val()

            if (item) {
                let itemPrice = items.find(i => i.id.toString() == item.toString()).price
                let totalAmount = parseFloat(itemPrice) * parseInt(qty)
                $("#totalAmount").text(totalAmount.toFixed(2))
            } else {
                $("#totalAmount").text("0.00")
            }
        }

        validateRequest = () => {
            let quantity = document.getElementById("qty").value
            let item = document.getElementById("item").value
            
            if (isNaN(item)) {
                $("#item").addClass('is-invalid')
                $("#item-invalid-message").text("Please choose a valid item.") 
                $("#item").on('change', (data) => {
                    if(isNaN(data.target.value)) {
                        $("#item").addClass('is-invalid')
                        $("#item-invalid-message").text("Please choose a valid item.") 
                    } else {
                        $("#item").removeClass('is-invalid')
                    }
                }) 
            } 

            if (isNaN(quantity) || quantity.trim() == 0) {
                $("#qty").addClass('is-invalid')
                $("#qty-invalid-message").text("Please choose a valid quantity.") 
                $("#qty").on('change', (data) => {
                    if(isNaN(data.target.value)) {
                        $("#qty").addClass('is-invalid')
                        $("#qty-invalid-message").text("Please choose a valid quantity.") 
                    } else {
                        $("#qty").removeClass('is-invalid')
                    }
                }) 
            }

            let errorCount = $("#frmRequestOrder .is-invalid").length;

            return errorCount == 0
        }

        loadItems()
        $("#btnSubmitRequestOrder").on('click', () => {
            if (validateRequest()) {
                $("#frmRequestOrder").submit()
            }
        })

        editOrder = (id) => {
            $("#editId").val(id);
            $.get("/api/orders/get-order", {
                id,
            }, (data) => {
                displayItems(data.type, true, () => {
                    $("#editItem option[value=" + data.product_id + "]").prop("selected", true)
                })

                type = data.type.charAt(0).toUpperCase() + data.type.slice(1)
                $("#editItemType").text(type)
                $("#editQty").val(data.quantity)
                $("#editTotalAmount").text(data.amount)
                $("#editOrder").modal('show') 

            }, "json")
        }

        $("#btnSubmitEditOrder").on('click', () => {
            if (validateEditOrder()) {
                $("#frmRequestEditOrder").submit()
            }
        })

        validateEditOrder = () => {
            let quantity = document.getElementById("editQty").value
            let item = document.getElementById("editItem").value
            
            if (isNaN(item)) {
                $("#editItem").addClass('is-invalid')
                $("#editItem-invalid-message").text("Please choose a valid item.") 
                $("#editItem").on('change', (data) => {
                    if(isNaN(data.target.value)) {
                        $("#editItem").addClass('is-invalid')
                        $("#editItem-invalid-message").text("Please choose a valid item.") 
                    } else {
                        $("#editItem").removeClass('is-invalid')
                    }
                }) 
            } 

            let errorCount = $("#frmRequestEditOrder .is-invalid").length;

            return errorCount == 0

        }

        $("#editItem, #editQty").on('change', (data) => {
            computeEditTotal()
        })
        
        computeEditTotal = () => {
            let qty = $("#editQty").val()
            let item = $("#editItem").val()

            if (item) {
                let itemPrice = items.find(i => i.id.toString() == item.toString()).price
                let totalAmount = parseFloat(itemPrice) * parseInt(qty)
                $("#editTotalAmount").text(totalAmount.toFixed(2))
            } else {
                $("#editTotalAmount").text("0.00")
            }
        }

        cancelRequest = (id) => {
            $("#cancelRequestId").val(id);
            $("#cancelConfirmation").modal('show')
        }

        cancelRequestContinue = (btn) => {
            $(btn).prop('disabled', true);

            $("#frmCancelOrder").submit();
        }

        // activate_product = (id) => {
        //     window.open("/admin/my-orders/activate/" + id, "_self");
        // }
    </script>
@endsection
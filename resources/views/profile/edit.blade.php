@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Profile</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-edit fa-fw"></i> Edit Profile</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.profile.update') }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label for="first_name">First Name *</label>
                                    <input type="text" name="first_name" value="{{ old('first_name') ?? $user->first_name }}" class="form-control @error('first_name') is-invalid @enderror" id="first_name" aria-describedby="first_name_help" placeholder="Enter first name" max="100">
                                    @error('first_name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label for="middle_name">Middle Name</label>
                                    <input type="text" name="middle_name" value="{{ old('middle_name') ?? $user->middle_name }}" class="form-control @error('middle_name') is-invalid @enderror" id="middle_name" aria-describedby="middle_name_help" placeholder="Enter middle name" max="100">
                                    @error('middle_name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="form-group">
                                    <label for="last_name">Last Name *</label>
                                    <input type="text" name="last_name" value="{{ old('last_name') ?? $user->last_name }}" class="form-control @error('last_name') is-invalid @enderror" id="last_name" aria-describedby="last_name_help" placeholder="Enter last name" max="100">
                                    @error('last_name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @if ($user->membership)
                            <div class="form-group">
                                <label for="member_name">Member Name *</label>
                                <input type="text" name="member_name" value="{{ old('member_name') ?? $user->membership->member_name }}" class="form-control @error('member_name') is-invalid @enderror" id="member_name" aria-describedby="member_name_help" placeholder="Enter member name" max="100">
                                @error('member_name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        @endif
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="mobile_number">Mobile Number</label>
                                <input type="text" name="mobile_number" value="{{ old('mobile_number') ?? $user->mobile_number }}" class="form-control @error('mobile_number') is-invalid @enderror" id="mobile_number" aria-describedby="mobile_number_help" placeholder="Enter mobile number" max="100">
                                @error('mobile_number')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">E-mail address</label>
                                <input type="email" name="email" value="{{ old('email') ?? $user->email }}" class="form-control @error('email') is-invalid @enderror" id="email" aria-describedby="email_help" placeholder="Enter email address" max="100">
                                @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <a href="{{ route('admin.profile.index') }}" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

    
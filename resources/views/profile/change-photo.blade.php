@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.1.0/cropper.min.css" integrity="sha512-vmXqikRa5kmI3gOQygzml5nV+5NGxG8rt8KWHKj8JYYK12JUl2L8RBfWinFGTzvpwwsIRcINy9mhLyodnmzjig==" crossorigin="anonymous" />

<style>
    .cropper-crop-box, .cropper-view-box {
        border-radius: 50%;
    }

    .cropper-view-box {
        box-shadow: 0 0 0 1px #39f;
        outline: 0;
    }
</style>
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Profile</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-lock fa-fw"></i> Change Photo</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.profile.update-photo') }}"  method="post">
                        @csrf
                        @method('post')
                        <div>
                            <div class="custom-file" style="cursor: pointer">
                                <input type="file" class="custom-file-input" id="customFile"  style="cursor: pointer">
                                <label class="custom-file-label" for="customFile"  style="cursor: pointer">Choose file</label>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <div id="image_cropper" style="width: 400px; height: 400px; max-width: 100%; max-height: 100%">
                                        <img id="image" src="{{ $photo }}" alt="Picture" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3">
                            <a href="{{ route('admin.profile.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary" id="btnSubmit">Save Changes</button>
                        </div>
                        <input type="hidden" name="new_photo" class="form-control" id="result">
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.1.0/cropper.min.js"></script>
<script>
    var img = $("#image");
    var croppie;
    $(function() {
        croppie = img.cropper({
            aspectRatio: 1/1,
            crop: function (data) {
                var result = $(this).cropper('getCroppedCanvas').toDataURL('image/jpeg');
                $("#result").val(result);
            },
            zoomable : true,
            scalable : false,
            movable : true,
            background : true,
            viewMode : 2,
            built : function(){
                $(this).cropper('getCroppedCanvas').toBlob(function (blob) {
                    var formData = new FormData();
                    formData.append('croppedImage', blob);
                    console.log(formData);
                });
            }
        });
    })

        


    $("#customFile").on('change', function(e) {
        // get the file name
        let fileName = e.target.files[0].name
        // replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);

        readURL(this);
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image').attr('src', e.target.result);
                croppie.cropper('replace', e.target.result)
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection

    
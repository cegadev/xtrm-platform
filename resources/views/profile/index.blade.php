@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Profile</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    @role('Member')
                    <ul class="nav nav-tabs mb-3">
                        <li class="nav-item">
                        <a class="nav-link {{ $tab == "profile" ? "active" : "" }}" href="?tab=profile"><i class="fa fa-user fa-fw"></i> Profile</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link {{ $tab == "bank" ? "active" : "" }}" href="?tab=bank"> <i class="fa fa-credit-card fa-fw"></i> Banks Information</a>
                        </li>
                    </ul>
                    @endrole
                    @if ($tab == "bank")
                        @role('Member')

                        @foreach ($banks->primary as $key => $bank)
                            @if (!is_null($bank) && !empty($bank))
                                <div class="row no-gutters mb-4">
                                    <div class="col-md-3 align-self-center">
                                        <div class="card border-0">
                                            <div class="row no-gutters">
                                                <div class="col-md-4">
                                                    <img src="/images/{{ \Str::lower($bank->name) }}.jpg" class="card-img" alt="">
                                                </div>
                                                <div class="col-md-8 align-self-center">
                                                    <div class="card-body">
                                                        <h3>{{ $bank->name }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col md-9">
                                        <form action="{{ route('admin.profile.update-bank-info', $key) }}" method="post">
                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Account Name</label>
                                                        <input type="text" class="form-control" name="account_name" value="{{ $bank->account_name ?? auth()->user()->membership->member_name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Account Number</label>
                                                        <input type="text" class="form-control" name="account_number" required value="{{ $bank->value }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-block btn-primary">SAVE CHANGES
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <hr>
                            @else
                                <div class="row no-gutters mb-4">
                                    <div class="col-md-3 align-self-center">
                                        <div class="card border-0">
                                            <div class="row no-gutters">
                                                <div class="col-md-4">
                                                    <img src="/images/{{ \Str::lower($key) }}.jpg" class="card-img" alt="">
                                                </div>
                                                <div class="col-md-8 align-self-center">
                                                    <div class="card-body">
                                                        @php
                                                            $keys = collect([
                                                                "bdo" => "BDO",
                                                                "gcash" => "GCash",
                                                                "paymaya" => "Paymaya"
                                                            ])
                                                        @endphp
                                                        <h3>{{ $keys->get($key) }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col md-9">
                                        <form action="{{ route('admin.profile.update-bank-info', $key) }}" method="post">
                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Account Name</label>
                                                        <input type="text" class="form-control" name="account_name" value="{{ old('account_name') ?? auth()->user()->membership->member_name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">Account Number</label>
                                                        <input type="text" class="form-control" name="account_number" required value="{{ old('account_number') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-block btn-primary">SAVE CHANGES
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <hr>
                            @endif
                        @endforeach
                        <h4>OTHER BANKS</h4>
                        <button class="btn btn-primary mt-3" onclick="newOtherBank()"><i class="fa fa-plus fa-fw"></i> Add New Bank</button>

                        @forelse ($banks->others as $bank)
                            <div class="row mt-3">
                                <div class="col-md-3 align-self-center">
                                    <div class="card border-0">
                                        <div class="row no-gutters">
                                            <div class="col-md-4">
                                                <i class="fa fa-landmark fa-4x"></i>
                                            </div>
                                            <div class="col-md-8 align-self-center">
                                                <div class="card-body">
                                                    <h3>{{ $bank->name }}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <form action="{{ route('admin.profile.update-bank-info', "others") }}" method="post">
                                        @csrf
                                        <input type="hidden" name="bank_id" value="{{ $bank->id }}">

                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Account Name</label>
                                                    <input type="text" class="form-control other-banks-account-name" data-id="{{ $bank->id }}" name="account_name" value="{{ $bank->account_name ?? auth()->user()->membership->member_name }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Account Number</label>
                                                    <input type="text" class="form-control other-banks-account-number" data-id="{{ $bank->id }}" placeholder="Enter account number here ..." name="account_number" required value="{{ $bank->account_number ?? $bank->value }}">
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-block btn-primary">SAVE CHANGES
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <hr>
                            @empty
                                <div class="mt-3">
                                    No other banks indicated. 
                                </div>
                            @endforelse
                                            
                        @endrole
                    @else 
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <div class="card" style="width: 100%">
                                    <img class="card-img-top rounded" src="{{ $user->avatar ? asset($user->avatar) : "https://www.signivis.com/img/custom/avatars/member-avatar-01.png"  }}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $user->first_name }} {{ $user->last_name}}</h5>
                                        <p class="card-text">
                                            <div>{{ strtoupper($user->roles->first()->name) }}
                                                @role('Member')
                                                #
                                                {{ sprintf('%08d', $user->membership->id) }}
                                                @endrole
                                            </div>
                                        </p>
                                        <a href="{{ route('admin.profile.change-photo') }}" class="btn btn-primary mb-2"><i class="fa fa-camera fa-fw"></i> Change Photo</a>
                                        <a href="{{ route('admin.profile.change-password') }}" class="btn btn-primary mb-2"><i class="fa fa-lock fa-fw"></i> Change Password</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 mb-3">
                                <div class="mb-4 text-right">
                                    <a href="{{ route('admin.profile.edit') }}" class="btn btn-primary"><i class="fa fa-edit fa-fw"></i> Edit Profile</a>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <label for="">FIRST NAME</label>
                                        <div style="font-size: 1.75em" class="mb-3">{{ $user->first_name ?? "(empty)" }}</div>
                                    </div>
                                    <div class="col">
                                        <label for="">LAST NAME</label>
                                        <div style="font-size: 1.75em" class="mb-3">{{ $user->last_name ?? "(empty)" }}</div>
                                    </div>
                                </div>
                                @if ($user->membership)
                                    <hr>
                                    <label for="">MEMBER NAME</label>
                                    <div style="font-size: 1.75em" class="mb-3">{{ $user->membership->member_name ?? "(empty)" }}</div>
                                @endif
                                <hr>
                                <label for="">MOBILE NUMBER</label>
                                <div style="font-size: 1.75em" class="mb-3">{{ $user->mobile_number ?? "(empty)" }}</div>
                                <hr>
                                <label for="">E-MAIL ADDRESS </label>
                                <div class="mb-3">
                                    <span style="font-size: 1.75em">
                                        {{ $user->email ?? "(empty)" }}
                                    </span>
                                    {{-- @role('Member')
                                    @if (!$user->hasVerifiedEmail())
                                        <a href="{{ route('verify-email') }}"><i class="fa fa-external-link-alt fa-fw"></i> Verify Email </a>
                                    @endif
                                    @endrole --}}
                                </div>
                                {{-- @role('Member')
                                @if (!$user->hasVerifiedEmail())
                                    <div class="alert alert-danger" role="alert">
                                        Email not yet verified. You won't be able to receive email notifications if you won't verify your email.
                                    </div>
                                @endif
                                @endrole --}}
                                @role('Member')
                                <hr>
                                <label for="">SPONSOR NAME</label>
                                <div style="font-size: 1.75em" class="mb-3">{{ $user->membership->parent_id ? $user->membership->parent->member_name : "(empty)" }}</div>

                                <hr>
                                <label for="memberSince" class="text-uppercase">Member Since</label>
                                <div style="font-size: 1.75em" class="mb-3">{{ $user->membership->created_at->format("F j, Y") }}</div>
                                @endrole
                                
                            </div>
                        </div>
                    @endif

                </div>
            </div>

        </div>
    </div>
    @role('Member')
    <div class="modal fade" tabindex="-1" role="dialog" id="newOtherBank">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Add New Bank</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.profile.new-bank-info') }}" id="frm_newBank" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="bank_name">Name *</label>
                        <input type="text" name="bank_name" id="bank_name" autocomplete="off" placeholder="Enter bank name (e.g. BPI, Security Bank, China Bank)" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="bank_account_name">Account Name *</label>
                        <input type="text" name="account_name" id="bank_account_name" autocomplete="off" placeholder="Enter Account Name" value="{{ auth()->user()->membership->member_name }}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Account Number *</label>
                        <input type="text" name="account_number" class="form-control" required>
                    </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="saveNewBank()">Save</button>
            </div>
          </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="removeBank">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Remove Bank Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.profile.remove-bank') }}" id="frm_removeBank" method="POST">
                    @csrf
                    <input type="hidden" name="remove_id" id="remove_id">
                </form>
                <div>
                    Are you sure you want to remove <strong id="remove_selected_bank"></strong>?
                </div>
                <div class="mt-1">
                    Account Name: <strong id="remove_selected_bank_account_name"></strong>
                </div>
                <div class="mt-1">
                    Account Number: <strong id="remove_selected_bank_number"></strong>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary" onclick="removeBankContinue()">Yes</button>
            </div>
          </div>
        </div>
    </div>
    @endrole
@endsection


@section('scripts')
    <script>
        newOtherBank = () => {
            $("#newOtherBank").modal('show')
        }

        saveNewBank = () => {
            $("#frm_newBank").submit()
        }

        removeBank = (id) => {
            let bank_name = $(".other-banks[data-id=" + id + "]").text();
            let bank_number = $(".other-banks-account-number[data-id=" + id + "]").val();
            let account_name = $(".other-banks-account-name[data-id=" + id + "]").val();
            $("#remove_id").val(id)
            $("#removeBank").modal('show')
            $("#remove_selected_bank").text(bank_name)
            $("#remove_selected_bank_number").text(bank_number)
            $("#remove_selected_bank_account_name").text(account_name)
        }

        removeBankContinue = () => {
            $("#frm_removeBank").submit();
        }
    </script>
@endsection

    
@extends('layouts.site')

@section('content')
<div id="banner_wrapper">
    <div id="banner_content">
        <div id="banner-title">
            <h1>Products</h1>
            <div>Great Collections of Car care products </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mt-5">
        <div class="mt-5">
            @foreach ($categories as $category)
                <div class="mb-5">
                    {{-- <h3 class="mb-4" style="text-transform: uppercase; font-weight: bold">
                        {{ $category->name }}
                    </h3> --}}

                    @if ($category->products->count() > 0)
                        <div class="row mb-4">
                            @foreach ($category->products as $product)
                                <div class="col-md-4 mb-4">
                                    <div class="card" style="width: 100%">
                                        <img class="card-img-top" src="{{ $product->product_image ?? \App\Http\Helpers\Helpers::defaultProductImage() }}" alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title" style="font-weight: 800">{{ $product->name }}</h5>
                                            {{-- <div>
                                                {!! \App\Http\Helpers\Helpers::generateRating(round($product->averageRating()), $product->publishedReviews->count(), $product->publishedReviews->count() > 0) !!}
                                            </div> --}}
                                            <p class="card-text mt-3" style="min-height: 2em">
                                                {{ $product->basic_description ?? "No description available." }}
                                            </p>
                                            <button onclick="viewProduct(this, {{ $product->id }})" class="btn btn-primary">View Product &rarr;</button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="mb-4 text-center">
                            No products available in this category.
                        </div>
                    @endif
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="viewProduct_modal">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body">
                <div>
                    <div class="row">
                        <div class="col-md-5 d-flex">
                            <div>
                                <img src="http://localhost:8080/storage/sample.jpeg" alt="" class="img-fluid" id="vp-productImage">
                            </div>
                        </div>
                        <div class="col-md-7">
                            <small id="vp-category" style="display: none">CATEGORY 1</small>
                            <h2 id="vp-productName">Product 1</h2>
                            {{-- <div id="vp-reviews">
                                @for ($i = 0; $i < 4; $i++)
                                <i class="fa fa-star small" style="color: #ffe31a"></i>
                                @endfor
                                <i class="fa fa-star small" style="color: black"></i>
                                <small>(15 reviews)</small>
                            </div> --}}
                            <div style="font-size: .95em" class="mt-4" id="vp-details">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam quas quam vitae, saepe adipisci ipsam reiciendis fugiat unde voluptatem maxime inventore architecto magnam autem explicabo eos maiores consequatur consequuntur ex?
                            </div>
                            <div class="mt-5">
                                <a href="" id="vp-detailsUrl">View full details &rarr;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>

        function viewProduct(btn, id) {
            $(btn).prop('disabled', true)


            $.get("/api/products/view-product/" + id, {}, (data) => {
                $("#viewProduct_modal").modal('show')

                let description = !data.basic_description ? "No description available." : data.basic_description.trim()
                if (data.category) {
                    $("#vp-category").text(data.category.name.toUpperCase())
                } else {
                    $("#vp-category").text("OTHERS")
                }

                let productImage = !data.product_image ? "https://dummyimage.com/600x400/1f1f1f/fff" : data.product_image

                $("#vp-productName").text(data.name)
                $("#vp-details").text(description)
                $("#vp-productImage").attr("src", productImage)
                $("#vp-detailsUrl").attr("href", "/products/" + data.slug)


                let str_reviews = "";
                data.published_reviews.forEach((item) => {
                    
                })

                let average = data.published_reviews.reduce((total, next) => total + next.rating, 0)
                let hasReviews = data.published_reviews.length > 0;
                let color = "black"
                for (i = 1; i <= 5; i++) {
                    if (i <= average) {
                        color = "#ffe31a"
                    } else {
                        color = "black"
                    }

                    outline = hasReviews ? "" : "-o"
                    str_reviews += `<i class="fa fa-star${outline} small" style="color: ${color}"></i>`
                }

                if (hasReviews) {
                    str_reviews += ` <small>${data.published_reviews.length} review</small>`
                } else {
                    str_reviews += ` <small>No reviews yet</small>`
                }

                $("#vp-reviews").html(str_reviews);
                $(btn).prop('disabled', false)
            }, "json")
        }


</script>
@endsection
@extends('layouts.site')


@section('styles')
    <link rel="stylesheet" href="/css/home.css?v=2">
@endsection

@section('content')
    {{-- <div id="banner_wrapper">
        <div class="container">
            <h1 class="banner-title">Carnuaba Wax</h1>
            <p>Your ALL-IN Shine Protector.</p>
            <div class="btn btn-outline-warning btn-lg banner-btn" style="margin-top: 1em">GET STARTED</div>
        </div>
    </div> --}}
    <div id="home-carousel-wrapper" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#home-carousel-wrapper" data-slide-to="0" class="active"></li>
          <li data-target="#home-carousel-wrapper" data-slide-to="1"></li>
          <li data-target="#home-carousel-wrapper" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            @foreach ($slides as $key => $slide)
                <div class="carousel-item {{ $key == 0 ? "active" : "" }}" 
                {{-- style="background-image: url('{{ $slide->data->image }}')"  --}}
                >
                <div class="ci-image" style="max-height: 90vh;
                background: #000;
                text-align: center;">
                    <img src="{{ asset($slide->data->image) }}" alt="" style="max-height: 100%; max-width: 100%">
                </div>
                    <div class="carousel-caption d-none d-md-block">
                        <div class="caption" style="visibility: hidden">
                            <div class="c-title">{{ $slide->data->title ?? "" }}</div>
                            <div class="c-description">{{ $slide->data->sub_title ?? "" }}</div>
                            <div class="mt-3">
                                <a href="{{ $slide->data->action ?? "#" }}" class="btn btn-outline-warning">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#home-carousel-wrapper" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#home-carousel-wrapper" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    <div id="packages_wrapper">
        <div id="packages_content">
            <div class="title">Interested in becoming a distributor?</div>
            <div class="container mt-3" id="distributor_wrapper">
                <div class="package-item-list row no-gutters">
                    @php
                        $desktop_col = $packages->count() == 4 ? "3" : "4";
                    @endphp
                    @foreach ($packages as $package)
                    
                        <div class="card border-0 package-item-wrapper col-sm-6 col-xs-6 col-md-{{ $desktop_col }} col-lg-{{ $desktop_col }} col-xl-{{ $desktop_col }}" style="margin-bottom: 1em;">
                            <div style="padding: 0 1em; " class="package-item card-body h-100">
                                <div class="name">
                                    {{ $package->name }}
                                </div>
                                {{-- <div class="image">
                                    <img src="/images/sample-package.jpg" alt="" class="home-package-image">
                                </div> --}}
                                <div class="inclusions d-flex align-items-center">
                                    <ul>
                                        @php
                                            $inclusions = explode(PHP_EOL, $package->settings->inclusions);
                                        @endphp
                                        @foreach ($inclusions as $inclusion)
                                            <li style="margin-bottom: 1em"><i class="fa fa-check text-success"></i> {{ $inclusion }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="footer">
                                    <span class="peso-sign"></span>
                                    @php
                                        $price = explode(".", number_format($package->price, 2));
                                    @endphp 
                                    <span class="amount">{{ $price[0] }}</span><span class="zeros">.{{ $price[1] }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <div id="parallax-wrapper">
        <div class="parallax" style="background-image: url('images/products-bg.jpg')">
            <div class="parallax-content container">
                @php
                    $item = $products->pull(1)
                @endphp
                <div class="row">
                    <div class="col-md-5 d-none d-md-block d-lg-block d-xl-block">
                        <img src="{{ $item->image ?? "/images/products.jpg"}}" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12 d-flex align-items-center">
                        <div class="">
                            <div class="title" style="text-align: left">{{ $item->title }}</div>
                            {!! $item->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax" style="background-image: {{ $backgrounds->pull(1) }}; background-position: right">
            <div class="parallax-content container">
                @php
                    $item = $products->pull(2)
                @endphp
                <div class="title">{{ $item->title }}</div>
                <div class="row mt-3">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <img src="{{ $item->images[0] ?? "/images/the-products-1.png" }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <img src="{{ $item->images[1] ?? "/images/the-products-2.png" }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="parallax" style="background-image: {{ $backgrounds->pull(2) }}; background-position: right">
            <div class="parallax-content container">
                @php
                    $item = $products->pull(3)
                @endphp
                <div class="title">{{ $item->title }}</div>
                <div class="row mt-3">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <img src="{{ $item->images[0] ?? "/images/the-products-3.png" }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <img src="{{ $item->images[1] ?? "/images/the-products-4.png" }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="about_us_wrapper">
        <div class="container" id="about_us">
            <div class="row">
                <div class="col-md-6 d-flex align-items-center">
                    <div>
                        {!! $home_about_us->text !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="{{ $home_about_us->image }}" alt="" style="width: 100%">
                </div>
            </div>
        </div>
    </div>
@endsection
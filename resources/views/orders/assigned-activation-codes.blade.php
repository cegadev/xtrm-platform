@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <h1 class="h3 mb-0 text-gray-800">Assigned Activation Codes</h1>
            <small>The following are activation codes released to you by the main office. These are shareable to your downline members.</small>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Activation Codes</h6>
                </div>
                <div class="card-body">
                    <div class="mb-3 nav nav-tabs">
                        <div class='nav-item'>
                            <a class="nav-link {{ $type == "all" ? "active" : "" }} " href="{{ route('my-orders.my-activation-codes.index', ['type' => 'all']) }}">
                                <i class="fa fa-square fa-fw text-dark"></i> All Types
                            </a>
                        </div>
                        <div class='nav-item'>
                            <a class="nav-link {{ $type == "product" ? "active" : "" }} " href="{{ route('my-orders.my-activation-codes.index', ['type' => 'product']) }}">
                                <i class="fa fa-key fa-fw text-info"></i> Product
                            </a>
                        </div>
                        <div class='nav-item'>
                            <a class="nav-link {{ $type == "package" ? "active" : "" }} " href="{{ route('my-orders.my-activation-codes.index', ['type' => 'package']) }}">
                                <i class="fa fa-key fa-fw text-primary"></i> Package
                            </a>
                        </div>
                    </div>
                    @if ($activation_codes->count())
                        Showing {{ number_format($activation_codes->count()) }} activation codes.
                    @else 
                        No activation codes available.
                    @endif
                    <div class="table-responsive mt-3">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width=50></th>
                                    <th>Activation Code</th>
                                    <th>Received at</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($activation_codes as $code)
                                    <tr>
                                        <td>
                                            <i class="fa fa-key fa-fw text-{{ $code->type == "product" ? "info" : "primary" }}"></i>
                                        </td>
                                        <td>{{ $code->code }}</td>
                                        <td>{{ \Carbon\Carbon::parse($code->released_at)->diffForHumans() }}</td>   
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">No activation code available.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('scripts')
    <script>
        
    </script>
@endsection
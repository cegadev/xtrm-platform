@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <h1 class="h3 mb-0 text-gray-800">Transactions</h1>
        </div>
        <div>
            <a href="{{ route('my-orders.activations.index') }}" class="btn btn-primary"><i class="fa fa-list fa-fw"></i> My Activations</a>
            @if ($show_history)
                <a href="?show_history=true#transactionHistories_wrapper" class="btn btn-primary"> <i class="fa fa-history fa-fw"></i> Go to Transactions History</a>
            @else 
                <a href="?show_history=true#transactionHistories_wrapper" class="btn btn-primary"> <i class="fa fa-history fa-fw"></i> Show Transactions History</a>
            @endif
        </div>
    </div>


    <div class="card border-left-primary shadow">
        <div class="card-header">
            <div class="font-weight-bold">My Activation Codes</div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-sm-12 align-self-center">
                    <i class="fa fa-key fa-fw"></i> Products &times; {{ $activation_codes->get("Product") }}
                </div>
                <div class="col-md-6 col-sm-12 align-self-center">
                    <div class="row">
                        @foreach ($packages as $package)
                            <div class="col-md-{{ $packages->count() == 4 ? "3" : "4" }} mb-2">
                                <i class="fa fa-key fa-fw" style="color: {{ $package->settings->color }}"></i> {{ $package->name }} &times; {{ $activation_codes->get("package-" . $package->id) }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">My Orders</h6>
                </div>
                <div class="card-body">
                    @if ($orders->count())
                        Showing {{ number_format($orders->count()) }} orders.
                    @endif

                    @if ($orders->count() == 0)
                        <div>No transactions to display.</div>
                    @endif
                    <div class="table-responsive mt-3 {{ $orders->count() == 0 ? "d-none" : "" }} ">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width=150>Order ID</th>
                                    <th>Items</th>
                                    <th>Received at</th>
                                    <th width=300></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($orders as $order)
                                    <tr>
                                        <td class="align-middle">
                                            #{{ \sprintf("%08d", $order->id) }}
                                        </td>
                                        <td class="align-middle">
                                            <div class="h5 mb-0 font-weight-bold order-names" data-id="{{ $order->id }}">{{ $order->product->name }}</div>
                                            @if ($order->product->type == "package")
                                            <small>Registrations Left - {{ $order->items->where('is_activated', false)->count() }} / {{ $order->items->count() }}</small>
                                            @else 
                                            <small>Activations - {{ $order->items->where('is_activated', false)->count() }} / {{ $order->items->count() }}</small>
                                            @endif
                                        </td>
                                        <td class="align-middle">
                                            @if ($order->user_id !== $order->items->first()->user_id)
                                                {{ \Carbon\Carbon::parse($order->items->first()->transferred_at)->diffForHumans() }}
                                            @else 
                                                {{ \Carbon\Carbon::parse($order->released_at)->diffForHumans() }}
                                            @endif
                                        </td>      
                                        <td class="text-right align-middle">

                                            @if ($order->product->type == "package")
                                                @php
                                                    $canRegister = $activation_codes->get("package-" . $order->product_id) > 0;
                                                @endphp

                                                @if ($canRegister)
                                                    <button onclick="transferItem({{ $order->id }})" class="btn btn-info">Transfer</button>

                                                    <button onclick="registerNewMemberWindow(this, {{ $order->id }})" class="btn btn-danger">Register</button>
                                                @else 
                                                    {{-- <button class="btn btn-danger" disabled >Register</button> --}}
                                                    <div>No activation codes available.</div>
                                                @endif
                                            @else 

                                                @php
                                                    $canActivate = $activation_codes->get('Product') > 0
                                                @endphp

                                                @if ($canActivate)
                                                    <button onclick="transferItem({{ $order->id }})" class="btn btn-info">Transfer</button>

                                                    <button class="btn btn-warning" onclick="activateProducts(this, {{ $order->id }})">Activate</button>
                                                @else
                                                    {{-- <button class="btn btn-warning" disabled>Activate</button> --}}
                                                    <div>No activation codes available.</div>
                                                @endif
                                            @endif

                                           

                                        </td>
                                    </tr>
                                @empty
                                    {{-- <tr>
                                        <td colspan="3">No orders for activation available.</td>
                                    </tr> --}}
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>

    @if ($show_history)
        <div class="row mt-4" id="transactionHistories_wrapper">
            <div class="col-md-12">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Transactions History</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="transactionHistories">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <th>Amount</th>
                                        <th>Type</th>
                                        <th>Transaction Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @include('admin.activation.modals.registration-form-2')

    <div class="modal fade" id="transferItem" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Transfer Item</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="" onsubmit="return false">
                <input type="hidden" name="transfer_order_id" id="transfer_order_id">
                <div class="form-group mb-3">
                    <label for="">Item</label>
                    <div class="h4 font-weight-bold" id="selectedOrderName">E-Starter</div>
                </div>
                <div class="form-group">
                    <label for="">Transfer To</label>
                    <input type="text" name="transfer_username" id="transfer_username" placeholder="Enter member's username" min="1" class="form-control">
                    <div class="invalid-feedback" id="transfer_username_err"></div>
                </div>
                <div class="form-group">
                    <label for="">Transfer Quantity</label>
                    <input type="number" name="quantity" id="quantity" min="1" value="1" class="form-control">
                    <div class="invalid-feedback" id="transfer_quantity_err"></div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="submitTransferItem(this)" id="btnTransferItem">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="activateProduct" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Activate Product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="" onsubmit="return false">
                <input type="hidden" name="activate_product_order_id" id="activate_product_order_id">
                <div class="form-group mb-3">
                    <label for="">Item</label>
                    <div class="h4 font-weight-bold" id="selectedProductName">E-Starter</div>
                </div>
                <div class="form-group">
                    <label for="">Activate Product Quantity</label>
                    <input type="number" name="prod_activate_quantity" id="prod_activate_quantity" min="1" value="1" class="form-control">
                    <div class="invalid-feedback" id="prod_activate_quantity_err"></div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="submitActivateProducts(this)" id="btnActivateProduct">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>
@endsection
@section('scripts')
        <script>
        let user_id = "{{ auth()->id() }}";
        let member_id = "{{ auth()->user()->membership->id }}";
        let activation_codes = [];

        $.get("{{ route('api-activation-code.my-codes') }}", {
            user_id,
        }, (data) => {
            activation_codes = data
        })

    </script>

    <script>
        transferItem = (orderId) => {
            $("#transfer_order_id").val(orderId)
            $("#transfer_username").val('')
            $("#quantity").val(1)

            let orderName = $(".order-names[data-id=" + orderId + "]").text()
            $("#selectedOrderName").text(orderName)
            $("#transferItem").modal('show')

        }

        submitTransferItem = (btn) => {
            $(btn).prop('disabled', true)
            let username = $("#transfer_username").val()
            let quantity = $("#quantity").val()
            let order_id = $("#transfer_order_id").val()

            let fields = {
                username: {
                    value: username,
                    field: $("#transfer_username"),
                    err: $("#transfer_username_err")
                },
                quantity: {
                    value: quantity,
                    field: $("#quantity"),
                    err: $("#transfer_quantity_err")
                },
            }

            $.ajax({
                url: "{{ route('api-orders.transfer-item') }}",
                data: {
                    user_id,
                    username,
                    quantity,
                    order_id,
                },
                method: "POST",
                complete: (xhr, textStatus) => {
                    if (textStatus == "error") {
                        let data = xhr.responseJSON;
                        if (xhr.status == 422) {
                            let errors = data.errors;
                            Object.keys(errors).forEach(key => {
                                console.log(key)
                                let field = fields[key].field
                                let errField = fields[key].err
                                field.addClass('is-invalid')
                                errField.text(errors[key].join(". "))
                            })
                            $(btn).prop('disabled', false)
                        } else if (xhr.status == 500) {
                            alert('Something went wrong. Please try again later.')
                            $(btn).prop('disabled', false)
                        } else {
                            alert(data.message)
                            $(btn).prop('disabled', false)
                        }
                    } else {
                        alert('Transferred successfully. '); window.location.reload()
                    }
                }
            })
        }
    </script>

    <script>
        $("#cancel_activation_btn").on('click', () => {
            $("#zoomPackageRegistration2").modal('hide')
        })
        let order_items = [];
        registerNewMemberWindow = (btn, orderId) => {
            $(btn).prop("disabled", true)
            document.getElementById("frmProductActivation").reset();
            $("#submit_activation_btn").off('click');
            $("#activation_order_id").val(orderId)


            $("#submit_activation_btn").prop("disabled", true);
            $("#submit_btn_loading").hide();
            $("#submit_btn_text").text("SUBMIT")

            $.get("{{ route('api-orders.get-order') }}", {
                id: orderId,
                user_id,
            }, (order) => {
                order_items = order.order_items
                $(btn).prop("disabled", false)
                let str_activations_left = "(" + order.activations_left + " registration" + (order.activations_left > 1 ? "s" : "") + " left)";
                if (order.keys_available) {
                    str_keys_available = `<div style='float: right'><i class='fa fa-key fa-fw'></i> &times; ${order.keys_available} </span> available</div>`;
                    $("#submit_activation_btn").prop('disabled', false)
                    $("#submit_activation_btn").off('click')
                                        .on('click', () => {
                                            $("#frmProductActivation").submit()
                                        })
                } else {
                    str_keys_available = `<div style='float: right'> No keys available.</div>`
                }
                $("#product-registration-selected-package").html(`Package: [${order.product_code}] - ${order.product_name} <span> ${str_keys_available}`);
                
                $("#zoomPackageRegistration2").modal('show')
                
                
            }, "json")

        }


        viewHistory = (id) => {
            $.get("{{ route('api-activation.view-history') }}", {
                order_id: id
            }, (data) => {
                let histories = "";
                if (data.length == 0) {
                    $("#histories_wrapper").text("No histories available yet.");
                    $("#histories").hide();
                } else {
                    data.forEach(h => {
                        histories += `<li class='list-group-item'>${h.member_name} <span style='float:right'>${h.ago}</span></li>`;
                    })
                    $("#histories_wrapper").text("");
                    $("#histories").show();
                    $("#histories").html(histories);
                }

                $("#activationHistory").modal('show')
            })
        }
        let isSaving = false;
        let registerNewMember = () => {

            $("#submit_activation_btn").prop("disabled", true);
            $("#submit_btn_loading").show();
            $("#submit_btn_text").text("LOADING...")

            let canSubmit = false;
            let errCount = 0;
            let fields = {
                first_name: {
                    field: "#member_first_name", 
                    label: "First name",
                    value: document.getElementById("member_first_name").value,
                    error: "#first-name-invalid-message"
                },
                middle_name: {
                    field: "#member_middle_name", 
                    label: "Middle name",
                    value: document.getElementById("member_middle_name").value,
                    error: "#middle-name-invalid-message",
                },
                last_name: {
                    field: "#member_last_name", 
                    label: "Last name",
                    value: document.getElementById("member_last_name").value,
                    error: "#last-name-invalid-message",
                },
                email: {
                    field: "#member_email",
                    label: "Email address",
                    value: document.getElementById("member_email").value,
                    error: "#email-invalid-message"
                },
                mobile_number: {
                    field: "#mobile_number",
                    label: "Mobile number",
                    value: document.getElementById("mobile_number").value,
                    error: "#mobile-number-invalid-message"
                },
                username: {
                    field: "#member_username",
                    label: "Username",
                    value: document.getElementById("member_username").value,
                    error: "#username-invalid-message"
                },
                password: {
                    field: "#member_password",
                    label: "Password",
                    value: document.getElementById("member_password").value,
                    error: "#password-invalid-message"
                },
                repeat_password: {
                    field: "#repeat_password",
                    label: "Repeat password",
                    value: document.getElementById("repeat_password").value,
                    error: "#repeat-password-invalid-message"
                },
                sponsor_username: {
                    field: "#sponsor_username",
                    label: "Sponsor username",
                    value: document.getElementById("sponsor_username").value,
                    error: "#sponsor-invalid-message"
                },
            }

            let data = {
                order_id: document.getElementById("activation_order_id").value,
                user_id,
                order_item_id: order_items[0].id,
            };
            Object.keys(fields).forEach(key => {
                let value = fields[key].value

                data[key] = value
            })

            // validate tnc
            let tnc = document.getElementById("tnc");
            $("#tnc-message").hide()
            if (!tnc.checked) {
                $("#tnc-message").show();
                errCount++;
            }

            // validate password
            $("#repeat_password").removeClass("is-invalid")
            if (data.password != data.repeat_password) {
                $("#repeat_password").addClass("is-invalid")
                $("#repeat-password-invalid-message").text("Repeat Password did not match.")
                errCount++;
            }

            if (errCount == 0 && isSaving == false) {
                isSaving = true;

                $.ajax({
                    url: "{{ route('api-activation.register-new-member') }}",
                    data: data,
                    method: "POST",
                    complete: (xhr, textStatus) => {
                        let response = xhr.responseJSON;

                        setTimeout(() => {
                            if (textStatus == "error") {
                                if (xhr.status == 500) {
                                    alert('Something went wrong. Try again later.')
                                } else if (xhr.status == 405) {
                                    alert(response.message)
                                } else {

                                    let errors = response.errors
                                    Object.keys(fields).forEach(key => {
                                        $(fields[key].field).removeClass('is-invalid')
                                    })

                                    Object.keys(errors).forEach(key => {
                                        let str_error = errors[key].join(". ")
                                        $(fields[key].field).addClass('is-invalid')
                                        $(fields[key].error).text(str_error)
                                    })
                                }
                            } else {
                                window.location = "{{ route('my-orders.activate.after-activation', ['type' => 'package']) }}";
                            }

                            isSaving = false;
                            
                            $("#submit_activation_btn").prop("disabled", false);
                            $("#submit_btn_loading").hide();
                            $("#submit_btn_text").text("SUBMIT")

                        }, 1000)

                    }
                })
                
            } else {
                $("#submit_activation_btn").prop("disabled", false);
                $("#submit_btn_loading").hide();
                $("#submit_btn_text").text("SUBMIT")
            }
            return false;
        }

        let activateProducts = (btn, order_id) => {
            $("#activate_product_order_id").val(order_id)
            $("#prod_activate_quantity").val("1")
            $("#prod_activate_quantity").removeClass('is-invalid')
            let name = $(".order-names[data-id=" + order_id + "]").text()
            $("#selectedProductName").text(name)
            $("#activateProduct").modal('show')
            
        }
        
        submitActivateProducts = (btn) => {
            $(btn).prop('disabled', true)
            let order_id = $("#activate_product_order_id").val();
            let quantity = $("#prod_activate_quantity").val()

            let fields = {
                quantity: {
                    field: "#prod_activate_quantity", 
                    label: "Quantity",
                    value: quantity,
                    error: "#prod_activate_quantity_err"
                }
            }

            $.ajax({
                url: "{{ route('api-orders.activate-product') }}",
                data: {
                    order_id,
                    user_id,
                    quantity
                },
                method: "POST",
                complete: (xhr, textStatus) => {
                    if (textStatus == "error") {
                        let data = xhr.responseJSON;

                        if (xhr.status == 500) {
                            alert('Something went wrong. Please try again later.')
                            $(btn).prop('disabled', false)
                        } else if (xhr.status == 422) {
                            let errors = data.errors
                            Object.keys(errors).forEach(key => {
                                let f = fields[key];
                                $(f.field).addClass('is-invalid')
                                $(f.error).text(errors[key])
                            })
                            $(btn).prop('disabled', false)
                            
                        } else {
                            alert(data.message)
                            $(btn).prop('disabled', false)

                        }


                    } else {
                        window.location = "{{ route('my-orders.activate.after-activation', ['type' => 'product']) }}";
                    }
                }
            })
        }

    </script>

    @if($show_history)
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        <script>

            $("#transactionHistories").DataTable({
                "processing": false,
                "serverSide": false,
                "ajax": {
                    "url": "/api/transactions",
                    "data": {
                        member_id,
                    },
                },
                "columns": [
                    { "data": "id" },
                    { "data": "amount" },
                    { "data": "type" },
                    { "data": "transaction_date" },
                ],
                "order": [[0, "desc"]]
            })
        </script>
    @endif
@endsection
    
@section('styles')
@if ($show_history)
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
@endif

@endsection
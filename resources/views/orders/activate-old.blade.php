@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <small>MY ORDERS</small>
            <h1 class="h3 mb-0 text-gray-800">Product Activation</h1>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Order #{{ $order->id }} Products</h6>
                </div>
                <div class="card-body">
                    <button class="btn btn-info" onclick="enterProductKey({{ $order->id }})"><i class="fa fa-key fa-fw"></i> Enter Activation Code</button>
                    <div class="table-responsive">
                        <table class="table mt-4">
                            <thead>
                                <tr>
                                    <th width=50>#</th>
                                    <th>Product Name</th>
                                    <th>Activated At</th>
                                    <th>Activation Code</th>
                                    <th width=300></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($activations as $key => $item)
                                    <tr>
                                        <td class="align-middle">{{ $key + 1 }}</td>
                                        <td class="align-middle">{{ $item->product->name }}</td>
                                        <td class="align-middle">{{ $item->activated_at ? \Carbon\Carbon::parse($item->activated_at)->diffForHumans() : "For Activation" }}</td>
                                        <td class="align-middle">
                                            @if ($item->is_activated)
                                                {{ $item->code }}
                                            @else
                                                MJP-XXXXXXXXXXXX
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->is_activated)
                                                <button class="btn btn-warning btn-block" style="cursor: default"><i class="fa fa-unlock fa-fw"></i> Activated</button>
                                            @else
                                                <button class="btn btn-info btn-block" disabled><i class="fa fa-lock fa-fw"></i> Locked</button>

                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>

                    <a href="{{ $back_url ?? route('my-orders.index', ['tab' => 'received']) }}" class="btn btn-primary"><i class="fa fa-arrow-left fa-fw"></i> Back to My Orders</a>

                </div>
            </div>
        </div>
        {{-- <div class="col-md-6">
            <div class="card border-left-info shadow py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Product Activation</h6>
                </div>
                <div class="card-body">
                    <div>Activation Left: <strong>3</strong></div>
                    <hr>
                    <form action="" class="mt-2">
                        <div class="form-group">
                            <label for="">Enter Product Activation Code</label>
                            <input type="text" class="form-control ">
                        </div>
                        <button class="btn btn-primary">Activate</button>
                    </form>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="modal fade" id="enterActivationCode" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Enter Activation Code</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('my-orders.activate.activate', $order->id) }}" id="activateForm" method="POST">
                  @csrf
                  <div class="form-group">
                      <label for="">Enter 12-character activation code</label>
                      <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">MJP-</span>
                            </div>
                            <input type="text" class="form-control" id="product_code" name="product_code" placeholder="" max="12" aria-describedby="basic-addon1">
                            <div class="invalid-feedback" id="invalidProductCode"></div>
                      </div>
                      <div class="mt-2">
                      Example: MJP-XXXXXXXXXXXX
                      </div>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="activation_code_submit" onclick="validateActivationCode()">Submit</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')
    <script>
        let order_id;
        enterProductKey = (id) => {
            order_id = id
            $("#product_code").val("")
            $("#enterActivationCode").modal('show')
        }

        validateActivationCode = () => {
            let product_code = $("#product_code").val()
            $.ajax({
                url: "/api/activation/validate-product-code",
                data: {
                    product_code,
                    order_id
                },
                complete: (xhr, textStatus) => {
                    let response = xhr.responseJSON;
                    switch(xhr.status) {
                        case 400:
                            $("#product_code").addClass("is-invalid")
                            $("#invalidProductCode").text(response.data)
                        break;

                        default:
                            $("#product_code").removeClass("is-invalid")
                            $("#activateForm").submit()
                        break;
                    }
                },
            })
        }
    </script>
@endsection
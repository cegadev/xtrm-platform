@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Create New Order</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <form method="POST" action="{{ route('orders.save-order') }}" onsubmit="return validateForm()" id="frmNewOrder">
                        @csrf
                        @method("POST")
                        <div class="form-group">
                          <label for="fg-member">Enter Member's username</label>
                          <input class="form-control" readonly id="choose-member" placeholder="Enter member's name or username" list="members">
                          <input type="hidden" name="member_id" id="member_id">
                          <datalist id="members"></datalist>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="fg-code">Choose Product</label>
                                <input class="form-control" readonly id="choose-product" placeholder="Enter product name or code" list="products">
                                <input type="hidden" name="product_id" id="product_id">

                                <datalist id="products"></datalist>
                            </div>
                            <div class="col-md-2">
                                <label for="fg-qty">Quantity</label>
                                <input type="number" class="form-control" name="quantity" id="qty" placeholder="1" min="1" value="1">
                            </div>
                            <div class="col-md-2">
                                <label for="fg-total">Total Amount</label>
                                <div class="d-flex align-items-center">
                                    <div>
                                        <span class="peso-sign"></span> <span id="total-amount">0.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 text-right">
                            <a href="{{ route('orders.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary"> <i class="fa fa-check fa-fw"></i> 
                                @if (auth()->user()->isMember())
                                    Save Order                                    
                                @else
                                    Save Order & Release
                                @endif
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let selectedMember = null;
        let selectedProduct = null;
        let quantity = 1;
        let isSubmitted = false;
        loadMembers = () => {
            $.get("{{ route('api-members.members-list') }}", (members) => {
                let str = "";
                members.forEach(member => {
                    let memberName = "" + member.username + " - " + member.first_name + (member.last_name ? ' ' + member.last_name : '') 
                    str += `<option id='${member.id}' value="${memberName}" />`;
                });

                $("#members").html(str);
                $("#choose-member").prop('readonly', false)

                $("#choose-member").on('change keyup', (item) => {
                    let itemId = $("#members option[value='" + item.target.value + "']").attr('id');
                    if (itemId) {
                        selectedMember = {
                            id: itemId,
                            name: item.target.value
                        }

                        if (isSubmitted) {
                            $("#choose-member").removeClass('is-invalid')
                        }
                    } else {
                        selectedMember = null;
                        if (isSubmitted) {
                            $("#choose-member").addClass('is-invalid')
                        }
                    }
                })
            }, "json")
        }


        loadProducts = () => {
            $.get("{{ route('api-products.products-list') }}", (products) => {
                let str = "";
                products.forEach(product => {
                    let productName = product.name + " " + "(" + product.code + ") - P" + product.price
                    str += `<option id='${product.id}' value="${productName}" />`;
                });

                $("#products").html(str);
                $("#choose-product").prop('readonly', false)

                $("#choose-product").on('change keyup', (item) => {
                    let itemId = $("#products option[value='" + item.target.value + "']").attr('id');
                    if (itemId) {
                        selectedProduct = {
                            id: itemId,
                            price: products.find(p => p.id == itemId).price
                        }

                        quantity = parseInt($("#qty").val())
                        computeTotal()

                        if (isSubmitted) {
                            $("#choose-product").removeClass('is-invalid')
                        }
                    } else {
                        selectedProduct = null
                        $("#total-amount").text("0.00")
                        if (isSubmitted) {
                            $("#choose-product").addClass('is-invalid')
                        }
                    }

                })
            }, "json")
        }

        loadMembers()
        loadProducts()

        $("#qty").on('change', (data) => {
            quantity = parseInt(data.target.value);

            if (selectedProduct) {
                computeTotal()
            }
        })

        computeTotal = () => {
            let total = selectedProduct.price * quantity
            let totalAmount = parseFloat(total);
            $("#total-amount").text(total.toFixed(2))
        }

        validateForm = () => {
            let errorCount = 0;
            isSubmitted = true
            if (selectedMember == null) {
                $("#choose-member").addClass('is-invalid')
                errorCount++
            }

            if (selectedProduct == null) {
                $("#choose-product").addClass('is-invalid')
                errorCount++
            }

            if (errorCount == 0) {
                $("#member_id").val(selectedMember.id)
                $("#product_id").val(selectedProduct.id)
                return true;
            }

            return false;
        }

    </script>
@endsection
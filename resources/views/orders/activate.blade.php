@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <small>VIEW</small>
            <h1 class="h3 mb-0 text-gray-800">Transaction History</h1>
        </div>
        <a href="{{ $back_url ?? route('my-orders.index') }}" class="btn btn-outline-secondary btn-icon-split">
            <span class="icon"><i class="fa fa-arrow-left"></i></span>
            
            <span class="text">Back to Orders</span>
        </a>
    </div>

   

    <!-- Content Row -->
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Order #{{ $order->id }} {{ \Str::ucfirst($order->product->type) }}s</h6>
                </div>
                <div class="card-body">
                    {{-- @if ($member_activation_codes->{$order->product->type} == 0 && $for_activation > 0)
                        <div class="alert alert-danger">No {{ $order->product->type }} activation keys available.</div>
                    @endif --}}
                    
                    @if ($tab == "pending")
                        @if ($for_activation == 0)
                            <div>No items for activation in this order.</div>
                        @endif

                        @for ($i = 0; $i < $for_activation; $i++)
                            <div class="card mb-3" id="item-{{ $i }}">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col align-self-center">
                                            <div class="h4">
                                                <i class="fa fa-cube{{ $order->product->type == "package" ? "s" : "" }}"></i>
                                                {{ $order->product->name }}
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="btn btn-warning" onclick="activateItem(this, {{ $i }})"><i class="fa fa-key fa-fw"></i> 
                                                <span id="activateText">Activate</span>
                                                <span class="spinner-border spinner-border-sm loader" style="display: none" role="status" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor

                    @else 
                        @if ($activated_items)
                            {{-- <div class="alert alert-success">Earn direct referral bonus per registration of new member.</div> --}}
                            @php
                                $remaining = $activated_items - $registrations->count()
                            @endphp
                            @if ($remaining && $order->type == "package")
                                <div class="h5 mb-3"> <small>REGISTRATIONS LEFT :</small>
                                    <strong> {{ $remaining }}</strong>
                                </div>
                            @endif
                        @endif

                        @for ($i = 0; $i < $activated_items; $i++)
                            <div class="card mb-3">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col align-self-center">
                                            <div class="row">
                                                <div class="col-auto align-self-center">
                                                    <i class="fa fa-2x fa-cubes"></i>
                                                </div>
                                                <div class="col">
                                                    <div class="h4 mb-0">
                                                        {{ $order->product->name }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($order->product->type == "package")
                                            <div class="col-md-5 align-self-center text-right">
                                                @if ($registrations->get($i))
                                                    <small>REGISTERED TO</small>
                                                    <div>
                                                        <i class="fa fa-user fa-fw"></i> <span class="font-weight-bold">{{ $registrations->get($i)->member_name }}</span> - {{ $registrations->get($i)->created_at->format('Y-m-d') }}
                                                    </div>
                                                @else 
                                                    <button class="btn btn-danger" onclick="registerNewMemberWindow(this, {{ $order->id }})"><i class="fa fa-user-plus fa-fw"></i> 
                                                        <span id="activateText">Register New Member</span>
                                                    </button>
                                                @endif
                                                
                                            </div>

                                        @else 
                                            <div class="col-md-5 align-self-center text-right">
                                                @php
                                                    $registration = $registrations->get($i)
                                                @endphp
                                                Registered at - {{ \Carbon\Carbon::parse($registration->activated_at)->format('Y-m-d')  }}
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        @endfor
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="registerActivationCode" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Register Activation Code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" onsubmit="return false" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">Enter Activation Code</label>
                            <input type="text" class="form-control" name="activation_code" id="activation_code">
                            <div class="valid-feedback" id="valid-message">The entered activation code is a valid.</div>
                            <div class="invalid-feedback" id="invalid-message">This is an invalid activation code.</div>
                        </div>
                        <div class="form-group" style="display: none" id="detected_type_wrapper">
                            <label for="">Detected Activation Type</label>
                            <div class="h5"><i class="fa fa-cube fa-fw" id="typeIcon"></i> <span id="detected_type"></span></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSubmit" onclick="validateCode()">
                        <span id="defaultSubmit">Submit</span>
                        <span id="validating" style="display: none">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Validating...
                        </span>
                        <span id="registering" style="display: none">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            Registering...
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    @include('admin.activation.modals.registration-form-2')

@endsection

@section('scripts')
    <script>
        let user_id = "{{ auth()->id() }}";
        let member_id = "{{ auth()->user()->membership->id }}";
        let order_id = "{{ $order->id }}";
        let activation_count = {
            product: "{{ $member_activation_codes->product  }}",
            package: "{{ $member_activation_codes->package  }}",
        }
        let type = "{{ $order->product->type }}"

        registerActivationCode = () => {
            $("#btnSubmit").prop("disabled", false)
            $("#defaultSubmit").show();
            $("#registering").hide();
            $("#validating").hide();

            $("#activation_code").val("");
            setTimeout(() => {
                $("#activation_code").focus()
            }, 1000);
            $("#activation_code").removeClass("is-valid is-invalid")
            $("#registerActivationCode").modal('show')
            $("#detected_type_wrapper").hide();
        }

        activateItem = (btn, key) => {
            $(btn).prop('disabled', true)
            $(btn).find('.loader').show();

            setTimeout(() => {
                $.ajax({
                    url: "{{ route('api-activation-code.activate-item-of-order') }}",
                    data: {
                        order_id,
                        user_id,
                    },
                    method: "POST",
                    complete: (xhr, textStatus) => {
                        let data = xhr.responseJSON;
                        if (textStatus == "error") {
                            $(btn).prop('disabled', false)
                            $(btn).find('.loader').hide()
                            alert(data.message)
                        } else {
                            $("#activateText").text("Activated")
                            $(btn).find('.loader').hide()   
                            
                            let newCount = parseInt(activation_count[type]) - 1;
                            activation_count[type] = newCount;

                            $(`#activation_code_count_${type}`).text(newCount);
                            $("#item-" + key).fadeOut();
                        }
                    }
                })
            }, 2000);
        }
      
        validateCode = () => {
            $("#btnSubmit").prop("disabled", true)
            $("#defaultSubmit").hide();
            $("#validating").hide();
            $("#registering").show();

            $("#detected_type_wrapper").hide();
            let element = document.getElementById("activation_code");
            let activation_code = element.value;

            element.classList.remove('is-invalid')
            element.classList.remove('is-valid')

            if (activation_code.trim() == "") {
                element.classList.add('is-invalid')
                $("#invalid-message").text("Product activation code is required.")

                setTimeout(() => {
                    $("#btnSubmit").prop("disabled", false)
                    $("#defaultSubmit").show();
                    $("#registering").hide();
                }, 2000);
            } else {
                $.ajax({
                    url: "{{ route('api-activation-code.validate-registration') }}",
                    data: {
                        user_id,
                        member_id,
                        activation_code
                    },
                    method: "POST",
                    complete: (xhr, textStatus) => {
                        setTimeout(() => {

                            let data = xhr.responseJSON;

                            if (textStatus == "error") {
                                if (xhr.status == 500) {
                                    alert('Something went wrong. Please try again later.')
                                }

                                if (xhr.status == 422) {
                                    let errors = data.errors.activation_code
                                    $("#invalid-message").text(errors.join('. '))
                                }

                                element.classList.add('is-invalid')
                                $("#btnSubmit").prop("disabled", false)
                                $("#defaultSubmit").show();
                                $("#registering").hide();
                            } else {
                                element.classList.add('is-valid')
                                let item = data;
                                $("#detected_type_wrapper").fadeIn();
                                $("#detected_type").text(item.data.type.toUpperCase());

                                if (item.data.type == "product") {
                                    document.getElementById("typeIcon").classList.remove('fa-cubes')
                                    document.getElementById("typeIcon").classList.add('fa-cube')
                                } else {
                                    document.getElementById("typeIcon").classList.remove('fa-cube')
                                    document.getElementById("typeIcon").classList.add('fa-cubes')
                                }

                                let activation_code_id = item.data.id;
                                
                                $("#btnSubmit").prop("disabled", true)
                                $("#defaultSubmit").hide();
                                // $("#validating").hide();
                                $("#registering").show();

                                // $.ajax({
                                //     url: "{{ route('api-activation-code.register-activation-code') }}",
                                //     data: {
                                //         user_id,
                                //         member_id,
                                //         activation_code_id
                                //     },
                                //     method: "POST",
                                //     complete: (xhr, textStatus) => {
                                //         console.log(xhr, textStatus)
                                //         let data = xhr.responseJSON;
                                //         if (textStatus == "error") {
                                //             if (xhr.status == 500) {
                                //                 alert('Failed to register activation code. Try again later')
                                //             }

                                //             if (xhr.status == 422) {
                                //                 alert(data.message)
                                //             }

                                //             $("#btnSubmit").prop("disabled", false)
                                //             $("#defaultSubmit").show();
                                //             $("#validating").hide();
                                //             $("#registering").hide();

                                //         } else {
                                            setTimeout(() => {
                                                window.location = "{{ route('my-orders.my-activation-codes.after-registercode', ['order_id' => $order->id]) }}"
                                            }, 2000)
                                        // }
                                    // }
                                // })
                                
                            }

                        }, 2000);
                    }
                })

                $("#valid-message").text("Product activation code is valid.")
            }
        }
    </script>

    <script>
                
        registerNewMemberWindow = (btn, orderId) => {
            $(btn).prop("disabled", true)
            document.getElementById("frmProductActivation").reset();
            $("#submit_activation_btn").off('click');
            $("#activation_order_id").val(orderId)


            $("#submit_activation_btn").prop("disabled", false);
            $("#submit_btn_loading").hide();
            $("#submit_btn_text").text("SUBMIT")

            $.get("{{ route('api-orders.get-order') }}", {
                id: orderId
            }, (order) => {
                $(btn).prop("disabled", false)
                let str_activations_left = "(" + order.activations_left + " registration" + (order.activations_left > 1 ? "s" : "") + " left)";
                $("#product-registration-selected-package").html(`Package: [${order.product_code}] - ${order.product_name} <span style='float: right'>${str_activations_left}</span>`);
                $("#zoomPackageRegistration2").modal('show')
                
                $("#submit_activation_btn").off('click')
                                        .on('click', () => {
                                            $("#frmProductActivation").submit()
                                        })
            }, "json")

        }


        viewHistory = (id) => {
            $.get("{{ route('api-activation.view-history') }}", {
                order_id: id
            }, (data) => {
                let histories = "";
                if (data.length == 0) {
                    $("#histories_wrapper").text("No histories available yet.");
                    $("#histories").hide();
                } else {
                    data.forEach(h => {
                        histories += `<li class='list-group-item'>${h.member_name} <span style='float:right'>${h.ago}</span></li>`;
                    })
                    $("#histories_wrapper").text("");
                    $("#histories").show();
                    $("#histories").html(histories);
                }

                $("#activationHistory").modal('show')
            })
        }
        let isSaving = false;
        let registerNewMember = () => {

            $("#submit_activation_btn").prop("disabled", true);
            $("#submit_btn_loading").show();
            $("#submit_btn_text").text("LOADING...")

            let canSubmit = false;
            let errCount = 0;
            let fields = {
                first_name: {
                    field: "#member_first_name", 
                    label: "First name",
                    value: document.getElementById("member_first_name").value,
                    error: "#first-name-invalid-message"
                },
                middle_name: {
                    field: "#member_middle_name", 
                    label: "Middle name",
                    value: document.getElementById("member_middle_name").value,
                    error: "#middle-name-invalid-message",
                },
                last_name: {
                    field: "#member_last_name", 
                    label: "Last name",
                    value: document.getElementById("member_last_name").value,
                    error: "#last-name-invalid-message",
                },
                email: {
                    field: "#member_email",
                    label: "Email address",
                    value: document.getElementById("member_email").value,
                    error: "#email-invalid-message"
                },
                mobile_number: {
                    field: "#mobile_number",
                    label: "Mobile number",
                    value: document.getElementById("mobile_number").value,
                    error: "#mobile-number-invalid-message"
                },
                username: {
                    field: "#member_username",
                    label: "Username",
                    value: document.getElementById("member_username").value,
                    error: "#username-invalid-message"
                },
                password: {
                    field: "#member_password",
                    label: "Password",
                    value: document.getElementById("member_password").value,
                    error: "#password-invalid-message"
                },
                repeat_password: {
                    field: "#repeat_password",
                    label: "Repeat password",
                    value: document.getElementById("repeat_password").value,
                    error: "#repeat-password-invalid-message"
                },
                sponsor_username: {
                    field: "#sponsor_username",
                    label: "Sponsor username",
                    value: document.getElementById("sponsor_username").value,
                    error: "#sponsor-invalid-message"
                },
            }

            let data = {
                order_id: document.getElementById("activation_order_id").value
            };
            Object.keys(fields).forEach(key => {
                let value = fields[key].value

                data[key] = value
            })

            // validate tnc
            let tnc = document.getElementById("tnc");
            $("#tnc-message").hide()
            if (!tnc.checked) {
                $("#tnc-message").show();
                errCount++;
            }

            // validate password
            $("#repeat_password").removeClass("is-invalid")
            if (data.password != data.repeat_password) {
                $("#repeat_password").addClass("is-invalid")
                $("#repeat-password-invalid-message").text("Repeat Password did not match.")
                errCount++;
            }

            if (errCount == 0 && isSaving == false) {
                isSaving = true;

                $.ajax({
                    url: "{{ route('api-activation.register-new-member') }}",
                    data: data,
                    method: "POST",
                    complete: (xhr, textStatus) => {
                        let response = xhr.responseJSON;

                        setTimeout(() => {
                            if (textStatus == "error") {
                                if (xhr.status == 500) {
                                    alert('Something went wrong. Try again later.')
                                } else if (xhr.status == 405) {
                                    alert(response.message)
                                } else {

                                    let errors = response.errors
                                    Object.keys(fields).forEach(key => {
                                        $(fields[key].field).removeClass('is-invalid')
                                    })

                                    Object.keys(errors).forEach(key => {
                                        let str_error = errors[key].join(". ")
                                        $(fields[key].field).addClass('is-invalid')
                                        $(fields[key].error).text(str_error)
                                    })
                                }
                            } else {
                                window.location = "{{ route('my-orders.activate.after-activation') }}";
                            }

                            isSaving = false;
                            
                            $("#submit_activation_btn").prop("disabled", false);
                            $("#submit_btn_loading").hide();
                            $("#submit_btn_text").text("SUBMIT")

                        }, 1000)

                    }
                })
                
            } else {
                $("#submit_activation_btn").prop("disabled", false);
                $("#submit_btn_loading").hide();
                $("#submit_btn_text").text("SUBMIT")
            }
            return false;
        }
    </script>
@endsection
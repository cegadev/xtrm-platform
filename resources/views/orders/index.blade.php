@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Orders</h1>
        <div>
            @role('Admin')
                <a href="{{ route('orders.create') }}" class="btn btn-primary shadow-sm"><i class="fa fa-plus fa-fw"></i> Create New Order</a>
                <a href="{{ route('activation-codes.index') }}" class="btn btn-primary shadow-sm"><i class="fa fa-key fa-fw"></i> Visit Activation Codes</a>
            @endrole
        </div>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    
                    
                    @can('orders.read')
                        <div class="mt-3">
                            <ul class="nav nav-tabs">
                                @foreach ($tabs as $tab)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $tab == $status ? "active" : "" }}" href="{{ route('orders.index', ['tab' => $tab]) }}">{{ Str::ucfirst($tab) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endcan
                    @if ($orders->count())
                        <div class="mt-2">
                            Showing {{ $orders->firstItem() }} to {{ $orders->lastItem() }} of {{ $orders->total() }} orders
                        </div>
                    @endif
                    <table class="table mt-3">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th width=300>Name</th>
                                <th>Item</th>
                                {{-- <th>Type</th> --}}
                                
                                    <th>Created At</th>

                                @php
                                    $status = $status == "pending" ? "created" : $status;
                                @endphp

                                @if ($status !== "created")
                                    <th>{{ Str::ucfirst($status) }} At</th>
                                @endif
                                <th class="text-right">Total Amount</th>
                                <th width=300></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($orders as $order)
                                <tr>
                                    <td class="align-middle">
                                        <a href="{{ route('orders.view', $order->id) }}">#{{ $order->id }}</a>
                                    </td>
                                    <td class="align-middle">
                                        {{ $order->user->first_name }} {{ $order->user->last_name }}
                                    </td>
                                    <td class="align-middle">
                                        {{ $order->quantity }} &times;
                                        {{ $order->product->name }}
                                    </td>
                                    {{-- <td class="align-middle">
                                        {{ Str::ucfirst($order->product->type) }}
                                    </td> --}}

                                    @if ($status !== "created")
                                        <td class="align-middle">{{ $order->created_at->format('Y-m-d') }}</td>
                                    @endif

                                    <td class="align-middle">
                                        @switch($status)
                                            @case("released")
                                                {{ \Carbon\Carbon::parse($order->released_at)->format('Y-m-d') }}
                                                @break
                                            @case("completed")
                                                {{ \Carbon\Carbon::parse($order->completed_at)->format('Y-m-d') }}
                                                @break
                                            @default
                                                {{ $order->created_at->format('Y-m-d') }}
                                                
                                        @endswitch
                                    </td>
                                    <td class="text-right align-middle">
                                        {{ number_format($order->amount, 2) }}
                                    </td>
                                    <td class="text-right">
                                        @if (auth()->user()->isMember())
                                            <a href="#" class="btn btn-light btn-icon-split">
                                                <span class="icon text-gray-600">
                                                    <i class="fas fa-cog"></i>
                                                </span>
                                                <span class="text">Manage</span>
                                            </a>
                                        @else
                                            @if ($order->status == "pending")
                                                {{-- <form action="{{ route('orders.approve-request', $order->id) }}" method="post" style="display: inline">
                                                    @csrf
                                                    @method("POST")
                                                    <button class="btn btn-success btn-icon-split">
                                                        <span class="icon text-success">
                                                            <i class="fas fa-check"></i>
                                                        </span>
                                                        <span class="text">Approve</span>
                                                    </button>
                                                </form> --}}
                                                <button class="btn btn-success btn-icon-split" onclick="confirmApprove({{ $order->id }})">
                                                    <span class="icon text-success">
                                                        <i class="fas fa-check"></i>
                                                    </span>
                                                    <span class="text">Approve</span>
                                                </button>
                                                
                                                @role('Admin')
                                                    <form action="{{ route('orders.cancel-request', $order->id) }}" method="post" style="display: inline">
                                                        @csrf
                                                        @method("POST")
                                                        <button class="btn btn-danger btn-icon-split">
                                                            <span class="icon text-danger">
                                                                <i class="fas fa-times"></i>
                                                            </span>
                                                            <span class="text">Cancel</span>
                                                        </button>
                                                    </form>
                                                @endrole
                                            @else
                                                <button class="btn btn-info btn-icon-split" onclick="window.location='{{ route('orders.view', [$order->id, 'back_url' => url()->full()])}}'">
                                                    <span class="icon text-white-50">
                                                        <i class="fas fa-info-circle"></i>
                                                    </span>
                                                    <span class="text">View Details</span>
                                                </button>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center">No orders to display.</td>
                                </tr>
                            @endforelse
                            
                        </tbody>
                    </table>

                    {{ $orders->onEachSide(5)->links() }}

                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="approveConfirmation">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Approve Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p class="mb-3">Are you sure you want to approve this order?</p>
              @role('Admin')
              <div>
                <div>Automatically generate activation codes?</div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="automaticallyGenerate" id="automaticallyGenerateYes" value="yes">
                    <label class="form-check-label" role="button" for="automaticallyGenerateYes">Yes</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="automaticallyGenerate" id="automaticallyGenerateNo" value="no" checked>
                    <label class="form-check-label" role="button" for="automaticallyGenerateNo">No</label>
                </div>
              </div>
              @endrole

              @role('Manager')
              <input type="hidden" name="automaticallyGenerate" value="no">
              @endrole
              
            </div>
            <div class="modal-footer">
                <form action="{{ route('orders.approve-order') }}" method="post" class="form-inline">
                    @csrf
                    @method('post')
                    <input type="hidden" name="order_id" id="order_id">
                    <input type="hidden" name="generate_codes" id="generate_codes">
                    <button class="btn btn-primary">Confirm</button>
                </form>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        confirmApprove = (orderId) => {
            $('#order_id').val(orderId)
            $('#automaticallyGenerateNo').prop('checked', true)
            $('#automaticallyGenerateNo').trigger('change')
            $('#approveConfirmation').modal('show')
            
        }

        $('input[name=automaticallyGenerate]').on('change', function (e) {
            $('#generate_codes').val(e.target.value)
        })
    </script>
@endsection
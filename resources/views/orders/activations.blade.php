@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="align-items-center justify-content-between mb-4">
        <div class="mb-3">
            <h1 class="h3 mb-0 text-gray-800">My Activations</h1>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/home">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('my-orders.transactions.index') }}">Transactions</a></li>
              <li class="breadcrumb-item active" aria-current="page">My Activations</li>
            </ol>
        </nav>
    </div>
    <div class="card border-left-success shadow h-100">
        <div class="card-body">
            <ul class="nav nav-tabs mb-3">
                <li class="nav-item">
                  <a class="nav-link {{ $type == 'product' ? 'active' : '' }}" href="?type=product">Products</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ $type == 'package' ? 'active' : '' }}" href="?type=package">Packages</a>
                </li>
            </ul>
            @if ($items->count())
                <div class="mb-3">
                    Showing {{ $items->firstItem() }} to {{ $items->lastItem() }} of {{ $items->total() }} items
                </div>
            @endif
            <div class="table-responsive mb-3">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Activated At</th>
                            <th>Transferred by</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($items as $item)
                            <tr>
                                <td>{{ $item->product_name }}</td>
                                <td>{{ $item->quantity }}</td>
                                <td>{{ \Carbon\Carbon::parse($item->activated_at)->format('F j, Y') }}</td>
                                <td>{{ $item->transferred_from_user }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">No items to display.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div>
                {{ $items->links() }}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection
    
@section('styles')
@endsection
@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-sm-between mb-4">
        <div>
            <small>ORDERS</small>
            <h1 class="h3 mb-0 text-gray-800">View Order</h1>
        </div>
        @if ($order->status == "pending")
        <div class="mt-3">
            <form action="{{ route('orders.approve-request', $order->id) }}" method="post">
                @csrf
                @method("POST")
                <button class="btn btn-success btn-icon-split">
                    <span class="icon text-success">
                        <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Approve Order</span>
                </button>
            </form>
        </div>
        @endif
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <ul class="nav nav-tabs mb-4">
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == "info" ? "active" : "" }}" href="{{ route('orders.view', [$order->id, 'tab' => 'info', 'back_url' => $back_url]) }}">Info</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link  {{ $tab == "inclusions" ? "active" : "" }}" href="{{ route('orders.view', [$order->id, 'tab' => 'inclusions', 'back_url' => $back_url]) }}">
                            @if ($order->product->type == "product")
                                Product Information
                            @else 
                                Inclusions
                            @endif
                        </a>
                        </li>
                        {{-- <li class="nav-item">
                          <a class="nav-link  {{ $tab == "product_codes" ? "active" : "" }}" href="{{ route('orders.view', [$order->id, 'tab' => 'product_codes', 'back_url' => $back_url]) }}">Product Activation Codes</a>
                        </li> --}}
                        @if ($order->product->type == "package")
                            <li class="nav-item">
                                <a class="nav-link  {{ $tab == "members" ? "active" : "" }}" href="{{ route('orders.view', [$order->id, 'tab' => 'members', 'back_url' => $back_url]) }}">Registered New Members</a>
                            </li>
                        @endif
                    </ul>
                    @switch($tab)
                        @case("info")
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width=200></th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Order ID</th>
                                        <td>#{{ $order->id }} </td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>{{ \Str::ucfirst($order->status) }} </td>
                                    </tr>
                                    <tr>
                                        <th>Requested by</th>
                                        <td>{{ auth()->id() == $order->requested_by ? "You" : $order->requestedBy->first_name . " " . $order->requestedBy->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Requested at</th>
                                        <td>{{ $order->created_at->format('Y-m-d h:i A') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Type</th>
                                        <td>{{ $order->product->type }}</td>
                                    </tr>
                                    <tr>
                                        <th>Quantity</th>
                                        <td>{{ $order->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Amount</th>
                                        <td> <span class="peso-sign"></span> {{ $order->amount }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            @break

                        @case("inclusions")
                            @if ($order->product->type == "product")
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img class="card-img-top" src="{{ $order->product->product_image ?? \App\Http\Helpers\Helpers::defaultProductImage() }}" alt="Card image cap">  
                                            </div>
                                            <div class="col-md-9">
                                                <div class="card-title font-weight-bold h4" >
                                                    {{ $order->product->name }} </div>
                                                        <div class="mb-3">
                                                            Price: <span class="peso-sign"></span> {{ $order->product->price }}
                                                        </div>
                                                        <div class="mb-3">
                                                            Quantity: {{ $order->quantity }}
                                                        </div>
                                                        <div class="mb-3">
                                                            Total Activations Left: {{ $order->activatedItems()->count() == $order->quantity ? "None" : ($order->quantity - $order->activatedItems()->count()) }}
                                                        </div>
                                                <p class="card-text mt-3">{{ $order->product->basic_description ?? "No description available." }}</p>   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 mb-3">
                                                <img class="card-img-top" src="{{ $order->product->product_image ?? \App\Http\Helpers\Helpers::defaultProductImage() }}" alt="Card image cap">  
                                            </div>
                                            <div class="col-md-9 mb-3">
                                                <div class="card-title font-weight-bold h4" >
                                                    {{ $order->product->name }} </div>
                                                        <div class="mb-3">
                                                            Price: <span class="peso-sign"></span> {{ $order->product->price }}
                                                        </div>
                                                        <div class="mb-3">
                                                            Quantity: {{ $order->quantity }}
                                                        </div>
                                                <p class="card-text mt-3">{{ $order->product->basic_description ?? "No description available." }}</p>   
                                            </div>
                                            <div class="col-md-12">
                                                <h3>Inclusions</h3>
                                                @php
                                                    $inclusions = explode(PHP_EOL, $order->package->settings->inclusions);
                                                @endphp
    
                                                <ul>
                                                    @foreach ($inclusions as $item)
                                                        <li>
                                                            {{ $item }}
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @break

                        {{-- @case("product_codes")
                                @if ($order->product->type == "product")
                                    @if ($order->product_codes->count())
                                    <div>The following are the available activation codes for the products included on this order.</div>
                                    <ul class="list-group mt-2">
                                        @forelse ($order->product_codes as $item)
                                            <li class="list-group-item">{{ $item->code }}
                                            <div style="float: right">
                                                {{ $item->is_activated ? "USED" : "AVAILABLE" }}
                                            </div>
                                            </li>
                                        @empty
                                            
                                        @endforelse
                                    </ul>
                                    @else 
                                        No product activation codes required for this product
                                    @endif
                                @else 
                                    No product activation codes available for Package.  
                                @endif
                            @break;
                        @default --}}
                            
                        @case("members")
                                @if ($order->product->type == "product")
                                    Members section is not available for products.
                                @else 
                                <div class="mb-2">
                                    @php
                                        $left = $order->items->count() - $order->activatedItems()->count();
                                    @endphp
                                    Registrations Left: <strong>{{ $left == 0 ? "None" : $left }} </strong>
                                </div>
                                    @if ($order->status == "completed" || $order->activatedItems()->count() > 0)
                                    <div class="row">
                                        @foreach ($order->members as $member)
                                            <div class="col-md-3">
                                                <div class="card" style="width: 18rem;">
                                                    <img class="card-img-top" src="{{  $member->user->avatar ? asset($member->user->avatar)  : "https://www.signivis.com/img/custom/avatars/member-avatar-01.png" }}" alt="Card image cap">
                                                    <div class="card-body">
                                                        <h5 class="card-title">
                                                            {{ $member->user->first_name }}
                                                            {{ $member->user->last_name }}
                                                        </h5>
                                                        <div class="card-text">
                                                            <div>{{ $member->user->email }}</div>
                                                            <div>#{{ sprintf('%08d', $member->user->membership->id) }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                        
                                            
                                    @else 
                                        Order not activated to any members yet.
                                    @endif
                                @endif
                            @break  
                    @endswitch
                                <div>
                                    <a href="{{ $back_url ?? route('orders.index') }}" class="btn btn-secondary mt-3"><i class="fa fa-arrow-left fa-fw"></i> Back to Orders</a>
                                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

    
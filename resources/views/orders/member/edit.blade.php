@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <small>MEMBERS</small>
            <h1 class="h3 mb-0 text-gray-800">Request New Order - {{ \Str::upper($type) }}</h1>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <form method="POST" action="{{ route('my-orders.update-order-request', $order->id) }}" id="frmEditOrder">
                        @csrf
                        @method("POST")
                        <input type="hidden" name="type" value="{{ $type }}">
                        
                        <div>
                            <small>STEP 1</small>
                            <h3>Choose {{ \Str::ucfirst($type) }}</h3>
                        </div>
                        <div class="form-check">
                            <div class="form-row">
                                @foreach ($products as $product)
                                    <div class="col-md-2 mb-3">
                                        <div class="card h-100 d-flex justify-content-center align-items-center">
                                            @if ($product->product_image)
                                                <div class="card-body text-center">
                                                    <div>
                                                        <label for="product-{{ $product->id }}" style="cursor: pointer">
                                                            <img src="{{ $product->product_image }}" alt="" class="img-fluid">
                                                            <h5 class="mt-2">{{ $product->name }}</h5>
                                                            <p>
                                                                <span class="peso-sign"></span> {{ number_format($product->price, 2) }}
                                                            </p>
                                                        <input {{ old('product') ?? $order->product_id == $product->id ? "checked" : "" }} type="radio" name="product" data-price="{{ $product->price }}" value="{{ $product->id }}" id="product-{{ $product->id }}">
                                                    </div>
                                                </div>
                                            @else 
                                                <div class="text-center">
                                                    <div class="p-4">
                                                        <label for="product-{{ $product->id }}" style="cursor: pointer">
                                                            <i class="fa fa-cube fa-7x"></i>
                                                        <h5 class="mt-2">{{ $product->name }}</h5>
                                                        <p>
                                                            <span class="peso-sign"></span> {{ number_format($product->price, 2) }}
                                                        </p>
                                                        <input {{ old('product') ?? $order->product_id == $product->id ? "checked" : "" }} type="radio" name="product"  data-price="{{ $product->price }}"  value="{{ $product->id }}" id="product-{{ $product->id }}">
                                                    </div>
                                                </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @error('product')
                                <div style="width: 100%; margin-top: .25rem; font-size: 80%; color: #e74a3b;">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mt-3">
                            <small>STEP 2</small>
                            <h3>Enter Quantity</h3>
                        </div>
                        <div class="form-row mb-3 justify-content-between">
                            <div class="col-md-5 mb-4">
                                <label for="" style="visibility: hidden" class="d-none d-sm-block d-md-block d-lg-block d-xl-block">QTY</label>
                                    <input min=1 max=100 value="{{ old('quantity') ?? $order->quantity }}" id="qty" type="number" name="quantity" class="form-control form-control-lg text-right @error('quantity') is-invalid @enderror">
                                @error('quantity')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="col-md-5 mb-4">
                                <label for="">TOTAL AMOUNT</label>
                                <div class="" style="font-size: 2em"><span class="peso-sign"></span> <span id="total_amount">{{ number_format(($order->quantity * $order->product->price), 2) }}</span></div>
                            </div>
                        </div>

                        <div class="mt-3">
                            <small>STEP 3</small>
                            <h3>Confirm Order</h3>
                            Are you sure you want to save changes?
                        </div>
                       
                        <div class="mt-3">
                            <a href="{{ route('my-orders.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary"> <i class="fa fa-save fa-fw"></i> 
                                Save Changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let computeOrder = () => {
            let qty = $("#qty").val();
            let product = $("input[name=product]:checked").attr('data-price');
            console.log(product)
            let amount = qty * product;
            $("#total_amount").text(numberWithCommas(amount.toFixed(2)))
        }

        $("input[name=product]").on('change keyup', () => {
            computeOrder()
        })

        $("#qty").on('change keyup', () => {
            computeOrder()
        })

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
@endsection
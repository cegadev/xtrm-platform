@extends('layouts.site')

@section('content')
<div id="banner_wrapper">
    <div id="banner_content">
        <div id="banner-title">
            <h1>Marketing</h1>
            <div>Enjoy our {{ $ways_of_earning->count() }} ways of earning</div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mt-5">
        <div class="mt-5 mb-5">
            @if ($ways_of_earning->count())
                <div class="accordiona" id="waysOfEarning_wrapper">
                    @foreach ($ways_of_earning as $key => $earning)
                        @php
                            $cnt = $earning->priority
                        @endphp
                        <div class="card mb-3">
                            <div class="card-header" id="#woe{{$cnt}}">
                                <div class="font-weight-bold text-secondary">{{ $earning->title }} #{{ $cnt }}</div>
                            </div>
                        
                            <div id="woe{{$cnt}}" class="collapses {{ $key == 0 ? "show" : "" }}" aria-labelledby="woe{{$cnt}}">
                                <div class="card-body">
                                    @if (empty($earning->description))
                                        <p>No description available.</p>
                                    @else
                                        {!! $earning->description !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div>No ways of earning available yet.</div>
            @endif
        </div>
    </div>
</div>
@endsection
@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
@if ($tab == 'breakdown')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endif
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Members</h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-xs-12 col-sm-12 align-self-center">
                    <div class="h3">
                        <i class="fa fa-user fa-fw"></i>
                        {{ $membership->member_name }}</div>
                    <div>
                        #{{ sprintf("%08d", $membership->id) }}
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 col-sm-12 align-self-center">
                    <span class="h3 mr-2">
                        <i class="fa fa-cubes fa-fw"></i> {{ $membership->package ? $membership->package->name : "N/A" }}
                    </span>
                
                    @role('Admin|Manager')
                        @if ($membership->package && $can_be_upgraded)
                            <span>
                                <button onclick="upgradeAccount()" class="btn btn-success"><i class="fa fa-arrow-up fa-fw"></i> Upgrade Account</button>
                            </span>
                        @endif
                    @endrole
                </div>
            </div>
        </div>
    </div>
    <div class="mb-3">
        @if ($back_url)
            <a href="{{ $back_url }}" class="btn btn-secondary"><i class="fa fa-arrow-left fa-fw"></i> Go Back</a>
        @endif
        <a href="{{ route('activation-codes.histories', ['username' => $membership->user->username]) }}" class="btn btn-primary"><i class="fa fa-list fa-fw"></i> View Activation Histories</a>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Member's Earnings</h6>
            
        </div>
        <div class="card-body">

            <div class="mb-3">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="{{ route('admin.members.member-earnings', [$membership->id, 'tab' => 'summary', 'back_url' => $back_url]) }}" class="nav-link {{ $tab == "summary" ? "active" : "" }}">Summary</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.members.member-earnings', [$membership->id, 'tab' => 'breakdown', 'back_url' => $back_url]) }}" class="nav-link {{ $tab == "breakdown" ? "active" : "" }}">Break Down</a>
                    </li>
                </ul>
            </div>

            @if ($tab == "summary")

            @if ($membership->parent)
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="p-2 align-self-center">
                                <i class="fa fa-user-circle fa-2x"></i>
                            </div>
                            <div class="flex-grow-1 pl-2">
                                <div class="small">UPLINE MEMBER</div>
                                <div class="h3">{{ $membership->parent->member_name }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="mb-5 p-2">
                <h3>Overall Earnings Summary</h3>
                <div class="row mb-3">
                    <div class="col-md-6 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <small>THIS WEEK</small>
                                <div class="h3 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->week, 2) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <small>THIS MONTH</small>
                                <div class="h3 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->month, 2) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-4 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <small>OVERALL</small>
                                <div class="h3 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->all, 2) }}
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <h3>Earnings by Products/Packages</h3>
                <div class="row">
                    <div class="col mb-3">
                        <div class="card">
                            <div class="card-body">
                                <small>DIRECT REFERRAL</small>
                                <div class="h3 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->direct, 2) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-3">
                        <div class="card">
                            <div class="card-body">
                                <small>INDIRECT REFERRAL BONUS</small>
                                <div class="h3 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->indirect, 2) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-3">
                        <div class="card">
                            <div class="card-body">
                                <small>UNILEVEL</small>
                                <div class="h3 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->unilevel, 2) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($membership->package && $membership->package->enabled_lic)
                        <div class="col-md-4 mb-3">
                            <div class="card">
                                <div class="card-body">
                                    <small>LEADERSHIP INDIRECT BONUS</small>
                                    <div class="h3 font-weight-bold">
                                        <span class="peso-sign"></span> {{ number_format($summary->lic, 2) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="row mb-3">
                    <div class="col-12 col-lg-4">
                        <div class="card mb-3">
                            <div class="card-body">
                                <small>TOTAL EARNINGS</small>
                                <div class="h3 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->total, 2) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3>Total Activation this week ({{ $weekStart }} - {{ $weekEnd }})</h3>
                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <small>PRODUCTS</small>
                                <div class="h3">{{ $products_activation_count }}</div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <h3>Available Commissions and Encashments</h3>
                <div class="row mb-3">
                    <div class="col-md-6 mb-3">
                        <div class="card h-100">
                            <div class="card-body">
                                <small>AVAILABLE COMMISSIONS</small>
                                <div class="h2 font-weight-bold">
                                    <span class="peso-sign"></span> {{ number_format($summary->available_commissions, 2) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="card h-100">
                            <div class="card-body">
                                <small>ENCASHMENTS</small>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="h3 font-weight-bold mb-0">
                                            <span class="peso-sign"></span> {{ number_format($summary->encashments->where("status", "pending")->sum("amount"), 2) }}
                                            
                                        </div>
                                        <small>(pending release)</small>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="h3 font-weight-bold mb-0">
                                            <span class="peso-sign"></span> {{ number_format($summary->encashments->where("status", "complete")->sum("amount"), 2) }}
                                            
                                        </div>
                                        <small>(released)</small>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            @else
            <div class="card mb-3">
                <div class="card-body">
                    <form action="">
                        <input type="hidden" name="tab" value="{{ $tab }}">
                        <input type="hidden" name="back_url" value="{{ $back_url }}">
                        <div class="form-row">
                            <div class="col-12 col-lg-3">
                                <div class="form-group">
                                    <label for="type">Type</label>
        
                                    <select id="type" class="form-control" name="type">

                                        @php
                                            $types = [
                                                'all' => 'All types',
                                                'direct referral' => 'Direct referral',
                                                'indirect referral' => 'Indirect referral',
                                                'unilevel' => 'Unilevel',
                                                'lic' => 'Leadership indirect commission',
                                            ]
                                        @endphp

                                        @foreach ($types as $key => $item)
                                            <option value="{{ $key }}" {{ $key == $type ? 'selected' : '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="custom-control custom-checkbox mb-3">
                                <input type="checkbox" class="custom-control-input" {{ $with_range ? 'checked' : '' }} name="with_range" id="daterangeToggle">
                                <label class="custom-control-label user-select-none" for="daterangeToggle" role="button">Choose date range</label>
                            </div>
                            <div class="form-group" {!! $with_range ? '' : 'style="display: none"' !!} id="chooserange">
                                <label for="dates">Select date range</label>
                                <input class="form-control" type="text" name="daterange" id="dates" value="{{ $daterange }}">
                            </div>
                        </div>
                        
                        <a href="{{ route('admin.members.member-earnings', [$membership, 'tab' => $tab, 'back_url' => $back_url]) }}" class="btn btn-secondary">Reset</a>
                        <button class="btn btn-primary">Apply </button>
                    </form>
                </div>
            </div>

            <div>
                <div class="mb-3">
                    <div class="row">
                        <div class="col-12 col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    <div>TOTAL DIRECT REFERRAL 
                                        
                                        @if ($with_range)
                                        (as of {{ $daterange }})
                                        @else 
                                        (ALL TIME)
                                        @endif
                                    </div>
                                    <div class="d-flex">
                                        <div class="align-self-center p-2">
                                            <i class="fa fa-users fa-2x"></i>
                                        </div>
                                        <div class="flex-grow-1 align-self-center">
                                            <div class="h2 font-weight-bold">{{ $total_direct ?? 0 }} member{{ $total_direct > 1 ? 's' : ''}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    Amount
                                </th>
                                <th>Type</th>
                                <th>Date</th>
                                <th>Added to Wallet at</th>
                                <th>Earned from Member</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($earnings as $earning)
                                <tr>
                                    <td><span class="peso-sign"></span> {{  number_format($earning->amount, 2) }}</td>
                                    <td>{{ $earning->type }}</td>
                                    <td>{{ $earning->created_at->format("Y-m-d") }}</td>
                                    <td>
                                        @if (in_array($earning->type, ['lic', 'unilevel', 'indirect referral']))
                                            {{ $earning->added_to_wallet_at->firstOfMonth()->format("Y-m-d") }}
                                        @else
                                            {{ $earning->added_to_wallet_at->format("Y-m-d") }}
                                        @endif
                                    </td>
                                    <td>{{ $earning->earnedFromMember->member_name }}</td>
                                </tr>    
                            @empty
                                <tr>
                                    <td colspan="4">No earnings to display.</td>
                                </tr>
                            @endforelse
                            
                        </tbody>
                    </table>
                </div>
                {{ $earnings->onEachSide(5)->links() }}

            </div>  
            @endif
            
           
        </div>
    </div>

    @role('Admin|Manager')
        @if ($current_package)
            <div class="modal fade" id="upgrade_account" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Upgrade Account</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3 font-weight-bold">
                                Current Package: {{ $current_package }}
                            </div>
                            <div>
                                <form action="{{ route('admin.members.member-upgrades', $membership->id) }}" method="POST" id="upgradeFrm">
                                    @csrf
                                    @method("POST")
                                    <label for="upgradeTo">Upgrade to:</label>
                                    <select name="package_id" id="upgradeTo" class="form-control">
                                        @foreach ($package_upgrades as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" onclick="continueUpgrade()">Upgrade</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endrole

@endsection

@section('scripts')
@if ($tab == 'breakdown')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(function () {
        $('#dates').daterangepicker()
    })

    $('#daterangeToggle').on('change', function() {
        let isChecked = $(this).prop('checked');

        if (isChecked) {
            $('#chooserange').show();
        } else {
            $('#chooserange').hide();
        }
    })
</script>
@endif
<script type="text/javascript">
    upgradeAccount = () => {
        $("#upgrade_account").modal('show')
    }

    continueUpgrade = () => {
        $("#upgradeFrm").submit()
    }
</script>
@endsection
@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Members</h1>
        @role('Admin')

        <div>
            <a href="{{ route('admin.genealogy.index') }}" class="btn btn-primary">
                <i class="fa fa-sitemap fa-fw"></i>
                View Genealogy</a>
            <a href="{{ route('admin.members.member-create', ['back_url' => url()->full() ]) }}" class="btn btn-primary"><i class="fa fa-user-plus fa-fw"></i> Create New Member</a>
        </div>
        @endrole
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Members List</h6>
        </div>
        <div class="card-body">
            @role('Admin')
                <ul class="nav nav-tabs mb-3">
                    <li class="nav-item">
                        <a class="nav-link {{ $tab == "all" ? "active" : "" }}" href="{{ route('admin.members.index', ['tab' => 'all']) }}">All Members <span class="badge badge-pill badge-danger">{{ $tab_counts->all }}</span> </a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link  {{ $tab == "regular" ? "active" : "" }}" href="{{ route('admin.members.index', ['tab' => 'regular']) }}">Regular Members <span class="badge badge-pill badge-danger">{{ $tab_counts->regular }}</span> </a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link  {{ $tab == "company" ? "active" : "" }}" href="{{ route('admin.members.index', ['tab' => 'company']) }}">Company Members/Accounts <span class="badge badge-pill badge-danger">{{ $tab_counts->company }}</span> </a>
                    </li>
                </ul>
            @endrole
            
            <form action="" class="">
                @if (auth()->user()->isManager())
                    @if (auth()->user()->managedBranch)
                    <div class="form-group">
                        <input type="checkbox" name="only_branch" value="true" id="chk_only_branch" {{ $only_branch ? "checked" : "" }}>
                        <label for="chk_only_branch">Only {{ auth()->user()->managedBranch->name }} Branch</label>
                    </div>
                    @endif
                @endif
                <input type="hidden" name="tab" value="{{ $tab }}">
                
                <div class="form-group">
                    <label for="">Search Member</label>

                    <div class="input-group mb-3">
                        <input type="text" value="{{ $keyword }}" name="keyword" class="form-control" placeholder="Enter keyword here (first name, last name, or username)" aria-label="Enter keyword here (first name, last name, or username)" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                        <button class="btn btn-outline-primary" type="submit"><i class="fa fa-search fa-fw"></i> Search</button>
                        </div>
                    </div>
                </div>

            </form>
            @if ($keyword)
                <a href="{{ route('admin.members.index', ['tab' => $tab]) }}"><i class="fa fa-times fa-fw"></i> Clear Search</a>
            @endif
            <div class="mt-2">
                Showing {{ $members->firstItem() }} to {{ $members->lastItem() }} of {{ $members->total() }} members
            </div>
            
                <div class="table-responsive mt-3">
                    <table class="table" id="members" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Sponsor Name</th>
                                @role('Admin')
                                <th>Total Earnings</th>
                                @endrole
                                <th>Registered At</th>
                                <th>Membership No.</th>
                                @role('Admin')
                                    <th width=400></th>
                                @endrole
                                @role('Manager')
                                    <th>Package</th>
                                @endrole
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($members as $member)
                                <tr>
                                    <td class="align-middle">
                                        
                                        @if ($member->membership->is_free_account)
                                            <span class="badge badge-pill badge-warning">Free Account</span>
                                        @endif

                                        <div>{{ $member->username }}</div>
                                    </td>
                                    <td class="align-middle">{{ $member->formal_name }}</td>
                                    <td class="align-middle">{{ $member->membership->parent ? $member->membership->parent->member_name : "N/A" }}</td>
                                    @role('Admin')
                                    <td class="align-middle">{{ number_format($member->membership->total_earnings, 2) }}</td>
                                    @endrole
                                    <td class="align-middle">{{ $member->created_at->format("Y-m-d ~ h:i a") }}</td>
                                    <td class="align-middle">#{{ sprintf("%08d", $member->membership->id) }}</td>
                                    @role('Admin')
                                        <td class="text-right">
                                            <a href="{{ route('admin.members.member-earnings', [$member->membership->id, 'back_url' => url()->full()]) }}" class="btn btn-outline-primary"><i class="fa fa-wallet  fa-fw"></i> Earning </a>
                                            <a href="{{ route('admin.members.member-edit', [$member->id, 'back_url' => url()->full()]) }}" class="btn btn-outline-primary"><i class="fa fa-edit fa-fw"></i> Edit </a>
                                            <a href="{{ route('admin.members.change-member-password', [$member->id, 'back_url' => url()->full()]) }}" class="btn btn-outline-info"><i class="fa fa-edit fa-fw"></i> Change Password</a>
                                        </td>
                                    @endrole

                                    @role('Manager')
                                        <td>
                                            {{ $member->membership->package->name ?? 'Not applicable' }}
                                        </td>
                                    @endrole
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="5">No members to display.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

            {{ $members->onEachSide(5)->links() }}
        </div>
      </div>

@endsection

@section('scripts')
<script src="{{ asset('sb-admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

@endsection
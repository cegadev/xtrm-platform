@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Members</h1>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit Member</h6>
        </div>
        <div class="card-body">
        <form class="needs-validation" method="POST" action="{{ route('admin.members.update-member', [$user->id]) }}" novalidate>
                @csrf
                @method('POST')
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">First name</label>
                        <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') ?? $user->first_name }}" placeholder="Enter first name" required>
                        @error('first_name')
                        <div class="invalid-feedback"> {{ $message }} </div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom02">Middle name</label>
                        <input type="text" class="form-control @error('middle_name') is-invalid @enderror" name="middle_name" value="{{ old('middle_name') ?? $user->middle_name }}"  placeholder="Enter middle name">
                        @error('middle_name')
                        <div class="invalid-feedback"> {{ $message }} </div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom02">Last Name</label>
                        <input type="text" class="form-control @error('last_name') is-invalid @enderror " name="last_name" value="{{ old('last_name') ?? $user->last_name }}"  placeholder="Enter last name" required>
                        @error('last_name')
                        <div class="invalid-feedback"> {{ $message }} </div>
                        @enderror
                    </div>
                    
                    <div class="col-md-4 mb-3">
                        <label for="validationCustomUsername">Username</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroupPrepend">@</span>
                            </div>
                            <input type="text" class="form-control @error('username') is-invalid @enderror" " name="username" value="{{ old('username') ?? $user->username }}" placeholder="Enter username" aria-describedby="inputGroupPrepend" required>
                            @error('username')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustomUsername">Email address</label>
                        <div class="input-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? $user->email }}" placeholder="Enter email address (Optional)" aria-describedby="inputGroupPrepend" required>
                            @error('email')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustomUsername">Mobile Number</label>
                        <div class="input-group">
                            <input type="text" class="form-control @error('mobile_number') is-invalid @enderror" " name="mobile_number" value="{{ old('mobile_number') ?? $user->mobile_number }}" placeholder="e.g. 09XX XXX XXXX" aria-describedby="inputGroupPrepend" required>
                            @error('mobile_number')
                            <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom02">Member Name</label>
                        <input type="text" class="form-control @error('member_name') is-invalid @enderror" name="member_name" value="{{ old('member_name') ?? $user->membership->member_name }}"  placeholder="Enter member name" required>
                        @error('member_name')
                        <div class="invalid-feedback"> {{ $message }} </div>
                        @enderror
                    </div>
                </div>
                <div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="is_company_account" {{ $user->membership->is_company_account == true ? 'checked' : '' }} id="isCompanyAccount">
                        <label class="form-check-label" for="isCompanyAccount">
                          Is Company Account?
                        </label>
                    </div>
                    <div class="form-check form-check-inline d-none">
                        <input class="form-check-input" type="checkbox" name="is_free" {{ $user->membership->is_free_account == true ? 'checked' : '' }} id="isFreeAccount">
                        <label class="form-check-label" for="isFreeAccount">
                          Is Free Account?
                        </label>
                    </div>
                </div>
                <div class="mt-3">
                    <a href="{{ $back_url ?? route('admin.members.index') }}" class="btn btn-secondary">Cancel</a>
                    <button class="btn btn-primary">Save Changes</button>
                </div>
            </form>
        </div>
      </div>

@endsection

@section('scripts')

<script type="text/javascript">
    $("#isRootLevel").on('change', (data) => {
        if (data.target.checked == false) {
            $("#isNotRoot").removeClass('d-none')
        } else {
            $("#isNotRoot").addClass('d-none')
        }
    })
</script>
@endsection
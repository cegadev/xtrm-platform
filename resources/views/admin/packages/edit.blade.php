@extends('layouts.admin')

@section('styles')
    
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Package</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <form method="POST" action="{{ route('packages.update-package', $package->id) }}" novalidate>
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="col-md-8 mb-3">
                                <label for="package_name" class="required">Name</label>
                                <input tabindex="1" id="package_name" type="text" class="form-control @error('package_name') is-invalid @enderror" name="package_name" value="{{ old('package_name') ?? $package->name }}" placeholder="Enter package name" required>
                                @error('package_name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                <label for="code" class="required">Package Code</label>
                                <input tabindex="2" id="code" type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ old('code') ?? $package->code }}" placeholder="Enter package code (e.g. PKG-1 for Package 1)" required>
                                @error('code')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3">
                                <label for="price" class="required">Price</label>
                                <input tabindex="4" type="number" class="form-control text-right @error('price') is-invalid @enderror " name="price" value="{{ old('price') ?? $package->price }}" required min="1">
                                @error('price')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="direct_referral_commission" class="required">Direct Referral Commission (Amount in Peso)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">
                                          <span class="peso-sign"></span>
                                      </span>
                                    </div>
                                    <input id="direct_referral_commission" tabindex="4" type="number" class="form-control text-right @error('direct_referral_commission') is-invalid @enderror " name="direct_referral_commission" value="{{ old('direct_referral_commission') ?? $package->settings->direct_referral_commission }}" required min="1">
                                    @error('direct_referral_commission')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="indirect_referral_commission" class="required">Indirect Referral Commission (Percentage of Direct referral)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">%</span>
                                    </div>
                                    <input tabindex="4" type="number" class="form-control text-right @error('indirect_referral_commission') is-invalid @enderror " name="indirect_referral_commission" value="{{ old('indirect_referral_commission') ?? $package->settings->indirect_referral_percentage }}" required min="1">
                                    @error('indirect_referral_commission')
                                    <div class="invalid-feedback"> {{ $message }}   </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="limit_per_day" class="required">Indirect Referral Limit per day</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">
                                          <span class="peso-sign"></span>
                                      </span>
                                    </div>
                                    <input tabindex="4" type="number" class="form-control text-right @error('limit_per_day') is-invalid @enderror " name="limit_per_day" value="{{ old('limit_per_day') ?? $package->settings->irb_limit_per_day }}" required min="1">
                                    @error('limit_per_day')
                                    <div class="invalid-feedback"> {{ $message }}   </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div x-data="{enabled_lic: {{ $package->enabled_lic }} }">
                            <div class="form-check mb-3">
                                <input id="enable-lic" class="form-check-input" type="checkbox" name="enabled_lic" value="1" x-model="enabled_lic">
                                <label for="enable-lic" role="button" class="form-check-label user-select-none">Enable Leadership Indirect Commission</label>
                            </div>
                            <div x-cloak x-show="enabled_lic">
                                <div class="form-row">
                                    <div class="col-12 col-lg-3">
                                        <label for="lic">Leadership Indirect Commission (Percentage)</label>
                                        @foreach ($lic_items as $item)
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">LEVEL {{ $item->level }} </span>
                                                </div>
                                                <input tabindex="4" type="number" class="form-control text-right " name="leadership_indirect_commission[]" value="{{ $item->percent }}" min="1">
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="col-12 col-lg-4">
                                        <label for="lic_daily_limit">Leadership Indirect Commission Limit per day</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text">
                                                  <span class="peso-sign"></span>
                                              </span>
                                            </div>
                                            <input tabindex="4" type="number" class="form-control text-right @error('lic_limit_per_day') is-invalid @enderror " name="lic_limit_per_day" value="{{ old('lic_limit_per_day') ?? $package->settings->lic_limit_per_day }}" required min="1">
                                            @error('lic_limit_per_day')
                                            <div class="invalid-feedback"> {{ $message }}   </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-8 mb-3">
                                <label for="inclusions">Inclusions (List separated by 'Enter')</label>
                                <textarea class="form-control" name="inclusions" id="" cols="30" rows="10" placeholder="Enter inclusions here ...">{{ old('inclusions') ?? $package->settings->inclusions }}</textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="">Description</label>
                                <textarea tabindex="5" id="summernote" name="description">{{ old('description') }}</textarea>
                            </div>
                        </div>
                        <div>
                            <a href="{{ route('packages.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter package description here ...',
                tabsize: 2,
                height: 200
            });
        });
    </script>
@endsection
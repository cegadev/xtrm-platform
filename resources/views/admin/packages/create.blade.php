@extends('layouts.admin')

@section('styles')
    
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Create New Package</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <form method="POST" action="{{ route('packages.save-package') }}" novalidate>
                        @csrf
                        @method('POST')
                        <div class="form-row">
                            <div class="col-md-8 mb-3">
                                <label for="package_name">Name</label>
                                <input tabindex="1" id="package_name" type="text" class="form-control @error('package_name') is-invalid @enderror" name="package_name" value="{{ old('package_name') }}" placeholder="Enter package name" required>
                                @error('package_name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                <label for="code">Package Code</label>
                                <input tabindex="2" id="code" type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ old('code') }}" placeholder="Enter package code (e.g. PKG-1 for Package 1)" required>
                                @error('code')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3">
                                <label for="price">Price</label>
                                <input tabindex="4" type="number" class="form-control text-right @error('price') is-invalid @enderror " name="price" value="{{ !old() ? "0" : old('price') }}" required min="1">
                                @error('price')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="direct_referral_commission">Direct Referral Commission (Amount in Peso)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">
                                          <span class="peso-sign"></span>
                                      </span>
                                    </div>
                                    <input id="direct_referral_commission" tabindex="4" type="number" class="form-control text-right @error('direct_referral_commission') is-invalid @enderror " name="direct_referral_commission" value="{{ !old() ? "0" : old('direct_referral_commission') }}" required min="1">
                                    @error('direct_referral_commission')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="indirect_referral_commission">Indirect Referral Commission (Percentage of Direct referral)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">%</span>
                                    </div>
                                    <input tabindex="4" type="number" class="form-control text-right @error('indirect_referral_commission') is-invalid @enderror " name="indirect_referral_commission" value="{{ !old() ? "10" : old('indirect_referral_commission') }}" required min="1">
                                    @error('indirect_referral_commission')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="indirect_referral_commission">Limit per day</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">
                                          <span class="peso-sign"></span>
                                      </span>
                                    </div>
                                    <input tabindex="4" type="number" class="form-control text-right @error('limit_per_day') is-invalid @enderror " name="limit_per_day" value="{{ !old() ? "1" : old('limit_per_day') }}" required min="1">
                                    @error('limit_per_day')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-8 mb-3">
                                <label for="inclusions">Inclusions (List separated by 'Enter')</label>
                                <textarea class="form-control" name="inclusions" id="" cols="30" rows="10" placeholder="Enter inclusions here ...">{{ old('inclusions') }}</textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="">Description</label>
                                <textarea tabindex="5" id="summernote" name="description">{{ old('description') }}</textarea>
                            </div>
                        </div>
                        <div>
                            <a href="{{ route('packages.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter package description here ...',
                tabsize: 2,
                height: 200
            });
        });
    </script>
@endsection
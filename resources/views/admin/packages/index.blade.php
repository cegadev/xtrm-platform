@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Packages</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    
                    @can('packages.write')
                        <a href="{{ route('packages.create') }}" class="btn btn-primary shadow-sm"><i class="fa fa-plus"></i> Create New Package</a>
                        <button id="btnEdit" class="btn btn-primary shadow-sm" disabled><i class="fa fa-edit"></i> Edit</button>
                    @endcan
                    <ul class="nav nav-tabs mt-3">
                        <li class="nav-item">
                          <a class="nav-link {{ $tab == 'active' ? 'active' : '' }}" href="?tab=active">Active Packages</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link {{ $tab == 'disabled' ? 'active' : '' }}" href="?tab=disabled">Disabled Packages</a>
                        </li>
                    </ul>
                    <div class="table-responsive">
                        <table class="table mt-3">
                            <thead>
                                <tr>
                                    <th width=50>
                                        <div>
                                            <div class="form-check">
                                                <input type="checkbox" name="" id="productsCheckAll">
                                            </div>
                                        </div>
                                    </th>
                                    <th width=300>Name</th>
                                    <th>Code</th>
                                    <th class="text-right">Price</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="productsList">
                                @forelse ($packages as $product)
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                                <input type="checkbox" name="product-item" id="" value="{{ $product->id }}" class="productCheckBox">
                                            </div>
                                        </td>
                                        <td class="packageName-{{ $product->id }}" >{{ $product->name }}</td>
                                        <td>{{ $product->code }}</td>
                                        <td class="text-right"><span class="peso-sign"></span> {{ number_format($product->price) }}</td>
                                        <td class="text-right">
                                            @if ($tab == 'active')
                                                <button class="btn btn-danger" onclick="disableItem({{ $product->id }})"><i class="fa fa-ban fa-fw"></i> Disable </button>
                                            @else 
                                                <button class="btn btn-info" onclick="enableItem({{ $product->id }})"><i class="fa fa-check fa-fw"></i> Enable</button>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No items to display.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>
    
    <div class="modal fade" id="enableModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Enable Package Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to enable <span id="enableName" class="font-weight-bold"></span>?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ route('packages.enable-package') }}" method="post" class="form-inline">
                    @csrf
                    @method('post')
                    <input type="hidden" name="enable_id" id="enable_id">
                    <button type="submit" class="btn btn-primary">Yes</button>
                </form>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
    </div>
    
    <div class="modal fade" id="disableModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Disable Package Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to disable <span id="disableName" class="font-weight-bold"></span>?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ route('packages.disable-package') }}" method="post" class="form-inline">
                    @csrf
                    @method('post')
                    <input type="hidden" name="disable_id" id="disable_id">
                    <button type="submit" class="btn btn-primary">Yes</button>
                </form>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#productsCheckAll").on('change', (data) => {
            $("#productsList tr td input[type=checkbox]").prop('checked', data.target.checked)
            triggerChecked()
        })

        $("#productsList tr td input[type=checkbox]").on('change', (data) => {
            triggerChecked()
        })

        triggerChecked = () => {
            let checkedCount = $(".productCheckBox:checked").length;
            if (checkedCount == 0) {
                $("#btnEdit").prop('disabled', true)
                $("#btnDelete").prop('disabled', true)
            } else {
                if (checkedCount == 1) {
                    $("#btnEdit").prop('disabled', false)
                    $("#btnDelete").prop('disabled', false)
                } else {
                    $("#btnEdit").prop('disabled', true)
                    $("#btnDelete").prop('disabled', false)
                }
            }
        }

        $("#btnEdit").on('click', (data) => {
            if (data.target.disabled == false) {
                let selectedProduct = $(".productCheckBox:checked")[0];
                if (selectedProduct.value) {
                    window.open("/admin/packages/edit/" + selectedProduct.value, "_self");
                }
            }
        })
        
        enableItem = (id) => {
            let name = $('.packageName-' + id).text();
            
            $("#enable_id").val(id)
            $("#enableName").text(name)
            $("#enableModal").modal('show')
        }
        
        disableItem = (id) => {
            let name = $('.packageName-' + id).text();
            
            $("#disable_id").val(id)
            $("#disableName").text(name)
            $("#disableModal").modal('show')
        }
    </script>
@endsection
<div class="row">

@hasanyrole('Admin|Member')
    <!-- Earnings (Monthly) Card Example -->
<div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
            @role('Member')
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Earnings </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                <span class="peso-sign"></span>
                {{ number_format($total_earnings, 2) }}
                </div>
            @endrole
            @role('Admin')
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Company Earnings </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                <span class="peso-sign"></span>
                {{ number_format($total_earnings, 2) }}
                </div>
            @endrole
            @role('Manager')
                @if ($logged_user->managedBranch)
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Branch Earnings </div>
                @else 
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Company Earnings </div>
                @endif
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                <span class="peso-sign"></span>
                {{ number_format($branch_earnings, 2) }}
                </div>
            @endrole
            </div>
            <div class="col-auto">
            @role('Member')
            <i class="fas fa-calendar fa-2x text-gray-300"></i>
            @else
            <i class="fas peso-sign fa-2x text-gray-300"></i>
            @endrole
            </div>
        </div>
        </div>
    </div>
</div>
@endhasanyrole

    <!-- Earnings (Monthly) Card Example -->
<div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
            @role('Member')
            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Available Commissions</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">
                <span class="peso-sign"></span>
                {{ number_format($available, 2) }}
            </div>
            @else 
            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">New Members this Month</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">
                {{ number_format($new_members_month) }}
            </div>
            @endrole
            </div>
            <div class="col-auto">
            @role('Member')
                <span class="fas fa-wallet text-gray-300 fa-2x" style="font-weight: 900"></span>
            @else 
                <span class="fas fa-users text-gray-300 fa-2x" style="font-weight: 900"></span>
            @endrole
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
            @role('Member')
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Encashment </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                <span class="peso-sign"></span>
                {{ number_format($encashment, 2) }}</div>
            @else
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Members </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ number_format($total_members) }}</div>
            @endrole
            
            </div>
            <div class="col-auto">
            @role('Member')
            <i class="fas peso-sign fa-2x text-gray-300"></i>
            @else 
            <i class="fas fa-sitemap fa-2x text-gray-300"></i>
            @endrole
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<div class="row align-items-center">
    <!-- Earnings (Monthly) Card Example -->
    <div class="col-md-5 mb-3">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-md font-weight-bold text-primary text-uppercase mb-1">Total Earnings </div>
                    <div class="h2 mb-0 font-weight-bold text-gray-800">
                    <span class="peso-sign"></span>
                    {{ number_format($total_earnings, 2) }}
                    </div>
                </div>
                <div class="col-auto">
                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="col-md-7 align-items-end mb-3">
        @if ($last_month_bonus)
            <div class="card mb-3">
                <div class="card-body">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Indirect Referral and Unilevel Bonus earned last month</div>

                    <div class="row no-gutters">
                        <div class="col-md-8 mb-3 align-self-center">
                            <div class="h3 mb-0 font-weight-bold text-gray-800">
                                <span class="peso-sign"></span>
                                {{ number_format($last_month_bonus, 2) }}
                            </div>
                        </div>
                        <div class="col-md-4 mb-3 align-self-center text-center">
                            <button class="btn btn-lg btn-warning" onclick="claimBonus(this)">CLAIM NOW</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row align-items-end">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-3">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Available Commissions</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <span class="peso-sign"></span>
                            {{ number_format($available, 2) }}
                        </div>
                        </div>
                        <div class="col-auto">
                            <span class="fas fa-wallet text-gray-300 fa-2x" style="font-weight: 900"></span>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-3">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Encashment </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <span class="peso-sign"></span>
                            {{ number_format($encashment, 2) }}</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas peso-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-3">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Downline Count </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                {{ $associates->count() }}
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-sitemap fa-2x text-gray-300"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Encashment Requests</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="mt-3">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="?tab=pending" class="nav-link {{ $tab == "pending" ? "active" : "" }}">Pending
                                    @if ($pending_count > 0)
                                        <span class="badge badge-pill badge-danger">{{ $pending_count > 9 ? "9+" : $pending_count }}</span>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="?tab=complete" class="nav-link {{ $tab == "complete" ? "active" : "" }}">Approved</a>
                            </li>
                            <li class="nav-item">
                                <a href="?tab=declined" class="nav-link {{ $tab == "declined" ? "active" : "" }}">Declined</a>
                            </li>
                        </ul>
                    </div>
                    
                    @if ($tab == "pending")
                        <form action="{{ route('admin.encashment-requests.action-requests') }}" method="POST">
                            @csrf

                        <div class="mt-3">
                            <button type="submit" value="approve" name="action" class="btn btn-success action_btns" disabled><i class="fa fa-check"></i> Approve</button>
                            <button type="submit" value="decline" name="action" class="btn btn-danger action_btns" disabled><i class="fa fa-ban"></i> Decline</button>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table mt-3">
                            <thead>
                                <tr>
                                    <th width=50>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input encashment_chkbox" id="chkAll">
                                            <label class="form-check-label" for="chkAll"></label>
                                        </div>
                                    </th>
                                    <th width=200>Encashment Number</th>
                                    <th width=200>Requested by</th>
                                    <th>Amount</th>
                                    <th>Processing Fee Applied</th>
    
                                    @php
                                        $status = $tab == "pending" ? "requested" : ($tab == "complete" ? "completed" : $tab) 
                                    @endphp
                                    <th>{{ Str::ucfirst($status) }} At</th>
                                    <th class="text-right">Total Amount (with 10% VAT)</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="encashmentList">
                                @forelse ($encashments as $encashment)
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            <input type="checkbox" value="{{ $encashment->id }}" name="encashment_chk[]" class="form-check-input encashment_chkbox" id="chk{{ $encashment->id }}">
                                            <label class="form-check-label" for="chk{{ $encashment->id }}"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.encashment-requests.view-encashment', [$encashment->id, 'back_url' => url()->full()]) }}">ENC-{{ sprintf("%08d", $encashment->id) }}</a>
                                    </td>
                                    <td>
                                        {{ $encashment->requestedBy->first_name }}
                                        {{ $encashment->requestedBy->last_name }}
                                    </td>
                                    <td>
                                        {{ number_format($encashment->amount, 2) }}
                                    </td>
                                    <td>
                                        {{ number_format($encashment->service_fee, 2) }}
                                    </td>
                                    <td>
                                        @php
                                            switch ($encashment->status) {
                                                case 'declined':
                                                    $date_shown = $encashment->declined_at;
                                                    break;
                                                
                                                case 'complete':
                                                    $date_shown = $encashment->approved_at;
                                                    break;
                                                
                                                default:
                                                    $date_shown = $encashment->requested_at;
                                                    break;
                                            }
    
                                        @endphp
                                        {{ \Carbon\Carbon::parse($date_shown)->format('Y-m-d') }}
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($encashment->amount_released, 2) }}
                                    </td>
                                </tr>
                                @empty
                                    <tr>
                                        <td colspan="7" class="text-center">No encashment requests to display.</td>
                                    </tr>
                                @endforelse
                                
                            </tbody>
                        </table>
                    </div>

                    @if ($tab == "pending")
                        </form>
                    @endif
                </div>

            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
        let selected_ids = [];

        $(".encashment_chkbox").on('change', function(data) {
            if ($(this).attr("id") == "chkAll") {
                let check_all_value = $(this).prop("checked")
                $("#encashmentList input[type=checkbox]").prop("checked", check_all_value)
            }

            let checkboxes = $("#encashmentList .encashment_chkbox");

            selected_ids = [];
            checkboxes.each((key, chk) => {
                if (chk.checked) {
                    selected_ids.push(chk.value)
                }
            })

            if (selected_ids.length > 0) {
                $(".action_btns").prop('disabled', false)
            } else {
                $(".action_btns").prop('disabled', true)
            }
        })
    </script>
@endsection
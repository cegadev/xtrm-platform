@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <small>
                ENCASHMENT REPORTS
            </small>
            <h1 class="h3 mb-0 text-gray-800">
                View Encashment
            </h1>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    @hasanyrole('Admin|Manager')
                    @if ($encashment->status == "pending")
                        <form action="{{ route('admin.encashment-requests.action-requests') }}" method="post">
                            @csrf
                            <input type="hidden" name="encashment_id" value="{{ $encashment->id }}">
                            <button class="btn btn-primary mb-3" name="action" value="approve"><i class="fa fa-check fa-fw"></i> Approve Request</button>
                            <button class="btn btn-danger mb-3" name="action" value="decline"><i class="fa fa-ban fa-fw"></i> Decline</button>
                        </form>
                    @endif
                    @endhasanyrole
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width=300></th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Encashment ID</th>
                                    <td>ENC-{{ sprintf("%08d", $encashment->id) }}</td>
                                </tr>
                                <tr>
                                    <th>Member Name</th>
                                    <td>
                                        {{ $encashment->requestedBy->first_name }}
                                        {{ $encashment->requestedBy->last_name }}

                                        &nbsp;
                                        &nbsp;
                                        -
                                        <a target="_blank" href="{{ route('admin.members.member-earnings', $encashment->membership_id) }}">view earnings <i class="fa fa-external-link"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Amount</th>
                                    <td>
                                        <span class="peso-sign"></span> {{ number_format($encashment->amount, 2) }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>VAT (10% applied)</th>
                                    <td>
                                        <span class="peso-sign"></span> {{ number_format($encashment->amount * $encashment->vat_applied, 2) }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Processing Fee </th>
                                    <td>
                                        <span class="peso-sign"></span> {{ number_format($encashment->service_fee, 2) }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Total Amount</th>
                                    <td>
                                        <span class="peso-sign"></span> {{ number_format($encashment->amount_released, 2) }}
                                    </td>
                                </tr>
                                @if ($encashment->bank_info)
                                    @if ($encashment->transaction_type == "cash")
                                        <tr>
                                            <th>Encashment Type</th>
                                            <td>
                                                CASH
                                            </td>
                                        </tr>
                                    @else 

                                        <tr>
                                            <th>Bank Info</th>
                                            <td>
                                                @php
                                                    $bankInfo = json_decode($encashment->bank_info);
                                                    $bank = DB::table("member_bank_infos")->whereId($bankInfo->bank_id)->first();
                                                @endphp

                                                <div><strong>Bank Account:</strong> {{ $bank->name }}</div>
                                                <div class="mt-2">
                                                    <strong>Account Name:</strong> {{ $encashment->member->member_name }}
                                                </div>
                                                <div class="mt-2">
                                                    <strong>Account Number:</strong> {{ $bankInfo->account_number }}
                                                </div>
                                            </td>
                                        </tr>
                                    @endif

                                @endif

                            </tbody>
                        </table>

                        <a href="{{ $back_url ?? route('admin.encashment-requests.index') }}" class="btn btn-secondary"><i class="fa fa-arrow-left"></i> Back to Requests</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script>
       
    </script>
@endsection
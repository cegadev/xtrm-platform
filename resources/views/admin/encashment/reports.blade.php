@extends('layouts.admin')
@section('page-name')
    - Encashment Reports
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.bootstrap4.min.css">
<style>
    [x-cloak] { display: none !important; }
</style>
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <small>REPORTS</small>
            <h1 class="h3 mb-0 text-gray-800"> Encashment</h1>
        </div>
    </div>
    <div class="row">
        
        <div class="col-md-4">
            <div class="card shadow mb-4" >
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-filter fa-fw"></i>  Filter Report</h6>
                </div>
                <div class="card-body">
                    <form action="" x-data="{member_id: '{{ $selected_member ?? null }}', date_range: '{{ $selected_date_range ?? '' }}', type: '{{ $selected_type }}'}">
                        <div class="form-group">
                            <label for="member">Select Member</label>
                            <select name="member_id" id="member_id" class="form-control" x-model="member_id">
                                <option value="">All Members</option>
                                @foreach ($members as $member)
                                    <option value="{{ $member->id }}" {{ $selected_member == $member->id ? "selected" : "" }} >{{ \Str::ucfirst($member->member_name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Date Range</label>
                            <input type="text" name="date_range" x-model="date_range" id="date_range" value="{{ $selected_date_range }}" class="form-control">
                        </div>
                        <div class="form-group" x-show="date_range" x-cloak>
                            <div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" x-model="type" name="type" id="inlineRadio1" value="requested">
                                    <label class="form-check-label" for="inlineRadio1">By requested date</label>
                                  </div>
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" x-model="type" name="type" id="inlineRadio2" value="released">
                                    <label class="form-check-label" for="inlineRadio2">By released date</label>
                                </div>
                            </div>
                        </div>
                        {{-- <button class="btn btn-outline-secondary btn-block"></button> --}}
                        @if (!empty($selected_member) || !empty($selected_date_range))
                            <div class="text-center mb-3">
                                <a href="{{ route('admin.reports.encashment.index') }}"><i class="fa fa-times fa-fw"></i> Clear Filter</a>
                            </div>
                        @endif
                        <button class="btn btn-warning btn-block">Generate Report</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-chart-bar fa-fw"></i>
                        Encashment Chart</h6>
                </div>
                <div class="card-body align-middle text-center">
                    @if ($encashments->count())
                        <div class="chart-bar">
                            <canvas id="myBarChart"></canvas>
                        </div>    
                    @else
                        <div>
                            <i class="fa fa-chart-bar fa-10x"></i>
                        </div>
                        No encashments data available.
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <i class="fa fa-table fa-fw"></i>
                Encashment Reports</h6>
        </div>
        <div class="card-body align-middle">
            <div class="table-responsive" id="encashmentsTable_wrapper">
                <table class="table mt-2" id="encashmentsTable">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Net Pay</th>
                            <th>Processing Fee</th>
                            <th>VAT</th>
                            <th>Type</th>
                            <th>Requested At</th>
                            <th>Released At</th>
                        </tr>
                    </thead>
                </table>
            </div>
            
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('sb-admin/vendor/chart.js/Chart.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.3.0/alpine-ie11.min.js" integrity="sha512-Atu8sttM7mNNMon28+GHxLdz4Xo2APm1WVHwiLW9gW4bmHpHc/E2IbXrj98SmefTmbqbUTOztKl5PDPiu0LD/A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
let member_id = "{{ $member_id ?? null}}";
let date_range = "{{ $selected_date_range }}";
let type = "{{ $selected_type }}";
// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}
showBarChart = (labels, data) => {

    var ctx = document.getElementById("myBarChart");

    var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels,
        datasets: [{
        label: "Encashment",
        backgroundColor: "#4e73df",
        hoverBackgroundColor: "#2e59d9",
        borderColor: "#4e73df",
        data,
        }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
        padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
        },
        scales: {
        xAxes: [{
            time: {
            unit: 'month'
            },
            gridLines: {
            display: false,
            drawBorder: false
            },
            ticks: {
            maxTicksLimit: 6
            },
            maxBarThickness: 25,
        }],
        yAxes: [{
            ticks: {
            min: 0,
            maxTicksLimit: 5,
            padding: 10,
            // Include a dollar sign in the ticks
            callback: function(value, index, values) {
                return 'P' + number_format(value);
            }
            },
            gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
            }
        }],
        },
        legend: {
        display: false
        },
        tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
            label: function(tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
            return datasetLabel + ': P' + number_format(tooltipItem.yLabel);
            }
        }
        },
    }
    });
}

$.get("{{ route('api-reports.encashment-chart') }}", {
    member_id,
}, (data) => {
    showBarChart(data.months, data.values)
})



</script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    $("#date_range").flatpickr({
        mode: "range",
        dateFormat: "M j, Y",
    })
</script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>


<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
<script>
    loadTable = () => {
        $("#encashmentsTable").DataTable({
            "processing": false,
            "serverSide": false,
            "ajax": {
                "url": "/api/reports/encashment-data",
                "data": {
                    member_id,
                    date_range,
                    type,
                    excel: true
                },
            },
            "columns": [
                { "data": "reference_number" },
                { "data": "name" },
                { "data": "amount" },
                { "data": "amount_released" },
                { "data": "service_fee" },
                { "data": "tax" },
                { "data": "transaction_type" },
                { "data": "requested_at" },
                { "data": "approved_at" },
            ],
            "columnDefs": [
                {
                    "targets": [4,5,6],
                    "visible": false,
                    "searchable": false
                }
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ],
            "initComplete": () => {
                $(".dt-button").addClass('btn btn-outline-primary').removeClass('dt-button')
            }
        })
    }

    loadTable();
</script>
@endsection
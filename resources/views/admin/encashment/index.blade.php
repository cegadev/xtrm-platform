@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<style>
    .selected_bank_wrapper {
        display: none
    }

    #transaction_spinner_wrapper{
        display: none;
    }
    #transaction_spinner_wrapper.active {
        display: flex;
        position: absolute;
        height: 100%;
        width: 100%;
        background-color: rgb(0 0 0 / 32%);
        justify-content: center;
        z-index: 1;
    }
    #transaction_spinner {
        width: 100%;

    }
</style>
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Encashment</h1>
    </div>
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Encashment</h6>
                </div>
                <div class="card-body">
                    <div>
                        <strong>Notes</strong>
                        <ul>
                            <li>
                                For every transaction, a minimum of Php 500.00 encashment is allowed.
                            </li>
                            <li>
                                Processing fee of <strong>
                                    <span class="peso-sign"></span> 20.00 
                                </strong>
                            </li>
                            {{-- <li>
                                Maintenance of atleast <strong>3 products activation</strong> is required to encash IRB and Unilevel Commission.
                            </li> --}}
                        </ul>
                    </div>
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="d-flex flex-row" style="height: 100%">
                                        <div class="align-self-center">
                                            <i class="fa fa-wallet fa-2x"></i>
                                        </div>
                                        <div class="pl-4 align-self-center">
                                            <div class="" style="font-size: 1.5em">
                                                Available Commission
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right align-self-center" style="font-size: 2em">
                                    <span class="peso-sign"></span> {{ number_format($available_commissions, 2) }}
                                </div>
                            </div>
                            
                        </div>
                        {{-- @if ($activation_count->product < 3)
                            <div class="card-footer">
                                Maintenance of atleast <strong>3 products activation</strong> haven't met. You cannot encash your earnings from <strong>IRB</strong> and <strong>Unilevel Commission</strong> at the moment. <br>
                            </div>
                        @endif --}}
                    </div>

                    <form action="{{ route('admin.encashment.create-request') }}" class="mt-4" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                <label for="">Enter Amount</label>
                                <input tabindex="1" type="text" name="amount" id="amount" placeholder="0" value="{{ old('amount') }}" class="form-control form-control-lg text-right @error('amount') is-invalid @enderror">
                            </div>
                        </div>
                        <div style="position: relative">
                            <div id="transaction_spinner_wrapper">
                                <div style="align-self: center">
                                    <div class="spinner-border" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row" style="position: relative">
                                
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                    <label for="">Transaction type</label>
                                    <div class="mb-3">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input chk_type" type="radio" name="type" id="cash" value="cash" checked>
                                            <label class="form-check-label" for="cash">Cash</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input chk_type" type="radio" name="type" id="bank" value="bank">
                                            <label class="form-check-label" for="bank">Bank Transfer</label>
                                        </div>
                                    </div>

                                    <select id="bank_selection" style="display: none" onchange="getBankInfo(this.value)" tabindex="2" class="form-control @error('transaction_type') is-invalid @enderror" name="transaction_type" id="transaction_type">
                                        <option disabled selected>Choose One</option>
                                        @foreach ($transaction_types as $key => $type)
                                            <option value="{{ $key }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                                <div class="form-group col-md-3 selected_bank_wrapper mb-3">
                                    <label for="">Bank Name</label>
                                    <input type="text" id="bank_name" readonly disabled name="bank_name" class="form-control">
                                    
                                    <select id="others_bank_name" name="others_bank_name" class="form-control d-none" onchange="getOtherBankInfo(this.value)">
                                        <option value="new"> ~ New Bank ~</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-9 selected_bank_wrapper">
                                
                                    <div id="new_bank_name_wrapper" class=" mb-3">
                                        <label for="">New Bank Name</label>
                                        <input type="text" id="new_bank_name" placeholder="Enter Bank Name" name="new_bank_name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Account Name</label>
                                        <input type="text" id="bank_account_name" placeholder="Enter Account Name" name="bank_account_name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Account Number</label>
                                        <input type="text" id="bank_account_number" placeholder="Enter Account Number" name="bank_account_number" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                <button tabindex="3" class="btn btn-primary mt-3">Submit Request</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-4"> --}}
            {{-- <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Current Week - Total Activation</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th class="align-middle">
                                Product
                            </th>
                            <td>{{ $activation_count->product }}</td>
                        </tr>
                        <tr>
                            <th>Package</th>
                            <td>{{ $activation_count->package }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Earnings Summary</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th class="align-middle">
                                Direct Referral
                                <br>
                                <small>
                                    <i class="fa fa-users"></i>  {{ $summary["all"]->get("direct referral") ? $summary["all"]->get("direct referral")->total : 0 }} members
                                </small>
                            </th>
                            <td class="align-middle">
                                <span class="peso-sign"></span> {{ $summary["all"]->get("direct referral") ? number_format($summary["all"]->get("direct referral")->amount, 2) : number_format(0, 2) }} 
                            </td>
                        </tr>
                        <tr>
                            <th>Indirect Referral Bonus</th>
                            <td>
                                <span class="peso-sign"></span> {{ $summary["all"]->get("indirect referral") ? number_format($summary["all"]->get("indirect referral")->amount, 2) : number_format(0, 2) }} 
                            </td>
                        </tr>
                        <tr>
                            <th>Unilevel Commission</th>
                            <td>
                                <span class="peso-sign"></span> {{ $summary["all"]->get("unilevel") ? number_format($summary["all"]->get("unilevel")->amount, 2) : number_format(0, 2) }} 
                            </td>
                        </tr>
                        <tr>
                            <th>Encashment 
                                <br>
                                <small>
                                    <span class="peso-sign"></span> 20.00 fee
                                </small>
                            </th>
                            <td class="align-middle">
                                (- <span class="peso-sign"></span> {{ number_format($total_encashment, 2) }})
                            </td>
                        </tr>
                    </table>
                </div>
            </div> --}}
            {{-- <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Current Week - Earnings Summary</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="align-middle">
                                Direct Referral
                            </th>
                            <td>
                                <i class="fa fa-users"></i> {{ $summary["current_week"]->get("direct referral") ? $summary["current_week"]->get("direct referral")->total : 0 }} 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="peso-sign"></span> {{ $summary["current_week"]->get("direct referral") ? number_format($summary["current_week"]->get("direct referral")->amount, 2) : number_format(0, 2) }} 
                            </td>
                        </tr>
                        <tr>
                            <th>Indirect Referral Bonus</th>
                            <td>
                                <span class="peso-sign"></span> {{ $summary["current_week"]->get("indirect referral") ? number_format($summary["current_week"]->get("indirect referral")->amount, 2) : number_format(0, 2) }} 
                            </td>
                        </tr>
                        <tr>
                            <th>Unilevel Commission</th>
                            <td>
                                <span class="peso-sign"></span> {{ $summary["current_week"]->get("unilevel") ? number_format($summary["current_week"]->get("unilevel")->amount, 2) : number_format(0, 2) }} 
                            </td>
                        </tr>
                    </table>
                </div>
            </div> --}}
            {{-- <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Current Month - Earnings Summary</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2" class="align-middle">
                                Direct Referral
                            </th>
                            <td>
                                <i class="fa fa-users"></i> 100 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="peso-sign"></span> 100.00
                            </td>
                        </tr>
                        <tr>
                            <th>Indirect Referral Bonus</th>
                            <td>
                                <span class="peso-sign"></span> 100.00
                            </td>
                        </tr>
                        <tr>
                            <th>Unilevel Commission</th>
                            <td>
                                <span class="peso-sign"></span> 100.00
                            </td>
                        </tr>
                    </table>
                </div>
            </div> --}}
        {{-- </div> --}}
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Encashment History</h6>
        </div>
        <div class="card-body">
            <table class="table" id="dataTable">
                <thead>
                    <tr>
                        <th>Reference No.</th>
                        <th>Amount</th>
                        <th>Date Requested</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($encashment_histories as $encashment)
                        <tr>
                            <td>
                                <a href="javascript:void(0)" onclick="viewEncashment({{ $encashment->id }})">
                                    ENC-{{ sprintf('%08d', $encashment->id) }}
                                </a>
                            </td>
                            <td>
                                <span class="peso-sign"></span> {{ number_format($encashment->amount) }}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($encashment->requested_at)->format('M d, Y - h:i A') }}
                            </td>
                            <td>
                                {{ \Str::ucfirst($encashment->status) }} - {{ $encashment->status_date->format("M d, Y - h:i A") }}
                            </td>
                        </tr>
                    @empty
                        {{-- <tr>
                            <td colspan="4">No Encashment Histories available.</td>
                        </tr> --}}
                    @endforelse
                    
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="encashmentInfo">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Encashment Information</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <table class="table">
                  <tbody>
                      <tr>
                          <th>Status</th>
                          <td id="eh-status">For Approval</td>
                      </tr>
                      <tr>
                        <th>Reference Number</th>
                        <td>
                          <span id="eh-reference"></span></td>
                        </tr>
                      <tr>
                          <th>Amount</th>
                          <td>
                            <span class="peso-sign"></span>  
                            <span id="eh-amount"></span></td>
                      </tr>
                      
                      <tr>
                          <th>Processing Fee</th>
                          <td>
                            <span class="peso-sign"></span>  
                            <span id="eh-fee"></span>
                            </td>
                      </tr>
                      <tr>
                          <th>10% VAT</th>
                          <td>
                            <span class="peso-sign"></span>  
                            <span id="eh-vat"></span></td>
                      </tr>
                      <tr>
                        <th>Transaction Type</th>
                        <td>
                        <span id="eh-type"></span></td>
                    </tr>
                      <tr>
                          <th>Net Pay</th>
                          <td >
                            <span class="peso-sign"></span>
                            <span id="eh-total"></span>  
                        </td>
                      </tr>
                  </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')
<script src="{{ asset('sb-admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $("#dataTable").DataTable({
        order: [[0, "desc"]]
    })
</script>
<script>
    $(".chk_type").on('change', (data) => {
        let value = data.target.value;
        if (value == "cash") {
            $("#bank_selection").hide()
            
        } else {
            $("#bank_selection option:first-child").prop("selected", true)
            $("#bank_selection").show()
            $(".selected_bank_wrapper").hide()
        }
    })

    let member_id = "{{ auth()->user()->membership->id ?? '' }}";
    viewEncashment = (id) => {
        let encashmentHistory = $.get('/api/encashment-history/' + id, {}, (data) => {
            $("#encashmentInfo").modal('show')
            $("#eh-status").text(data.status)
            $("#eh-amount").text(data.amount)
            $("#eh-fee").text(data.service_fee)
            $("#eh-vat").text(data.vat)
            $("#eh-reference").text(data.reference)
            $("#eh-type").text(data.transaction_type)
            $("#eh-total").text(data.amount_released)
        }, "json")
    }

    getBankInfo = (type) => {
        $("#transaction_spinner_wrapper").addClass('active')
        $.get("{{ route('api-bank-infos.index') }}", {
            member_id,
            type
        }, (data) => {
            $("#transaction_spinner_wrapper").removeClass('active')
            let bankInfo;
            $('#bank_name').addClass('d-none')
            $('#others_bank_name').addClass('d-none')

            $(".selected_bank_wrapper").show();
            $("#new_bank_name_wrapper").hide()
            if (["gcash", "paymaya", "bdo"].includes(type)) {
                $('#bank_name').removeClass('d-none')
                if (data.length == 0) {
                    let bankName = type == "bdo" ? "BDO" : type.charAt(0).toUpperCase() + type.slice(1)
                    $("#bank_name").val(bankName)
                    $("#bank_account_number").val("")
                    $("#bank_account_name").val("")
                } else {
                    bankInfo = data[0]
                    $("#bank_name").val(bankInfo.name)
                    $("#bank_account_number").val(bankInfo.value)
                    $("#bank_account_name").val(bankInfo.account_name)
                }
            } else {
                $("#new_bank_name_wrapper").show()
                $('#others_bank_name').removeClass('d-none')
                let options = ""
                options += "<option value='new'>~ New Bank~</option>";

                data.forEach(d => {
                    options += `<option value='${d.id}'>${d.name}</option>`;
                });
                $("#bank_account_number").val("")
                $("#bank_account_name").val("")
                $("#new_bank_name").val("")

                $("#others_bank_name").html(options);
            }
        })
    }

    getOtherBankInfo = (id) => {
        $("#new_bank_name").val("")
        if (id == "new") {
            $("#bank_account_number").val("")
            $("#bank_account_name").val("")
            $("#new_bank_name_wrapper").show()
        } else {
            $("#new_bank_name_wrapper").hide()
            $("#transaction_spinner_wrapper").addClass('active')
            $.get('/api/bank-infos/' + id, {}, (data) => {
                $("#transaction_spinner_wrapper").removeClass('active')
                $("#new_bank_name").val(data.name)
                $("#bank_account_number").val(data.value)
                $("#bank_account_name").val(data.account_name)
            })
        }
    }
</script>
@endsection
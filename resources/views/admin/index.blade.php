@extends('layouts.admin')


@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
      </div>

    

      @role('Member') 
        @include('admin.member-dashboard-cards')
      @endrole

      @role('Admin')
        @include('admin.dashboard-cards')
      @endrole

      <!-- Content Row -->
      <div class="row">

        <!-- Content Column -->
        @hasanyrole('Admin|Member')
        <div class="col-lg-5 mb-4">
          @role('Member')
          <div class="card shadow mb-3">
            <div class="card-header m-0 font-weight-bold text-primary">My Purchases</div>
            <div class="card-body">
                {{-- <small>SHAREABLE CODES TO DOWNLINE</small> --}}
                <div class="row">
                  <div class="col-md-7 col-sm-9">
                      <span>
                          <i class="fa fa-key text-warning fa-2x"></i>
                          <span class="h3 ml-2">{{ $activation_codes }}</span>
                      </span>
                  </div>
                  <div class="col-md-5 col-sm-3 text-right align-self-center">
                    <a href="{{ route('my-orders.transactions.index') }}">View  </a>
                  </div>
                </div>
            </div>
          </div>
        
          <!-- Project Card Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">My Earnings</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-borderless">
                  <thead>
                    <tr>
                      <th>Commission</th>
                      <th width=150>Income</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($ways_of_earning as $woe)
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-xl-2">
                              <i class="fa fa-clipboard-list fa-3x"></i>
                            </div>
                            <div class="col align-self-center">
                              {{ $woe->label }}
                            </div>
                          </div>
                        </td>
                        <td class="align-middle">
                          <span class="peso-sign"></span> {{ number_format($woe->amount, 2) }}
                        </td>
                      </tr>
                    @endforeach
  
                  </tbody>
                </table>
                @role('Member')
                <a href="{{ route('reports.earnings.index') }}">View details &rarr;</a>
                @endrole
              </div>
            </div>
          </div>
          @endrole
          @hasrole('Admin')
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Company Income</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-borderless">
                  <thead>
                    <tr>
                      <th>Type</th>
                      <th width=150>Income</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($company_income as $key => $ci)
                      <tr>
                        <td>
                          <div class="row">
                            <div class="col-xl-2">
                              <i class="fa peso-sign fa-2x"></i>
                            </div>
                            <div class="col align-self-center">
                              {{ $ci->label }}
                            </div>
                          </div>
                        </td>
                        <td class="align-middle">
                          @if ($key == "encashment")
                            <span class="peso-sign"></span> - {{ number_format($ci->amount, 2) }}  
                          @else 
                            <span class="peso-sign"></span> {{ number_format($ci->amount, 2) }}
                          @endif
                        </td>
                      </tr>
                    @endforeach
                      <tr>
                        <td class="text-right">
                          <strong>TOTAL</strong>
                        </td>
                        <td class="align-middle">
                          <span class="peso-sign"></span> {{ \number_format($company_income->sum('amount') - $company_income->get('encashment')->amount, 2) }}
                        </td>
                      </tr>
                  </tbody>
                </table>
                @role('Member')
                <a href="{{ route('reports.earnings.index') }}">View details &rarr;</a>
                @endrole
              </div>
            </div>
          </div>
          @endhasrole
        </div>
        @endhasanyrole
        
        @role('Admin')
        <div class="col-lg-7 mb-4">
          @php
              $total_associates = $associates->count();
          @endphp
          <!-- My Associates -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              @if (auth()->user()->isMember())
                <h6 class="m-0 font-weight-bold text-primary">My Associates</h6>
              @endif
              @if (auth()->user()->isManager() || auth()->user()->isAdmin())
            <h6 class="m-0 font-weight-bold text-primary">Members &rarr; {{ $total_associates }} as of {{ Carbon\Carbon::now()->format("M j, Y") }}</h6>
              @endif
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-borderless">
                  <thead>
                    <tr>
                      <th>No. </th>
                      <th>Username </th>
                      <th>Name</th>
                      <th width=100>Package</th>
                      <th>Sponsor Name</th>
                      <th>Date </th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($associates->take(10) as $key => $associate)
                      <tr>
                        <td class="align-middle">{{ $key + 1 }}</td>
                        <td class="align-middle">{{ $associate->user->username }}</td>
                        <td class="align-middle">
                          <div class="">{{ $associate->user->first_name }} {{ $associate->user->last_name }}</div>
                        </td>
                        <td>{{ $associate->package->name ?? "N/A" }} </td>
                        <td>{{ $associate->parent->member_name ?? "N/A" }}</td>
                        <td>{{ $associate->created_at->format("Y-m-d") }}</td>
                      </tr>
                    @empty
                      <tr>
                        @if ($logged_user->managedBranch)
                          <td colspan="4" class="text-center">No associates yet.</td>
                        @else 
                          <td colspan="4" class="text-center">No associates to this branch yet.</td>
                        @endif
                      </tr>
                    @endforelse

                  </tbody>
                </table>
              </div>
              @if ($total_associates > 10)
                <a target="_blank" rel="nofollow" href="{{ route('admin.genealogy.index', ['view' => 2]) }}">View All &rarr;</a>
              @endif
            </div>
          </div>
        </div>
        @endrole
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h5>Quick Links</h5>
              <ul>
                <li>
                  <a href="{{ route('orders.index', ['tab' => 'pending']) }}">Pending Orders</a>
                </li>
                <li>
                  <a href="{{ route('admin.members.index', ['tab' => 'all']) }}">All Members</a>
                </li>
                <li>
                  <a href="{{ route('admin.profile.index') }}">My Profile</a>
                </li>
                <li>
                  <a href="{{ route('admin.profile.change-password', ['back_url' => url()->current()]) }}">Change Password</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
    </div>

@endsection


@section('scripts')
    
  <!-- Page level plugins -->
  <script src="{{ asset('sb-admin/vendor/chart.js/Chart.min.js') }}"></script>

  <!-- Page level custom scripts -->

  <script>
    @role('Member')
      const member_id = "{{ $membership->id }}";
      const obj = {
        member_id,
      }
    @else 
      const obj = {}
    @endrole  
  </script>

  <script>
    // $.get("/api/membership/earnings", obj, (data) => {
    //   let monthly_earnings = data.monthly_earnings.map(i => i.amount);

    //   showEarningsChart(monthly_earnings)
    // }, "json")
  </script>
  
  <script>
   

    // showEarningsChart = (monthly_earnings) => {
    //     // Set new default font family and font color to mimic Bootstrap's default styling
    //     Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    //     Chart.defaults.global.defaultFontColor = '#858796';

    //     function number_format(number, decimals, dec_point, thousands_sep) {
    //       // *     example: number_format(1234.56, 2, ',', ' ');
    //       // *     return: '1 234,56'
    //       number = (number + '').replace(',', '').replace(' ', '');
    //       var n = !isFinite(+number) ? 0 : +number,
    //         prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    //         sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    //         dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    //         s = '',
    //         toFixedFix = function(n, prec) {
    //           var k = Math.pow(10, prec);
    //           return '' + Math.round(n * k) / k;
    //         };
    //       // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    //       s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    //       if (s[0].length > 3) {
    //         s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    //       }
    //       if ((s[1] || '').length < prec) {
    //         s[1] = s[1] || '';
    //         s[1] += new Array(prec - s[1].length + 1).join('0');
    //       }
    //       return s.join(dec);
    //     }

    //     // Area Chart Example
    //     var ctx = document.getElementById("myAreaChart");
    //     var myLineChart = new Chart(ctx, {
    //       type: 'line',
    //       data: {
    //         labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    //         datasets: [{
    //           label: "Earnings",
    //           lineTension: 0.3,
    //           backgroundColor: "rgba(78, 115, 223, 0.05)",
    //           borderColor: "rgba(78, 115, 223, 1)",
    //           pointRadius: 3,
    //           pointBackgroundColor: "rgba(78, 115, 223, 1)",
    //           pointBorderColor: "rgba(78, 115, 223, 1)",
    //           pointHoverRadius: 3,
    //           pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
    //           pointHoverBorderColor: "rgba(78, 115, 223, 1)",
    //           pointHitRadius: 10,
    //           pointBorderWidth: 2,
    //           data: monthly_earnings,
    //         }],
    //       },
    //       options: {
    //         maintainAspectRatio: false,
    //         layout: {
    //           padding: {
    //             left: 10,
    //             right: 25,
    //             top: 25,
    //             bottom: 0
    //           }
    //         },
    //         scales: {
    //           xAxes: [{
    //             time: {
    //               unit: 'date'
    //             },
    //             gridLines: {
    //               display: false,
    //               drawBorder: false
    //             },
    //             ticks: {
    //               maxTicksLimit: 7
    //             }
    //           }],
    //           yAxes: [{
    //             ticks: {
    //               maxTicksLimit: 5,
    //               padding: 10,
    //               // Include a dollar sign in the ticks
    //               callback: function(value, index, values) {
    //                 return 'P ' + number_format(value);
    //               }
    //             },
    //             gridLines: {
    //               color: "rgb(234, 236, 244)",
    //               zeroLineColor: "rgb(234, 236, 244)",
    //               drawBorder: false,
    //               borderDash: [2],
    //               zeroLineBorderDash: [2]
    //             }
    //           }],
    //         },
    //         legend: {
    //           display: false
    //         },
    //         tooltips: {
    //           backgroundColor: "rgb(255,255,255)",
    //           bodyFontColor: "#858796",
    //           titleMarginBottom: 10,
    //           titleFontColor: '#6e707e',
    //           titleFontSize: 14,
    //           borderColor: '#dddfeb',
    //           borderWidth: 1,
    //           xPadding: 15,
    //           yPadding: 15,
    //           displayColors: false,
    //           intersect: false,
    //           mode: 'index',
    //           caretPadding: 10,
    //           callbacks: {
    //             label: function(tooltipItem, chart) {
    //               var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
    //               return datasetLabel + ': P' + number_format(tooltipItem.yLabel);
    //             }
    //           }
    //         }
    //       }
    //     });
    // }
  </script>

  <script>
    // $.get("/api/membership/earnings-type", obj, (data) => {
    //   let labels = Object.keys(data); 
    //   let values = []; 
    //   labels.forEach((a) => {
    //     values.push(data[a]);
    //   }); 
    //   showEarningsTypeChart(labels, values)
    // }, "json")

    // showEarningsTypeChart = (labels, data) => {

    //       // Set new default font family and font color to mimic Bootstrap's default styling
    //   Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    //   Chart.defaults.global.defaultFontColor = '#858796';

    //   // Pie Chart Example
    //   var ctx = document.getElementById("myPieChart");
    //   var myPieChart = new Chart(ctx, {
    //     type: 'doughnut',
    //     data: {
    //       labels: labels,
    //       datasets: [{
    //         data: data,
    //         backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
    //         hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
    //         hoverBorderColor: "rgba(234, 236, 244, 1)",
    //       }],
    //     },
    //     options: {
    //       maintainAspectRatio: false,
    //       tooltips: {
    //         backgroundColor: "rgb(255,255,255)",
    //         bodyFontColor: "#858796",
    //         borderColor: '#dddfeb',
    //         borderWidth: 1,
    //         xPadding: 15,
    //         yPadding: 15,
    //         displayColors: false,
    //         caretPadding: 10,
    //       },
    //       legend: {
    //         display: false
    //       },
    //       cutoutPercentage: 80,
    //     },
    //   });
    // }

    function claimBonus (btn) {
      $(btn).prop('disabled', true);
      $.ajax({
        url: "{{ route('bonuses.claim-bonus') }}",
        method: 'POST',
        data: {
          _token: "{{ csrf_token() }}"
        },
        success: function(data) {
          alert('Bonuses successfully claimed.');
          window.location.reload();
        }
      })
    }
  </script>
@endsection
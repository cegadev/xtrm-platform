@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">My Earnings</h1>
    </div>
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Direct Referral </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                <span class="peso-sign"></span>
                {{ number_format($earnings_breakdown->direct, 2) }}
                </div>
            </div>
          </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
              <div class="card-body">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Indirect Referral Bonus</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <span class="peso-sign"></span>
                  {{ number_format($earnings_breakdown->indirect, 2) }}
                  </div>
              </div>
            </div>
          </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
              <div class="card-body">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Unilevel Commission </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <span class="peso-sign"></span>
                  {{ number_format($earnings_breakdown->unilevel, 2) }}
                  </div>
              </div>
            </div>
        </div>

        
      </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Earnings Report</h6>
        </div>
        
        <div class="card-body">
            @can('members.write')
                <a href="{{ route('admin.members.member-create') }}" class="btn btn-primary"><i class="fa fa-user-plus fa-fw"></i> Create New Member</a>
            @endcan
            <form action="" class="mt-3">
                <label for="">Search Keyword</label>

                <div class="input-group mb-3">
                    <input type="text" value="{{ $keyword }}" name="keyword" class="form-control" placeholder="Enter keyword here" aria-label="Enter keyword here (first name, last name, or username)" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-outline-primary" type="button"><i class="fa fa-search fa-fw"></i> Search</button>
                    </div>
                </div>
            </form>
            @if ($keyword)
                <a href="?page=1"><i class="fa fa-times fa-fw"></i> Clear Search</a>
            @endif

            @if ($earnings->count())
                <div class="mt-2">
                    Showing {{ $earnings->firstItem() }} to {{ $earnings->lastItem() }} of {{ $earnings->total() }} earnings
                </div>
            @endif
            <div class="table-responsive mt-3">
                <div class="table-responsive">
                    <table class="table" id="earnings" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Reference No.</th>
                                <th>Type</th>
                                {{-- <th width=500>Notes</th> --}}
                                <th>Amount</th>
                                <th>Date Received</th>
                                <th>Earned from Member</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($earnings as $earning)
                                <tr>
                                    <td>{{ sprintf('%08d', $earning->id) }}</td>
                                    <td>{{ Str::ucfirst($earning->type) }}</td>
                                    {{-- <td>{{ $earning->notes }}</td> --}}
                                    <td>{{ number_format($earning->amount, 2) }}</td>
                                    <td>{{ $earning->created_at->format("M j, Y") }}</td>
                                    <td>{{ $earning->earnedFromMember->member_name ?? 'N/A' }}</td>
                                    {{-- <td>{{ $earning->membership }}</td> --}}
                                    {{-- <td class="text-right"> --}}
                                        {{-- <a href="{{ route('admin.members.member-edit', $earning->id) }}" class="btn btn-outline-primary"><i class="fa fa-edit fa-fw"></i> Edit </a> --}}
                                        {{-- <a href="" class="btn btn-outline-danger"><i class="fa fa-times"></i> </a> --}}
                                    {{-- </td> --}}
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center">No earnings found.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

            {{ $earnings->onEachSide(5)->links() }}
        </div>
      </div>

@endsection

@section('scripts')
@endsection
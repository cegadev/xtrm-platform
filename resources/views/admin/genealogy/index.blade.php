@extends('layouts.admin')

@section('styles')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.10/css/jquery.orgchart.min.css">

<style>
    /* #chart-container .orgchart{
        width: 100%
    } */
    
    .edge.topEdge, .edge.leftEdge, .edge.rightEdge {
        display: none;
    }

    i.oci.oci-leader.symbol {
        display: none;
    }

    .edge.bottomEdge {
        display: block
    }
</style>

<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Genealogy</h1>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">My Associates</h6>
        </div>
        <div class="card-body">
            <ul class="nav nav-pills mb-3">
                <li class="nav-item">
                  <a class="nav-link {{ $view == 1 ? "active" : "" }}" href="?view=1"><i class="fa fa-sitemap fa-fw"></i> Graphical</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ $view == 2 ? "active" : "" }}" href="?view=2"><i class="fa fa-table fa-fw"></i> Tabular</a>
                </li>
            </ul>
            @if ($view == 1)    
                <div>
                    <div class="h5">Legends</div>
                    @foreach ($packages as $item)
                        <div>
                            <i class="fa fa-square fa-fw" style="color: {{ $item->settings->color }}"></i> {{ $item->name }} (<span class="peso-sign"></span> {{ number_format($item->price, 2) }})
                        </div>
                    @endforeach
                </div>

                <div id="chart-container" class="mt-4"></div>
            @else
                <div class="p-2">
                    <table class="table mt-3" id="members">
                        <thead>
                            <tr>
                                <th width=100>Level</th>
                                <th width=300>Username</th>
                                <th>Name</th>
                                <th>Registered At</th>
                                <th>Sponsor Name</th>
                                <th>Package</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($results as $result)
                                <tr>
                                    <td>{{ $result->depth }}</td>
                                    <td>{{ $result->user->username }}</td>
                                    <td>{{ $result->member_name }}</td>
                                    <td>{{ $result->user->created_at->format("Y-m-d") }}</td>
                                    <td>{{ $result->parent->member_name ?? "N/A" }}</td>
                                    <td>{{ $result->package->code ?? "N/A" }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
      </div>

@endsection

@section('scripts')

@if ($view == 1)
<script src="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.10/js/jquery.orgchart.min.js"></script>
    
@php
    $userId = auth()->id();
    $isMember = auth()->user()->isMember();
    $genealogy_api = route('api-memberships.genealogy', ['user_id' => $isMember ? $userId : null]);
@endphp
<script type="text/javascript">

let isMember = "{{ $isMember }}";
'use strict';

 $(function() {
            
    var oc = $('#chart-container').orgchart({
        'visibleLevel': 2,
        'pan': true,
        'data' : "{{ $genealogy_api }}",
        'nodeTitle': 'member_name',
        'nodeContent': 'depth',
        'createNode': function($node, data) {
            if (data.package) {
                $node.find('.title').css('background-color', data.package.settings.color)
                $node.find('.content').css('border-color', data.package.settings.color)
            } else {
                $node.find('.title').css('background-color', '#000')
                $node.find('.content').css('border-color', '#000')
            }
            $node.on('click', function(event) {
                if (!$(event.target).is('.edge, .toggleBtn')) {
                    var $this = $(this);
                    var $chart = $this.closest('.orgchart');
                    var newX = window.parseInt(($chart.outerWidth(true)/2) - ($this.offset().left - $chart.offset().left) - ($this.outerWidth(true)/2));
                    var newY = window.parseInt(($chart.outerHeight(true)/2) - ($this.offset().top - $chart.offset().top) - ($this.outerHeight(true)/2));
                    $chart.css('transform', 'matrix(1, 0, 0, 1, ' + newX + ', ' + newY + ')');
                }
            });
            
            let content = $node.find(".content");
            let level = content.text();

            // if (!level) {
                // if (isMember) {
                //     content.text(data.user.username)
                // } else {
                //     content.text("Level 0")
                // }
                content.text(data.user.username)

            // } else {
                // level = ordinal_suffix_of(level);
                // content.text(level + " level")
            // }

            return data;
        },
        
    });

    function ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

    $(window).resize(function() {
      var width = $(window).width();
      if(width > 576) {
        oc.init({'verticalLevel': undefined});
      } else {
        oc.init({'verticalLevel': 2});
      }
    });
});
</script>

@else 

<script src="{{ asset('sb-admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    // Call the dataTables jQuery plugin
$(document).ready(function() {
        $('#members').DataTable({
            "pageLength": 100,
            "ordering" : true,
            "order": [[3, "desc"]],
            "columnDefs": [
                {
                    type: "date", targets: 3
                }
            ]
            
        });
    });
</script>
@endif


@endsection
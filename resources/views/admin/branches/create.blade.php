@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Branches</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Create Branch</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('branches.save') }}" method="POST">
                    
                        @csrf
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="">Branch Name *</label>
                                    <input type="text" value="{{ old('branch_name') }}" name="branch_name" class="form-control @error('branch_name') is-invalid @enderror">
                                    @error('branch_name')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label for="">Address *</label>
                            <input type="text" value="{{ old('branch_address') }}" name="branch_address" class="form-control @error('branch_address') is-invalid @enderror">
                            @error('branch_address')
                                {{ $message }}
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="">Contact Numbers</label>
                                    <input type="text" value="{{ old('contact_numbers') }}" name="contact_numbers" placeholder="Enter contact numbers (separated by comma)" class="form-control @error('contact_numbers') is-invalid @enderror">
                                    @error('contact_numbers')
                                        {{ $message }}
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="">Email address</label>
                                    <input type="email" value="{{ old('branch_email') }}" name="branch_email" class="form-control @error('branch_email') is-invalid @enderror">
                                    @error('branch_email')
                                        {{ $message }}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-4">
                            <a href="{{ route("branches.index") }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        
    </script>
@endsection
@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Branches</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Branch List</h6>
                </div>
                <div class="card-body">
                    <form class="mb-3" action="">
                        <label for="">Search Branch</label>
                        <input type="text" placeholder="Enter branch name or address here ..." class="form-control" name="keyword" value="{{ $keyword }}">

                        @if ($keyword)
                            
                            <div class="mt-2">
                                <a href="{{ route('branches.index') }}">
                                    <i class="fa fa-times"></i> Clear keyword
                                </a>
                            </div>
                        @endif

                    </form>
                    

                    <a href="{{ route('branches.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create New Branch</a>
                    @if ($branches->count())
                        
                    <div class="mt-2">
                        Showing {{ $branches->firstItem() }} to {{ $branches->lastItem() }} of {{ $branches->total() }} branches
                    </div>
                    @endif
                    @foreach ($branches as $branch)
                        <div class="mt-2">
                            <div class="border-bottom">
                                <div class="card border-0">
                                    <div class="card-body">
                                        <div class="row no-gutters">
                                            <div class="col-md-1">
                                                <i class="fa fa-store fa-4x"></i>
                                            </div>
                                            <div class="col-md-8 align-self-center">
                                                <h3>{{ $branch->name }}</h3>
                                                <p>
                                                    {{ $branch->address }}
                                                </p>
                                            </div>
                                            <div class="col-md-3 align-self-center">
                                            <a href="{{ route('branches.view', $branch->id) }}" class="btn btn-block btn-outline-primary">View Branch</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="mt-3">
                        {{ $branches->onEachSide(5)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        
    </script>
@endsection
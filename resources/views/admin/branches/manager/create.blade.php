@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Branch Manager</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Create Branch Manager</h6>
                </div>
                <div class="card-body">
                    <div>
                        <small>FOR BRANCH</small>
                        <h3><i class="fa fa-store fa-fw"></i> {{ $branch->name }}</h3>
                        <hr>
                    </div>

                    <form action="{{ route('branches.managers.save', $branch->id) }}" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">First name</label>
                                <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="Enter first name" required>
                                @error('first_name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">Middle name</label>
                                <input type="text" class="form-control @error('middle_name') is-invalid @enderror" " name="middle_name" value="{{ old('middle_name') }}"  placeholder="Enter middle name (Optional)">
                                @error('middle_name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">Last Name</label>
                                <input type="text" class="form-control @error('last_name') is-invalid @enderror" " name="last_name" value="{{ old('last_name') }}"  placeholder="Enter last name" required>
                                @error('last_name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustomUsername">Username</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                                    </div>
                                    <input type="text" class="form-control @error('username') is-invalid @enderror" " name="username" value="{{ old('username') }}" placeholder="Enter username" aria-describedby="inputGroupPrepend" required>
                                    @error('username')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustomUsername">Email address</label>
                                <div class="input-group">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" " name="email" value="{{ old('email') }}" placeholder="Enter email address" aria-describedby="inputGroupPrepend" required>
                                    @error('email')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustomUsername">Mobile Number</label>
                                <div class="input-group">
                                    <input type="text" class="form-control @error('mobile_number') is-invalid @enderror" " name="mobile_number" value="{{ old('mobile_number') }}" placeholder="e.g. 09XX XXX XXXX" aria-describedby="inputGroupPrepend" required>
                                    @error('mobile_number')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustomUsername">Password</label>
                                <div class="input-group">
                                    <input type="text" class="form-control @error('password') is-invalid @enderror" " value="{{ old('password') }}" name="password" placeholder="" aria-describedby="inputGroupPrepend" required>
                                    @error('password')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-4">
                            <a href="{{ route("branches.view", [$branch->id, 'tab' => 'managers']) }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        
    </script>
@endsection
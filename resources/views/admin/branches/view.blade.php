@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Branches</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">View Branch</h6>
                </div>
                <div class="card-body">
                    <small>#{{ sprintf("%08d", $branch->id) }}</small>
                    <h2>{{ $branch->name }}</h2>
                    
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == "info" ? "active" : "" }}" href="?tab=info">Info</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link {{ $tab == "managers" ? "active" : "" }}" href="?tab=managers">Managers</a>
                        </li>
                        {{-- <li class="nav-item">
                          <a class="nav-link {{ $tab == "earnings" ? "active" : "" }}" href="?tab=earnings">Earnings</a>
                        </li>
                        <li class="nav-item">
                            <a href="?tab=settings" class="nav-link {{ $tab == "settings" ? "active" : "" }}">Settings</a>
                        </li> --}}
                    </ul>
                    
                    @switch($tab)
                        @case("managers")
                            <a href="{{ route('branches.managers.create', $branch->id) }}" class="btn btn-primary mt-3"><i class="fa fa-plus fa-fw"></i> Create New Manager</a>
                            <div class="table-responsive mt-3">
                                <table class="table" id="managersList">
                                    <thead>
                                        <tr>
                                            <th>Manager ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Added at</th>
                                            <th width=160></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($managers as $manager)
                                            <tr data-manager="{{ $manager->id }}">
                                                <td class="align-middle">
                                                    #{{ sprintf("%08d", $manager->id) }}
                                                </td>
                                                <td class="align-middle fname">{{ $manager->user->first_name }}</td>
                                                <td class="align-middle lname">{{ $manager->user->last_name }}</td>
                                                <td class="align-middle">
                                                    {{ $manager->created_at->format('Y-m-d - h:i A') }}
                                                </td>
                                                <td>
                                                    @if ($manager->is_suspended)
                                                        <button class="btn btn-outline-success" onclick="enableManager({{ $manager->id }})">
                                                            <i class="fa fa-check fa-fw"></i> ENABLE
                                                        </button>
                                                    @else
                                                        <button class="btn btn-outline-danger" onclick="removeManager({{ $manager->id }})"><i class="fa fa-times fa-fw"></i> DISABLE</button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" class="text-center">No managers yet.</td>
                                            </tr>
                                        @endforelse
                                        
                                    </tbody>
                                </table>
                            </div>
                            @break
                        @case("earnings")
                            
                            @break
                        @case("settings")
                            
                            @break
                        @default
                            <div class="mt-4">
                                <a href="{{ route('branches.edit', $branch->id) }}" class="btn btn-primary">
                                    <i class="fa fa-edit"></i> Edit 
                                </a>
                            </div>
                            <div class="table-responsive mt-2">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th width=300></th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>Branch Name</th>
                                            <td>{{ $branch->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Address</th>
                                            <td>{{ $branch->address }}</td>
                                        </tr>
                                        <tr>
                                            <th>Contact Numbers</th>
                                            <td>{{ $branch->contact_numbers ?? "None" }}</td>
                                        </tr>
                                        <tr>
                                            <th>Email Address</th>
                                            <td>{{ $branch->email ?? "None" }}</td>
                                        </tr>
                                        <tr>
                                            <th>Created at</th>
                                            <td>{{ $branch->created_at->diffForHumans() }}</td>
                                        </tr>
                                        <tr>
                                            <th>Last modified at</th>
                                            <td>{{ $branch->updated_at->diffForHumans() }}</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                    @endswitch

                    <div class="mt-3">
                        <a href="{{ route('branches.index') }}">&larr; Back to List</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('branches.managers.remove', $branch->id) }}" method="post" id="removeFrm">
        @csrf
        <input type="hidden" name="remove_id" id="remove_id">
    </form
    
    ><form action="{{ route('branches.managers.enable', $branch->id) }}" method="post" id="enableFrm">
        @csrf
        <input type="hidden" name="enable_id" id="enable_id">
    </form>

    <div class="modal fade" id="removeManager" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Disable Manager Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to remove <strong id="removeName"></strong> as manager of <strong>{{ $branch->name }}</strong>?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
              <button type="button" class="btn btn-primary" onclick="continueRemove()">YES</button>
            </div>
          </div>
        </div>
    </div>
    <div class="modal fade" id="enableManager" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Enable Manager Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to enable <strong id="enableName"></strong> as manager of <strong>{{ $branch->name }}</strong>?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
              <button type="button" class="btn btn-primary" onclick="continueEnable()">YES</button>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        removeManager = (id) => {
            let manager = $("#managersList tbody tr[data-manager=" +  id + "]")
            let name = manager.find('td.fname').text() + " " + manager.find('td.lname').text();
            $("#remove_id").val(id)
            $("#removeName").text(name)
            $("#removeManager").modal('show')
        }

        enableManager = (id) => {
            let manager = $("#managersList tbody tr[data-manager=" +  id + "]")
            let name = manager.find('td.fname').text() + " " + manager.find('td.lname').text();
            $("#enable_id").val(id)
            $("#enableName").text(name)
            $("#enableManager").modal('show')
        }

        continueRemove = () => {
            $("#removeFrm").submit()
        }
        
        continueEnable = () => {
            $("#enableFrm").submit()
        }
    </script>
@endsection
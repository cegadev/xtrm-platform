@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Activation Codes</h1>
        <button class="btn btn-primary" onclick="generateCode()"><i class="fa fa-plus fa-fw"></i> Generate Activation Code</button>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header text-primary font-weight-bold">
                    Released Activation Codes this Month
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div>
                                <small>FOR PRODUCT</small>
                            </div>
                            <i class="fa fa-key fa-2x"></i>
                            <span class="h3">{{ $released_codes->where('type', 'product')->count() }}</span>
                        </div>
                        <div class="col">
                            <div>
                                <small>FOR PACKAGE</small>
                            </div>
                            <i class="fa fa-key fa-2x"></i>
                            <span class="h3">{{ $released_codes->where('type', 'package')->count() }}</span>
                        </div>
                        <div class="col">
                            <div>
                                <small>TOTAL</small>
                            </div>
                            <i class="fa fa-key fa-2x"></i>
                            <span class="h3">{{ $released_codes->count() }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Activation Codes</h6>
                </div>
                <div class="card-body">
                   <ul class="nav nav-tabs">
                       <li class="nav-item active">
                           <a href="{{ route('activation-codes.index', ['tab' => 'draft']) }}" class="nav-link {{ $tab == "draft" ? "active" : "" }}">Draft</a>
                       </li>
                       <li class="nav-item active">
                           <a href="{{ route('activation-codes.index', ['tab' => 'released']) }}" class="nav-link {{ $tab == "released" ? "active" : "" }}">Released</a>
                       </li>
                       <li class="nav-item active">
                           <a href="{{ route('activation-codes.index', ['tab' => 'activated']) }}" class="nav-link {{ $tab == "activated" ? "active" : "" }}">Used</a>
                       </li>
                       <li class="nav-item active">
                           <a href="{{ route('activation-codes.index', ['tab' => 'unused']) }}" class="nav-link {{ $tab == "unused" ? "active" : "" }}">Not Used yet</a>
                       </li>
                   </ul>

                   @if ($tab == "draft")
                       <button class="btn btn-info mt-3" disabled id="btn_sendCodes" onclick="sendCode()"><i class="fa fa-paper-plane"></i> Send Codes</button>
                   @endif
                   <div class="mt-2">
                       @if ($activation_codes->count())
                        Showing {{ $activation_codes->firstItem() }} to {{ $activation_codes->lastItem() }} of {{ $activation_codes->total() }} activation codes
                       @endif
                </div>
                   <div class="mt-3">
                        <span>
                            <a href="{{ route('activation-codes.index', ['tab' => $tab, 'type' => 'product']) }}">
                                <i class="fa fa-key fa-fw text-info"></i> Product
                            </a>
                        </span>
                        <span style="margin-left: 1em">
                            <a href="{{ route('activation-codes.index', ['tab' => $tab, 'type' => 'package']) }}">
                                <i class="fa fa-key fa-fw text-primary"></i> Package
                            </a>
                        </span>
                        @if ($type)
                            <div class="mt-2">
                                <small>
                                    <a href="{{ route('activation-codes.index', ['tab' => $tab]) }}"><i class="fa fa-times fa-fw"></i> Clear type Filter</a>
                                </small>
                            </div>
                        @endif
                   </div>
                   
                   <div class="table-responsive mt-3">
                       @switch($tab)
                           @case("draft")
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width=50>
                                            <div class="">
                                                <input type="checkbox" name="chkAll" id="chkAll">
                                                <label for=""></label>
                                            </div>
                                        </th>
                                        <th>Activation Code</th>
                                        <th>Type</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($activation_codes as $code)
                                        <tr>
                                            <td>
                                                <div class="">
                                                    <input type="checkbox" class="chk_code" name="selected_codes[]" data-type="{{ $code->type }}" id="code-{{ $code->id }}" value="{{ $code->id }}">
                                                    <label for="code-{{ $code->id }}"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <i class="fa fa-key fa-fw text-{{ $code->type == "product" ? "info" : "primary" }}"></i> &nbsp;
                                                {{ $code->code }}

                                                @if ($code->status == 'draft' && $code->order_id)
                                                    - <i>requested by <strong>{{ $code->order->requestedBy->username }}</strong></i>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($code->type == "product")
                                                    Product
                                                @else 
                                                    Package - {{ $code->package ? $code->package->name : "Invalid"}}
                                                @endif
                                            </td>
                                            
                                            <td>{{ $code->created_at->format("Y-m-d") }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4">No activation codes to display.</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                               
                               @break
                           @case("released")
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Activation Code</th>
                                            <th>Type</th>
                                            <th>Released at</th>
                                            <th>Released to</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($activation_codes as $code)
                                            <tr>
                                                <td>
                                                    <i class="fa fa-key fa-fw text-{{ $code->type == "product" ? "info" : "primary" }}"></i> &nbsp;
                                                    {{ $code->code }}
                                                </td>
                                                <td>
                                                    @if ($code->type == "product")
                                                        Product
                                                    @else 
                                                        Package - {{ $code->package ? $code->package->name : "Invalid"}}
                                                    @endif
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($code->released_at)->format("Y-m-d") }}</td>
                                                <td>
                                                    {{ $code->releasedToUser->first_name }}
                                                    {{ $code->releasedToUser->last_name }}
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4">No activation codes to display.</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                               @break

                            @case("activated")
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Activation Code</th>
                                            <th>Type</th>
                                            <th>Released to</th>
                                            <th>Activated By</th>
                                            <th>Activated At</th>
                                            <th>Registered To</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($activation_codes as $code)
                                            <tr>
                                                <td>
                                                    <i class="fa fa-key fa-fw text-{{ $code->type == "product" ? "info" : "primary" }}"></i> &nbsp;
                                                    {{ $code->code }}
                                                </td>
                                                <td>
                                                    @if ($code->type == "product")
                                                        Product
                                                    @else 
                                                        Package - {{ $code->package->name }}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $code->releasedToUser->first_name }}
                                                    {{ $code->releasedToUser->last_name }}
                                                </td>
                                                <td>
                                                    {{ $code->activatedByUser->first_name }}
                                                    {{ $code->activatedByUser->last_name }}
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($code->activated_at)->format("Y-m-d") }}</td>
                                                <td>
                                                    @if ($code->type == "product")
                                                        N/A
                                                    @else
                                                        {{ $code->registeredToUser->first_name }}
                                                        {{ $code->registeredToUser->last_name }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4">No activation codes to display.</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                @break

                            @case("unused")
                            <table class="table">
                                <thead>
                                        <tr>
                                            <th>Activation Code</th>
                                            <th>Type</th>
                                            <th>Received at</th>
                                            <th>Received by</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($activation_codes as $code)
                                            <tr>
                                                <td>
                                                    <i class="fa fa-key fa-fw text-{{ $code->type == "product" ? "info" : "primary" }}"></i> &nbsp;
                                                    {{ $code->code }}
                                                </td>
                                                <td>
                                                    @if ($code->type == "product")
                                                        Product
                                                    @else 
                                                        Package - {{ $code->package->name }}
                                                    @endif
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($code->released_at)->format("Y-m-d") }}</td>
                                                <td>
                                                    {{ $code->releasedToUser->first_name }}
                                                    {{ $code->releasedToUser->last_name }}
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4">No activation codes to display.</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                                @break
                           @default
                               
                       @endswitch
                   </div>
                {{ $activation_codes->onEachSide(5)->links() }}
                   
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Activation codes <> Items for activation</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="pending_activation_codes">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Needed activation codes</th>
                                    <th width=200></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="neededCodes" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">User needed codes</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="font-weight-bold mb-3">
                    <i class="fa fa-user fa-fw"></i> <span  id="viewNeededUsername" ></span>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th width=150>Needed Count</th>
                            </tr>
                        </thead>
                        <tbody id="itemsList"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
            </div>
          </div>
        </div>
      </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="generateCode">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Generate Activation Code</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('activation-codes.generate') }}" id="frmGenerate" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="type">Product Code For</label>
                        <select name="type" id="type" class="form-control" required>
                            <option value="" disabled selected>Choose Type</option>
                            <option value="product">Product</option>
                            <option value="package">Package</option>
                        </select>
                    </div>
                    <div class="form-group" id="selectPackage">
                        <label for="">Select Package</label>
                        <select name="package_id" id="package_id" class="form-control" id="">
                            @foreach ($packages as $package)
                                <option value="{{ $package->id }}">{{ $package->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Quantity</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" value="1" min="1" max="2000">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="saveCode()">Save changes</button>
            </div>
          </div>
        </div>
      </div>
      
      <div class="modal fade" tabindex="-1" role="dialog" id="sendCode" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Send Activation Codes To</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form onsubmit="return false" id="frmSendCode" method="POST">
                    @csrf
                    <div style="display: none" class="alert alert-danger" id="invalidSend">
                        <ul></ul>
                    </div>
                    <input type="hidden" name="activation_codes" id="activation_codes">
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <div><small>PRODUCT</small></div>
                                <div>
                                    <i class="fa fa-key fa-fw"></i>
                                    <span id="selected_product_code_count">1</span>
                                </div>
                            </div>
                            <div class="col">
                                <div><small>PACKAGE</small></div>
                                <div>
                                    <i class="fa fa-key fa-fw"></i>
                                    <span id="selected_package_code_count">1</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Enter member username</label>
                        <input type="text" class="form-control" name="sendto_member_username" id="sendto_member_username" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="sendActivationCodes()" id="send_activation">
                    <span id="send_default">Send</span>
                    <span id="send_loading" style="display: none">
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </span>
                </button>
            </div>
          </div>
        </div>
      </div>


      @endsection

@section('scripts')
    <script>
        let generateCode = () => {
            $("#generateCode").modal('show')
            $("#selectPackage").hide()
            $("#selectPackage option:first-child").prop('selected', true);
        }

        $("#type").on('change click', (data) => {
            let value = data.target.value
            if (value == "package") {
                $("#selectPackage").show()
                $("#selectPackage option:first-child").prop('selected', true);
            } else {
                $("#selectPackage").hide()
            }
        })

        saveCode = () => {
            $("#frmGenerate").submit()
        }

        $("#chkAll").on('change click', (data) => {
            $(".chk_code").prop('checked', data.target.checked)
            checkSelected()
        })

        $(".chk_code").on('change click', () => {
            checkSelected()
        })
        let selected;
        checkSelected = () => {
            selected = [];
            $(".chk_code:checked").each((key, data) => {
                selected.push({
                    value: data.value,
                    type: $(data).attr('data-type'),
                })
            })

            let codes_length = $(".chk_code:checked").length
            if (codes_length) {
                $("#btn_sendCodes").prop("disabled", false)
            } else {
                $("#btn_sendCodes").prop("disabled", true)
            }
        }

        sendCode = () => {
            let count = {
                product: selected.filter(s => s.type == 'product').length,
                package: selected.filter(s => s.type == 'package').length,
            }

            $("#invalidSend").hide();
            $("#sendto_member_username").removeClass('is-invalid')
            if (selected.length) { 
                $("#activation_codes").val(selected.map(s => s.value).join(","))
                $("#selected_product_code_count").text(count.product)
                $("#selected_package_code_count").text(count.package)
                $("#sendCode").modal('show')
            }
        }

        sendActivationCodes = () => {
            $("#send_activation").prop("disabled", true)
            $("#send_default").hide()
            $("#send_loading").show()
            $("#sendto_member_username").removeClass('is-invalid')
            $("#invalidSend").hide();
            $.ajax({
                url: "{{ route('activation-codes.send-activation-codes') }}",
                data: {
                    sendto_member_username: document.getElementById("sendto_member_username").value,
                    activation_codes: selected.map(s => s.value),
                    _token: "{{ csrf_token() }}"
                },
                method: "POST",
                complete: (xhr, textStatus) => {
                    let data = xhr.responseJSON;
                    if (textStatus == "error") {
                        if (xhr.status == 422) {
                            let str_error = ""
                            let errors = data.errors;
                            Object.keys(errors).forEach((key) => {
                                let element = $("#" + key)
                                errors[key].forEach(e => {
                                    str_error += `<li>${e}</li>`
                                })
                                element.addClass('is-invalid')
                            })

                            $("#invalidSend").html(str_error).show()
                        } 

                        if (xhr.status == 500) {
                            alert('Something went wrong. Please try again later.')
                        }

                        $("#send_activation").prop("disabled", false)
                        $("#send_default").show()
                        $("#send_loading").hide()
                    } else {
                        setInterval(() => {
                            window.location = "{{ route('activation-codes.after-send-activation-code') }}"
                            // $("#send_activation").prop("disabled", false)
                            // $("#send_default").show()
                            // $("#send_loading").hide()
                        }, 2000)
                    }
                }
            })
        }

    </script>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

    <script>
        let orderItems = [];
        let packages = [];
        loadPending = () => {
            $.ajax({
                url: "/api/activation-code/pending-users",
                complete: function(xhr, textStatus) {
                    let data = xhr.responseJSON;
                    orderItems = data.data;
                    packages = data.packages
                    loadTable(orderItems);
                }
            })
        }

        loadTable = (items) => {
            $("#pending_activation_codes").DataTable({
                data: items,
                "columns": [
                    { "data": "username" },
                    { "data": "needed" },
                    {
                        "mRender": function(data, type, full) {
                            return "<button class='btn btn-primary' onclick='viewNeeded(this, "+ full.id +")' data-username=\""+ full.username +"\">View Needed</button>"
                        }
                    }
                ]
            })


        }

        loadPending()

        viewNeeded = (btn, id) => {
            let username = $(btn).data('username')
            $("#viewNeededUsername").text(username)

            let user_orders = orderItems.filter((i) => {
                return i.id == id
            })[0].data
            let uo_product = user_orders.product.orders - user_orders.product.released;
            
            let strItems = "";
            let strPackages = "";
            
            let prd_addedClass = uo_product > 0 ? 'table-danger font-weight-bold' : ''
            strItems = `<tr>
                            <td>
                                <strong><i class="fa fa-cube fa-fw"></i> PRODUCT</strong>
                            </td>
                            <td class="text-center ${prd_addedClass}">${uo_product}</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong><i class="fa fa-cubes fa-fw"></i> PACKAGE</strong>   
                            </td>
                        </tr>`

            

            let uo_packages = user_orders.packages.map((p) => {
                return {
                    id: p.id,
                    needed: p.orders - p.released
                }
            })

            packages.forEach((p) => {
                let needed = uo_packages.filter((_p) => {
                    return _p.id == p.id;
                }).map((_p) => {
                    return _p.needed
                })[0]
                let addedClass = needed > 0 ? 'table-danger font-weight-bold' : ''
                strItems += `<tr>
                                <td style="padding-left: 2em">${p.name}</td>
                                <td class="text-center ${addedClass}">${needed}</td>
                            </tr>`
            })

            

            $("#itemsList").html(strItems);
            $("#neededCodes").modal('show')

        }
    </script>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    
@endsection
@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="mb-4">
        <h1 class="h3 mb-0 text-gray-800">Activation Histories</h1>
    </div>
    <div class="card mb-3">
        <div class="card-body">
            <form action="">
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Enter username here ..." value="{{ old('username', $username) }}">
                </div>
                <button class="btn btn-primary"><i class="fa fa-search fa-fw"></i> Find user</button>
                @if ($username)
                    <a href="{{ route('activation-codes.histories') }}" class="btn btn-outline-secondary">Clear</a>
                @endif
            </form>
        </div>
    </div>

    @if ($showHistory)
        @if (!empty($selectedUser->membership))
            <div class="mb-3">
                <a href="{{ route('admin.members.member-earnings', $selectedUser->membership->id) }}" class="btn btn-outline-primary">
                    <i class="fa fa-wallet fa-fw"></i> View Member Earnings</a>
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs mb-3">
                    <li class="nav-item">
                        <a class="nav-link {{ $type == 'product' ? 'active' : '' }}" href="{{ route('activation-codes.histories', ['username' => $username, 'type' => 'product']) }}">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $type == 'package' ? 'active' : '' }}" href="{{ route('activation-codes.histories', ['username' => $username, 'type' => 'package']) }}">Package</a>
                    </li>
                </ul>
                @if ($selectedUser)
                    <div>
                        <div class="small">SELECTED USER</div>
                        <div class="h5 font-weight-bold"><i class="fa fa-user fa-fw"></i> {{ $selectedUser->first_name }} {{ $selectedUser->last_name }}</div>
                    </div>
                @endif
                @if ($items->count())
                    <div class="mb-3">
                        Showing {{ $items->firstItem() }} to {{ $items->lastItem() }} of {{ $items->total() }} items
                    </div>
                @endif

                <div class="table-responsive mb-3">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Activated At</th>
                                <th>Transferred by</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($items as $item)
                                <tr>
                                    <td>{{ $item->product_name }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->activated_at)->format('F j, Y h:i A') }}</td>
                                    <td>{{ $item->transferred_from_user }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">No items to display.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div>
                    {{ $items->links() }}
                </div>
            </div>
        </div>
    @else
        <div class="card">
            <div class="card-body">
                <i class="fa fa-list fa-fw"></i> Select a user to show activation history.
            </div>
        </div>
    @endif
@endsection
@section('scripts')

@endsection

@section('styles')
    
@endsection
@extends('layouts.admin')
@section('page-name')
    - Sales Reports
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.bootstrap4.min.css">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <small>REPORTS</small>
            <h1 class="h3 mb-0 text-gray-800"> Sales</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-filter fa-fw"></i>
                        Filter Report</h6>
                </div>
                <div class="card-body align-middle">
                    <form action="" method="get">
                        <label for="">Product Type</label>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="product_type" id="pt_all" value="all" {{ $product_type == "all" ? "checked" : "" }}>
                                <label class="form-check-label" for="pt_all">All</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="product_type" id="pt_product" value="product" {{ $product_type == "product" ? "checked" : "" }}>
                                <label class="form-check-label" for="pt_product">Product</label>
                              </div>
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="product_type" id="pt_package" value="package" {{ $product_type == "package" ? "checked" : "" }}>
                                <label class="form-check-label" for="pt_package">Package</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Choose Item</label>
                            <select name="product_id" id="product_id" class="form-control">
                                <option value="all">All</option>
                                @foreach ($products as $product)
                                    <option {{ $product_id == $product->id ? "selected" : "" }} value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Date Range</label>
                            <input type="text" name="date_range" id="date_range" value="{{ $selected_date_range }}" class="form-control">
                        </div>
                        <button class="btn btn-warning btn-block">Generate Report</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">
                        <i class="fa fa-chart-bar fa-fw"></i>
                        Sales Chart ({{ $selected_date_range }}) </h6>
                </div>
                <div class="card-body align-middle">
                    <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">
                <i class="fa fa-chart-line fa-fw"></i>
                Sales Reports</h6>
        </div>
        <div class="card-body align-middle">
            <div class="table-responsive" id="salesTable_wrapper">
                <table class="table mt-2" id="salesTable">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>From</th>
                            <th>Notes</th>
                            <th>Transacted by</th>
                            <th>Transaction Date</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('sb-admin/vendor/chart.js/Chart.min.js') }}"></script>
<script>

let product_type = "{{ $product_type ?? null}}";
let product_id = "{{ $product_id ?? null}}";
let date_range = "{{ $selected_date_range }}";
// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

showBarChart = (labels, data) => {

    var ctx = document.getElementById("myBarChart");

    var myBarChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels,
        datasets: [{
        label: "Sales",
        backgroundColor: "#4e73df",
        hoverBackgroundColor: "#2e59d9",
        borderColor: "#4e73df",
        data,
        }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
        padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
        },
        scales: {
        yAxes: [{
            time: {
            unit: 'month'
            },
            gridLines: {
            display: false,
            drawBorder: false
            },
            // ticks: {
            // maxTicksLimit: 6
            // },
            maxBarThickness: 25,
        }],
        xAxes: [{
            ticks: {
            min: 0,
            maxTicksLimit: 5,
            padding: 10,
            // Include a dollar sign in the ticks
            callback: function(value, index, values) {
                return 'P' + number_format(value);
            }
            },
            gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
            }
        }],
        },
        legend: {
        display: false
        },
        tooltips: {
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
        callbacks: {
            label: function(tooltipItem, chart) {
            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
            return datasetLabel + ': P' + number_format(tooltipItem.xLabel);
            }
        }
        },
    }
    });
}

$.get("{{ route('api-reports.sales-chart') }}", {
    product_type,
    date_range,
}, (data) => {
    let labels = data.map(d => d.name)
    let values = data.map(d => d.earning)

    showBarChart(labels, values)
})
</script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    $("#date_range").flatpickr({
        mode: "range",
        dateFormat: "M j, Y",
    })

    let products_list = [];

    $.get("{{ route('api-products.products-list') }}", {}, (data) => {
        var a_products = data.filter(p => p.type == "product")
        var a_packages = data.filter(p => p.type == "package")
        products_list = a_products.concat(a_packages)

        if (product_id != "all") {
            getProductsByType(product_type)
        }
    }, "json")

    $("input[name=product_type]").on('change', (data) => {
        getProductsByType(data.target.value)
    })

    getProductsByType = (type) => {
        let options = "";
        let selected_products;
        if (type == "all") {
            selected_products = products_list
        } else {
            selected_products = products_list.filter(p => p.type == type);
        }

        options += "<option value='all'>All</option>"

        selected_products.forEach(p => {
            selected = product_id == p.id ? "selected" : ""

            options += `<option value='${p.id}' ${selected}>${p.name}</option>`;
        })

        $("#product_id").html(options)
    }
</script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>


<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>
<script>
    loadTable = () => {
        $("#salesTable").DataTable({
            "processing": false,
            "serverSide": false,
            "ajax": {
                "url": "/api/reports/sales-data",
                "data": {
                    product_id,
                    product_type,
                    date_range,
                    excel: true
                },
            },
            "columns": [
                { "data": "reference_number" },
                { "data": "from" },
                { "data": "notes" },
                { "data": "transacted_by" },
                { "data": "transaction_date" },
                { "data": "amount" },
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ],
            "initComplete": () => {
                $(".dt-button").addClass('btn btn-outline-primary').removeClass('dt-button')
            }
        })
    }

    loadTable();
    downloadReport = () => {

    }
</script>
@endsection
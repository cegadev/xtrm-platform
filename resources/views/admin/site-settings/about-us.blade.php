@extends('layouts.admin')

@section('styles')
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>ABOUT US</small></h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-body p-4">
                    @php
                        $tabs = ["Content", "Right Image"];
                    @endphp
                    <ul class="nav nav-tabs mb-4">
                        @foreach ($tabs as $tab)
                            @php
                                $key = \Str::slug($tab)
                            @endphp
                            <li class="nav-item">
                                <a class="nav-link {{ $key == $current_tab ? "active" : "" }}" href="?tab={{ $key }}">{{ $tab }}</a>
                            </li>
                        @endforeach
                    </ul>

                    @if ($current_tab == "content")
                        <form action="{{ route('site-settings.about-us.update', ['tab' => 'content']) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method("POST")
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="">About Us details</label>
                                    <textarea tabindex="5" id="summernote" name="details">{{ old('details') ?? $about_us->data }}</textarea>
                                </div>
                            </div>
                            <div>
                                <a href="{{ route('site-settings.index') }}" class="btn btn-secondary">Cancel</a>
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    @endif

                    @if ($current_tab == "right-image")
                        <form action="{{ route('site-settings.about-us.update', ['tab' => 'right-image']) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method("POST")
                            <div class="form-group">
                                <label for="sitelogo">Right Image</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <div id="preview" class="form-row">
                                <div class="col-4">
                                    <label for="">Preview</label>
                                    <div class="card">
                                        <img src="{{ $about_us->data }}" id="sitelogo-preview" class="card-img-top" alt="Site logo">
                                    </div>
                                </div>
                            </div>
                            <div class="mt-3">
                                <a href="{{ route('site-settings.index') }}" class="btn btn-secondary">Cancel</a>
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    @endif

                    



                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
@if ($current_tab == "content")
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter package description here ...',
                tabsize: 2,
                height: 300
            });
        });
    </script>
@endif

@if ($current_tab == "right-image")
    <script>
         $("#customFile").on('change', function(e) {
            // get the file name
            let fileName = e.target.files[0].name

            // replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);

            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#sitelogo-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endif
@endsection
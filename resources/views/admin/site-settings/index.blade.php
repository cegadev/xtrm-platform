@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-body p-4">
                    <h4>General</h4>

                    <div class="list-group mt-3 mb-5">
                        <a href="{{ route('site-settings.site-logo.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Site Logo</a>
                        <a href="{{ route('site-settings.site-logo.icon') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Site Icon</a>
                    </div>


                    <h4>Homepage</h4>
                    <div class="list-group mt-3 mb-5">
                        <a href="{{ route('site-settings.slider.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Homepage Slider</a>
                        <a href="{{ route('site-settings.about-us.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> About Us Blocks</a>
                        <a href="{{ route('site-settings.packages.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Packages Sorting</a>
                        <a href="{{ route('site-settings.products.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Products Backgrounds Blocks</a>
                        <a href="{{ route('site-settings.backgrounds.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Backgrounds</a>
                    </div>

                    <h4>Pages</h4>
                    <div class="list-group mt-3 mb-5">
                        <a href="{{ route('site-settings.pages.about-us.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> About Us</a>
                        <a href="{{ route('site-settings.pages.ways-of-earning.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Ways of Earning</a>
                        <a href="{{ route('site-settings.pages.contact-us.index') }}" class="list-group-item list-group-item-action"><i class="fa fa-pen fa-fw"></i> Contact Us</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        
    </script>
@endsection
@extends('layouts.admin')

@section('styles')
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>HOMEPAGE PACKAGES SORTING</small></h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-body p-4">
                    <p>Drag items to change sorting.</p>
                    <ul class="list-group mb-3" id="sortList">
                        @foreach ($packages as $item)
                            <div class="list-group-item sortListItem" role="button" data-item="{{ $item->id }}" data-priority="{{ $item->homepage_priority ?? 0 }}"><i class="fa fa-sort fa-fw"></i> {{ $item->name }} <span style="float: right">(P {{ number_format($item->price, 2) }})</span></div>
                        @endforeach
                    </ul>
                    <button class="btn btn-secondary action-btns disabled" disabled id="reset">Reset</button>
                    <button class="btn btn-primary action-btns disabled" disabled id="saveChanges">Save Changes</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.13.3/underscore-min.js" integrity="sha512-01+8Rdvg1kM8Bu9AJTtDhyyuZmJHOjD1+fxA9S1sh3A7NyRFHpVCqt5vT7JqMxCTf/oEJIPOzYnc0R8l052+UA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"></script>
<script>
    $('#sortList').sortable({
        change: () => {
            $('.action-btns').removeClass('disabled')
            $('.action-btns').prop('disabled', false)
            $('#saveChanges').off('click')
                .on('click', function () {
                    saveOrder($(this))
                })
            $('#reset').off('click')
                .on('click', function () {
                    resetOrder()
                })
        }
    })

    function saveOrder(btn) {
        let items = $('.sortListItem');
        let order = [];
        items.each(function () {
            order.push($(this).data('item'));
        });

        $(btn).parent().find('.btn').addClass('disabled');
        $(btn).text('Saving changes ...');

        $.post("{{ route('site-settings.packages.save-sorting') }}", {
            _token: "{{ csrf_token() }}",
            ids: order 
        }, function (res) {
            alert(res.message)
            window.location.reload()
        });
    }

    function resetOrder() {
        let items = $('.sortListItem')
        
        let current = items.map(function () {
            return {
                'priority' : $(this).data('priority'),
                'item' : $(this).get(0).outerHTML
            }
        })

        original = _.sortBy(current, 'priority').map(i => i.item)
        let strList = ""
        original.forEach(item => {
            strList += item
        });

        $('#sortList').html(strList)
        $('.action-btns').addClass('disabled')
        $('.action-btns').off('click')

    }
</script>
@endsection
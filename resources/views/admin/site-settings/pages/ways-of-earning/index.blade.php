@extends('layouts.admin')

@section('styles')
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <style>
        .sortListItem:hover {
            background-color: #eee
        }
    </style>
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>Ways of Earning</small></h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-edit fa-fw"></i> Ways of Earning</h6>
                </div>
                <div class="card-body p-4">

                    @role('Admin')
                        <a href="{{ route('site-settings.pages.ways-of-earning.create') }}" class="btn btn-primary shadow-sm"><i class="fa fa-plus"></i> Add New Record</a>
                        <button id="btnEdit" class="btn btn-primary shadow-sm" disabled><i class="fa fa-edit"></i> Edit</button>
                        <button id="btnDelete" class="btn btn-primary shadow-sm" disabled><i class="fa fa-trash"></i> Delete</button>
                        <button id="btnSort" data-toggle="modal" data-target="#sortModal" class="btn btn-primary shadow-sm"><i class="fa fa-sort fa-fw"></i> Sort Items</button>
                    @endrole
                    <table class="table mt-3">
                        <thead>
                            <tr>
                                <th width=50>
                                    <div>
                                        <div class="form-check">
                                            <input type="checkbox" name="" id="productsCheckAll">
                                        </div>
                                    </div>
                                </th>
                                <th>Title</th>
                            </tr>
                        </thead>
                        <tbody id="productsList">
                            @foreach ($ways_of_earning as $earning)
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            <input type="checkbox" name="product-item" id="" value="{{ $earning->id }}" class="productCheckBox">
                                        </div>
                                    </td>
                                    <td>{{ $earning->title }} <span class="badge badge-secondary" style="float: right">{{ $earning->priority ?? 0 }}</span></td>
                                </tr>
                            @endforeach

                            @if ($ways_of_earning->count() == 0)
                                <tr>
                                    <td colspan="3" class="text-center">No records available yet.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <form action="{{ route('site-settings.pages.ways-of-earning.delete') }}" method="POST" id="frmDelete">
        @csrf
        @method("DELETE")
        <input type="hidden" name="ids" id="delete_ids" required>
    </form>
    <div class="modal" tabindex="-1" role="dialog" id="sortModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Sort Items</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Drag items to change sorting.</p>
                <ul class="list-group" id="sortList">
                    @foreach ($ways_of_earning as $item)
                        <div class="list-group-item sortListItem" role="button" data-item="{{ $item->id }}" data-priority="{{ $item->priority ?? 0 }}"><i class="fa fa-sort fa-fw"></i> {{ $item->title }}</div>
                    @endforeach
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary action-btns"  data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary action-btns" id="saveSortingBtn">Save changes</button>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.13.3/underscore-min.js" integrity="sha512-01+8Rdvg1kM8Bu9AJTtDhyyuZmJHOjD1+fxA9S1sh3A7NyRFHpVCqt5vT7JqMxCTf/oEJIPOzYnc0R8l052+UA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"></script>
<script>
    $("#productsCheckAll").on('change', (data) => {
        $("#productsList tr td input[type=checkbox]").prop('checked', data.target.checked)
        triggerChecked()
    })

    $("#productsList tr td input[type=checkbox]").on('change', (data) => {
        triggerChecked()
    })

    triggerChecked = () => {
        let checkedCount = $(".productCheckBox:checked").length;
        if (checkedCount == 0) {
            $("#btnEdit").prop('disabled', true)
            $("#btnDelete").prop('disabled', true)
        } else {
            if (checkedCount == 1) {
                $("#btnEdit").prop('disabled', false)
                $("#btnDelete").prop('disabled', false)
            } else {
                $("#btnEdit").prop('disabled', true)
                $("#btnDelete").prop('disabled', false)
            }
        }
    }

    $("#btnEdit").on('click', (data) => {
        if (data.target.disabled == false) {
            let selectedProduct = $(".productCheckBox:checked")[0];
            if (selectedProduct.value) {
                window.open("/admin/site-settings/pages/ways-of-earning/edit/" + selectedProduct.value, "_self");
            }
        }
    })


    $("#btnDelete").on('click', (data) => {
        if (data.target.disabled == false) {
            ids = [];
            $("input[name=product-item]:checked").each(function() {
                ids.push($(this).val())
            })

            if (ids.length > 0) {
                $("#delete_ids").val(ids.join(","))
                $("#frmDelete").submit();
            }
        }
    });

    $('#sortList').sortable({
        change: () => {
            $('.action-btns').prop('disabled', false).removeClass('disabled')
            $('#saveSortingBtn').off('click')
                .on('click', function () {
                    saveOrder($(this));
                })
        }
    })

    function saveOrder(btn) {
        let items = $('.sortListItem');
        let order = [];
        items.each(function () {
            order.push($(this).data('item'));
        });

        $(btn).parent().find('.btn').addClass('disabled').prop('disabled');
        $(btn).text('Saving changes ...');

        $.post("{{ route('site-settings.pages.ways-of-earning.save-sorting') }}", {
            _token: "{{ csrf_token() }}",
            ids: order 
        }, function (res) {
            alert(res.message)
            window.location.reload()
        });
    }

    function resetOrder() {
        let items = $('.sortListItem')
        
        let current = items.map(function () {
            return {
                'priority' : $(this).data('priority'),
                'item' : $(this).get(0).outerHTML
            }
        })

        original = _.sortBy(current, 'priority').map(i => i.item)
        let strList = ""
        original.forEach(item => {
            strList += item
        });

        $('#sortList').html(strList)
    }


    $('#sortModal').on('hide.bs.modal', function (e) {
        resetOrder()
    })
    
    $('#sortModal').on('show.bs.modal', function (e) {
        $('.action-btns').prop('disabled', true).addClass('disabled')
    })
</script>
@endsection
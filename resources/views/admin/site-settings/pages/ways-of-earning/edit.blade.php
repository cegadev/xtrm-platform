@extends('layouts.admin')

@section('styles')
    
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Ways of Earning</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <form method="POST" action="{{ route('site-settings.pages.ways-of-earning.update', $data->id) }}" novalidate>
                        @csrf
                        @method('POST')
                        <div class="form-row">
                            <div class="col-md-8 mb-3">
                                <label for="woe_title">Title</label>
                                <input tabindex="1" id="woe_title" type="text" class="form-control @error('woe_title') is-invalid @enderror" name="woe_title" value="{{ old('woe_title') ?? $data->title }}" placeholder="Enter title here" required>
                                @error('woe_title')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="">Description</label>
                                <textarea tabindex="5" id="summernote" name="description">{{ old('description') ?? $data->description }}</textarea>
                            </div>
                        </div>
                        <div>
                            <a href="{{ route('site-settings.pages.ways-of-earning.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter product description here ...',
                tabsize: 2,
                height: 200,
            });

            $('#summernote').summernote('fontName', 'Arial')
        });

    </script>
@endsection
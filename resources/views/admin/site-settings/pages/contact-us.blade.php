@extends('layouts.admin')

@section('styles')
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>Contact Us</small></h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-edit fa-fw"></i> Contact Us</h6>
                </div>
                <div class="card-body p-4">
                    <form action="{{ route('site-settings.pages.contact-us.update') }}" method="POST">
                        @csrf
                        @method("POST")
                        <div class="form-group">
                            <label for="">Email address * ~ add comma (,) for multiple email addresses ~</label>
                            <input type="text" class="form-control  @error('contact-info-emails') is-invalid @enderror" name="contact-info-emails" value="{{ old('contact-info-emails') ?? $emails }}">
                            @error('contact-info-emails')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Mobile number * ~ add comma (,) for multiple mobile numbers ~</label>
                            <input type="text" class="form-control  @error('contact-info-mobile-numbers') is-invalid @enderror" name="contact-info-mobile-numbers" value="{{ old('contact-info-mobile-numbers') ?? $mobile_numbers }}">
                            @error('contact-info-mobile-numbers')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Phone number * ~ add comma (,) for multiple phone numbers ~</label>
                            <input type="text" class="form-control  @error('contact-info-phone-numbers') is-invalid @enderror" name="contact-info-phone-numbers" value="{{ old('contact-info-phone-numbers') ?? $phone_numbers }}">
                            @error('contact-info-phone-numbers')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Address *</label>
                            <input type="text" class="form-control  @error('contact-info-address') is-invalid @enderror" name="contact-info-address" value="{{ old('contact-info-address') ?? $address }}">
                            @error('contact-info-address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <a href="{{ route('site-settings.index') }}" class="btn btn-secondary">Cancel</a>
                        <button class="btn btn-primary">Save Changes</button>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter package description here ...',
                tabsize: 2,
                height: 500
            });
        });
</script>

@endsection
@extends('layouts.admin')

@section('styles')
    <!-- include summernote css/js -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs4.min.css" integrity="sha512-ngQ4IGzHQ3s/Hh8kMyG4FC74wzitukRMIcTOoKT3EyzFZCILOPF0twiXOQn75eDINUfKBYmzYn2AA8DkAk8veQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>About Us</small></h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-edit fa-fw"></i> About Us</h6>
                </div>
                <div class="card-body p-4">
                    <form action="{{ route('site-settings.pages.about-us.update') }}" method="POST">
                        @csrf
                        @method("POST")
                        <label for="">Details *</label>
                        <textarea tabindex="5" id="summernote" name="details">{{ old('details') ?? $about_us->data }}</textarea>
                        <div class="mt-3">
                            <a href="{{ route('site-settings.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs4.min.js" integrity="sha512-ZESy0bnJYbtgTNGlAD+C2hIZCt4jKGF41T5jZnIXy4oP8CQqcrBGWyxNP16z70z/5Xy6TS/nUZ026WmvOcjNIQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter package description here ...',
                tabsize: 2,
                height: 500,
                callbacks: {
                    onImageUpload: function (image, editor, welEditable) {
                        editor = $(this)
                        uploadImageContent(image[0], editor, welEditable)
                    }
                }
            });

            function uploadImageContent(image, editor, welEditable) {

                let data = new FormData;
                data.append('image', image)
                data.append('_token', "{{ csrf_token() }}")

                let xml = new XMLHttpRequest
                xml.onreadystatechange = function () {
                    if (xml.readyState == 4 && xml.status == 200) {
                        let url = xml.responseText
                        $(editor).summernote('insertImage', url);
                    }
                }
                xml.open("POST", "{{ route('site-settings.pages.about-us.insert-image') }}", true)

                xml.send(data)
            }
        });

</script>

@endsection
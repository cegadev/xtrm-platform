@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>HOMEPAGE SLIDER</small></h1>
    </div>

    <!-- Content Row -->
    {{-- @foreach ($sliders as $key => $slider) --}}
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="card shadow h-100 py-2 mb-3">
                    <div class="card-body p-4">

                        <ul class="nav nav-tabs mb-4">
                            @for ($i = 0; $i < $count; $i++)
                                @php
                                    $index = $i + 1;
                                @endphp
                                <li class="nav-item">
                                    <a class="nav-link {{ $key == $index ? "active" : "" }}" href="?key={{ $index }}">Slide {{ $index }} </a>
                                </li>
                            @endfor
                        </ul>

                        <form action="{{ route('site-settings.slider.update', ['key' => $key]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method("POST")
                            <div class="form-group">
                                <label for="sitelogo">Title</label>
                                <input type="text" class="form-control" name="title" placeholder="Enter Title here ... " value="{{ $slider->data->title }}">
                                @error('title')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="sitelogo">Sub title</label>
                                <input type="text" class="form-control" name="subtitle" placeholder="Enter Sub title here ... " value="{{ $slider->data->sub_title }}">
                                @error('subtitle')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="sitelogo">Action (URL)</label>
                                <input type="text" class="form-control" name="action" placeholder="Enter URL here ... " value="{{ $slider->data->action }}">
                                @error('action')
                                    <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="sitelogo">Choose Background Image</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <div id="preview" class="form-row">
                                <label for="">Background Image Preview</label>
                                <div class="card">
                                    <img src="{{ asset($slider->data->image) }}" id="sitelogo-preview" class="card-img-top" alt="Site logo">
                                </div>
                            </div>
                            <div class="mt-3">
                                <a href="{{ route('site-settings.index') }}" class="btn btn-secondary">Cancel</a>
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    {{-- @endforeach --}}

@endsection

@section('scripts')
    <script>
        $("#customFile").on('change', function(e) {
                    // get the file name
                    let fileName = e.target.files[0].name

                    // replace the "Choose a file" label
                    $(this).next('.custom-file-label').html(fileName);

                    readURL(this);
                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#sitelogo-preview').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

    </script>
@endsection
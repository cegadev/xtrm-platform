@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>SITE LOGO</small></h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-body p-4">
                    <form action="{{ route('site-settings.site-logo.update') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method("POST")
                        <div class="form-group">
                            <label for="sitelogo">Site logo</label>
                            {{-- <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div> --}}
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                        <div id="preview" class="form-row">
                            <div class="col-3">
                                <label for="">Preview</label>
                                <div class="card" style="width: 18rem;">
                                    <img src="{{ $sitelogo->data }}" id="sitelogo-preview" class="card-img-top" alt="Site logo">
                                </div>
                            </div>
                        </div>
                        <div class="mt-3">
                            <a href="{{ route('site-settings.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $("#customFile").on('change', function(e) {
            // get the file name
            let fileName = e.target.files[0].name

            // replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);

            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#sitelogo-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endsection
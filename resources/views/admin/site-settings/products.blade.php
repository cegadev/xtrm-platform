@extends('layouts.admin')

@section('styles')
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>HOMEPAGE PRODUCTS</small></h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-body p-4">
                    <ul class="nav nav-tabs mb-3">
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'home-block-1' ? 'active' : '' }}" aria-current="page" href="?tab=home-block-1">Block 1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'home-block-2' ? 'active' : '' }}" href="?tab=home-block-2">Block 2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $tab == 'home-block-3' ? 'active' : '' }}" href="?tab=home-block-3">Block 3</a>
                        </li>
                    </ul>

                    <form action="{{ route('site-settings.products.save-products') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('post')
                        <input type="hidden" name="field" value="{{ $tab }}">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ old('title') ?? $selected->data->title ?? "" }}">
                        </div>
                        @if ($tab == 'home-block-1')
                            <div class="form-group">
                                <label for="summernote">Description</label>
                                <textarea tabindex="5" id="summernote" name="description">{{ old('details') ?? $selected->data->description ?? "" }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="product-block-1">Products Image</label>
                                <div class="row">
                                    <div class="col-12 col-lg-4">
                                        <div id="current1">
                                            <img src="{{ $selected->data->image }}"  id="imgPreview1" alt="" style="max-width: 100%">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-8">
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image" id="home-product-1" accept="image/*" aria-describedby="inputGroupFileAddon01">
                                                <label class="custom-file-label" for="home-product-1">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @else
                            <div class="form-group">
                                <label for="product-block-1">Image 1 (Recommended PNG)</label>
                                <div class="row">
                                    <div class="col-12 col-lg-4">
                                        <div>
                                            <img src="{{ $selected->data->images[0] }}"  id="imgPreview1" alt="" style="max-width: 100%">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-8">
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image1" id="product-1" accept="image/*">
                                                <label class="custom-file-label" for="product-1">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="product-block-1">Image 2 (Recommended PNG)</label>
                                <div class="row">
                                    <div class="col-12 col-lg-4">
                                        <div>
                                            <img src="{{ $selected->data->images[1] }}"  id="imgPreview2" alt="" style="max-width: 100%">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-8">
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="image2" id="product-2" accept="image/*">
                                                <label class="custom-file-label" for="product-2">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <a href="{{ route('site-settings.index') }}" class="btn btn-secondary">Cancel</a>
                        <button class="btn btn-primary">Save Changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
@if ($tab == 'home-block-1')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: 'Enter package description here ...',
            tabsize: 2,
            height: 300
        });
    });

    let img = document.getElementById('imgPreview1')
    let imgUpload = document.getElementById('home-product-1');
    imgUpload.onchange = evt => {
        const [file] = imgUpload.files
        if (file) {
            img.src = URL.createObjectURL(file)
        }
    }
</script>
@else

<script>

    let img1 = document.getElementById('imgPreview1')
    let imgUpload1 = document.getElementById('product-1');
    imgUpload1.onchange = evt => {
        const [file1] = imgUpload1.files
        if (file1) {
            img1.src = URL.createObjectURL(file1)
        }
    }
    
    let img2 = document.getElementById('imgPreview2')
    let imgUpload2 = document.getElementById('product-2');
    imgUpload2.onchange = evt => {
        const [file2] = imgUpload2.files
        if (file2) {
            img2.src = URL.createObjectURL(file2)
        }
    }
</script>
@endif
@endsection
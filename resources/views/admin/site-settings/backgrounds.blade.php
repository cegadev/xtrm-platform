@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Site Settings - <small>BACKGROUNDS</small></h1>
    </div>

    
    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow h-100 py-2">
                <div class="card-body p-4">
                    <div class="alert alert-primary" role="alert">
                        <strong>Note:</strong> Recommended resolution is 1024 x 576
                    </div>
                    <div class="row">
                        @foreach ($backgrounds as $key => $background)
                            <div class="col-12 col-lg-6">
                                <div class="card mb-3">
                                    <div class="card-header">Background {{ $key + 1 }}</div>
                                    <div class="card-body">
                                        <img src="{{ $background->data->url }}" alt="{{ $background->field }}" width="100%">
                                    </div>
                                    <div class="card-footer">
                                        <button type="button" onclick="editBackground({{ $key }})" class="btn btn-primary btn-block"><i class="fa fa-edit fa-fw"></i> Edit</button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Background</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="h5" id="backgroundTitle"></div>
            <form action="{{ route('site-settings.backgrounds.upload') }}" method="post" id="frmUpload" enctype="multipart/form-data">
                @csrf
                @method('post')
                <input type="hidden" name="index" id="bgIndex">
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input" id="bgUpload" accept="image/*">
                        <label class="custom-file-label" for="bgUpload">Choose file</label>
                    </div>
                </div>
            </form>
            <div id="preview" style="display: none">
                <img src="" alt="" id="imgPreview" style="max-width: 100%">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="saveBackground" disabled>Save changes</button>
        </div>
      </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function editBackground(index) {
        $('#bgIndex').val(index)
        document.getElementById('saveBackground').disabled = true
        document.getElementById('bgUpload').value = ""
        $('#preview').hide()
        $('#backgroundTitle').text('Background ' + (index + 1) )
        $('#editModal').modal('show')
    }

    let img = document.getElementById('imgPreview')
    let bgUpload = document.getElementById('bgUpload');
    bgUpload.onchange = evt => {
        const [file] = bgUpload.files
        if (file) {
            img.src = URL.createObjectURL(file)
            $('#preview').show()
            document.getElementById('saveBackground').disabled = false
        }
    }

    $('#saveBackground').on('click', function (){
        $(this).text('Uploading ...')
        $(this).parent().find('.btn').prop('disabled', true)
        $('#frmUpload').submit();
    })

</script>
@endsection
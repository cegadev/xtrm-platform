@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Activation</h1>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Items for Activation</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div class="table-responsive">
                    <table class="table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Description</th>
                                <th>Registrations Left</th>
                                <th width=200>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($released_orders as $order)
                                <tr>
                                    <td>
                                        <div style="font-size: 1.2em; font-weight: bold">{{ $order->product->name }}</div>
                                        <p>You have {{ $order->quantity }} item{{ $order->quantity > 1 ? 's' : '' }} in this order.</p>
                                    </td>
                                    <td width=300>
                                        {{-- {{ $order->total_activation_left }} registration{{ $order->total_activation_left > 1 ? "s available" : " left" }}. --}}
                                        <div style="font-size: 1.4em">{{ $order->total_activation_left }} / {{ $order->quantity }}</div>
                                        <a href="javascript:void(0)" onclick="viewHistory({{ $order->id }})"><SMALL>VIEW HISTORY</SMALL></a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.activation.transfer', [$order->id, "back_url" => url()->current()]) }}" class="btn btn-success">Transfer</a>
                                        <button class="btn btn-danger registerbtns" data-order_id="{{ $order->id }}" onclick="registerProduct2(this, {{ $order->id }})">Registration</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      </div>

      @include('admin.activation.modals.registration-form-2')
      <div class="modal fade" id="activationHistory" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Activation History</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="histories_wrapper">

                </div>
                <ul style="display: none" class="list-group" id="histories">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Morbi leo risus</li>
                    <li class="list-group-item">Porta ac consectetur ac</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')
<script src="{{ asset('sb-admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('sb-admin/js/demo/datatables-demo.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js"></script>

<script type="text/javascript">

const packageRegistrationModal = $("#zoomPackageRegistration2");
let isValidated = false;

registerProduct2 = (btn, orderId) => {
    $(btn).prop("disabled", true)
    document.getElementById("frmProductActivation").reset();
    $("#submit_activation_btn").off('click');
    $("#activation_order_id").val(orderId)


    $("#submit_activation_btn").prop("disabled", false);
    $("#submit_btn_loading").hide();
    $("#submit_btn_text").text("SUBMIT")

    $.get("{{ route('api-orders.get-order') }}", {
        id: orderId
    }, (order) => {
        $(btn).prop("disabled", false)
        let str_activations_left = "(" + order.activations_left + " registration" + (order.activations_left > 1 ? "s" : "") + " left)";
        $("#product-registration-selected-package").html(`Package: [${order.product_code}] - ${order.product_name} <span style='float: right'>${str_activations_left}</span>`);
        $("#zoomPackageRegistration2").modal('show')
        
        @if($selected)
            $("#cancel_activation_btn").off('click')
                                    .on('click', () => {
                                        document.getElementById("frmProductActivation").reset();
                                        window.location = "{{ route('my-orders.index', ['tab' => 'received']) }}"
                                    })
        @else
        $("#cancel_activation_btn").off('click')
                                .on('click', () => {
                                    document.getElementById("frmProductActivation").reset();
                                    $("#zoomPackageRegistration2").modal('hide')
                                })
        @endif
        
        $("#submit_activation_btn").off('click')
                                .on('click', () => {
                                    $("#frmProductActivation").submit()
                                })

        // $("#submit_activation_btn").off('click')
        //                             .on('click', () => {
        //                                 if ($("#tnc").prop("checked") == false) {
        //                                     $("#tnc").addClass('is-invalid')
        //                                 } else {
        //                                     $("#tnc").removeClass('is-invalid')
        //                                     validateActivation();
        //                                 }
        //                                 // $("#frmProductActivation").submit()
        //                             });
    }, "json")

}


@if($selected)
$(".registerbtns[data-order_id=" + {{ $selected }} + "]").click();
@endif

validateActivationOrder = () => {
    let order_id = $("#activation_order_id").val();
    let user_id = {{ auth()->id() }};
    let fname = $("#member_first_name")
    let email = $("#member_email");
    let username = $("#member_username");
    
    $.post("{{ route('api-activation.validate-activation') }}", {
            id: order_id,
            user_id,
            username: username.val(),
            email: email.val()
        }, (data, textStatus, xhr) => {
            console.log(data, textStatus, xhr)
            switch(xhr.status) {
                case 200:

                    break;
                case 400:
                    console.log('asd', data.data)
                    // if (isValidEmail)
                    break;
                case 404:
                    break;
            }
        }, "json")
}


viewHistory = (id) => {
    $.get("{{ route('api-activation.view-history') }}", {
        order_id: id
    }, (data) => {
        let histories = "";
        if (data.length == 0) {
            $("#histories_wrapper").text("No histories available yet.");
            $("#histories").hide();
        } else {
            data.forEach(h => {
                histories += `<li class='list-group-item'>${h.member_name} <span style='float:right'>${h.ago}</span></li>`;
            })
            $("#histories_wrapper").text("");
            $("#histories").show();
            $("#histories").html(histories);
        }

        $("#activationHistory").modal('show')
    })
}
let isSaving = false;
let registerNewMember = () => {

    $("#submit_activation_btn").prop("disabled", true);
    $("#submit_btn_loading").show();
    $("#submit_btn_text").text("LOADING...")

    let canSubmit = false;
    let errCount = 0;
    let fields = {
        first_name: {
            field: "#member_first_name", 
            label: "First name",
            value: document.getElementById("member_first_name").value,
            error: "#first-name-invalid-message"
        },
        middle_name: {
            field: "#member_middle_name", 
            label: "Middle name",
            value: document.getElementById("member_middle_name").value,
            error: "#middle-name-invalid-message",
        },
        last_name: {
            field: "#member_last_name", 
            label: "Last name",
            value: document.getElementById("member_last_name").value,
            error: "#last-name-invalid-message",
        },
        email: {
            field: "#member_email",
            label: "Email address",
            value: document.getElementById("member_email").value,
            error: "#email-invalid-message"
        },
        mobile_number: {
            field: "#mobile_number",
            label: "Mobile number",
            value: document.getElementById("mobile_number").value,
            error: "#mobile-number-invalid-message"
        },
        username: {
            field: "#member_username",
            label: "Username",
            value: document.getElementById("member_username").value,
            error: "#username-invalid-message"
        },
        password: {
            field: "#member_password",
            label: "Password",
            value: document.getElementById("member_password").value,
            error: "#password-invalid-message"
        },
        repeat_password: {
            field: "#repeat_password",
            label: "Repeat password",
            value: document.getElementById("repeat_password").value,
            error: "#repeat-password-invalid-message"
        },
        sponsor_username: {
            field: "#sponsor_username",
            label: "Sponsor username",
            value: document.getElementById("sponsor_username").value,
            error: "#sponsor-invalid-message"
        },
    }

    let data = {
        order_id: document.getElementById("activation_order_id").value
    };
    Object.keys(fields).forEach(key => {
        let value = fields[key].value

        data[key] = value
    })

    // validate tnc
    let tnc = document.getElementById("tnc");
    $("#tnc-message").hide()
    if (!tnc.checked) {
        $("#tnc-message").show();
        errCount++;
    }

    // validate password
    $("#repeat_password").removeClass("is-invalid")
    if (data.password != data.repeat_password) {
        $("#repeat_password").addClass("is-invalid")
        $("#repeat-password-invalid-message").text("Repeat Password did not match.")
        errCount++;
    }

    if (errCount == 0 && isSaving == false) {
        isSaving = true;

        $.ajax({
            url: "{{ route('api-activation.register-new-member') }}",
            data: data,
            method: "POST",
            complete: (xhr, textStatus) => {
                let response = xhr.responseJSON;

                setTimeout(() => {
                    if (textStatus == "error") {
                        if (xhr.status == 500) {
                            alert('Something went wrong. Try again later.')
                        } else if (xhr.status == 405) {
                            alert(response.message)
                        } else {

                            let errors = response.errors
                            Object.keys(fields).forEach(key => {
                                $(fields[key].field).removeClass('is-invalid')
                            })

                            Object.keys(errors).forEach(key => {
                                let str_error = errors[key].join(". ")
                                $(fields[key].field).addClass('is-invalid')
                                $(fields[key].error).text(str_error)
                            })
                        }
                    } else {
                        window.location = "{{ route('my-orders.activate.after-activation') }}";
                    }

                    isSaving = false;
                    
                    $("#submit_activation_btn").prop("disabled", false);
                    $("#submit_btn_loading").hide();
                    $("#submit_btn_text").text("SUBMIT")

                }, 1000)

            }
        })
        
    } else {
        $("#submit_activation_btn").prop("disabled", false);
        $("#submit_btn_loading").hide();
        $("#submit_btn_text").text("SUBMIT")
    }
    return false;
}
</script>
@endsection
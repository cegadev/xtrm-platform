@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Activation</h1>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Items for Activation</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div class="table-responsive">
                    <table class="table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Description</th>
                                <th>Registrations Left</th>
                                <th width=200>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($released_orders as $order)
                                <tr>
                                    <td>
                                        <div style="font-size: 1.2em; font-weight: bold">{{ $order->product->name }}</div>
                                        <p>You have {{ $order->quantity }} item{{ $order->quantity > 1 ? 's' : '' }} in this order.</p>
                                    </td>
                                    <td width=300>
                                        {{-- {{ $order->total_activation_left }} registration{{ $order->total_activation_left > 1 ? "s available" : " left" }}. --}}
                                        <div style="font-size: 1.4em">{{ $order->total_activation_left }} / {{ $order->quantity }}</div>
                                        <a href="javascript:void(0)" onclick="viewHistory({{ $order->id }})"><SMALL>VIEW HISTORY</SMALL></a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.activation.transfer', [$order->id, "back_url" => url()->current()]) }}" class="btn btn-success">Transfer</a>
                                        <button class="btn btn-danger registerbtns" data-order_id="{{ $order->id }}" onclick="registerProduct2(this, {{ $order->id }})">Registration</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
      </div>

      @include('admin.activation.modals.registration-form-2')
      <div class="modal fade" id="activationHistory" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Activation History</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="histories_wrapper">

                </div>
                <ul style="display: none" class="list-group" id="histories">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Morbi leo risus</li>
                    <li class="list-group-item">Porta ac consectetur ac</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('scripts')
<script src="{{ asset('sb-admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('sb-admin/js/demo/datatables-demo.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js"></script>

<script type="text/javascript">

const packageRegistrationModal = $("#zoomPackageRegistration2");
let isValidated = false;

registerProduct2 = (btn, orderId) => {
    $(btn).prop("disabled", true)
    document.getElementById("frmProductActivation").reset();
    $("#submit_activation_btn").off('click');
    $("#activation_order_id").val(orderId)
    $.get("{{ route('api-orders.get-order') }}", {
        id: orderId
    }, (order) => {
        $(btn).prop("disabled", false)
        let str_activations_left = "(" + order.activations_left + " registration" + (order.activations_left > 1 ? "s" : "") + " left)";
        $("#product-registration-selected-package").html(`Package: [${order.product_code}] - ${order.product_name} <span style='float: right'>${str_activations_left}</span>`);
        $("#zoomPackageRegistration2").modal('show')
        
        @if($selected)
            $("#cancel_activation_btn").off('click')
                                    .on('click', () => {
                                        document.getElementById("frmProductActivation").reset();
                                        window.location = "{{ route('my-orders.index', ['tab' => 'received']) }}"
                                    })
        @else
        $("#cancel_activation_btn").off('click')
                                .on('click', () => {
                                    document.getElementById("frmProductActivation").reset();
                                    $("#zoomPackageRegistration2").modal('hide')
                                })
        @endif
        
        $("#submit_activation_btn").off('click')
                                    .on('click', () => {
                                        if ($("#tnc").prop("checked") == false) {
                                            $("#tnc").addClass('is-invalid')
                                        } else {
                                            $("#tnc").removeClass('is-invalid')
                                            validateActivation();
                                        }
                                        // $("#frmProductActivation").submit()
                                    });
    }, "json")

}
let emailsTaken = []
let usernamesTaken = []
let isSubmitted = false;
validateActivation = (fieldKey) => {
    let order_id = $("#activation_order_id").val();
    let user_id = {{ auth()->id() }};
    let fname = $("#member_first_name")
    let lname = $("#member_last_name")
    let mobile_number = $("#mobile_number")
    let email = $("#member_email");
    let username = $("#member_username");
    let password = $("#member_password");
    let repeat_password = $("#repeat_password");
    let sponsor = $("#sponsor_username");
    
    isValidated = true;
    const emailPattern = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
    /*
    * Basic validation then server side validation
    * First name and last name is required
    * Email must be valid
    * Contact number must be valid
    * Password must be atleast 6 characters
    * Password and Repeat password must match 
    */
    // check fields must not be empty
    const fields = {
            "fname": {
                field: fname,
                name: "First name",
                invalid_feedback: "#first-name-invalid-message"
            }, 
            "lname": {
                field: lname,
                name: "Last name",
                invalid_feedback: "#last-name-invalid-message"
            }, 
            "mobile": {
                field: mobile_number,
                name: "Mobile number",
                invalid_feedback: "#mobile-number-invalid-message"
            }, 
            "email": {
                field: email,
                name: "Email address",
                invalid_feedback: "#email-invalid-message"
            }, 
            "username": {
                field: username,
                name: "Username",
                invalid_feedback: "#username-invalid-message"
            }, 
            "password": {
                field: password,
                name: "Password",
                invalid_feedback: "#password-invalid-message"
            },
            "repeat_password": {
                field: repeat_password,
                name: "Repeat Password",
                invalid_feedback: "#repeat-password-invalid-message"
            },
            "sponsor_username": {
                field: sponsor,
                name: "Sponsor Username",
                invalid_feedback: "#sponsor-invalid-message"
            }
        };
    
    if (fieldKey) { // validate single field
        let field = fields[fieldKey].field;            
        let fieldValue = field.val().trim();
        let except = ["email"]
        if (!except.includes(fieldKey) && fieldValue.length == 0) {
            console.log(fieldKey)
            field.addClass('is-invalid')
            $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' must not be empty')
        } else {
            field.removeClass('is-invalid')
        }

        if (fieldKey == "password") {
            if (fieldValue.length > 0 && fieldValue.length < 6) {
                field.addClass('is-invalid')
                $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' must be atleast 6 characters.')
            } else if (fields.repeat_password.field.val().trim().toString() != fieldValue.toString()) {
                fields.repeat_password.field.addClass('is-invalid')
                $(fields.repeat_password.invalid_feedback).text(fields.repeat_password.name + ' must match the Password')
            } else if (fields.repeat_password.field.val().trim().toString() == fieldValue.toString()) {
                fields.repeat_password.field.removeClass('is-invalid')
            }
        }

        if (fieldKey == "repeat_password") {
            const passwordValue = fields.password.field.val().trim();

            if (fieldValue.length == 0) {
                field.addClass('is-invalid')
                $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' must not be empty and must match the Password')
            } else if (passwordValue.toString() != fieldValue.toString() ){ // Repeat password must match password
                field.addClass('is-invalid')
                $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' must match the Password')
            } else {
                field.removeClass('is-invalid')
            }
        }

        if (fieldKey == "email" && fieldValue.length > 0) {
            if (emailPattern.test(fieldValue) == false) {
                field.addClass('is-invalid')
                $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' must be a valid email address')
            } else {
                if (emailsTaken.includes(fieldValue)) {
                    field.addClass('is-invalid')
                    $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' already taken')
                } else {
                    field.removeClass('is-invalid')
                }
            }
        }

        if (fieldKey == "username" && fieldValue.length > 0) {
            if (usernamesTaken.includes(fieldValue)) {
                field.addClass('is-invalid')
                $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' already taken')
            } else {
                field.removeClass('is-invalid')
            }
        }

        if (fieldKey == "mobile") {
            if (fieldValue.length > 0 && fieldValue.length < 11) {
                field.addClass('is-invalid')
                $(fields[fieldKey].invalid_feedback).text(fields[fieldKey].name + ' must be atleast 11 digits')
            } else {
                field.removeClass('is-invalid')
            }
        }
    } else {
        let isValidEmail = true;
        let isValidUsername = true;
        Object.keys(fields).forEach(key => {
            let field = fields[key].field;
            let fieldValue = field.val().trim();
            if (fieldValue.length == 0 && !["repeat_password", "email"].includes(key)) {
                field.addClass('is-invalid')
                $(fields[key].invalid_feedback).text(fields[key].name + ' must not be empty')
                if (key == "email") { isValidEmail = false }
                if (key == "username") { isValidUsername = false }
            } 
            
            field.on('change keyup', () => {
                validateActivation(key);
            })

            if (key == "password" && (fieldValue.length > 0 && fieldValue.length < 6)) {
                field.addClass('is-invalid')
                $(fields[key].invalid_feedback).text(fields[key].name + ' must be atleast 6 characters.')
            }

            if (key == "repeat_password") {
                const passwordValue = fields.password.field.val().trim();

                if (fieldValue.length == 0) {
                    field.addClass('is-invalid')
                    $(fields[key].invalid_feedback).text(fields[key].name + ' must not be empty and must match the Password')
                } else if (passwordValue.toString() != fieldValue.toString() ){ // Repeat password must match password
                    field.addClass('is-invalid')
                    $(fields[key].invalid_feedback).text(fields[key].name + ' must match the Password')
                } else {
                    field.removeClass('is-invalid')
                }
            }

            if ((key == "username" && fieldValue.length == 0)) {
                isValidUsername = false;
            }

            if (key == "email" && fieldValue.length > 0 && emailPattern.test(fieldValue) == false) {
                field.addClass('is-invalid')
                $(fields[key].invalid_feedback).text(fields[key].name + ' must be a valid email address')
                isValidEmail = false;
            }

            if (key == "mobile" && (fieldValue.length > 0 && fieldValue.length < 11)) {
                field.addClass('is-invalid')
                $(fields[key].invalid_feedback).text(fields[key].name + ' must be atleast 11 digits')
            }
        })

        if (isValidEmail || isValidUsername) {
            $.post("{{ route('api-activation.validate-new-user') }}", {
                username: fields.username.field.val(),
                email: fields.email.field.val(),
                sponsor_username: fields.sponsor_username.field.val(),
            }, (data) => {
                if (data.status == "failed") {
                    if (isValidEmail && data.data.email == "exists") {
                        fields.email.field.addClass('is-invalid')
                        $(fields.email.invalid_feedback).text('Email address already taken')
                        emailsTaken.push(fields.email.field.val())
                    }
                    
                    if (isValidUsername && data.data.username == "exists") {
                        fields.username.field.addClass('is-invalid')
                        $(fields.username.invalid_feedback).text('Username already taken')
                        usernamesTaken.push(fields.username.field.val())
                    }

                    if (data.data.sponsor == "exists") {
                        fields.sponsor_username.field.removeClass('is-invalid')
                    } else {
                        fields.sponsor_username.field.addClass('is-invalid')
                        $(fields.sponsor_username.invalid_feedback).text('Sponsor is invalid.');
                    }
                }

                if ($("#frmProductActivation .is-invalid").length == 0 && isSubmitted == false) {
                    $("#submit_activation_btn").prop('disabled', true)
                    $("#frmProductActivation").submit();
                    isSubmitted = true;
                }

            });

        }

    }

    // return false;
}

@if($selected)
$(".registerbtns[data-order_id=" + {{ $selected }} + "]").click();
@endif

validateActivationOrder = () => {
    let order_id = $("#activation_order_id").val();
    let user_id = {{ auth()->id() }};
    let fname = $("#member_first_name")
    let email = $("#member_email");
    let username = $("#member_username");
    
    $.post("{{ route('api-activation.validate-activation') }}", {
            id: order_id,
            user_id,
            username: username.val(),
            email: email.val()
        }, (data, textStatus, xhr) => {
            console.log(data, textStatus, xhr)
            switch(xhr.status) {
                case 200:

                    break;
                case 400:
                    console.log('asd', data.data)
                    // if (isValidEmail)
                    break;
                case 404:
                    break;
            }
        }, "json")
}


viewHistory = (id) => {
    $.get("{{ route('api-activation.view-history') }}", {
        order_id: id
    }, (data) => {
        let histories = "";
        if (data.length == 0) {
            $("#histories_wrapper").text("No histories available yet.");
            $("#histories").hide();
        } else {
            data.forEach(h => {
                histories += `<li class='list-group-item'>${h.member_name} <span style='float:right'>${h.ago}</span></li>`;
            })
            $("#histories_wrapper").text("");
            $("#histories").show();
            $("#histories").html(histories);
        }

        $("#activationHistory").modal('show')
    })
}
</script>
@endsection
<script type="text/javascript">
    let stepper;
  
    document.addEventListener('DOMContentLoaded', function () {
      stepper = new Stepper(document.querySelector('.bs-stepper'))
    })
  
    const md = $("#zoomPackageRegistration");
    const am_form = $("#am_modal");
    const totalSteps = am_form.find('.step').length;
    const nextBtn = $("#am_continue_btn");
    const prevBtn = $("#am_prev_btn");
  
    md.on('shown.bs.modal', function() {
      am_form.find('input:first').focus();
    })
  
    function registerProduct() {
      am_form.attr('data-step', 1) 
      md.modal('show');
      stepper.to(1)
    }
  
    function stepRegisterProduct(type) {
      let isContinue = type == 'continue';
      let step = parseInt(am_form.attr('data-step'));
  
      if (step == 1) {
        if (isContinue) {
          stepper.next(); am_form.attr('data-step', (step + 1))
        }else {
          md.modal('hide')
        }
      } else if (step == totalSteps) {
        if (isContinue) {
          alert('submit!')
        } else {
          stepper.previous(); am_form.attr('data-step', (step - 1))
        }
      } else {
        if (isContinue) {
          stepper.next(); am_form.attr('data-step', (step + 1))
        } else {
          stepper.previous(); am_form.attr('data-step', (step - 1))
        }
      }
  
      const currentStep = parseInt(am_form.attr('data-step'));
  
      nextBtn.removeClass('btn-success')
      nextBtn.addClass('btn-primary')
      if (currentStep == 1) { // if first step
        prevBtn.text('CANCEL');
        nextBtn.text('NEXT');
      } else if (currentStep == totalSteps) { // is last step
        prevBtn.text('BACK');
        nextBtn.text('SUBMIT');
        nextBtn.removeClass('btn-primary')
        nextBtn.addClass('btn-success')
      } else {
        prevBtn.text('BACK');
        nextBtn.text('NEXT');
      }
  
    }
  </script>
  
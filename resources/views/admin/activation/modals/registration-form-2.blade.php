<div class="modal fade fullscreen-sm" id="zoomPackageRegistration2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Product Package Registration</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="padding: 1em 1.3em">
            
            <form onsubmit="return registerNewMember()" id="frmProductActivation">
                <input type="hidden" name="order_id" id="activation_order_id">
                {{-- @csrf --}}
                {{-- @method('POST') --}}
                <div id="product-registration-selected-package" style="font-weight: bold"></div>
                <hr>
                <div class="row">
                    <div class="col-md-5 col-xl-5">
                        <div class="form-group">
                            <label for="">First Name</label>
                            <input name="member_first_name" type="text" class="form-control" id="member_first_name">
                            <div class="invalid-feedback" id="first-name-invalid-message"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Middle Name</label>
                            <input name="member_middle_name" type="text" class="form-control" placeholder="" id="member_middle_name">
                            <div class="invalid-feedback" id="middle-name-invalid-message"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Last Name</label>
                            <input name="member_last_name" type="text" class="form-control" id="member_last_name">
                            <div class="invalid-feedback" id="last-name-invalid-message"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input name="member_email" type="email" class="form-control" id="member_email" placeholder="Optional">
                            <div class="invalid-feedback" id="email-invalid-message"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Mobile Number</label>
                            <input name="mobile_number" type="text" class="form-control" id="mobile_number">
                            <div class="invalid-feedback" id="mobile-number-invalid-message"></div>
                        </div>
                    </div>
                    <div class="col-md-7 col-xl-7">
                        <div class="form-group">
                            <label for="">Username</label>
                            <input autocomplete="off" name="member_username" type="text" class="form-control" id="member_username">
                            <div class="invalid-feedback" id="username-invalid-message"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input autocomplete="off" name="member_password" type="password" class="form-control" id="member_password">
                            <div class="invalid-feedback" id="password-invalid-message"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Repeat Password</label>
                            <input autocomplete="off" name="repeat_password" type="password" class="form-control" id="repeat_password">
                            <div class="invalid-feedback" id="repeat-password-invalid-message"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Sponsor Username</label>
                            <input type="text" class="form-control" value="{{ auth()->user()->username }}" id="sponsor_username" name="sponsor_username">
                            <div class="invalid-feedback" id="sponsor-invalid-message"></div>
                        </div>
                        {{-- <div class="form-group">
                        <label for="">Upline Username</label>
                        <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                        <label for="">Position</label>
                        <select id="inputState" class="form-control">
                            <option disabled selected>Choose Position</option>
                            <option>Left</option>
                            <option>Right</option>
                        </select>
                        </div> --}}
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" name="tnc" type="checkbox" id="tnc">
                                <label class="" for="tnc">
                                    Check to confirm the data you entered.
                                </label>
                                <div class="invalid-feedback" id="tnc-message">You must agree to the confirmation.</div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary am_btn" type="button" id="cancel_activation_btn">CANCEL</button>
          <button class="btn btn-primary am_btn" id="submit_activation_btn" type="button">
            <span class="spinner-grow spinner-grow-sm" id="submit_btn_loading" style="display: none" role="status" aria-hidden="true"></span>
  <span id="submit_btn_text">LOADING...</span>
          </button>
        </div>
      </div>
    </div>
  </div>
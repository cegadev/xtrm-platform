<div class="modal fade fullscreen-sm" id="zoomPackageRegistration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Product Package Registration</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" method="post">
            <div class="bs-stepper" id="am_modal" data-step=1>
              <div class="bs-stepper-header" role="tablist">
                <!-- your steps here -->
                <div class="step" data-target="#personal-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="personal-part" id="personal-part-trigger">
                    <span class="bs-stepper-circle">1</span>
                    <span class="bs-stepper-label">Personal</span>
                  </button>
                </div>
                <div class="line"></div>
                <div class="step" data-target="#account-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="account-part" id="account-part-trigger">
                    <span class="bs-stepper-circle">2</span>
                    <span class="bs-stepper-label">User Account</span>
                  </button>
                </div>
                <div class="line"></div>
                <div class="step" data-target="#settings-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="settings-part" id="settings-part-trigger">
                    <span class="bs-stepper-circle">3</span>
                    <span class="bs-stepper-label">Membership</span>
                  </button>
                </div>
              </div>
              <div class="bs-stepper-content">
                <!-- your steps content here -->
                <div id="personal-part" class="content" role="tabpanel" aria-labelledby="personal-part-trigger">
                  <div class="form-group">
                    <label for="">First Name</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Middle Name</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Last Name</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Email Address</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Mobile Number</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div id="account-part" class="content" role="tabpanel" aria-labelledby="account-part-trigger">
                  <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Password</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Repeat Password</label>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <div id="settings-part" class="content" role="tabpanel" aria-labelledby="settings-part-trigger">
                  <div class="form-group">
                    <label for="">Sponsor Username</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Upline Username</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Position</label>
                    <select class="custom-select" id="inputGroupSelect01">
                      <option selected>Choose Position</option>
                      <option value="1">Left</option>
                      <option value="2">Right</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer" id="activation_modal_footer">
          <button class="btn btn-secondary am_btn" type="button" id="am_prev_btn" onclick="stepRegisterProduct('back')">CANCEL</button>
          <button class="btn btn-primary am_btn" id="am_continue_btn" type="button" onclick="stepRegisterProduct('continue')">NEXT</button>
        </div>
      </div>
    </div>
</div>
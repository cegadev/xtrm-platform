@extends('layouts.admin')

@section('styles')
<link rel="stylesheet" href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div>
            <small>ACTIVATION</small>
            <h1 class="h3 mb-0 text-gray-800">Transfer Ownership</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Package</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">PACKAGE NAME</label>
                        <h2>{{ $order->package->name }}</h2>
                    </div>
                    <div class="row mb-2">
                        <div class="col">
                            <label for="">PRICE</label>
                            <h2>{{ number_format($order->package->price, 2) }}</h2>
                        </div>
                        <div class="col">
                            <label for="">QUANTITY</label>
                            <h2>&times; {{ $order->quantity }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Total Amount</h6>
                </div>
                <div class="card-body">
                    <h1>{{ number_format($order->amount, 2) }}</h1>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Transfer To</h6>
                </div>
                <div class="card-body">
                <form method="POST" action="{{ route('admin.activation.send-package', $order->id) }}">
                    @csrf

                    <div class="form-group">
                            <label for="">Member username *</label>
                            <input type="text" value="{{ old('username') }}" name="username" id="username" placeholder="Please enter member's username" class="form-control @error('username') is-invalid @enderror ">    
                            @error('username')
                                <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Notes</label>
                            <textarea name="notes" class="form-control" id="" cols="30" rows="4" placeholder="Enter notes to receiver"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Enter Password *</label>
                            <input type="password" name="password" id="password" placeholder="Please enter your password for verification" class="form-control @error('password') is-invalid @enderror">
                            @error('password')
                                <div class="invalid-feedback"> {{ $message }} </div>
                            @enderror
                        </div>
                        <a href="{{ $back_url ?? route('admin.activation.index') }}" class="btn btn-secondary"> Cancel</a>
                        <button class="btn btn-primary"><i class="fa fa-paper-plane"></i> Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('scripts')

<script type="text/javascript">
</script>
@endsection
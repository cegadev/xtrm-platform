@extends('layouts.admin')

@section('styles')
    
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Product</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <form method="POST" action="{{ route('products.update-product', [$product->id]) }}" novalidate enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="col-md-8 mb-3">
                                <label for="product_name">Name</label>
                                <input tabindex="1" id="product_name" type="text" class="form-control @error('product_name') is-invalid @enderror" name="product_name" value="{{ old('product_name') ?? $product->name }}" placeholder="Enter product name" required>
                                @error('product_name')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                <label for="code">Product Code</label>
                                <input tabindex="2" id="code" type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{  old('code') ?? $product->code }}" placeholder="Enter product code (e.g. P1 for Product 1)" required>
                                @error('code')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="category">Category</label>
                                <select tabindex="3" name="category" id="category" class="form-control @error('category') is-invalid @enderror">
                                    <option disabled> Choose Category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ old('category') ?? $product->product_category_id == $category->id ? "selected" : "" }} > {{ $category->name }} </option>
                                    @endforeach
                                </select>
                                @error('category')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="price">Price</label>
                                <input tabindex="4" type="number" class="form-control text-right @error('price') is-invalid @enderror " name="price" value="{{ old('price') ?? $product->price }}" required min="1">
                                @error('price')
                                <div class="invalid-feedback"> {{ $message }} </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="">Product Image</label>
                                <div class="custom-file mb-3">
                                    <input type="file" name="image" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                                <label for="">Preview</label>
                                <div>
                                    <img src="{{ $product->product_image ?? \App\Http\Helpers\Helpers::defaultProductImage() }}" id="image-preview" alt="" style="max-width: 100%">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="">Basic Description (Product thumbnail) </label>
                                <textarea tabindex="5" rows="4" placeholder="Enter basic description here" class="form-control" name="basic_description">{{ old('basic_description') ?? $product->basic_description }}</textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="">Full Page Description</label>
                                <textarea tabindex="5" id="summernote" name="description">{{ old('description') ?? $product->description }}</textarea>
                            </div>
                        </div>
                        <div>
                            <a href="{{ route('products.index') }}" class="btn btn-secondary">Cancel</a>
                            <button class="btn btn-primary">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Enter product description here ...',
                tabsize: 2,
                height: 200
            });
        });

        $("#customFile").on('change', function(e) {
            // get the file name
            let fileName = e.target.files[0].name

            // replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);

            readURL(this);
        });
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Products</h1>
    </div>

    <!-- Content Row -->
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    
                    @can('products.write')
                        <a href="{{ route('products.create') }}" class="btn btn-primary shadow-sm"><i class="fa fa-plus"></i> Create New Product</a>
                        <button id="btnEdit" class="btn btn-primary shadow-sm" disabled><i class="fa fa-edit"></i> Edit</button>
                    @endcan

                    <ul class="nav nav-tabs mt-3">
                        <li class="nav-item">
                          <a class="nav-link {{ $tab == 'active' ? 'active' : '' }}" href="?tab=active">Active Packages</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link {{ $tab == 'disabled' ? 'active' : '' }}" href="?tab=disabled">Disabled Packages</a>
                        </li>
                    </ul>
                    <table class="table mt-3">
                        <thead>
                            <tr>
                                <th width=50>
                                    <div>
                                        <div class="form-check">
                                            <input type="checkbox" name="" id="productsCheckAll">
                                        </div>
                                    </div>
                                </th>
                                <th width=300>Name</th>
                                <th>Code</th>
                                <th>Category</th>
                                <th class="text-right" width=200>Total Amount</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="productsList">
                            @forelse ($products as $product)
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            <input type="checkbox" name="product-item" id="" value="{{ $product->id }}" class="productCheckBox">
                                        </div>
                                    </td>
                                    <td class="productName-{{ $product->id }}">{{ $product->name }}</td>
                                    <td>{{ $product->code }}</td>
                                    <td>{{ $product->category ? $product->category->name : 'None' }}</td>
                                    <td class="text-right"><span class="peso-sign"></span> {{ number_format($product->price, 2) }}</td>
                                    <td class="text-right">
                                        @if ($tab == 'active')
                                            <button class="btn btn-danger" onclick="disableItem({{ $product->id }})"><i class="fa fa-ban fa-fw"></i> Disable </button>
                                        @else 
                                            <button class="btn btn-info" onclick="enableItem({{ $product->id }})"><i class="fa fa-check fa-fw"></i> Enable</button>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">No products to display.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>

    <div class="mt-4">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Product Categories</h1>
        </div>

        <div class="card">
            <div class="card-body border-left-success">
                <button class="btn btn-primary" onclick="createCategory()"><i class="fa fa-plus fa-fw"></i> Add Category</button>
                <div class="table mt-3">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th width=300></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($categories as $category)
                                <tr>
                                    <td class="align-middle category-item" data-id="{{ $category->id }}"><span>{{ $category->name }}</span>
                                        - ({{ $category->products_count }} products attached)
                                    </td>
                                    <td>
                                        <button class="btn btn-primary" onclick="editCategory({{ $category->id }})"><i class="fa fa-edit fa-fw"></i> Edit</button>
                                        <button class="btn btn-danger" onclick="selectDeleteCategory({{ $category->id }})"><i class="fa fa-trash fa-fw"></i> Delete</button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No categories available.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="createCategory" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Add Product Category</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="" onsubmit="return false">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="category_name" id="category_name" placeholder="Enter Category name" class="form-control">
                        <div class="invalid-feedback" id="category_name_invalid"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="saveCategory()">Save Category</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
    </div>
    
    <div class="modal fade" id="editCategory" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Edit Product Category</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="" onsubmit="return false">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="hidden" name="edit_category_id" id="edit_category_id" class="form-control">
                        <input type="text" name="edit_category_name" id="edit_category_name" placeholder="Enter Category name" class="form-control">
                        <div class="invalid-feedback" id="category_name_invalid"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="updateCategory()">Save changes</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteCategory" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Product Category Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="delete_category_id" id="delete_category_id">
                <p>Are you sure you want to delete <strong id="to_delete"></strong>?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="deleteCategory()">Yes, delete it</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="enableModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Enable Product Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to enable <span id="enableName" class="font-weight-bold"></span>?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ route('products.enable-product') }}" method="post" class="form-inline">
                    @csrf
                    @method('post')
                    <input type="hidden" name="enable_id" id="enable_id">
                    <button type="submit" class="btn btn-primary">Yes</button>
                </form>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
    </div>
    
    <div class="modal fade" id="disableModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Disable Product Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to disable <span id="disableName" class="font-weight-bold"></span>?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ route('products.disable-product') }}" method="post" class="form-inline">
                    @csrf
                    @method('post')
                    <input type="hidden" name="disable_id" id="disable_id">
                    <button type="submit" class="btn btn-primary">Yes</button>
                </form>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#productsCheckAll").on('change', (data) => {
            $("#productsList tr td input[type=checkbox]").prop('checked', data.target.checked)
            triggerChecked()
        })

        $("#productsList tr td input[type=checkbox]").on('change', (data) => {
            triggerChecked()
        })

        triggerChecked = () => {
            let checkedCount = $(".productCheckBox:checked").length;
            if (checkedCount == 0) {
                $("#btnEdit").prop('disabled', true)
                $("#btnDelete").prop('disabled', true)
            } else {
                if (checkedCount == 1) {
                    $("#btnEdit").prop('disabled', false)
                    $("#btnDelete").prop('disabled', false)
                } else {
                    $("#btnEdit").prop('disabled', true)
                    $("#btnDelete").prop('disabled', false)
                }
            }
        }

        $("#btnEdit").on('click', (data) => {
            if (data.target.disabled == false) {
                let selectedProduct = $(".productCheckBox:checked")[0];
                if (selectedProduct.value) {
                    window.open("/admin/products/edit/" + selectedProduct.value, "_self");
                }
            }
        })


        $("#btnDelete").on('click', (data) => {
            if (data.target.disabled == false) {
                ids = [];
                $("input[name=product-item]:checked").each(function() {
                    ids.push($(this).val())
                })

                if (ids.length > 0) {
                    $("#delete_ids").val(ids.join(","))
                    $("#frmDelete").submit();
                }
            }
        });
    </script>
    <script>
        createCategory = () => {
            $("#category_name").val("")
            $("#createCategory").modal('show')
        }
        
        editCategory = (id) => {
            $("#edit_category_id").val(id)
            let name = $(".category-item[data-id=" + id + "] span").text()
            $("#edit_category_name").val(name)
            $("#editCategory").modal('show')
        }

        selectDeleteCategory = (id) => {
            $("#delete_category_id").val(id);
            let name = $(".category-item[data-id=" + id + "] span").text()
            $("#to_delete").text(name)
            $("#deleteCategory").modal('show')
        }

        saveCategory = () => {
            $("#category_name").removeClass('is-invalid')

            $.ajax({
                url: "/api/categories/save",
                method: "POST",
                data: {
                    category_name: $("#category_name").val()
                },
                complete: function(xhr, textStatus) {
                    let data = xhr.responseJSON;

                    if (textStatus == "error") {
                        if (xhr.status == 500) {
                            alert("Something went wrong. Please try again later.")
                        }

                        if (xhr.status == 422) {
                            let errors = data.errors.category_name
                            $("#category_name").addClass('is-invalid')
                            $("#category_name_invalid").text(errors.join('. '))
                        }
                    } else {

                        if (xhr.status == 200) {
                            alert('Category Successfully created!');
                            location.reload()
                        }
                    }
                }
            })
        }
        
        updateCategory = () => {
            let id = $("#edit_category_id").val()
            $("#edit_category_name").removeClass('is-invalid')

            $.ajax({
                url: "/api/categories/update",
                method: "POST",
                data: {
                    id: id,
                    category_name: $("#edit_category_name").val()
                },
                complete: function(xhr, textStatus) {
                    let data = xhr.responseJSON;

                    if (textStatus == "error") {
                        if (xhr.status == 500) {
                            alert("Something went wrong. Please try again later.")
                        }

                        if (xhr.status == 422) {
                            let errors = data.errors.category_name
                            $("#edit_category_name").addClass('is-invalid')
                            $("#edit_category_name").text(errors.join('. '))
                        }
                    } else {

                        if (xhr.status == 200) {
                            alert('Category Successfully updated!');
                            location.reload()
                        }
                    }
                }
            })
        }

        deleteCategory = () => {
            let id = $("#delete_category_id").val()

            $.ajax({
                url: "/api/categories/delete",
                method: "POST",
                data: {
                    id: id,
                },
                complete: function(xhr, textStatus) {
                    let data = xhr.responseJSON;

                    if (textStatus == "error") {
                        if (xhr.status == 500) {
                            alert("Something went wrong. Please try again later.")
                        }
                    } else {

                        if (xhr.status == 200) {
                            alert('Category Successfully deleted!');
                            location.reload()
                        }
                    }
                }
            })
        }


        
        enableItem = (id) => {
            let name = $('.productName-' + id).text();
            
            $("#enable_id").val(id)
            $("#enableName").text(name)
            $("#enableModal").modal('show')
        }
        
        disableItem = (id) => {
            let name = $('.productName-' + id).text();
            
            $("#disable_id").val(id)
            $("#disableName").text(name)
            $("#disableModal").modal('show')
        }
    </script>
@endsection
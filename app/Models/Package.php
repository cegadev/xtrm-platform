<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;
    
    protected $table = "products";

    protected $fillable = [
        'name',
        'code',
        'price',
        'type',
        'slug',
        'description',
        'disabled_at',
        'enabled_lic',
        'homepage_priority'
    ];

    public function settings() {
        return $this->hasOne(PackageSetting::class, "package_id");
    }
}

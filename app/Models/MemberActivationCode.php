<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberActivationCode extends Model
{
    protected $fillable = [
        "user_id",
        "member_id",
        "activation_code_id",
    ];

    public function activation_code() {
        return $this->belongsTo(ActivationCode::class);
    }
}

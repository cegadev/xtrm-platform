<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageSetting extends Model
{
    protected $fillable = [
        "package_id",
        "direct_referral_commission",
        "indirect_referral_percentage",
        "notes",
        "inclusions",
        "irb_limit_per_day",
        "irb_limit_per_month",
        'leadership_indirect_commission_percentage',
        'lic_limit_per_day',
        'lic_limit_per_month'
    ];

    public function package() {
        return $this->belongsTo(Package::class);
    }
}

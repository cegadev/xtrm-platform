<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = [
        "name",
        "parent_id",
        "disabled_at"
    ];

    public function products() {
        return $this->hasMany(\App\Models\Product::class)
                    ->whereNull('disabled_at');
    }
}

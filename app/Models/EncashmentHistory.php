<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EncashmentHistory extends Model
{
    protected $fillable = [
        "membership_id",
        "transaction_type",
        "amount",
        "vat_applied",
        "amount_released",
        "service_fee",
        "status",
        "requested_at",
        "approved_at",
        "declined_at",
        "requested_by",
        "approved_by",
        "declined_by",
        "bank_info",
        "transaction_recorded_at",
    ];

    public function member() {
        return $this->belongsTo(Membership::class, "membership_id");
    }
    
    public function requestedBy() {
        return $this->belongsTo(\App\User::class, "requested_by");
    }

    public function decoded_bank_info() {
        return \json_decode($this->bank_info);
    }
}

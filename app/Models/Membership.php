<?php

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{    
    use NodeTrait;

    protected $fillable = [
        "user_id",
        "parent_id",
        "order_id",
        "product_id",
        "top_level",
        "ancestor_ids",
        "current_month_earnings",
        "current_year_earnings",
        "total_earnings",
        "member_name",
        "available_commissions",
        "is_company_account",
        "is_free_account",
        "backup",
        "prev_month_bonus_claimed_amount",
        "prev_month_bonus_claimed_at"
    ];

    // Specify parent id attribute mutator
    public function setParentAttribute($value) {
        $this->setParentIdAttribute($value);
    }


    /**
     * user
     *
     * @return void
     */
    public function user() {
        return $this->belongsTo(\App\User::class);
    }


    public function earnings() {
        return $this->hasMany(Earning::class, "member_id");
    }

    public function package() {
        return $this->belongsTo(Package::class, "product_id");
    }

    public function encashment_histories() {
        return $this->hasMany(EncashmentHistory::class, "membership_id");
    }

    public function bank_infos() {
        return $this->hasMany(MemberBankInfo::class, "member_id");
    }

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function parent() {
        return $this->belongsTo(Membership::class, "parent_id");
    }

    public function availableCommissions() {
        return $this->total_earnings - $this->encashment_histories->whereIn("status", ["pending", "complete"])->sum("amount");
    }

    public function eWallet() {
        return $this->hasMany(Transaction::class, "member_id")->orderBy("id", "desc")->first()->balance ?? 0;
    }
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        "user_id",
        "member_id",
        "debit",
        "credit",
        "balance",
        "type",
        "resource_id",
        "transacted_by",
        "added_to_wallet_at"
    ];
    
    public function earnings() {
        return $this->hasOne(Earning::class, "id", "resource_id");
    }

    public function encashments() {
        return $this->hasOne(EncashmentHistory::class, "id", "resource_id");
    }

    public function membership()
    {
        return $this->belongsTo(Membership::class, 'member_id');
    }
}

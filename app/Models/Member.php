<?php

namespace App\Models;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

    use HasRoles;

    protected $table = "users";

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function membership() {
        return $this->hasOne(Membership::class, 'user_id');
    }

    public function getFormalNameAttribute() {
        $name = $this->last_name . ', ' . $this->first_name;

        if (!empty($this->middle_name)) {
            return $name . " " . $this->middle_name;
        }
        
        return $name;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCode extends Model
{
    protected $fillable = [
        "code",
        "order_id",
        "product_id",
        "is_activated",
        "activated_at",
        "activated_by"
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function order() {
        return $this->belongsTo(Order::class);
    }
}

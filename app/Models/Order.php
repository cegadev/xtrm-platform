<?php

namespace App\Models;

use App\Models\Package;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = [
        'user_id',
        'product_id',
        'quantity',
        'amount',
        'status',
        'requested_by',
        'created_at',
        'updated_at',
        'total_activation_left',
        'is_transferred',
        'branch_id'
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }
    
    public function package() {
        return $this->belongsTo(Package::class, "product_id");
    }
    
    public function user() {
        return $this->belongsTo(\App\User::class);
    }

    public function package_histories() {
        return $this->hasMany(ReleasePackageHistory::class);
    }

    public function transfer_record() {
        return $this->hasMany(TransferOrder::class, "order_id");
    }

    public function product_codes() {
        return $this->hasMany(ProductCode::class, "order_id");
    }

    public function requestedBy() {
        return $this->belongsTo(\App\User::class, "requested_by");
    }

    public function items() {
        return $this->hasMany(OrderItem::class, "order_id");
    }
    
    public function itemsForActivation() {
        return $this->hasMany(OrderItem::class, "order_id")->whereNull('activated_at');
    }

    public function activatedItems() {
        return $this->hasMany(OrderItem::class, "order_id")->whereNotNull("activated_at");
    }

    public function members() {
        return $this->hasMany(Membership::class, "order_id");
    }
}

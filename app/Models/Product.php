<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name',
        'code',
        'price',
        'product_category_id',
        'type',
        'slug',
        'basic_description',
        'description',
        'product_image',
        'disabled_at'
    ];

    public function category() {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function reviews() {
        return $this->hasMany(Review::class);
    }

    public function publishedReviews() {
        return $this->hasMany(Review::class)->where("published", true);
    }

    public function averageRating() {
        return $this->publishedReviews()->avg("rating");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferItem extends Model
{
    protected $fillable = [
        "order_id",
        "order_item_id",
        "product_id",
        "from",
        "to",
        "transferred_at"
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }
}

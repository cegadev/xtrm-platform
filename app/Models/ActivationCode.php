<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivationCode extends Model
{
    protected $fillable = [
        "created_by",
        "type", 
        "code",
        "status", 
        "order_id",
        "product_id",
        "released_to",
        "released_at",
        "activated_at",
        "activated_by",
        "registered_at",
        "registered_to",
        "transferred_from",
        "transferred_at",
    ];

    public function releasedToUser() {
        return $this->belongsTo(\App\User::class, "released_to");
    }

    public function activatedByUser() {
        return $this->belongsTo(\App\User::class, "activated_by");
    }

    public function package() {
        return $this->belongsTo(Package::class, "product_id");
    }

    public function registeredToUser() {
        return $this->belongsTo(\App\User::class, "registered_to");
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function transferredFrom()
    {
        return $this->belongsTo(\App\User::class, 'transferred_from');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}

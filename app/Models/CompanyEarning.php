<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyEarning extends Model
{
    protected $fillable = [
        "type",
        "amount",
        "resource_id",
        "resource_type",
        "notes",
        "transacted_by",
        "order_id"
    ];

    public function transactedBy() {
        return $this->belongsTo(\App\User::class, "transacted_by");
    }

    public function order() {
        return $this->belongsTo(Order::class, "order_id");
    }
}

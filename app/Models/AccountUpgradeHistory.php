<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountUpgradeHistory extends Model
{
    protected $fillable = [
        "user_id",
        "member_id",
        "from_package",
        "to_package",
        "upgraded_by",
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Earning extends Model
{
    protected $fillable = [
        'member_id',
        'type',
        'amount',
        'notes',
        'earned_from_member_id',
        'product_id',
        'order_id',
        'valid_at',
        'added_to_wallet_at',
    ];

    protected $dates = [
        'added_to_wallet_at',
        'valid_at',
        
    ];

    public function membership() {
        return $this->belongsTo(Membership::class, "member_id");
    }

    public function earnedFromMember() {
        return $this->belongsTo(Membership::class, "earned_from_member_id");
    }

    public function transaction() {
        return $this->hasOne(Transaction::class, "resource_id")->where('type', 'earnings');
    }
}

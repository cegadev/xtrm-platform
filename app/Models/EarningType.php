<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EarningType extends Model
{
    protected $fillable = [
        "title", "description", "priority"
    ];
}

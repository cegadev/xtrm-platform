<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    protected $fillable = [
        "user_id",
        "product_id",
        "message",
        "rating",
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function from() {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}

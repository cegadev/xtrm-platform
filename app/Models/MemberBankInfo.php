<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberBankInfo extends Model
{
    use SoftDeletes;
    
    protected $fillable = ["member_id", "type", "name", "value", "notes", "is_other", "account_name"];

    public function member() {
        return $this->belongsTo(Membership::class, "member_id");
    }
}

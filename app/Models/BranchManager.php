<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchManager extends Model
{

    protected $fillable = [
        "branch_id",
        "user_id",
        "is_suspended"
    ];

    public function user() {
        return $this->belongsTo(\App\User::class);
    }

    public function branch() {
        return $this->belongsTo(Branch::class, "branch_id");
    }
}

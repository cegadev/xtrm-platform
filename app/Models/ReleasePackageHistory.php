<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReleasePackageHistory extends Model
{
    protected $table = "release_package_history";
    
    protected $fillable = [
        "order_id",
        "user_id",
        "status"
    ];

    public function user() {
        return $this->belongsTo(\App\User::class, "user_id");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransferOrder extends Model
{
    protected $fillable = [
        "order_id",
        "from_id",
        "to_id",
        "notes"
    ];
}

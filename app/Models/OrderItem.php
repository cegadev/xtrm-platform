<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        "order_id",
        "product_id",
        "type",
        "user_id",
        "is_activated",
        "activated_by",
        "activated_at",
        "activation_code_id",
        "transferred_at",
    ];

    public function order() {
        return $this->belongsTo(Order::class, "order_id");
    }

    public function user() {
        return $this->belongsTo(User::class, "user_id");
    }
}

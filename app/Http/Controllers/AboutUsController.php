<?php

namespace App\Http\Controllers;

use App\Models\CustomField;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index() {
        $about_us = CustomField::where('field', 'about-us-page-contents')->first();

        return view('about', [
            "about_us" => $about_us
        ]);
    }
}

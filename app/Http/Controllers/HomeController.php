<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\CustomField;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function launchingSoon() {
        

        return view('not-ready');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // if (!auth()->check() && env('APP_ENV') == 'prod') {
        //     return redirect('/not-ready');
        // }
        
        $packages = Package::where('type', 'package')
                        ->whereNull('disabled_at')
                        ->with('settings')
                        ->get()->sortBy('homepage_priority')->values();

        $customFields = CustomField::whereIn('field', [
            'slider1', 
            'slider2', 
            'slider3', 
            'about-us', 
            'about-us-right-image', 
            'home-background-1',
            'home-background-2',
            'home-block-1',
            'home-block-2',
            'home-block-3',
        ])->get(); 
        $slides = $customFields->whereIn('field', ['slider1','slider2','slider3'])->map(function($slide) {
            $slide->data = json_decode($slide->data);
            return $slide;
        })->values();

        $home_about_us = (object)[
            "text"  => $customFields->firstWhere('field', 'about-us')->data,
            "image" => $customFields->firstWhere('field', 'about-us-right-image')->data
        ];

        $backgrounds = $customFields->whereIn('field', ['home-background-1', 'home-background-2'])->values()->map(function ($record, $key) {
            $data = json_decode($record->data);
            
            return (object) [
                'order'      => $key + 1,
                'background' => ($data->darken ? "radial-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.75)), " : "") . "url('{$data->url}')"
            ];
        })->keyBy('order')->map(function ($record) {
            return $record->background;
        });

        $products = $customFields->whereIn('field', ['home-block-1', 'home-block-2', 'home-block-3'])->values()->map(function ($record, $key) {
            $record->data = \json_decode($record->data);
            $record->order = $key + 1;
            return $record;
        })->keyBy('order')->map(function ($record) {
            return $record->data;
        });

        return view('welcome', compact('packages', 'slides', 'home_about_us', 'backgrounds', 'products'));
    }
}

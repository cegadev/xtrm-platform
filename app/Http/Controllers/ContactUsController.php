<?php

namespace App\Http\Controllers;

use App\Models\CustomField;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index() {
        $data = CustomField::whereIn("field", ["contact-info-emails", "contact-info-mobile-numbers", "contact-info-phone-numbers", "contact-info-address"])->get()->keyBy("field");

        return view('contact-us', [
            "mobile_numbers" => $data->get("contact-info-mobile-numbers")->data,
            "phone_numbers" => $data->get("contact-info-phone-numbers")->data,
            "emails" => $data->get("contact-info-emails")->data,
            "address" => $data->get("contact-info-address")->data
        ]);
    }
}

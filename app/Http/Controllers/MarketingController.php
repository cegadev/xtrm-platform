<?php

namespace App\Http\Controllers;

use App\Models\EarningType;
use Illuminate\Http\Request;

class MarketingController extends Controller
{
    public function index() {
        // $ways_of_earning = collect([
        //     (object)[
        //         "title" => "Direct Selling",
        //         "description" => "<p>Purchase any Classic Zoom perfume at Php 220 - Php 130 discount</p><p>Purchase any Elegant Zoom perfume at Php 520 - Php 280 discount</p>",
        //     ],
        //     (object)[
        //         "title" => "Direct Referral",
        //         "description" => "<p>Purchase any Classic Zoom perfume at Php 220 - Php 130 discount</p><p>Purchase any Elegant Zoom perfume at Php 520 - Php 280 discount</p>",
        //     ],
        //     (object)[
        //         "title" => "Indirect Referral",
        //         "description" => "<p>Purchase any Classic Zoom perfume at Php 220 - Php 130 discount</p><p>Purchase any Elegant Zoom perfume at Php 520 - Php 280 discount</p>",
        //     ],
        //     (object)[
        //         "title" => "Unilevel Commission",
        //         "description" => "<p>Purchase any Classic Zoom perfume at Php 220 - Php 130 discount</p><p>Purchase any Elegant Zoom perfume at Php 520 - Php 280 discount</p>",
        //     ],
        //     (object)[
        //         "title" => "Finders Fee",
        //         "description" => "<p>Purchase any Classic Zoom perfume at Php 220 - Php 130 discount</p><p>Purchase any Elegant Zoom perfume at Php 520 - Php 280 discount</p>",
        //     ],
        // ]);

        $ways_of_earning = EarningType::all()->sortBy('priority');

        return view('marketing', [
            "ways_of_earning" => $ways_of_earning
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;

class ProductController extends Controller
{
    public function index() {
        $categories = ProductCategory::with('products')
                                ->with('products.publishedReviews')
                                ->get();
                                
        $products_with_no_categories = Product::whereNull('product_category_id')->where('type', 'product')->get();
        $categories->push((object)[
            "name" => "Others",
            "products" => $products_with_no_categories
        ]);

        return view('products', [
            "categories" => $categories
        ]);
    }

    public function viewProduct($productSlug) {
        $product = Product::whereSlug($productSlug)
                            ->with('category')
                            ->with('publishedReviews.from')
                            ->first();
        $totalRelated = 4;
        $related_products = Product::where('product_category_id', $product->product_category_id)
                                    ->where('id', '<>', $product->id)
                                    ->whereType('product')
                                    ->limit($totalRelated)->get();

        $no_related = $related_products->count() == 0;

        if ($related_products->count() < $totalRelated) {
            $except = $related_products->pluck("id");
            $except[] = $product->id;
            $left = $totalRelated - $related_products->count();
            $more_products = Product::whereNotIn('id', $except)
                                ->take($left)
                                ->whereType('product')
                                ->inRandomOrder()->limit($left)
                                ->get();

            $related_products = $related_products->merge($more_products);
        }

        $reviews = $product->publishedReviews->shuffle()->take(2);

        return view('products-info', [
            "data" => $product,
            "no_related" => $no_related,
            "related_products" => $related_products->shuffle(),
            "reviews" => $reviews
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Earning;
use App\Models\Membership;
use Illuminate\Http\Request;
use App\Models\ActivationCode;
use App\Models\CompanyEarning;
use App\Models\EncashmentHistory;
use App\Http\Controllers\Controller;
use App\Http\Helpers\TransactionsHelper;

class DashboardController extends Controller
{
    //

    public function index() {
        $loggedUser = auth()->user();
        


        if (auth()->user()->isMember()) {
            $member = $loggedUser->membership;
            TransactionsHelper::claimBonus($member);

            $monthly_earnings = 0;
            $yearly_earnings = 0;
            $now = \Carbon\Carbon::now();
            $membership = auth()->user()->membership;
            $ways = collect([
                'direct referral'   => 'Direct Referral',
                'indirect referral' => 'Indirect Referral Bonus', 
                'unilevel'          => 'Unilevel Commission',
                'lic'               => 'Leadership Commission'
            ]);

            $ways_of_earning = $ways->map(function($record, $key) use($membership, $now){
                                return (object)[
                                    "label" => $record,
                                    "amount" => $membership->earnings->filter(function($item) use($key, $now) {
                                                if ($key == "direct referral") {
                                                    return $item->type == $key;
                                                } else {
                                                    return $item->type == $key && !is_null($item->added_to_wallet_at);
                                                }
                                            })->sum("amount")
                                ];
                            })->values();

            $associates = Membership::with('user')->orderBy('created_at', 'desc')->descendantsOf($membership->id);
            $activation_codes = ActivationCode::where('status', 'released')
                                ->where('released_to', auth()->id())
                                ->whereNull('activated_at')
                                ->count();

            $last_month_bonus = 0;
            $firstDayOfMonth = Carbon::now()->firstOfMonth();
            $not_claimed_bonuses = Earning::where("member_id", $membership->id)
                                            ->whereIn("type", ["indirect referral", "unilevel", "lic"])
                                            ->whereNotNull("valid_at")
                                            ->whereNull("added_to_wallet_at")
                                            ->whereDate("valid_at", $firstDayOfMonth)
                                            ->get();
            if (is_null($membership->prev_month_bonus_claimed_at) && $not_claimed_bonuses->count() > 0) {
                $last_month_bonus  = $not_claimed_bonuses->sum("amount");
            }
            return view('admin.index', [
                "logged_user" => $loggedUser,
                "membership" => $membership,
                "associates" => $associates,
                "ways_of_earning" => $ways_of_earning,
                "total_earnings" => $membership->total_earnings,
                "available" => $membership->available_commissions,
                "encashment" => $membership->encashment_histories->whereIn('status',['complete', 'pending'])->sum('amount'),
                "activation_codes" => $activation_codes,
                "last_month_bonus" => $last_month_bonus
            ]);
        } else {
            if ($loggedUser->managedBranch) {
                $associates = Membership::with('user')->whereHas('order', function($q) use ($loggedUser){
                    $q->where('branch_id', $loggedUser->managedBranch->id);
                })->get();
                $new_members = Membership::whereHas('order', function($q) use ($loggedUser){
                    $q->where('branch_id', $loggedUser->managedBranch->id);
                })->whereMonth("created_at", Carbon::now()->month)->count();
            } else {
                $associates = Membership::with('user')->latest()->get();
                $new_members = Membership::whereMonth("created_at", Carbon::now()->month)->count();
            }
            // $earnings = Earning::all()->map(function($record) {
            //     $record->month = $record->created_at->format("Y-M");
            //     $record->year = $record->created_at->format("Y");
            //     return $record;
            // });

            // $ways_of_earning = collect([
            //     "direct referral" => "Direct Referral", 
            //     "indirect referral" => "Indirect Referral Bonus",
            //     "unilevel"          => "Unilevel Commission"
            // ])->map(function($record, $key) use($earnings){
            //     return (object)[
            //         "label" => $record,
            //         "amount" => $earnings->filter(function($item) use($key) {
            //                     return $item->type == $key;
            //                 })->sum("amount")
            //     ];
            // })->values();
            $company_earnings = CompanyEarning::groupBy("type")->selectRaw("sum(amount) as total")->addSelect('type')->get()->keyBy('type');
            $encashment = EncashmentHistory::whereStatus('complete')->sum("amount");

            $company_income = collect([
                "product purchase" => "Product Orders",
                "package purchase" => "Package Orders",
                "service fee" => "Procesing Fee",
                "encashment" => "Encashment (payout)",
            ])->map(function($record, $key) use ($company_earnings, $encashment) {
                if ($key == "encashment") {
                    return (object)[
                        "label" => $record,
                        "amount" => $encashment
                    ];
                } else {
                    return (object)[
                        "label" => $record,
                        "amount" => $company_earnings->get($key) ? $company_earnings->get($key)->total : 0
                    ];
                }
            });

            return view('admin.index', [
                "logged_user" => $loggedUser,
                "total_members" => $associates->count(),
                "associates" => $associates,
                "total_earnings" => CompanyEarning::sum("amount"),
                "branch_earnings" => $loggedUser->managedBranch ? CompanyEarning::whereHas("order", function($q) use ($loggedUser){
                    $q->where("branch_id", $loggedUser->managedBranch->id);
                })->sum("amount") : CompanyEarning::sum("amount"),
                // "current_month_earnings" => $earnings->filter(function($record) {
                //                             return $record->month == Carbon::now()->format("Y-M");
                //                         })->sum("amount"),
                // "current_year_earnings" => $earnings->filter(function($record) {
                //                             return $record->year == Carbon::now()->format("Y");
                //                         })->sum("amount"),
                // "ways_of_earning" => $ways_of_earning,
                "company_income" => $company_income,
                "new_members_month" => $new_members
            ]);
        }
    }
}

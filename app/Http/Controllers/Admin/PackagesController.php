<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Package;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\PackageSetting;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    public function index(Request $request) {
        $tab = $request->tab ?? 'active';
        $availableTabs = ['active', 'disabled'];

        if (!in_array($tab, $availableTabs)) { abort(404); }

        $packages = Package::where('type', 'package');
        
        $packages = $packages->where(function ($q) use ($tab) {
            if ($tab == 'active') {
                $q->whereNull('disabled_at');
            } else {
                $q->whereNotNull('disabled_at');
            }
        });

        $packages = $packages->paginate(100);

        return view('admin.packages.index', [
            'packages' => $packages,
            'tab' => $tab
        ]);
    }

    /**
     * create
     *
     * @return void
     */
    public function create() {
        return view('admin.packages.create');
    }

    /**
     * save
     *
     * @param Request $request
     * @return void
     */
    public function save(Request $request) {
        $request->validate([
            'package_name' => 'required|max:255|unique:products,name',
            'code' => 'required|max:100|unique:products,code',
            'price' => 'required|numeric|min:1',
            'direct_referral_commission' => 'required|numeric',
            'indirect_referral_commission' => 'required|numeric|between:0,100',
            'limit_per_day' => 'required|numeric|min:1',
        ]);

        \DB::transaction(function () use($request) {
            $lastItem = Package::where('type', 'package')->whereNull('disabled_at')->orderBy('homepage_priority', 'desc')->first();

            $lastNumber = !empty($lastItem) ? $lastItem->homepage_priority : 0;

            $package = Package::create([
                "name" => $request->package_name,
                "price" => $request->price,
                "code" => $request->code,
                "type" => "package",
                "slug" => \Str::slug($request->package_name),
                'description' => $request->description ?? "",
                "homepage_priority" => $lastNumber + 1
            ]);

            $settings = new PackageSetting([
                "direct_referral_commission" => $request->direct_referral_commission,
                "indirect_referral_percentage" => $request->indirect_referral_commission,
                "inclusions" => $request->inclusions,
                "irb_limit_per_day" => $request->limit_per_day,
                "irb_limit_per_month" => $request->limit_per_day * 30
            ]);

            $package->settings()->save($settings);
        });

        return redirect()->route('packages.index')->with('success', 'Package successfully created.');
    }
    
    /**
     * edit
     *
     * @param Package $package
     * @return void
     */
    public function edit(Package $package) {
        $defaultItems = collect(range(1, 10))->map(function ($level) {
            return (object) [
                'level' => $level,
                'percent' => 0
            ];
        });

        $lic_items = !is_null($package->settings->leadership_indirect_commission_percentage) ? collect(json_decode($package->settings->leadership_indirect_commission_percentage)) : $defaultItems;

        return view('admin.packages.edit', compact('package', 'lic_items'));
    }
    

    /**
     * update
     *
     * @param Package $package
     * @param Request $request
     * @return void
     */
    public function update(Package $package, Request $request) {
        $request->validate([
            'package_name' => 'required|max:255|unique:products,name,' . $package->id,
            'code' => 'required|max:100|unique:products,code,' . $package->id,
            'price' => 'required|numeric|min:1',
            'direct_referral_commission' => 'required|numeric',
            'indirect_referral_commission' => 'required|numeric|between:0,100',
            'limit_per_day' => 'required|numeric|min:1',
        ]);

        $lic_values = null;
        if ($request->has('enabled_lic')) {
            $lic_values = collect($request->leadership_indirect_commission)->map(function ($value, $key) {
                return [
                    'level' => $key + 1,
                    'percent' => $value
                ];
            })->toJson();
        }

        \DB::transaction(function () use ($package, $request, $lic_values) {
            $package->name = $request->package_name;
            $package->code = $request->code;
            $package->price = $request->price;
            $package->slug = \Str::slug($request->package_name);
            $package->description = $request->description ?? "";
            $package->enabled_lic = $request->has('enabled_lic');
            $package->save();

            $settings = $package->settings;
            $settings->direct_referral_commission = $request->direct_referral_commission;
            $settings->indirect_referral_percentage = $request->indirect_referral_commission;
            $settings->inclusions           = $request->inclusions;
            $settings->irb_limit_per_day    = $request->limit_per_day;
            $settings->irb_limit_per_month  = $request->limit_per_day * 30;
            
            $settings->leadership_indirect_commission_percentage = $lic_values;
            $settings->lic_limit_per_day    = $request->lic_limit_per_day;
            $settings->lic_limit_per_month  = $request->has('enabled_lic') ? $request->lic_limit_per_day * 30 : 0;
            $package->settings()->save($settings);
        });

        return redirect()->route('packages.index')->with('success', 'Package successfully updated.');
    }

    public function enableItem(Request $request) {
        $request->validate([
            'enable_id' => 'required'
        ]);

        Package::whereId($request->enable_id)->update([
            'disabled_at' => null
        ]);

        return back()->with('success', 'Package successfully enabled.');
    }
    
    public function disableItem(Request $request) {
        $request->validate([
            'disable_id' => 'required'
        ]);

        Package::whereId($request->disable_id)->update([
            'disabled_at' => Carbon::now()
        ]);

        return back()->with('success', 'Package successfully disabled.');
    }
}

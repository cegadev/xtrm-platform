<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Carbon\Carbon;
use App\Models\Earning;
use App\Models\ProductCode;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\ActivationCode;
use App\Models\CompanyEarning;
use App\Models\MemberBankInfo;
use App\Models\EncashmentHistory;
use App\Http\Controllers\Controller;
use App\Http\Helpers\TransactionsHelper;

class EncashmentController extends Controller
{
    public function index() {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $weeklyLimitActivation = 3;
        $membership = auth()->user()->membership;

        $summary = collect();
        $now = Carbon::now();
        $weekStart = $now->startOfWeek()->toDateTimeString();
        $weekEnd = $now->endOfWeek()->toDateTimeString();


        $productsActivationsCount = ActivationCode::whereBetween("activated_at", [$weekStart, $weekEnd])
                                        ->where("activated_by", auth()->id())
                                        ->where('type', 'product')
                                        ->count();

        // $packagesActivationCount = Earning::where("type", "direct referral")
        //                                 ->where("member_id", $membership->id)
        //                                 ->whereBetween("created_at", [$weekStart, $weekEnd])
        //                                 ->count();

        $all_earnings = $membership->earnings->groupBy("type")->map(function($records) {
            return (object)[
                "total" => $records->count(),
                "amount" => $records->sum("amount"),
            ];
        });


        // $summary["all"] = $all_earnings;

        $available_commissions = $membership->eWallet();

        // dd($available_commissions);
        // if ($productsActivationsCount < $weeklyLimitActivation) {
        //     $irb = $all_earnings->get("indirect referral") ? $all_earnings->get("indirect referral")->amount : 0;
        //     $unilevel = $all_earnings->get("unilevel") ? $all_earnings->get("unilevel")->amount : 0;

        //     $available_commissions = $available_commissions - ($irb + $unilevel);
        // }
        
        // $total_encashment = $membership->encashment_histories->where('status', '<>', 'declined')->sum('amount_released');

        // $summary["current_week"] = Earning::where("member_id", $membership->id)
        //                     ->whereBetween("created_at", [$weekStart, $weekEnd])
        //                     ->get()
        //                     ->groupBy("type")->map(function($records) {
        //                         return (object)[
        //                             "total" => $records->count(),
        //                             "amount" => $records->sum("amount"),
        //                         ];
        //                     });
        // $start = Carbon::parse('first day of this month')->startOfDay()->toDateTimeString();
        // $end = Carbon::parse('last day of this month')->endOfDay()->toDateTimeString();
                            
        // $summary["current_month"] = Earning::where("member_id", $membership->id)
        //                     ->whereBetween("created_at", [$start, $end])
        //                     ->get()
        //                     ->groupBy("type")->map(function($records) {
        //                         return (object)[
        //                             "total" => $records->count(),
        //                             "amount" => $records->sum("amount"),
        //                         ];
        //                     });
        $bank_infos = $membership->bank_infos;

        $transaction_types = [
            "paymaya" => "Paymaya",
            "gcash" => "G Cash",
            "bdo" => "BDO",
            "others" => "Others",
        ];
        
        return view('admin.encashment.index', [ 
            "available_commissions" => $available_commissions ?? 0,
            "encashment_histories" => $membership->encashment_histories->map(function($history) {

                switch($history->status) {
                    case "complete":
                        $status_date = Carbon::parse($history->approved_at);
                    break;

                    case "declined":
                        $status_date = Carbon::parse($history->declined_at);
                    break;
                    
                    default:
                        $status_date = Carbon::parse($history->requested_at);
                    break;
                }

                $history->status = $history->status == "pending" ? "for Approval" : $history->status;
                $history->status_date = $status_date;
                return $history;
            }),
            "transaction_types" => $transaction_types
            // "summary" => $summary,
            // "activation_count" => (object) [
            //     "product" => $productsActivationsCount,
            //     "package" => $packagesActivationCount
            // ],
            // "total_encashment" => $total_encashment
        ]);
    }

    /**
     * createRequest
     *
     * @param Request $request
     * @return void
     */
    public function createRequest(Request $request) {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $now = Carbon::now();
        $vat = 0.10; // 10%
        $service_fee = 20;
        $weeklyLimitActivation = 3;
        $membership = auth()->user()->membership;
        $weekStart = $now->startOfWeek()->toDateTimeString();
        $weekEnd = $now->endOfWeek()->toDateTimeString();
        $transaction_type = $request->transaction_type;
        $available = $membership->available_commissions ?? 0;
        $type = $request->type;

        
        // $productsActivationsCount = ActivationCode::whereBetween("activated_at", [$weekStart, $weekEnd])
        //                                 ->where("activated_by", auth()->id())
        //                                 ->where("type", "product")
        //                                 ->count();
        // if ($productsActivationsCount < $weeklyLimitActivation) {
        //     $all_earnings = $membership->earnings->groupBy("type")->map(function($records) {
        //         return (object)[
        //             "total" => $records->count(),
        //             "amount" => $records->sum("amount"),
        //         ];
        //     });

        //     $irb = $all_earnings->get("indirect referral") ? $all_earnings->get("indirect referral")->amount : 0;
        //     $unilevel = $all_earnings->get("unilevel") ? $all_earnings->get("unilevel")->amount : 0;

        //     $available = $available - ($irb + $unilevel);
        // }
        $amount = intval($request->amount);
        $added_vat = $amount * $vat;
        $total_encashment = $amount - ($added_vat + $service_fee);

        $addons = ($added_vat) + $service_fee;
        $must_not_exceed = $available;
        $new_balance = $membership->available_commissions - $amount;

        $validations = [
            "type" => "required|in:cash,bank",
            "amount" => "required|numeric|min:500|lte:{$must_not_exceed}",
            "transaction_type" => "required_if:type,==,bank",
            "bank_account_number" => "required_if:type,==,bank",
            "bank_account_name" => "required_if:type,==,bank",
        ];
        
        if ($type == "bank") {
            if ($transaction_type == "others" && $request->others_bank_name == "new") {
                $validations["new_bank_name"] = "required";
            }
        }

        
        $validate = Validator::make($request->all(), $validations,
        [
            "amount.lt" => "Insufficient funds. Encashment of amount " . number_format($amount, 2) . " is invalid.", 
        ]);

        $validate->validate();
        

        
        \DB::transaction(function () use ($membership, $available, $total_encashment, $request, $vat, $added_vat, $service_fee, $type, $transaction_type, $new_balance){
            if ($type == "bank") {
                $bank_info = "";
                if (in_array($transaction_type, ["gcash", "paymaya", "bdo"])) {
                    $bank = $membership->bank_infos()->firstOrCreate([
                        "type" => $transaction_type
                    ],
                    [
                        "type" => $transaction_type,
                        "name" => $transaction_type == "bdo" ? "BDO" : \Str::ucfirst($transaction_type),
                        "value" => $request->bank_account_number,
                        "account_name" => $request->bank_account_name,
                    ]);
                    
                    $bank_info = json_encode([
                        "member_id"     => $membership->id,
                        "bank"          => $transaction_type,
                        "bank_id"       => $bank->id ?? null,
                        "account_number" => $request->bank_account_number,
                        "account_name" => $request->bank_account_name,
                    ]);

                } else {
                    $bank = $membership->bank_infos()->firstOrCreate([
                        "type" => "other",
                        "name" => $request->new_bank_name,
                    ], [
                        "value" => $request->bank_account_number,
                        "account_name" => $request->bank_account_name,
                        "is_other" => true
                    ]);

                    $bank_info = json_encode([
                        "member_id"     => $membership->id,
                        "bank"          => $transaction_type,
                        "bank_id"       => $bank->id ?? null,
                        "account_number" => $request->bank_account_number,
                        "account_name"  => $request->bank_account_name,
                    ]);
                }
            } else {
                $bank_info = json_encode([
                    "member_id" => $membership->id,
                    "bank_id" => null
                ]);
            }
            
            $membership->update([
                "available_commissions" => $new_balance
            ]);
                
            $encashment = new EncashmentHistory([
                "transaction_type" => $type == "cash" ? "cash" : $request->transaction_type,
                "amount" => $request->amount,
                "vat_applied" => $vat,
                "service_fee" => $service_fee,
                "amount_released" => $total_encashment,
                "status" => "pending",
                "requested_at" => Carbon::now(),
                "requested_by" => auth()->id(),
                "bank_info" => $bank_info,
                "membership_id" => $membership->id,
            ]);

            $encashment->save();

            TransactionsHelper::createTransactionRecord($membership, $request->amount, "encashment", $encashment->id, auth()->user());

            $encashment->update([
                "transaction_recorded_at" => Carbon::now()
            ]);
        });

        return redirect()->back()->with("success", "Encashment request has been successfully submitted. Expect the approval on the upcoming Tuesday.");
    }

    public function requests(Request $request) {
        $tab = $request->tab ?? "pending";
        $histories = EncashmentHistory::where("status", $tab)->with("requestedBy")->latest()->get();

        $pendingCount = EncashmentHistory::where("status", "pending")->count();
        return view('admin.encashment.requests', [
            "encashments" => $histories,
            "tab" => $tab,
            "pending_count" => $pendingCount
        ]);
    }

    /**
     * actionRequests
     *
     * @param Request $request
     * @return void
     */
    public function actionRequests(Request $request) {

        $request->validate([
            'action' => 'required|in:approve,decline'
        ]);
        
        $action = $request->action;
        $isMultiple = !empty($request->encashment_chk);

        if ($isMultiple) {
            $ids = $request->encashment_chk;
            
            \DB::transaction(function () use ($action, $ids) {
                collect($ids)->each(function($id) use ($action) {
                    $this->processEncashment($action, $id);
                });
            });
        } else {
            $this->processEncashment($action, $request->encashment_id);
        }

        if ($action == "approve") {
            return redirect()->back()->with('success', 'Encashment requests successfully approved!');
        }
        
        if ($action == "decline") {
            return redirect()->back()->with('success', 'Encashment requests successfully declined!');
        }
    }
    
    /**
     * Process Encashment
     *
     * @param string $action
     * @param integer $id
     * @return void
     */
    private function processEncashment(string $action, int $id) 
    {
        if ($action == "approve") {
            \DB::transaction(function () use ($id) {
                $encashment = EncashmentHistory::find($id);
                $encashment->status = "complete";
                $encashment->approved_at = Carbon::now();
                $encashment->approved_by = auth()->id();
                $encashment->save();


                $company_earning = new CompanyEarning;
                $company_earning->type = "service fee";
                $company_earning->amount = $encashment->service_fee;
                $company_earning->resource_id = $encashment->id;
                $company_earning->resource_type = "encashment";
                $company_earning->notes = "Encashment approved.";
                $company_earning->transacted_by = auth()->id();
                $company_earning->save();
            });
        }

        if ($action == "decline") {
            \DB::transaction(function() use ($id) {

                $encashment = EncashmentHistory::find($id);
                $encashment->status = "declined";
                $encashment->declined_at = Carbon::now();
                $encashment->declined_by = auth()->id();
                $encashment->save();

                $member = $encashment->member;
                $member->update([
                    "available_commissions" => $member->available_commissions + $encashment->amount
                ]);

                TransactionsHelper::refundEncashmentTransaction($member, $encashment, auth()->user());
            });
        }
    }

    public function view(EncashmentHistory $encashment, Request $request) {
        return view('admin.encashment.view', [
            "encashment" => $encashment,
            "back_url" => $request->back_url ?? null,
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Branch;
use Illuminate\Http\Request;
use App\Models\BranchManager;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class BranchManagerController extends Controller
{
    public function create(Branch $branch) {
        return view('admin.branches.manager.create', [
            "branch" => $branch
        ]);
    }

    public function save(Branch $branch, Request $request) {
        $request->validate([
            "first_name"    => "required|max:255",
            "last_name"     => "required|max:255",
            "middle_name"   => "max:255",
            "username"      => "required|max:50|unique:users,username",
            "mobile_number" => "min:11",
            "password"      => "required|min:6|max:50",
            "email"         => "required|email|unique:users,email"
        ]);

        \DB::transaction(function () use ($request, $branch) {
                
            $user = new User([
                "first_name" => $request->first_name,
                "middle_name" => $request->middle_name,
                "last_name" => $request->last_name,
                "username"  => $request->username,
                "email"  => $request->email,
                "mobile_number"  => $request->mobile_number,
                "password" => Hash::make($request->password),
            ]);

            $user->assignRole('Manager');

            $user->save();
            
            BranchManager::create([
                "branch_id" => $branch->id,
                "user_id" => $user->id
            ]);
        });

        return redirect()->route('branches.view', [$branch->id, "tab" => "managers"])->with('success', "Branch Manager successfully added to " . $branch->name . ".");
    }

    public function remove(Branch $branch, Request $request) {
        $remove = $request->remove_id;
        BranchManager::find($remove)->update([
            "is_suspended" => true
        ]);

        return redirect()->route('branches.view', [$branch->id, "tab" => "managers"])->with('success', "Branch Manager successfully disabled to " . $branch->name . ".");
    }

    public function enable(Branch $branch, Request $request) {
        $enable = $request->enable_id;
        BranchManager::find($enable)->update([
            "is_suspended" => false
        ]);

        return redirect()->route('branches.view', [$branch->id, "tab" => "managers"])->with('success', "Branch Manager successfully enabled to " . $branch->name . ".");
    }
}

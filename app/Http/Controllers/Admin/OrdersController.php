<?php

namespace App\Http\Controllers\Admin;

use DB;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Branch;
use App\Models\Earning;
use App\Models\Product;
use App\Models\Membership;
use App\Models\ProductCode;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Models\ActivationCode;
use App\Models\CompanyEarning;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\MemberActivationCode;
use App\Models\ReleasePackageHistory;
use App\Http\Repository\ActivationCodeRepository;

class OrdersController extends Controller
{
    public function index(Request $request) {
        $status = $request->tab ?? 'pending';
        $user = \Auth::user();

        $orders = Order::with('product', 'user')
                        ->where(function($q) use ($status) {
                            if ($status == "released") {
                                $q->whereIn('status', ["released", "received"]);
                            } else {
                                $q->where('status', $status);
                            }
                        });

        if ($user->isManager() && $user->managedBranch) {
            $orders = $orders->where(function($q) use ($user) {
                $q->where("branch_id", $user->managedBranch->id);
            });
        }
        if (in_array($status, ["released", "received"])) {
            $orders = $orders->orderBy("released_at", "DESC");
        }
        $orders = $orders->paginate(20);

        $tabs = ['pending', 'released', 'completed'];
        return view('orders.index', [
            'orders' => $orders->appends(['tab' => $status]),
            'tabs' => $tabs,
            'status' => $status
        ]);
    }

    /**
     * Create order
     *
     * @return void
     */
    public function create() {
        return view('orders.create');
    }

    /**
     * Save order
     *
     * @param Request $request
     * @return void
     */
    public function save(Request $request) {
        /**
         * Find product or fail
         */
        \DB::transaction(function () use ($request) {
            $product = Product::findOrFail($request->product_id);
            $logged_user = auth()->user();

            $order = new Order;
            $order->user_id             = $request->member_id;
            $order->product_id          = $request->product_id;
            $order->quantity            = $request->quantity;
            $order->amount              = $product->price * \intval($request->quantity);
            $order->requested_by        = auth()->id();


            if (auth()->user()->hasPermissionTo('orders.write')) {
                $order->status              = 'released'; 
                $order->released_at         = Carbon::now();
                $order->released_by         = auth()->id();

                if ($product->type == 'package') {
                    // $order->total_activation_left = $request->quantity;
                    
                    if ($logged_user->isManager() && $logged_user->managedBranch) {
                        $order->branch_id = $logged_user->managedBranch->id;
                    }
                }
        
                if ($product->type == 'product') {
                    $order->status          = 'received'; 
                }

                
            } else {
                $order->status              = 'pending';
            }

            $order->save();

            CompanyEarning::create([
                "type"          => $product->type . " purchase",
                "amount"        => $order->amount,
                "resource_id"   => $order->user_id,
                "resource_type" => "order",
                "notes"         => \Str::ucfirst($product->type) . " purchase approved.",
                "transacted_by" => auth()->id(),
                "order_id"      => $order->id
            ]);

            if ($product->type == 'product') {
                $packageHistory = new ReleasePackageHistory([
                    "user_id" => $request->member_id
                ]);

                $order->package_histories()->save($packageHistory);

                $product_codes = [];
                for ($i = 0; $i < $order->quantity; $i++) {
                    $product_key = "MJP-" . Helpers::generate_license();

                    $product_code = new ProductCode([
                        "product_id" => $order->product_id,
                        "code" => $product_key,
                    ]);

                    $product_codes[] = $product_code;
                }

                $order->product_codes()->saveMany($product_codes);

                if ($order->user->hasVerifiedEmail()) {
                    Helpers::notifyProductCodes($order);
                }
            } 


            // create order items
            Helpers::createOrderItems($order);
        });

        return redirect()->route('orders.index', ['tab' => 'released'])->with('success', 'Order successfully saved!');
            
    }
    
    /**
     * myOrders
     *
     * @return void
     */
    public function myOrders(Request $request) {
        $tab = $request->tab ?? "requests";

        $tabs = ["requests", "received", "completed"];
        if (!in_array($tab, $tabs)) {
            abort(404);
        }

        $receivedOrders = Order::whereIn('status', ['received', 'released'])
                            // ->whereHas('package_histories', function($q) {
                            //     $q->where('user_id', auth()->id())
                            //         ->where('status', 'received');
                            // })
                            ->whereHas('items', function($q){
                                $q->where('user_id', auth()->id())
                                    ->whereNull('activated_at');
                            })
                            ->where('user_id', auth()->id())
                            ->latest()
                            ->count();
        if ($tab == "received") {
            $orders = Order::whereIn('status', ['received', 'released'])
                            // ->whereHas('package_histories', function($q) {
                            //     $q->where('user_id', auth()->id())
                            //         ->whereIn('status', ['received', 'released']);
                            // })
                            // ->with('product')
                            ->whereHas('items', function($q){
                                $q->where('user_id', auth()->id())
                                    ->whereNull('activated_at');
                            })
                            ->where('user_id', auth()->id())
                            ->latest()
                            ->paginate(30);
        } else {
            $status = $tab == "requests" ? "pending" : $tab;
            $orders = Order::where('status', $status)
                            ->where('user_id', auth()->id())
                            ->with('product')
                            ->latest()
                            ->paginate(30);
        }
        
        return view('orders.my-orders', [
            'orders' => $orders->appends(['tab' => $tab]),
            'tab' => $tab,
            'received_orders_count' => $receivedOrders
        ]);
    }
    
    /**
     * saveOrderRequest
     *
     * @param  mixed $request
     * @return void
     */
    public function saveOrderRequest(Request $request) {
        $type = $request->type ?? "product";
        $request->validate([
            "quantity" => "required|numeric|min:1|max:100",
            "product" => "required",
        ], [
            "product.required" => \Str::ucfirst($type) . " is required."
        ]);

        $order = collect();
        
        \DB::transaction(function () use ($request, &$order) {
            $product = Product::find($request->product);
            
            $order = new Order([
                "user_id"       => auth()->id(),
                "requested_by"  => auth()->id(),
                "product_id"    => $request->product,
                "quantity"      => $request->quantity,
                "amount"        => $product->price * $request->quantity,
                "status"        => "pending",
            ]);
            
            $order->save();
                
            $url = route('orders.view', $order->id);
            Helpers::notifyNewOrder($order, $url);
        });

        return redirect()->route('my-orders.index', ['tab' => 'requests'])->with('success', 'Order has been placed. Order ID: #' . $order->id);
    }
    
    /**
     * approveRequest
     *
     * @param  mixed $order
     * @param  mixed $request
     * @return void
     */
    public function approveRequest(Order $order, Request $request) {

        if ($order->status !== "pending") {
            if ($order->status == "cancelled") {
                return redirect()->back()->withErrors("Order cannot be approved. It is already cancelled.");
            } else {
                return redirect()->back()->withErrors("Order cannot be approved. It is already completed.");
            }
        }

        \DB::transaction(function () use ($order, $request) {
                
            $product_type = $order->product->type;
            $order->released_at = Carbon::now();
            $order->released_by = auth()->id();
            
            if ($product_type == "package") {
                $order->status = "released";

                // $order->total_activation_left = $order->quantity;
            } else if ($product_type == "product") {
                $order->status = "received";

                $package_history = new ReleasePackageHistory([
                    "user_id" => $order->requested_by
                ]);

                $order->package_histories()->save($package_history);

                $product_codes = [];
                for ($i = 0; $i < $order->quantity; $i++) {
                    $product_key = "MJP-" . Helpers::generate_license();

                    $product_code = new ProductCode([
                        "product_id" => $order->product_id,
                        "code" => $product_key,
                    ]);

                    $product_codes[] = $product_code;
                }

                $order->product_codes()->saveMany($product_codes);
            }

            Helpers::createOrderItems($order);


            CompanyEarning::create([
                "type"          => $order->product->type . " purchase",
                "amount"        => $order->amount,
                "resource_id"   => $order->user_id,
                "resource_type" => "order",
                "notes"         => \Str::ucfirst($order->product->type) . " purchase approved.",
                "transacted_by" => auth()->id(),
                "order_id"      => $order->id,
            ]);
            
            $order->save();

            if ($order->user->hasVerifiedEmail()) {
                if ($order->product->type == "product") {
                    $url = route('my-orders.index', ["tab" => "received"]);
                } else {
                    $url = route('admin.activation.index');
                }
                
                Helpers::notifyRecievedOrder($order, $url);
            }

            if ($order->product->type == "product") {
                if ($order->user->hasVerifiedEmail()) {
                    Helpers::notifyProductCodes($order);
                }
            }
        });


        return redirect()->back()->with('success', 'Order request successfully released.');
    }

    /**
     * Cancel Request
     *
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function cancelRequest(Order $order, Request $request) {
        $order->status = "cancelled";
        $order->cancelled_at = Carbon::now();
        $order->save();

        return back()->with('success', "Order successfully cancelled.");
    }

    /**
     * updateOrderRequest
     *
     * @param Request $request
     * @return void
     */
    public function updateOrderRequest(Order $order, Request $request) {
        if (auth()->id() !== $order->user_id) {
            abort(404);
        }
        $product = $order->product;
        
        $request->validate([
            "quantity" => "required|numeric|min:1|max:100",
            "product" => "required",
        ], [
            "product.required" => \Str::ucfirst($product->type) . " is required."
        ]);

        if ($order->status !== "pending") {
            return redirect()->route('my-orders.index')->withErrors("Failed to update order. Order has already been released.");
        }
        
        $order->product_id  = $request->product;
        $order->quantity    = $request->quantity;
        $order->amount      = $product->price * intval($request->quantity);
        $order->save();

        return redirect()->route('my-orders.index', ['tab' => 'requests'])->with("success", "Order successfully updated");
    }
    
    // /**
    //  * updateOrderRequest
    //  *
    //  * @param Request $request
    //  * @return void
    //  */
    // public function updateOrderRequest(Request $request) {
    //     $id = $request->edit_id;
    //     $order = Order::find($id);
        
    //     if ($order->status !== "pending") {
    //         return redirect()->back()->withErrors("Failed to update order. Order has already been released.");
    //     }

    //     $product = Product::find($request->editItem);
        
    //     $order->product_id  = $request->editItem;
    //     $order->quantity    = $request->editQty;
    //     $order->amount      = $product->price * intval($request->editQty);
    //     $order->save();

    //     return redirect()->route('my-orders.index', ['tab' => 'requests'])->with("success", "Order successfully updated");
    // }

    /**
     * cancelOrder
     *
     * @param Request $request
     * @return void
     */
    public function cancelOrder(Request $request) {
        $order = Order::find($request->id);
        $order->status = "cancelled";
        $order->cancelled_at = Carbon::now();
        $order->save();

        return redirect()->route('my-orders.index', ['tab' => 'requests'])->with("success", "Order successfully cancelled");
    }

    /**
     * activateOrder
     *
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function activateOrder(Order $order, Request $request) {
        $orderItemsOwners = $order->items->pluck('activated_by')->toArray();

        if (auth()->id() !== $order->user_id && !in_array(auth()->id(), $orderItemsOwners)) {
            abort(404);
        }
        $tab = $request->tab ?? "activated";
        $tabs = ["activated"];
        if (!in_array($tab, $tabs)) { abort(404); }

        $activated_items = ActivationCode::where("order_id", $order->id)->count();

        $activation_codes = MemberActivationCode::where("user_id", auth()->id())
                                            ->whereHas('activation_code', function($q) {
                                                $q->where('status', 'activated')
                                                    ->whereNull('order_id');
                                            })
                                            ->with('activation_code')
                                            ->get();
        $for_activation = $order->quantity - $activated_items; 
        $registrations = collect();
        if ($tab == "activated") {
            if ($order->product->type == "package") {
                $registrations = Membership::where('order_id', $order->id)->get();
            } else {
                $registrations = ActivationCode::where('order_id', $order->id)->orderBy('registered_at', 'desc')->get();
            }
        }

        return view('orders.activate', [
            "order" => $order,
            "back_url" => $request->back_url ?? null,
            "member_activation_codes" => (object)[
                "product" => $activation_codes->where("activation_code.type", "product")->count(),
                "package" => $activation_codes->where("activation_code.type", "package")->count(),
            ],
            "for_activation" => $for_activation,
            "activated_items" => $activated_items,
            "registrations" => $registrations,
            "tab" => $tab
        ]);
    }

    /**
     * activateProduct
     *
     * @param Order $order
     * @return void
     */
    private function activateProduct(Order $order) {
        $total_activation_of_order = $order->product_codes;

        $unilevelLimit = 5;
        $points = 5;
        $monthlyLimitPerTeam = 10;
        $me = auth()->user()->membership;

        $ancestors = collect(explode(",", $me->ancestor_ids))->reverse()->values()->take($unilevelLimit);
        $currentMonth = Carbon::now()->format("Y-m");
        $currentYear = Carbon::now()->format("Y");

        if (!$me->isRoot() && $ancestors->count()) {
            \DB::transaction(function () use ($ancestors, $points, $unilevelLimit, $me, $order, $currentMonth, $currentYear, $monthlyLimitPerTeam){
                $canEarn = collect();
                $members = Membership::whereIn("id", $ancestors)
                        ->with('earnings')
                        ->get()->keyBy("id")
                        ->map(function($record, $key) use($me, $currentMonth, $currentYear, $monthlyLimitPerTeam, &$canEarn) {
                            $earnings_this_month_to_this_member = $record->earnings->filter(function($record) use ($me, $currentMonth, $currentYear, $monthlyLimitPerTeam, &$canEarn) {
                                return $record->created_at->format("Y-m") == $currentMonth 
                                        && $record->earned_from_member_id == $me->id 
                                        && $record->type == "unilevel";
                            })->count();


                            $record->current_month_earnings = $record->earnings->filter(function($record) use ($currentMonth) {
                                return $record->created_at->format("Y-m") == $currentMonth;
                            })->sum("amount");
                            
                            $record->current_year_earnings = $record->earnings->filter(function($record) use ($currentYear) {
                                return $record->created_at->format("Y") == $currentYear;
                            })->sum("amount");
                            $canEarn->put($key, $earnings_this_month_to_this_member < $monthlyLimitPerTeam);
                            return $record;
                        });

                /**
                 * If has ancestors, then give unilevel commission
                 */
                // set points
                $ancestors_points = $ancestors->map(function($record, $key) use ($points){
                    return (object)[
                        "ancestor" => $record,
                        "points" => $points - $key
                    ];
                });


                $earnings = collect();
                $ancestors_points->each(function($record) use ($members, &$earnings, $me, $order, $canEarn) {
                    $member = $members->get($record->ancestor);

                    $earning = new Earning;
                    $commission = $record->points;
                    $can_earn = $canEarn->get($member->id) ?? false;
                    if ($can_earn) {
                        $notes = "Earned unilevel commission from member #" . $me->id . ".";
                    } else {
                        $notes = "Can't earn unilevel commission from member #". $me->id . ". Limit reached to earn unilevel commission from this member.";
                        $commission = 0;
                    }

                    $earning->fill([
                        "member_id"                     => $member->id,
                        "type"                          => "unilevel",
                        "amount"                        => $commission,
                        "notes"                         => $notes,
                        "earned_from_member_id"         => $me->id,
                        "product_id"                    => $order->product_id,
                        "order_id"                      => $order->id,
                    ]);
                    $member->current_month_earnings = $member->current_month_earnings + $commission;
                    $member->current_year_earnings = $member->current_year_earnings + $commission;
                    $member->total_earnings = $member->total_earnings + $commission;
                    $earnings->push($earning);
                });

                $earnings->each(function($earning){
                    $earning->save();
                });

                $members->each(function ($member) {
                    $member->available_commissions = Helpers::computeCommission($member);
                    $member->save();
                });

            });
        }
    } 

    /**
     * markOrderComplete
     *
     * @param Order $order
     * @return void
     */
    public function markOrderComplete(Order $order) {
        $unilevelLimit = 5;
        $points = 5;
        $monthlyLimitPerTeam = 10;

        if ($order->status != "received") {
            return redirect()->back()->withErrors("Failed to continue action. Transaction was already completed.");
        }

        $me = auth()->user()->membership;
        $ancestors = collect(explode(",", $me->ancestor_ids))->reverse()->values()->take($unilevelLimit);
        $currentMonth = Carbon::now()->format("Y-m");
        $currentYear = Carbon::now()->format("Y");
        
        if ($me->isRoot()) {
            $order->status = "completed";
            $order->completed_at = Carbon::now();
            $order->save();

            return redirect()->back()->with("success", "Successfully marked as completed.");
        }

        
        if ($ancestors->count()) {
            \DB::transaction(function () use ($ancestors, $points, $unilevelLimit, $me, $order, $currentMonth, $currentYear, $monthlyLimitPerTeam){
                for ($i = 0; $i < $order->quantity; $i++) { 
                    $canEarn = collect();
                    $members = Membership::whereIn("id", $ancestors)
                            ->with('earnings')
                            ->get()->keyBy("id")
                            ->map(function($record, $key) use($me, $currentMonth, $currentYear, $monthlyLimitPerTeam, &$canEarn) {
                                $earnings_this_month_to_this_member = $record->earnings->filter(function($record) use ($me, $currentMonth, $currentYear, $monthlyLimitPerTeam, &$canEarn) {
                                    return $record->created_at->format("Y-m") == $currentMonth 
                                            && $record->earned_from_member_id == $me->id 
                                            && $record->type == "unilevel";
                                })->count();


                                $record->current_month_earnings = $record->earnings->filter(function($record) use ($currentMonth) {
                                    return $record->created_at->format("Y-m") == $currentMonth;
                                })->sum("amount");
                                
                                $record->current_year_earnings = $record->earnings->filter(function($record) use ($currentYear) {
                                    return $record->created_at->format("Y") == $currentYear;
                                })->sum("amount");
                                $canEarn->put($key, $earnings_this_month_to_this_member < $monthlyLimitPerTeam);
                                return $record;
                            });

                    /**
                     * If has ancestors, then give unilevel commission
                     */
                    // set points
                    $ancestors_points = $ancestors->map(function($record, $key) use ($points){
                        return (object)[
                            "ancestor" => $record,
                            "points" => $points - $key
                        ];
                    });

                    $earnings = collect();
                    $ancestors_points->each(function($record) use ($members, &$earnings, $me, $order, $canEarn) {
                        $member = $members->get($record->ancestor);

                        $earning = new Earning;
                        $commission = $record->points;
                        $can_earn = $canEarn->get($member->id) ?? false;
                        if ($can_earn) {
                            $notes = "Earned unilevel commission from member #" . $me->id . ".";
                        } else {
                            $notes = "Can't earn unilevel commission from member #". $me->id . ". Limit reached to earn unilevel commission from this member.";
                            $commission = 0;
                        }

                        $earning->fill([
                            "member_id"                     => $member->id,
                            "type"                          => "unilevel",
                            "amount"                        => $commission,
                            "notes"                         => $notes,
                            "earned_from_member_id"         => $me->id,
                            "product_id"                    => $order->product_id,
                            "order_id"                      => $order->id,
                        ]);
                        $member->current_month_earnings = $member->current_month_earnings + $commission;
                        $member->current_year_earnings = $member->current_year_earnings + $commission;
                        $member->total_earnings = $member->total_earnings + $commission;
                        $earnings->push($earning);
                    });

                    $earnings->each(function($earning){
                        $earning->save();
                    });

                    $members->each(function ($member) {
                        $member->available_commissions = Helpers::computeCommission($member);
                        $member->save();
                    });
                }


                $order->status = "completed";
                $order->completed_at = Carbon::now();
                $order->save();

            });
        }

        return redirect()->back()->with("success", "Successfully marked as completed.");
    }

    /**
     * postActivate
     *
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function postActivate(Order $order, Request $request) {
        if (auth()->id() !== $order->user_id) {
            abort(404);
        }

        $request->validate([
            "product_code" => "required|min:12"
        ]);

        $available_codes = $order->product_codes;
        $product_code = "MJP-" . $request->product_code;

        $matched_code = $available_codes->firstWhere("code", $product_code);
        if ($matched_code) {
            if ($matched_code->is_activated) {
                return redirect()->back()->withErrors("The entered product code was already used. Please enter another one.");
            } else {
                \DB::transaction(function () use ($matched_code, $order) {
                    $this->activateProduct($order);
                    
                    $matched_code->is_activated = true;
                    $matched_code->activated_at = Carbon::now();
                    $matched_code->activated_by = auth()->id();
                    $matched_code->save();
                    

                    $activation_left = $order->product_codes->filter(function($record) {
                        return (bool)$record->is_activated == false;
                    })->count();

                    if ($activation_left == 0) {
                        $order->status = "completed";
                        $order->completed_at = Carbon::now();
                        $order->save();
                    }
                });

                return redirect()->back()->with('success', "Product successfully activated.");
            }
        } else {
            return redirect()->back()->withErrors("The entered product code is invalid. Please enter a valid product code.");
        }
    }

    /**
     * view
     *
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function view(Order $order, Request $request) {
        $tab = $request->tab ?? "info";
        $tabs = ["info", "inclusions", "members"];
        if (!in_array($tab, $tabs)) {
            abort(404);
        }

        return view('orders.view',[
            "order" => $order,
            "tab" => $tab,
            "back_url" => $request->back_url ?? null
        ]);
    }

    /**
     * memberNewRequest
     *
     * @return void
     */
    public function memberNewRequest(Request $request) {
        $type = $request->type ?? "product";
        $types = ["product", "package"];

        if (!in_array($type, $types)) {
            abort(404);
        }

        $products = Product::where("type", $type)->whereNull('disabled_at')->get();
        $branches = Branch::orderBy("name")->get();
        
        return view('orders.member.create', [
            "type" => $type,
            "products" => $products,
            "branches" => $branches
        ]);
    }

    /**
     * memberEditRequest
     *
     * @param Order $order
     * @param Request $request
     * @return void
     */
    public function memberEditRequest(Order $order, Request $request) {
        if (auth()->id() !== $order->user_id) {
            abort(404);
        }

        $products = Product::where("type", $order->product->type)->get();
        $branches = Branch::orderBy("name")->get();
        
        return view('orders.member.edit', [
            "type" => $order->product->type,
            "products" => $products,
            "branches" => $branches,
            "order" => $order
        ]);
    }
    
    public function afterActivation(Request $request) {
        if ($request->type == "package") {
            return back()->with('success', 'New member successfully registered.');

        } else {
            return back()->with('success', 'Product(s) has been successfully activated');
        }
    }

    public function approveOrder(Request $request)
    {
        $request->validate([
            'order_id' => 'required',
            'generate_codes' => 'required'
        ]);

        $message = 'Order request successfully released.';

        DB::beginTransaction();
        try {
            $order = Order::findOrFail($request->order_id);
            $type = $order->product->type;

            if ($order->status !== "pending") {
                if ($order->status == "cancelled") {
                    return redirect()->back()->withErrors("Order cannot be approved. It is already cancelled.");
                } else {
                    return redirect()->back()->withErrors("Order cannot be approved. It is already completed.");
                }
            }


            if ($request->generate_codes == 'yes') {
                $quantity = $order->quantity;

                app(ActivationCodeRepository::class)->generateCode($type, $order->product_id, $quantity, $order->id);
                $message = "Order request successfully released. Activation codes were also created, visit Activation Codes page to view them.";
            }

            $order->released_at = now();
            $order->released_by = auth()->id();

            if ($type == 'package') {
                $order->status = 'released';
            } else {
                $order->status = 'received';

                $packageHistory = new ReleasePackageHistory([
                    'user_id' => $order->requested_by
                ]);

                $order->package_histories()->save($packageHistory);
            }

            Helpers::createOrderItems($order);

            CompanyEarning::create([
                "type"          => $order->product->type . " purchase",
                "amount"        => $order->amount,
                "resource_id"   => $order->user_id,
                "resource_type" => "order",
                "notes"         => \Str::ucfirst($type) . " purchase approved.",
                "transacted_by" => auth()->id(),
                "order_id"      => $order->id,
            ]);

            $order->save();


            if ($order->user->hasVerifiedEmail()) {
                if ($type == "product") {
                    $url = route('my-orders.index', ["tab" => "received"]);
                } else {
                    $url = route('admin.activation.index');
                }
                
                Helpers::notifyRecievedOrder($order, $url);
            }

            if ($type == "product") {
                if ($order->user->hasVerifiedEmail()) {
                    Helpers::notifyProductCodes($order);
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }

        return redirect()->back()->with('success', $message);
    }
}

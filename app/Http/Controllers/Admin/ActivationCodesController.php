<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Models\ActivationCode;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ActivationCodesController extends Controller
{
    public function index(Request $request) {
        $tab = $request->tab ?? "draft";
        $tabs = ["draft", "released", "activated", "unused"];
        if (!in_array($tab, $tabs)) { abort(404); }

        if ($tab == "unused") {
            $activation_codes =  ActivationCode::latest()
                                    ->where("status", "released")
                                    ->when($request->type, function($q, $type) {
                                        $q->where('type', $type);
                                    })
                                    ->with('package')
                                    ->whereNull("activated_at")
                                    ->orderBy('released_at', 'desc')
                                    ->paginate(100);
        } else {
            $activation_codes = ActivationCode::when($tab, function($q, $status) {
                                    if ($status == "released") {
                                        $q->whereIn("status", ["released", "activated"]);
                                    } else {
                                        $q->where("status", $status);
                                    }
                                })
                                ->when($request->type, function($q, $type) {
                                    $q->where('type', $type);
                                })
                                ->with('package')
                                ->with('registeredToUser');

            switch ($tab)  {
                case "released":
                    $activation_codes = $activation_codes->orderBy('released_at', 'desc');
                    break;
                case "activated":
                    $activation_codes = $activation_codes->orderBy('activated_at', 'desc');
                    break;
                default:
                    $activation_codes = $activation_codes->orderBy('created_at', 'desc');
                    break;
            }
            
            $activation_codes = $activation_codes->paginate(100);
        }
        $released_codes = ActivationCode::whereNotNull('released_at')
                                        ->whereMonth("released_at", Carbon::now()->month)
                                        ->get();
        return view('admin.activation_codes.index', [
            "tab" => $tab,
            "activation_codes" => $activation_codes->appends([
                "tab" => $tab,
                "type" => $request->type,
            ]),
            "released_codes" => $released_codes,
            "type" => $request->type,
            "packages" => Package::whereType('package')->select('id', 'name')->whereNull('disabled_at')->orderBy('price')->get()
        ]);
    }

    /**
     * generateCode
     *
     * @param Request $request
     * @return void
     */
    public function generateCode(Request $request) {
        $request->validate([
            "type" => "required",
            "quantity" => "required|min:1|numeric",
            "package_id" => "required_if:type,package"
        ], [
            "type.required" => "Choose type of activation code",
        ]);

        for ($i = 0; $i < $request->quantity; $i++) {
            $product_key = "MJP-" . Helpers::generate_license();

            $product_code = new ActivationCode([
                "code" => $product_key,
                "type" => $request->type,
                "status" => "draft",
                "product_id" => $request->type == "package" ? $request->package_id : null,
                "created_by" => auth()->id(),
            ]);

            $product_codes[] = $product_code;
        }

        collect($product_codes)->each(function($code) {
            $code->save();
        });

        return redirect()->route('activation-codes.index')->with('success', 'Activation codes successfully generated.');
    }

    public function sendActivationCodes(Request $request) {
        $activation_codes = $request->activation_codes;
        $member_username = $request->sendto_member_username;

        $request->validate([
            "sendto_member_username" => ["required", Rule::exists("users", "username")->where(function($q) {
                $q->where("role_id", 1);
            })],
            "activation_codes" => "required"
        ], [
            "sendto_member_username.required" => "Member username is required."
        ]);

        $codes = ActivationCode::whereIn("id", $activation_codes)->where('status', 'draft')->get();

        if ($codes->count() != collect($activation_codes)->count()) {
            return response()->json([
                "message" => "The given data is invalid.",
                "errors" => [
                    "activation_codes" => "Some selected activation codes is not valid anymore. Refresh the page and try again."
                ]
            ], 422);
        }

        $to = User::where("username", $member_username)->first();

        \DB::transaction(function () use ($codes, $to) {
            $codes->each(function($code) use ($to) {
                $code->update([
                    "status" => "released",
                    "released_to" => $to->id,
                    "released_at" => Carbon::now()
                ]);
            });
        });

        return response()->json([
            "status" => "success",
            "message" => "Activation Codes succesfully sent."
        ], 200);
    }

    /**
     * afterSendActivationCode
     *
     * @return void
     */
    public function afterSendActivationCode() {
        return redirect()->route('activation-codes.index')->with('success', 'Product activation codes successfully sent.');
    }

    /**
     * assignedToMe
     *
     * @param Request $request
     * @return void
     */
    public function assignedToMe(Request $request) {
        $type = $request->type ?? "all";

        $activation_codes = ActivationCode::where('status', 'released')
                                ->where('released_to', auth()->id())
                                ->when($request->type, function($q, $type) {
                                    if (in_array($type, ["product", "package"])) {
                                        $q->where("type", $type);
                                    }
                                })
                                ->latest()
                                ->get();

        return view('orders.assigned-activation-codes', [
            "activation_codes" => $activation_codes,
            "type" => $type
        ]);
    }

    public function afterRegisterCode(Request $request) {
        return redirect()->route('my-orders.activate.index', $request->order_id)->with('Success', 'Activation code was successfully registered. ');
    }

    public function histories(Request $request)
    {
        $request->validate([
            'type' => ['nullable', 'in:product,package'],
            'username' => ['nullable', 'exists:users,username']
        ]);

        $showHistory = $request->username ?? false;

        $type = $request->type ?? 'product';
        $username = $request->username ?? null;
        $items = null;
        $selectedUser = null;
        if ($showHistory) {
            $items = ActivationCode::query();
            
            $items = $items->join('products', function ($join) {
                                        $join->on('products.id', '=', 'activation_codes.product_id')
                                            ->whereNull('deleted_at');
                                    })
                                    ->leftJoin('users', function ($join) {
                                        $join->on('users.id', '=', 'activation_codes.transferred_from');
                                    })
                                    ->join('users as activated_user', 'activated_user.id', '=', 'activation_codes.activated_by')
                                    ->whereNotNull('activation_codes.activated_at')
                                    ->groupBy(\DB::raw('activation_codes.order_id, DATE(activation_codes.activated_at)'))
                                    ->orderBy('activation_codes.id', 'desc')
                                    ->where(function ($query) use ($username) {
                                        $query->where('activated_user.username', $username);
                                    })
                                    ->where(function ($query) use ($type) {
                                        $query->where('products.type', $type);
                                    })
                                    ->selectRaw("products.name as product_name, products.type, count(*) as quantity , IF(ISNULL(activation_codes.transferred_from), 'N/A', CONCAT_WS(' ', users.first_name, users.last_name)) as transferred_from_user, activation_codes.activated_at");
                        
            $items = $items->paginate(20);
            
            $items = $items->appends([
                'username' => $username,
                'type' => $type,
            ]);

            $selectedUser = User::where('username', $username)->first();
        }


        return view('admin.activation_codes.histories', compact('showHistory', 'type', 'username', 'items', 'selectedUser'));        
    }
}

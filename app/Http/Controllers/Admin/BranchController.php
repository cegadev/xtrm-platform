<?php

namespace App\Http\Controllers\Admin;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Models\BranchManager;
use App\Http\Controllers\Controller;

class BranchController extends Controller
{
    public function index(Request $request) {
        $keyword = $request->keyword ?? null;
        $branches = Branch::when($request->keyword, function($q, $keyword) {
                                $q->where('name', 'like', '%'. $keyword .'%')
                                    ->orWhere('address', 'like', '%'. $keyword .'%');
                            })
                            ->latest()
                            ->paginate(20);
        return view('admin.branches.index', [
            "branches" => $branches,
            "keyword" => $keyword
        ]);
    }

    public function create() {
        return view('admin.branches.create');
    }

    public function save(Request $request) {
        $request->validate([
            'branch_name' => 'required|max:255',
            'branch_address' => 'required|max:255',
            'branch_email' => 'nullable|email',
            'contact_numbers' => 'nullable|min:11'
        ]);

        $branch = new Branch([
            "name" => $request->branch_name,
            "address" => $request->branch_address,
            "email" => $request->branch_email,
            "contact_numbers" => $request->contact_numbers,
        ]);

        $branch->save();

        return redirect()->route('branches.index')->with('success', 'New Branch successfully created.');
    }

    public function view(Branch $branch, Request $request) {
        $tab = $request->tab ?? "info";
        $available_tabs = ["info", "managers", "earnings", "settings"];
        $managers = collect();
        if (!in_array($tab, $available_tabs)) {
            abort(404);
        };

        if ($tab == "managers") {
            $managers = BranchManager::whereBranchId($branch->id)->get();
        } 
        
        return view('admin.branches.view', [
            "branch" => $branch,
            "tab" => $tab,
            "managers" => $managers
        ]);
    }

    public function edit(Branch $branch) {
        return view('admin.branches.edit', [
            "branch" => $branch
        ]);
    }

    public function update(Branch $branch, Request $request) {
        $request->validate([
            'branch_name' => 'required|max:255',
            'branch_address' => 'required|max:255',
            'branch_email' => 'nullable|email',
            'contact_numbers' => 'nullable|min:11'
        ]);

        $branch->name = $request->branch_name;
        $branch->address = $request->branch_address;
        $branch->email = $request->branch_email;
        $branch->contact_numbers = $request->contact_numbers;
        $branch->save();

        return redirect()->route('branches.view', $branch->id)->with('success', 'Branch successfully updated.');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Earning;
use App\Models\Membership;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Models\TransferOrder;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\ReleasePackageHistory;

class ActivationController extends Controller
{
    public function index(Request $request) {
        $released_orders = Order::where('status', 'released')
                                ->where('user_id', auth()->id())
                                ->whereHas('product', function($q) {
                                    $q->where('type', 'package');
                                })
                                ->with('product')
                                ->get();

        return view('admin.activation.index', [
            'released_orders' => $released_orders,
            'selected' => $request->selected ?? null
        ]);
    }

    /**
     * validateActivation
     *
     * @param Request $request
     * @return void
     */
    public function validateActivation(Request $request) {
        $order = Order::findOrFail($request->order_id);

    }

    /**
     * activate
     *
     * @param Request $request
     * @return void
     */
    public function activate(Request $request) {
        $role = Role::where('identity', 'member')->first();
        $order = Order::findOrFail($request->order_id);

        // create user 
        $request->validate([
            'member_username' => 'required|unique:users,username',
            'member_email' => 'nullable|unique:users,email',
            'member_password' => 'min:6|max:255',
            'member_first_name' => 'required|max:255',
            'member_middle_name' => 'max:255',
            'member_last_name' => 'required|max:255',
        ]);
        
        DB::transaction(function () use ($order, $role, $request){
            /**
             * Create New User
             */
            $new_member = User::create([
                'username'      => $request->member_username,
                'email'         => $request->member_email ?? null,
                'first_name'    => $request->member_first_name,
                'middle_name'   => $request->member_middle_name,
                'last_name'     => $request->member_last_name,
                'role_id'       => $role->id,
                'password'      => Hash::make($request->member_password)
            ]);
            
            $parent = auth()->user();
            
            $new_member->assignRole('Member');
            /**
             * Save new membership info
             */
            $membership = new Membership([
                "order_id"      => $order->id,
                "product_id"    => $order->product->id,
                "member_name"   => $new_member->first_name . " " . $new_member->last_name
            ]);

            $parent_membership = $parent->membership;
            $membership->user_id = $new_member->id;
            $membership->ancestor_ids = is_null($parent_membership->ancestor_ids) ? $parent_membership->id : $parent_membership->ancestor_ids . "," . $parent_membership->id;
            $membership->appendToNode($parent_membership)->save();

            /**
             * Create Relase of Package logs
             */
            $packageHistory = new ReleasePackageHistory([
                "user_id"   => $new_member->id,
                "status"    => "activated"
            ]);

            $order->package_histories()->save($packageHistory);
            
            $total_activation_left = $order->total_activation_left - 1;
            $order->total_activation_left = $total_activation_left;

            // check if this order has more activations left, if none left then make orders complete
            if ($total_activation_left == 0) {
                $order->completed_at = Carbon::now();
                $order->status = "completed";
            }

            $order->save();

            /**
             * Earn direct referral commision
             */
            
            $package = $order->package;

            $direct_referral_commission = $package->settings->direct_referral_commission;
            $original_direct_referral_commission = $direct_referral_commission;
            // get package availed by this member
            $parent_package = $parent_membership->package;
            
            if (!$parent_membership->top_level && $package->settings->direct_referral_commission > $parent_package->settings->direct_referral_commission) { 
                // if package by parent is lower then new member, then let parent earn smaller amount based on his package.
                $direct_referral_commission = $parent_package->settings->direct_referral_commission;
            }

            if (!empty($package->settings->direct_referral_commission)) {
                $earnings = new Earning([
                    "member_id"     => $parent_membership->id,
                    "type"          => "direct referral",
                    "amount"        => $direct_referral_commission ?? 0,
                    "notes"         => "Earned direct referral commision from member #" . $membership->id . ".",
                    "earned_from_member_id" => $membership->id,
                    "order_id"      => $order->id, 
                ]);
                
                $earnings->save();
                
                $current_earnings = $this->getMemberEarnings($parent_membership->id);

                // update earnings
                $parent_membership->current_month_earnings  = $current_earnings->current_month;
                $parent_membership->current_year_earnings   = $current_earnings->current_year;
                $parent_membership->total_earnings          = $current_earnings->total_earnings;
                $parent_membership->available_commissions     = Helpers::computeCommission($parent_membership);
                $parent_membership->save();
                
                // if parent member has parent
                if ($parent_membership->parent_id && $direct_referral_commission > 0) {
                    $indirect_referral_commission = $original_direct_referral_commission / intval($package->settings->indirect_referral_percentage);

                    $indirect_parent = Membership::find($parent_membership->parent_id);
                    
                    if (!$indirect_parent->top_level) {
                        if ($package->settings->direct_referral_commission > $indirect_parent->package->settings->direct_referral_commission) {
                            $indirect_referral_commission = $indirect_parent->package->settings->direct_referral_commission / intval($package->settings->indirect_referral_percentage);
                        }
                    }
                    
                    // earn indirect referral to the upline of the parent member of new member
                    $indirectEarnings = new Earning([
                        "member_id"     => $indirect_parent->id,
                        "type"          => "indirect referral",
                        "amount"        => $indirect_referral_commission ?? 0,
                        "notes"         => "Earned indirect referral commision from member #" . $membership->id . ".",
                        "earned_from_member_id" => $membership->id,
                        "order_id"      => $order->id,
                    ]);

                    $indirectEarnings->save();


                    $indirect_parent_current_earnings = $this->getMemberEarnings($indirect_parent->id);

                    // update earnings
                    $indirect_parent->current_month_earnings    = $indirect_parent_current_earnings->current_month;
                    $indirect_parent->current_year_earnings     = $indirect_parent_current_earnings->current_year;
                    $indirect_parent->total_earnings            = $indirect_parent_current_earnings->total_earnings;
                    $indirect_parent->available_commissions     = Helpers::computeCommission($indirect_parent);
                    $indirect_parent->save();
                }
            }

        });
        
        return redirect()->back()->with('success', 'Product successfully registered to ' . $request->member_first_name . " " . $request->member_last_name);
    }


    /**
     * getMemberEarnings
     *
     * @param int $member_id
     * @return void
     */
    private function getMemberEarnings(int $member_id) {
        $membership = Membership::whereId($member_id)->with('earnings')->first();
        $earnings = $membership->earnings->map(function($record) {
            $month = Carbon::parse($record->created_at)->format("m");
            $year = Carbon::parse($record->created_at)->format("Y");

            $record->month = $month . "-" . $year;
            $record->year = $year;
            return $record;
        });

        $now = Carbon::now();

        $currentMonth = $now->format("m") . "-" . $now->format("Y");
        $currentMonthEarnings = $earnings->filter(function($record) use ($currentMonth) {
            return $record->month == $currentMonth;
        })->sum("amount");
        
        $currentYear = $now->format("Y");
        $currentYearEarnings = $earnings->filter(function($record) use ($currentYear) {
            return $record->year == $currentYear;
        })->sum("amount");


        $totalEarnings = $earnings->sum("amount");

        return (object) [
            "current_year"      => $currentYearEarnings,
            "current_month"     => $currentMonthEarnings,
            "total_earnings"    => $totalEarnings
        ];
    }

    public function transfer(Order $order, Request $request) {
        if ($order->user_id !== auth()->id()) {
            abort(404);
        }
        return view('admin.activation.transfer', [
            "order" => $order,
            "back_url" => $request->back_url ?? null
        ]);
    }

    public function sendPackage(Order $order, Request $request) {
        $user = auth()->user();
        $request->validate([
            "username" => [
                "required",
                Rule::exists('users')->where(function($q) use ($user) {
                    $q->where('role_id', 1)
                    ->where('username', '<>', $user->username);
                }),
            ],
            "password" => [
                "required",
                function ($attribute, $value, $fail) {
                    if (! Hash::check($value, auth()->user()->password)) {
                        $fail('Password is incorrect');
                    }
                }
            ]
        ]);

        $to = User::where("username", $request->username)->first();

        $transferRecord = new TransferOrder([
            "from_id" => $user->id,
            "to_id" => $to->id,
            "notes" => $request->notes
        ]);

        $order->transfer_record()->save($transferRecord);
        $order->user_id = $to->id;
        $order->is_transferred = true;
        $order->save();

        return redirect()->route('admin.activation.index')->with('success', 'Order successfully transferred to ' . $to->first_name . " " . $to->last_name . ".");
    }
}

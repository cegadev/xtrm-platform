<?php

namespace App\Http\Controllers\Admin;

use Image;
use Carbon\Carbon;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    public function index(Request $request) {
        $tab = $request->tab ?? 'active';

        $products = Product::with('category')
                            ->where('type', 'product');

        $products = $products->where(function ($q) use ($tab) {
            if ($tab == 'active') {
                $q->whereNull('disabled_at');
            } else {
                $q->whereNotNull('disabled_at');
            }
        });
        
        $products = $products->paginate(100);
                    

        return view('admin.products.index', [
            'products' => $products,
            'tab' => $tab,
            'categories' => ProductCategory::whereNull('disabled_at')
                            ->withCount('products')
                            ->get()
        ]);
    }

    private function getProductCategories() {
        $categories = ProductCategory::orderBy('name', 'asc')
                        ->whereNull('disabled_at')
                        ->select('id', 'name')
                        ->get();

        return $categories;
    }

    /**
     * create
     *
     * @return void
     */
    public function create() {
        $categories = $this->getProductCategories();
                                    
        return view('admin.products.create', [
            'categories' => $categories
        ]);
    }

    /**
     * save
     *
     * @param Request $request
     * @return void
     */
    public function save(Request $request) {
        $validations = [
            'product_name' => 'required|max:255|unique:products,name',
            'code' => 'required|max:100|unique:products,code',
            'category' => 'required|exists:product_categories,id',
            'price' => 'required|numeric|min:1',
            'image' => 'mimes:jpeg,png|max:1014',
        ];


        
        $request->validate($validations);

        $product = new Product([
            "name" => $request->product_name,
            "product_category_id" => $request->category,
            "price" => $request->price,
            "code" => $request->code,
            "type" => "product",
            "slug" => \Str::slug($request->product_name),
            "basic_description" => $request->basic_description,
            "description" => $request->description ?? "",
        ]);

        if ($request->hasFile('image')) {
            $product_name = Str::slug($request->product_name) . "-" . Carbon::now()->format("YmdHis");

            $path = $request->file('image');

            $productImage = Image::make($path)->fit(400);
            $extension = $request->image->extension();
            $name = $product_name . "." . $extension;

            Storage::put('product/' . $name, $productImage->encode());

            $product->product_image = asset("storage/product/" . $name);
        }

        $product->save();

        return redirect()->route('products.index')->with('success', 'Product successfully created.');
    }

    /**
     * edit
     *
     * @param Product $product
     * @return void
     */
    public function edit(Product $product) {
        $categories = $this->getProductCategories();

        return view('admin.products.edit', [
            "product" => $product,
            "categories" => $categories
        ]);
    }

    /**
     * update
     *
     * @param Product $product
     * @param Request $request
     * @return void
     */
    public function update(Product $product, Request $request) {
        $request->validate([
            'product_name' => 'required|max:255|unique:products,name,' . $product->id,
            'code' => 'required|max:100|unique:products,code,' . $product->id,
            'category' => 'required|exists:product_categories,id',
            'price' => 'required|numeric|min:1'
        ]);

        $product->name = $request->product_name;
        $product->code = $request->code;
        $product->price = $request->price;
        $product->product_category_id = $request->category;
        $product->slug = Str::slug($request->product_name);
        $product->basic_description = $request->basic_description;
        $product->description = $request->description ?? "";
        
        if ($request->hasFile('image')) {
            $product_name = Str::slug($request->product_name) . "-" . Carbon::now()->format("YmdHis");

            $path = $request->file('image');

            $productImage = Image::make($path)->fit(400);
            $extension = $request->image->extension();
            $name = $product_name . "." . $extension;

            Storage::put('product/' . $name, $productImage->encode());
            $product->product_image = asset("storage/product/" . $name);
        }


        $product->save();
        return redirect()->route('products.index')->with('success', 'Product successfully updated.');
    }

    public function enableItem(Request $request) {
        $request->validate([
            'enable_id' => 'required'
        ]);

        Product::whereId($request->enable_id)->update([
            'disabled_at' => null
        ]);

        return back()->with('success', 'Product successfully enabled.');
    }
    
    public function disableItem(Request $request) {
        $request->validate([
            'disable_id' => 'required'
        ]);

        Product::whereId($request->disable_id)->update([
            'disabled_at' => Carbon::now()
        ]);

        return back()->with('success', 'Product successfully disabled.');
    }
}

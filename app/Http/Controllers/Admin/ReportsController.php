<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Product;
use App\Models\Membership;
use Illuminate\Http\Request;
use App\Models\EncashmentHistory;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    public function encashment(Request $request) {
        $request->validate([
            'type' => 'in:released,requested'
        ]);

        $member_id = $request->member_id ?? null;
        $date_range = $request->date_range ?? null;
        $type = $request->type ?? 'requested';

        $dateRangeTypes = [
            'released'  => 'approved_at',
            'requested' => 'requested_at'
        ];

        $encashments = EncashmentHistory::latest()
        ->when($request->member_id, function($q, $member_id) {
            $q->where('membership_id', $member_id);
        })
        ->where('status', 'complete')
        ->when($request->date_range, function($q, $date_range) use ($type, $dateRangeTypes) {
            $range = explode("to", $date_range);

            $dateRangeType = $dateRangeTypes[$type] ?? 'approved_at';

            if (count($range) > 1) {
                $start_at = Carbon::parse($range[0])->startOfDay()->toDateTimeString();
                $end_at = Carbon::parse($range[1])->endOfDay()->toDateTimeString();
                $q->whereBetween($dateRangeType, [$start_at, $end_at]);
            } else {
                $q->whereDate($dateRangeType, Carbon::parse($range[0])->toDateString());
            }
        })
        ->select('id')
        ->get();

        $members = Membership::orderBy("member_name")->get();


        return view('admin.encashment.reports', [
            "member_id" => $member_id,
            "encashments" => $encashments,
            "members" => $members,
            "selected_member" => $member_id,
            "selected_date_range" => $date_range,
            "selected_type" => $type,
        ]);
    }

    public function sales(Request $request) {
        $product_type = $request->product_type ?? "all";
        $product_id = $request->product_id ?? null;

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $this_week = Carbon::now()->startOfWeek()->format("M d, Y") . " to " . Carbon::now()->endOfWeek()->format("M d, Y");
        $date_range = $request->date_range ?? $this_week;


        $products = Product::where("type", "product")->orderBy("name")->get();
        $packages = Product::where("type", "package")->orderBy("name")->get();

        $products_list = $products->merge($packages);


        return view('admin.sales.reports', [
            "selected_date_range" => $date_range,
            "product_type" => $product_type,
            "product_id" => $product_id,
            "products" => $products_list,
        ]);
    }
}

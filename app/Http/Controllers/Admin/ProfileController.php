<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\MemberBankInfo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index(Request $request) {

        $user = User::find(auth()->id());
        $tab = $request->tab ?? "profile";


        $banksList = collect();
        if ($tab == "bank" && auth()->user()->isMember()) {
            $banks = $user->membership->bank_infos;

            $banksList = (object)[
                "primary" => [
                    "gcash"         => $banks->firstWhere("type", "gcash"),
                    "paymaya"       => $banks->firstWhere("type", "paymaya"),
                    "bdo"           => $banks->firstWhere("type", "bdo"),
                ],
                "others" => $banks->where("type", "other")
            ];
        }

        return view('profile.index', [
            "user" => $user,
            "tab" => $tab,
            "banks" => $banksList
        ]);
    }

    /**
     * edit
     *
     * @return void
     */
    public function edit() {
        $user = auth()->user();
        return view('profile.edit', [
            "user" => $user
        ]);
    }

    /**
     * update
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request) {
        $user = auth()->user();

        $validation = [
            "first_name" => "required|max:50",
            "middle_name" => "max:50",
            "last_name" => "required|max:50",
            "mobile_number" => "required|max:50",
            "email" => "nullable|max:50|unique:users,email," . $user->id,
        ];

        if ($user->membership) {
            $validation['member_name'] = 'required';
        }

        $request->validate($validation);

        $user->first_name = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name = $request->last_name;
        $user->mobile_number = $request->mobile_number;
        $user->email = $request->email;


        if ($user->membership) {
            $user->membership->update([
                'member_name' => $request->member_name
            ]);
        }

        $user->save();

        return redirect()->route('admin.profile.index')->with('success', 'Profile successfully updated.');
    }

    /**
     * changePassword
     *
     * @return void
     */
    public function changePassword(Request $request) {
        $backUrl = $request->back_url ?? null;
        return view('profile.change-password', compact('backUrl'));
    }

    /**
     * updatePassword
     *
     * @param Request $request
     * @return void
     */
    public function updatePassword(Request $request) {
        $user = auth()->user();

        $request->validate([
            'current_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!\Hash::check($value, $user->password)) {
                    return $fail(__('The current password is incorrect.'));
                }
            }],
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);

        $user->password = Hash::make($request->new_password);
        $user->save();

        return redirect()->route('admin.profile.index')->with('success', 'Password successfully updated!');
    }

    /**
     * changePhoto
     *
     * @return void
     */
    public function changePhoto() {
        $user = auth()->user();

        $photo = asset($user->avatar) ?? "https://www.signivis.com/img/custom/avatars/member-avatar-01.png";

        return view('profile.change-photo', [
            "photo" => $photo
        ]);
    }

    /**
     * updatePhoto
     *
     * @param Request $request
     * @return void
     */
    public function updatePhoto(Request $request) {
        $base64_image = $request->new_photo;

        if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
            $data = substr($base64_image, strpos($base64_image, ',') + 1);

            $data = base64_decode($data);

            $name = auth()->user()->username . Carbon::now()->format('YmdHis') . ".jpeg";
            Storage::put("/avatars/" . $name, $data);

            auth()->user()->update([
                "avatar" => "storage/avatars/" . $name
            ]);
        }

        return redirect()->route('admin.profile.index')->with('success', "Avatar successfully updated!");
    }
    
    /**
     * verifyEmail
     *
     * @param Request $request
     * @return void
     */
    public function verifyEmail(Request $request) {
        $user = auth()->user();

        if ($user->hasVerifiedEmail()) {
            return back()->with('success', 'Your email was already verified. No actions executed.');
        }

        $user->sendEmailVerificationNotification();
        return back()->with('success', 'Email verification link was sent to your email. - <strong>' . $user->email .'</strong>');
    }

    /**
     * updateBankInfo
     *
     * @param Request $request
     * @return void
     */
    public function updateBankInfo($key, Request $request) {
        $request->validate([
            "account_number" => ["required","max:255"],
            "account_name" => ["required","max:255"],
        ]);
        
        $accepted_keys = ["gcash", "bdo", "paymaya", "others"];

        if ($key == "others") {
            $id = $request->bank_id;
            MemberBankInfo::where("id", $id)
                            ->update([
                                "account_name"  => $request->account_name,
                                "value"         => $request->account_number
                            ]);
        } else {
            MemberBankInfo::updateOrCreate([
                "member_id" => auth()->user()->membership->id,
                "type" => $key,
                "name" => $key == "bdo" ? "BDO" : \Str::ucfirst($key),
            ], [ "value" => $request->account_number, "account_name" => $request->account_name ]);

        }

        return back()->with('success', 'Bank Information successfully saved.');
    }

    /**
     * newBank
     *
     * @param Request $request
     * @return void
     */
    public function newBank(Request $request) {
        $request->validate([
            "account_number"    => "required|max:255",
            "account_name"      => "required|max:255",
            "bank_name"              => "required|max:255",
        ]);

        MemberBankInfo::create([
            "name" => $request->bank_name,
            "account_name" => $request->account_name,
            "value" => $request->account_number,
            "type" => "other",
            "member_id" => auth()->user()->membership->id,
            "is_other" => true
        ]);

        return back()->with('success', 'New bank information successfully saved.');
    }

    /**
     * removeBank
     *
     * @param Request $request
     * @return void
     */
    public function removeBank(Request $request) {
        $request->validate([
            "remove_id" => "required|exists:member_bank_infos,id"
        ]);

        $bank = MemberBankInfo::whereId($request->remove_id)->delete();

        return back()->with('success', 'Bank Information has been successfully deleted.');
    }
}

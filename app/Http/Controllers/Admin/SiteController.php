<?php

namespace App\Http\Controllers\Admin;

use Image;
use Carbon\Carbon;
use App\Models\Package;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SiteController extends Controller
{
    public function index() {
        return view('admin.site-settings.index');
    }

    /**
     * sitelogo
     *
     * @return void
     */
    public function sitelogo() {
        $sitelogo = CustomField::where('field', 'site-logo')->first();

        return view('admin.site-settings.site-logo', [
            'sitelogo' => $sitelogo
        ]);
    }

    /**
     * updateSiteLogo
     *
     * @param Request $request
     * @return void
     */
    public function updateSiteLogo(Request $request) {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $logoName = "site-logo-" .Carbon::now()->format("YmdHis");

                $validated = $request->validate([
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);

                $extension = $request->image->extension();

                $name = $logoName . "." . $extension;

                $path = $request->file('image');
                $image = Image::make($path)->resize(null, 500, function ($constraint) {
                    $constraint->aspectRatio();
                });

                Storage::put('site/' . $name, $image->encode());
                
                CustomField::where('field', 'site-logo')->update(['data' => "/storage/site/" . $name]);

                return redirect()->back()->with('success', 'Site logo successfully updated.');
            }
        }
        abort(500, 'Could not upload image :(');
    }

    /**
     * siteIcon
     *
     * @return void
     */
    public function siteIcon() {
        $icon = CustomField::where('field', 'site-icon')->first();

        return view('admin.site-settings.site-icon', [
            'icon' => $icon
        ]);
    }

    /**
     * updateSiteIcon
     *
     * @param Request $request
     * @return void
     */
    public function updateSiteIcon(Request $request) {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $logoName = "site-icon-" .Carbon::now()->format("YmdHis");

                $validated = $request->validate([
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);

                $extension = $request->image->extension();

                $name = $logoName . "." . $extension;

                $path = $request->file('image');
                $image = Image::make($path)->resize(200, 200);

                Storage::put('site/' . $name, $image->encode());

                CustomField::where('field', 'site-icon')->update(['data' => "/storage/site/" . $name]);

                return redirect()->back()->with('success', 'Site icon successfully updated.');
            }
        }
        abort(500, 'Could not upload image :(');
    }

    /**
     * slider
     *
     * @return void
     */
    public function slider(Request $request) {
        $slideKey = $request->key ?? 1;

        $slides = CustomField::whereIn('field', ['slider1', 'slider2', 'slider3'])->get()
                    ->keyBy("field")
                    ->map(function($record) {
                        $record->data = json_decode($record->data);
                        return $record;
                    });
        $slidesTotal = $slides->count();
                    
        $slideKeys = [];
        for($i = 1; $i <= $slidesTotal; $i++) {
            $slideKeys[] = $i;
        }

        if (!in_array($slideKey, $slideKeys)) {
            throw new \Exception("Slide Key Invalid");
        }

        $slider = $slides->get("slider" . $slideKey);
                    
        return view('admin.site-settings.slider', [
            "slider"    => $slider,
            "key"       => $slideKey,
            "count"     => $slidesTotal
        ]);
    }

    /**
     * updateSlider
     *
     * @return void
     */
    public function updateSlider(Request $request) {
        $slideKey = $request->key;

        $request->validate([
            "title" => "required|max:100",
            "subtitle" => "required|max:100",
            'image' => 'mimes:jpeg,png|max:5048',
        ]);

        $data = [
            "title" => $request->title,
            "sub_title" => $request->subtitle,
            "action" => $request->action,
        ];

        $currentSlide = CustomField::where("field", "slider" . $slideKey)->first();
        
        if ($request->hasFile("image")) {

            $extension = $request->image->extension();

            $name = "slider-" . time() . "." . $extension;

            $request->image->move(public_path('images/slides'), $name);
            
            $data["image"] = "/images/slides/" .  $name;
        } else {

            $currentImage = json_decode($currentSlide->data)->image;

            $data["image"] = $currentImage;
        }

        CustomField::where("field", "slider" . $slideKey)->update([
            "data" => json_encode($data)
        ]);

        return redirect()->route('site-settings.slider.index', ['key' => $slideKey])->with('success', 'Slider ' . $slideKey . ' was successfully updated.');
    }

    /**
     * aboutUs
     *
     * @param Request $request
     * @return void
     */
    public function aboutUs(Request $request) {
        $current_tab = $request->tab ?? "content";

        $data = CustomField::whereIn('field', ['about-us', 'about-us-right-image'])->get();
        
        if ($current_tab == "content") {
            $about_us = $data->firstWhere("field", "about-us");
        } else if ($current_tab == "right-image") {
            $about_us = $data->firstWhere("field", "about-us-right-image");
        }

        return view('admin.site-settings.about-us', [
            "about_us" => $about_us,
            "current_tab" => $current_tab
        ]);
    }
    
    public function updateAboutUs(Request $request) {
        $tab = $request->tab ?? "content";
        if ($request->tab == "content") {
            $details = $request->details;
            
            $request->validate([
                "details" => "required"
            ]);

            CustomField::where('field', 'about-us')->update([
                'data' => $details
            ]);

        }
        
        
        if ($request->tab == "right-image") {
            
            if ($request->hasFile("image")) {

                $extension = $request->image->extension();
    
                $name = "about-us-" . time() . "." . $extension;
    
                $request->image->move(public_path('images'), $name);
                
                $imageName = "/images/" .  $name;
                
                CustomField::where('field', 'about-us-right-image')->update([
                    'data' => $imageName
                ]);
            } 
        }

        return redirect()->route('site-settings.about-us.index', ['tab' => $tab])->with('success', 'Homepage about us was successfully updated.');
        
    }

    public function backgrounds() {
        $backgrounds = CustomField::whereIn('field', ['home-background-1', 'home-background-2'])->get()->map(function ($record) {
            $record->data = json_decode($record->data);
            return $record;
        });

        return view('admin.site-settings.backgrounds', compact('backgrounds'));        
    }

    public function upload(Request $request) {
        $request->validate([
            'index' => 'required|max:1',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);

        
        $extension = $request->file('image')->getClientOriginalExtension();
 
        $name = "bg-" . Carbon::now()->format('YmdHis') . "." . $extension;

        $index = $request->index + 1;

        $request->image->move(public_path('images/backgrounds'), $name);

        $data = [
            'url' => "/images/backgrounds/" . $name,
            'darken' => true
        ];

        CustomField::where('field', 'home-background-' . $index)->update([
            'data' => json_encode($data)
        ]);

        return back()->withSuccess('Background image successfully updated.');
    } 

    public function products(Request $request)
    {
        $tab = $request->tab ?? "home-block-1";
        $products = CustomField::whereIn('field', ['home-block-1', 'home-block-2', 'home-block-3'])->get()->map(function ($record) {
            $record->data = json_decode($record->data);
            return $record;
        })->keyBy('field');

        $selected = $products->pull($tab);


        return view('admin.site-settings.products', compact('tab', 'products', 'selected'));
    }

    public function saveProducts(Request $request)
    {
        $request->validate([
            'field' => 'required|in:home-block-1,home-block-2,home-block-3',
            'title' => 'required',
        ]);

        $data = [
            'title' => $request->title
        ];

        $customField = CustomField::where('field', $request->field)->first();
        $current = json_decode($customField->data);

        if ($request->field == 'home-block-1') {
            $data['description'] = $request->description ?? "";

            if ($request->has('image')) {
                $extension = $request->file('image')->getClientOriginalExtension();
                
                $name = "homeproduct1-" . Carbon::now()->format('YmdHis') . "." . $extension;

                $request->image->move(public_path('images'), $name);

                $data['image'] = "/images/" . $name;
            } else {
                $data['image'] = $current->image;
            }
        } else {
            if ($request->has('image1')) {
                $extension = $request->file('image1')->getClientOriginalExtension();
                
                $name = "product1-" . Carbon::now()->format('YmdHis') . "." . $extension;

                $request->image1->move(public_path('images'), $name);

                $data['images'][0] = "/images/" . $name;
            } else {
                $data['images'][0] = $current->images[0];
            }
            
            if ($request->has('image2')) {
                $extension = $request->file('image2')->getClientOriginalExtension();
                
                $name = "product2-" . Carbon::now()->format('YmdHis') . "." . $extension;

                $request->image2->move(public_path('images'), $name);

                $data['images'][1] = "/images/" . $name;
            } else {
                $data['images'][1] = $current->images[1];
            }
        }

        $customField->update([
            'data' => json_encode($data)
        ]);

        return back()->withSuccess('Block successfully updated!');
    }

    public function packages()
    {
        $packages = Package::whereType('package')->whereNull('disabled_at')->get()->sortBy('homepage_priority')->values();
        return view('admin.site-settings.packages', compact('packages'));
    }

    public function savePackageSorting(Request $request)
    {
        $request->validate([
            'ids' => 'required'
        ]);

        $sortOrder = collect($request->ids)->map(function ($record, $key) {
            return (object) [
                'id'    => $record,
                'order' => $key + 1
            ];
        })->keyBy('id');

        \DB::transaction(function () use ($sortOrder) {
            foreach($sortOrder as $item) {
                Package::where('id', $item->id)->update([
                    'homepage_priority' => $item->order
                ]);
            }
        });

        return response()->json([
            'message' => "Packages homepage sorting successfully updated."
        ], 200);
    }
}

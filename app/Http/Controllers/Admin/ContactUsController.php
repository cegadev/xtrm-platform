<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContactUs;
use App\Models\CustomField;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    public function index() {
        $data = CustomField::whereIn("field", ["contact-info-emails", "contact-info-mobile-numbers", "contact-info-phone-numbers", "contact-info-address"])->get()->keyBy("field");

        return view('admin.site-settings.pages.contact-us', [
            "mobile_numbers" => $data->get("contact-info-mobile-numbers")->data,
            "phone_numbers" => $data->get("contact-info-phone-numbers")->data,
            "emails" => $data->get("contact-info-emails")->data,
            "address" => $data->get("contact-info-address")->data
        ]);
    }

    public function update(Request $request) {
        $request->validate([
            "contact-info-emails" => "required",
            "contact-info-mobile-numbers" => "required",
            "contact-info-phone-numbers" => "required",
            "contact-info-address" => "required"
        ]);

        DB::transaction(function () use ($request){
            CustomField::where("field", "contact-info-emails")->update(["data" => $request->{"contact-info-emails"}]);
            CustomField::where("field", "contact-info-mobile-numbers")->update(["data" => $request->{"contact-info-mobile-numbers"}]);
            CustomField::where("field", "contact-info-phone-numbers")->update(["data" => $request->{"contact-info-phone-numbers"}]);
            CustomField::where("field", "contact-info-address")->update(["data" => $request->{"contact-info-address"}]);
        });

        return redirect()->route('site-settings.index')->with('success', 'Contact us successfully updated!');
    }

    public function postMessage(Request $request) {
        $request->validate([
            "email" => "required|email",
            "subject" => "required|max:255",
            "message" => "required|max:500",
            "name" => "required"
        ]);
        
        ContactUs::create([
            "name" => $request->name,
            "subject" => $request->subject,
            "message" => $request->message,
            "email" => $request->email,
            "user_id" => auth()->check() ? auth()->id() : null,
        ]);

        return back()->with('success', "Message successfully submitted. Thanks for contacting us.");
    }
}

<?php
namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Models\ActivationCode;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class TransactionController extends Controller
{
    public function index(Request $request) {
        $type = $request->type ?? "all";
        

        $orders = Order::whereIn('status', ['released', 'received'])
        ->whereHas('items', function($q) {
            $q->where('user_id', auth()->id())
            ->whereNull('activated_at');
        })
        ->with(['items' => function($q) {
            $q->where('user_id', auth()->id());
        }])
        ->paginate(10);

        $packages = Package::whereType('package')->whereNull('disabled_at')->with('settings')->orderBy('price')->get();
        $my_activation_codes = ActivationCode::whereReleasedTo(auth()->id())->where('status', 'released')->whereNull('activated_at')->get();


        $activation_codes = collect([
            "Product" => $my_activation_codes->where('product_id', null)->count(),
        ]);

        $packages->each(function($package) use (&$activation_codes, $my_activation_codes) {
            $activation_codes->put("package-" . $package->id, $my_activation_codes->where('product_id', $package->id)->count());
        });

        $show_history = isset($request->show_history) && $request->show_history == "true";

        return view('orders.transactions', [
            'orders' => $orders,
            'type' => $type,
            'packages' => $packages,
            'activation_codes' => $activation_codes,
            'show_history' => $show_history
        ]);
    }

    public function activations(Request $request)
    {
        $request->validate([
            'type' => ['nullable', 'in:product,package']
        ]);

        $type = $request->type ?? 'product';
        
        // displayed activation histories must be less than next month
        // example september activations must only be seen once september ends
        $firstOfMonth = Carbon::today()->firstOfMonth()->startOfDay();

        $items = ActivationCode::query();
        
        $items = $items->join('products', function ($join) {
                                    $join->on('products.id', '=', 'activation_codes.product_id')
                                        ->whereNull('deleted_at');
                                })
                                ->leftJoin('users', function ($join) {
                                    $join->on('users.id', '=', 'activation_codes.transferred_from');
                                })
                                ->whereNotNull('activation_codes.activated_at')
                                ->where('activation_codes.activated_by', auth()->id())
                                ->groupBy(\DB::raw('activation_codes.order_id, DATE(activation_codes.activated_at)'))
                                ->orderBy('activation_codes.id', 'desc')
                                ->where(function ($query) use ($type) {
                                    $query->where('products.type', $type);
                                })
                                ->whereDate('activation_codes.activated_at', '<', $firstOfMonth->toDateString())
                                ->selectRaw("activation_codes.id, products.name as product_name, products.type, count(*) as quantity , IF(ISNULL(activation_codes.transferred_from), 'N/A', CONCAT_WS(' ', users.first_name, users.last_name)) as transferred_from_user, activation_codes.activated_at");
                       
        $items = $items->paginate(20);

        $items = $items->appends([
            'type' => $type
        ]);

        return view('orders.activations', compact('items', 'type'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Earning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EarningsController extends Controller
{
    public function index(Request $request) {
        $member = auth()->user()->membership;
        $keyword = $request->keyword ?? "";
        $now = \Carbon\Carbon::now();

        $earnings = Earning::where("member_id", $member->id)
                            ->whereNotNull("added_to_wallet_at")
                            ->when($request->keyword, function($q, $keyword) {
                                $q->where(function($q) use ($keyword) {
                                    $q->where('type', 'like', '%' . $keyword . '%')
                                        ->where('notes', 'like', '%' . $keyword .'%')
                                        ->orWhereHas('earnedFromMember', function($q) use ($keyword) {
                                            $q->where('member_name', 'like', '%'. $keyword .'%');
                                        });
                                });
                                
                            })
                            // ->where(function($q) use ($now) {
                            //     $q->whereType("direct referral")
                            //     ->orWhere(function($q) use ($now) {
                            //         $q->whereIn("type", ["unilevel", "indirect referral"])
                            //             ->whereDate("valid_at", $now->firstOfMonth()->toDateString());
                            //     });
                            // })
                            ->with('earnedFromMember', 'membership')
                            ->where('amount', '>', 0)
                            ->latest()->paginate(50);

        $realEarnings = Earning::where("member_id", $member->id)
                                ->whereNotNull("added_to_wallet_at")
                                // ->where(function($q) use ($now) {
                                //     $q->whereType("direct referral")
                                //     ->orWhere(function($q) use ($now) {
                                //         $q->whereIn("type", ["unilevel", "indirect referral"])
                                //             ->whereDate("valid_at", $now->firstOfMonth()->toDateString());
                                //     });
                                // })
                                ->select("type", \DB::raw("sum(amount) as total"))
                                ->groupBy("type")
                                ->get()->keyBy("type")->map(function($record) {
                                    return $record->total;
                                });

        $direct_referral = $realEarnings->get("direct referral") ?? 0;
        $indirect_referral = $realEarnings->get("indirect referral") ?? 0;
        $unilevel = $realEarnings->get("unilevel") ?? 0;

        return view('admin.earnings.index', [
            "earnings" => $earnings->appends(['keyword' => $keyword]),
            "keyword" => $keyword,
            "earnings_breakdown" => (object)[
                "direct" => $direct_referral ?? 0,
                "indirect" => $indirect_referral ?? 0,
                "unilevel" => $unilevel ?? 0,
            ]
        ]);
    }
}

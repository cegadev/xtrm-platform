<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Member;
use App\Models\Earning;
use App\Models\Package;
use App\Models\Product;
use App\Models\Membership;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Models\ActivationCode;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\AccountUpgradeHistory;
use App\Http\Helpers\TransactionsHelper;

class MembersController extends Controller
{
    public function index(Request $request) {
        $keyword = $request->keyword ?? null;
        $only_branch = $request->only_branch ?? null;
        $tab = $request->tab ?? "all";
        $tabs = ["all", "regular"];
        $tabCounts = (object)[
            'all' => 0,
            'regular' => 0,
            'company' => 0,
        ];
        
        if (auth()->user()->hasRole('Admin')) {
            $tabs[] = "company";
        }

        $request->validate([
            "tab" => ["nullable", "in:" . implode(",", $tabs)]
        ]);

        $loggedUser = auth()->user();

        $allMembers = Membership::select("id", "is_company_account")->get();

        $members = Member::with('membership')
                        ->where('role_id', 1)
                        ->where(function ($q) use ($request) {
                            $q->when($request->keyword, function($q, $keyword) {
                                $q->where("first_name", "like", "%{$keyword}%")
                                    ->orWhere("last_name", "like", "%{$keyword}%")
                                    ->orWhere("middle_name", "like", "%{$keyword}%")
                                    ->orWhere("username", "like", "%{$keyword}%");
                            });
                        })->latest();
                        
        if ($loggedUser->hasRole('Manager')) {
            $members = $members->whereHas('membership', function($q) {
                $q->where('is_company_account', 0);
            });
            
        } else {
            $tabCounts = (object)[
                'all' => $allMembers->count(),
                'regular' => $allMembers->where("is_company_account", 0)->count(),
                'company' => $allMembers->where("is_company_account", 1)->count(),
            ];
        }

        if ($tab == "regular") {
            $members = $members->whereHas('membership', function($q) {
                $q->where('is_company_account', 0);
            });
        }
        
        if ($tab == "company") {
            $members = $members->whereHas('membership', function($q) {
                $q->where('is_company_account', 1);
            });
        }

        if ($loggedUser->isManager() && $loggedUser->managedBranch && $only_branch) {
            $members = $members->whereHas("membership.order", function($q) use ($loggedUser) {
                $q->where("branch_id", $loggedUser->managedBranch->id);
            });
        }

        $members = $members->latest()->paginate(100);

        return view('admin.members.index', [
            'members' => $members,
            'keyword' => $keyword,
            'only_branch' => $only_branch,
            'tab' => $tab,
            'tab_counts' => $tabCounts
        ]);
    }
    
    /**
     * create
     *
     * @return void
     */
    public function create(Request $request) {
        $packages = Product::where('type', 'package')
                        ->whereNull('disabled_at')
                        ->orderBy('price')
                        ->get();
        return view('admin.members.create', [
            'packages' => $packages,
            'back_url' => $request->back_url ?? null
        ]);
    }
    
    /**
     * edit
     *
     * @param User $user
     * @return void
     */
    public function edit(User $user, Request $request) {
        $packages = Product::where('type', 'package')
                        ->orderBy('price')
                        ->get();

        return view('admin.members.edit', [
            "user" => $user,
            "packages" => $packages,
            'back_url' => $request->back_url ?? null
        ]);
    }

    /**
     * saveMember
     *
     * @param  mixed $request
     * @return void
     */
    public function saveMember(Request $request) {
        $validation = [
            "first_name"    => "required|max:255",
            "last_name"     => "required|max:255",
            "middle_name"   => "required|max:255",
            "username"      => "required|min:4|max:50|unique:users,username",
            "mobile_number" => "min:11",
            "password"      => "required|min:6|max:50",
            "email"         => "nullable|email|unique:users,email",
        ];

        if (is_null($request->is_root)) {
            $validation["package_id"] = "required";
            $validation["upline_username"]  = ["required", Rule::exists('users', 'username')->where(function($q) use($request) {
                                                    $q->where('role_id', 1);
                                                })
                                            ];
        }

        $request->validate($validation);
        $fullname = $request->first_name . "-" . $request->last_name . "-" . $request->middle_name;
        
        $findDuplicate = User::where(\DB::raw("CONCAT_WS('-', lower(first_name),lower(last_name),lower(middle_name))"), \Str::lower($fullname))->first();

        if ($findDuplicate) {
            return back()->withErrors([
                "Can't proceed. This member's name is a duplicate entry."
            ]);
        }
        
        DB::transaction(function () use ($request){
            $user = new User([
                "first_name" => $request->first_name,
                "middle_name" => $request->middle_name,
                "last_name" => $request->last_name,
                "username" => $request->username,
                "email" => $request->email,
                "password" => Hash::make($request->password),
                "mobile_number" => $request->mobile_number,
                "role_id" => 1, // member
            ]);

            $user->save();

            $user->assignRole('Member');

            
            $membership = new Membership([
                "user_id" => $user->id,
                "member_name" => $user->first_name . " " . $user->last_name,
                "top_level" => isset($request->is_root) ? true : false,
                "is_company_account" => !is_null($request->is_company_account),
                "is_free_account" => !is_null($request->is_free)
            ]);
            
            if (isset($request->is_root)) {
                $membership->top_level = 1;
                $membership->makeRoot();
                $membership->save();
            } else {
                $is_free_account = !is_null($request->is_free);
                $upline = User::where('username', $request->upline_username)->first();
                $upline_membership = $upline->membership;
                $membership->product_id = $request->package_id;
                $membership->ancestor_ids = is_null($upline_membership->ancestor_ids) ? $upline_membership->id : $upline_membership->ancestor_ids . "," . $upline_membership->id;
                $membership->appendToNode($upline_membership)->save();


                $package = Package::find($request->package_id);
                if (!$is_free_account){
                    $direct_referral = TransactionsHelper::earnDirectReferral($upline_membership, $membership, $package, null, auth()->id());

                    if (!$upline_membership->isRoot()) {
                        $indirect_parent = Membership::find($upline_membership->parent_id);

                        TransactionsHelper::irbBonus($indirect_parent, $membership, $package, null, $direct_referral);

                        // LIC bonus
                        if ($upline_membership->package->enabled_lic) {
                            $lic_earnings = TransactionsHelper::initializeLicBonus($upline_membership);

                            foreach ($lic_earnings as $earning) {
                                TransactionsHelper::licBonus($earning->enabled_lic, $earning->member, $membership, $membership->package, $earning->percent, $membership->order);
                            }
                        }
                    }
                }

            }
        });
        
        return redirect()->route('admin.members.index')->with('success', 'Member successfully added!');
    }

    /**
     * updateMember
     *
     * @param User $user
     * @param Request $request
     * @return void
     */
    public function updateMember(User $user, Request $request) 
    {
        $validation = [
            "first_name"    => "required|max:255",
            "last_name"     => "required|max:255",
            "middle_name"   => "required|max:255",
            "username"      => "required|min:4|max:50|unique:users,username," . $user->id,
            "mobile_number" => "min:11",
            "email"         => "nullable|email|unique:users,email," . $user->id
        ];

        if ($user->membership) {
            $validation['member_name'] = 'required';
        }

        $request->validate($validation);

        $fullname = $request->first_name . "-" . $request->last_name . "-" . $request->middle_name;
        
        $findDuplicate = User::where(\DB::raw("CONCAT_WS('-', lower(first_name),lower(last_name),lower(middle_name))"), \Str::lower($fullname))->where("id", "<>", $user->id)->first();

        if ($findDuplicate) {
            return back()->withErrors([
                "Can't proceed. This member's name is a duplicate entry."
            ]);
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->middle_name = $request->middle_name;
        $user->username = $request->username;
        $user->mobile_number = $request->mobile_number;
        $user->email = $request->email;

        if ($user->membership) {
            $user->membership->update([
                'member_name' => $request->member_name
            ]);
        }

        $user->save();

        if ($user->role_id == 1) {
            $user->membership()->update([
                "is_company_account" => !is_null($request->is_company_account),
            ]);
        }

        return redirect()->route('admin.members.index')->with('success', 'Member successfully updated!');
    }

    /**
     * genealogy
     *
     * @param Request $request
     * @return void
     */
    public function genealogy(Request $request) {
        $view = $request->view ?? 1;

        $data = [
            "view" => $view,
        ];

        if ($view == 2) {
            if (auth()->user()->isMember()) {
                $member = auth()->user()->membership;
                $results = Membership::defaultOrder()
                                        ->with('user')
                                        ->with('package')
                                        ->descendantsOf($member->id);
                $results = $results->map(function($record) use ($member) {
                    $anc_ids = array_reverse(explode(",", $record->ancestor_ids));
                    $depth = is_null($record->parent_id) ? 0 : (array_search($member->id, $anc_ids));
                    $record->depth = $depth + 1;
                    return $record;
                });
            } else {
                $results = Membership::defaultOrder()
                                    ->with('user')
                                    ->with('package')
                                    ->withDepth()
                                    ->get();
            }
            $data["results"] = $results;
        }
        
        $packages = Package::where('type', 'package')->whereNull('disabled_at')->with('settings')->orderBy('price')->get();
        $data["packages"] = $packages;

        return view('admin.genealogy.index', $data);
    }

    /**
     * changePassword
     *
     * @param User $user
     * @param Request $request
     * @return void
     */
    public function changePassword(User $user, Request $request) {
        return view('admin.members.change-password', [
            "user" => $user,
            "back_url" => $request->back_url ?? null
        ]);
    }

    /**
     * updatePassword
     *
     * @param User $user
     * @param Request $request
     * @return void
     */
    public function updatePassword(User $user, Request $request) {
        $request->validate([
            "password" => "required|min:6|max:255",
            "password_confirmation" => "required|min:6|same:password"
        ], [
            "password_confirmation.same" => "Confirm password did not match."
        ]);

        $user->password = Hash::make($request->password);
        $user->save();
        
        return redirect()->to($request->back_url)->with('success', $user->first_name . ' ' . $user->last_name . " password was successfully updated.");
    }

    public function earnings(Membership $membership, Request $request) {
        $request->validate([
            'tab'      => 'in:summary,breakdown',
            'type'     => 'in:all,direct referral,indirect referral,lic,unilevel'
        ]);

        $tab = $request->tab ?? "summary";
        $data = [];

        if ($tab == "breakdown") {
            
            $earnings = Earning::where("member_id", $membership->id)
                    ->with('earnedFromMember')
                    ->whereNotNull('added_to_wallet_at')
                    ->when($request->type, function ($query, $type) {
                        if ($type != 'all') {
                            $query->where('type', $type);
                        }
                    })
                    ->when($request->with_range, function ($query) use ($request) {
                        list($startAt, $endAt) = explode(" - ", $request->daterange);
                        $startAt = Carbon::parse($startAt);
                        $endAt = Carbon::parse($endAt);
                        $query->whereBetween('created_at', [$startAt, $endAt]);
                    })
                    ->orderBy('id', 'desc')
                    ->paginate(100);

            $data["membership"] = $membership;
            $data["earnings"] = $earnings->appends([
                'tab'       => $tab,
                'back_url'  => $request->back_url,
                'type'      => $request->type,
                'with_range' => $request->with_range,
                'daterange' => $request->daterange
            ]);


            $data['total_direct'] = $membership->earnings()->where('type', 'direct referral')->when($request->with_range, function ($query) use ($request) {
                                        list($startAt, $endAt) = explode(" - ", $request->daterange);
                                        $startAt = Carbon::parse($startAt);
                                        $endAt = Carbon::parse($endAt);
                                        $query->whereBetween('created_at', [$startAt, $endAt]);
                                    })->count();
            $data["tab"] = $tab;
            $data["back_url"] = $request->back_url ?? null;
        } else {

            Carbon::setWeekStartsAt(Carbon::SUNDAY);
            Carbon::setWeekEndsAt(Carbon::SATURDAY);

            $summary = Earning::select("id", "created_at", "amount", "type", "valid_at")
                                ->where("member_id", $membership->id)
                                ->whereNotNull("added_to_wallet_at")
                                ->get();
            $now = Carbon::now();
            $weekStart = $now->startOfWeek()->toDateTimeString();
            $weekEnd = $now->endOfWeek()->toDateTimeString();


            $weekTotalProductActivation = ActivationCode::where("activated_by", $membership->user_id)
                                                    ->whereNotNull("activated_at")
                                                    ->whereType("product")
                                                    ->where("status", "activated")
                                                    ->count();

            
            $data = [
                "summary" => (object)[
                    "week" => $summary->filter(function($record) use ($weekStart, $weekEnd) {
                        return $record->created_at->between($weekStart, $weekEnd);
                    })->sum("amount"),
                    "month" => $summary->filter(function($record) use ($now) {
                        return $record->created_at->format("Y-m") == $now->format("Y-m");
                    })->sum("amount"),
                    "direct" => $summary->where("type", "direct referral")->sum("amount"),
                    "indirect" => $summary->where("type", "indirect referral")->sum("amount"),
                    "unilevel" => $summary->where("type", "unilevel")->sum("amount"),
                    "lic" => $summary->where("type", "lic")->sum("amount"),
                    "available_commissions" => $membership->available_commissions,
                    "encashments" => $membership->encashment_histories->whereIn("status", ["complete", "pending"]),
                    "total" => $summary->sum('amount')
                ],
                "products_activation_count" => $weekTotalProductActivation,
                "weekStart" => now()->startOfWeek()->format("F j, Y"),
                "weekEnd" => now()->endOfWeek()->format("F j, Y")
            ];
            $data["membership"] = $membership;

            $data["tab"] = $tab;
            $data["back_url"] = $request->back_url ?? null;
        }

        $current_package = $membership->package ? $membership->package->code : null;
        $current_package_price = $membership->package ? $membership->package->price : 0;
        $packages = Package::where("type", "package")
        ->orderBy("price")
        ->whereNull("disabled_at")
        ->get();

        $highestPackage = $packages->last();
        $data["can_be_upgraded"] = is_null($current_package) ? false : ($current_package == $highestPackage->code ? false : true);
        $data["current_package"] = $current_package;
        $data["package_upgrades"] = $packages->where("price", ">", $current_package_price);
        $data['type'] = $request->type;
        $data['with_range'] = $request->with_range;
        $data['daterange'] = $request->daterange;
        return view('admin.members.view', $data);
    }

    public function claimBonus() {
        $user = auth()->user();
        $membership = $user->membership;
        $firstDayOfMonth = Carbon::now()->firstOfMonth();
        $now = Carbon::now();
        $not_claimed_bonuses = Earning::where("member_id", $membership->id)
                                            ->whereIn("type", ["indirect referral", "unilevel", "lic"])
                                            ->whereNotNull("valid_at")
                                            ->whereNull("added_to_wallet_at")
                                            ->whereDate("valid_at", $firstDayOfMonth)
                                            ->get();
        $earnings = collect();
        $bonuses = collect();
        $balance = $membership->eWallet();
        $not_claimed_bonuses->each(function($bonus) use (&$earnings, $membership, &$balance, &$bonuses, $now, $firstDayOfMonth) {
            $balance = $balance + $bonus->amount;
            $transaction = new Transaction;
            $transaction->fill([
                "user_id"       => $membership->user_id,
                "member_id"     => $membership->id,
                "debit"         => $bonus->amount,
                "credit"        => 0,
                "balance"       => $balance,
                "type"          => "earnings",
                "resource_id"   => $bonus->id,
                "transacted_by" => auth()->id(),
            ]);
            
            $earnings->push($transaction);

            $bonuses->push($bonus->fill([
                "added_to_wallet_at" => $firstDayOfMonth
            ]));
        });

        \DB::transaction(function () use ($earnings, $bonuses, $membership, $balance, $not_claimed_bonuses){
            $earnings->each(function($earning) {
                $earning->save();
            });

            $bonuses->each(function($bonus) {
                $bonus->save();
            });

            $membership->total_earnings = TransactionsHelper::getTotalEarnings($membership);
            $membership->available_commissions = $balance;
            $membership->prev_month_bonus_claimed_amount = $not_claimed_bonuses->sum("amount");
            $membership->prev_month_bonus_claimed_at = Carbon::now();
            $membership->save();
        });


        return $earnings;
    }

    /**
     * Upgrade Account
     *
     * @param Membership $member
     * @param Request $request
     * @return void
     */
    public function upgradeAccount(Membership $member, Request $request) {
        $request->validate([
            "package_id" => "required"
        ]);

        \DB::transaction(function () use ($request, $member) {
                
            $history = new AccountUpgradeHistory([
                "user_id"       => $member->user_id,
                "member_id"     => $member->id,
                "from_package"  => $member->package->id,
                "to_package"    => $request->package_id,
                "upgraded_by"   => auth()->id()
            ]);

            $member->product_id = $request->package_id;

            $history->save();
            $member->save();
        });

        return back()->with('success', "Account successfully upgraded!");
    }
}

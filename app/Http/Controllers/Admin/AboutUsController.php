<?php

namespace App\Http\Controllers\Admin;

use Image;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class AboutUsController extends Controller
{
    public function edit() {

        $about_us = CustomField::where('field', 'about-us-page-contents')->first();

        return view('admin.site-settings.pages.about-us', [
            "about_us" => $about_us
        ]);
    }

    public function update(Request $request) {
        $request->validate([
            "details" => "required"
        ]);

        $about_us = CustomField::where('field', 'about-us-page-contents')->first();
        $about_us->data = $request->details;
        $about_us->save();

        return redirect()->route('site-settings.index')->with('success', 'About Us Page successfully updated.');
    }

    public function insertImage(Request $request)
    {
        $request->validate([
            'image' => 'required|image'
        ]);

        $extension = $request->image->extension();

        $name = $request->image->getClientOriginalName();
        $type = $request->image->getClientOriginalExtension();
        $path = $request->file('image');
        $image = Image::make($path)->resize(650, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $name = now()->format('mdYHis') . '-' . $name;

        $store = "images/about-us/{$name}";

        $image->save(public_path($store));


        return response("/" . $store, 200);
    }
}

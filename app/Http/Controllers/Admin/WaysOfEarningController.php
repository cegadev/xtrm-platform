<?php

namespace App\Http\Controllers\Admin;

use App\Models\EarningType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WaysOfEarningController extends Controller
{
    public function index() {

        $ways_of_earning = EarningType::all()->sortBy('priority')->values();

        return view('admin.site-settings.pages.ways-of-earning.index', [
            "ways_of_earning" => $ways_of_earning
        ]);
    }

    public function create() {
        return view('admin.site-settings.pages.ways-of-earning.create');
    }

    public function save(Request $request) {
        $request->validate([
            "woe_title" => "required|max:255",
            "description" => "required",
        ]);

        $lastItem = EarningType::orderBy('priority', 'desc')->first();
        $lastNumber = !empty($lastItem) ? $lastItem->priority : 0;
        
        $woe = new EarningType([
            "title" => $request->woe_title,
            "description" => $request->description,
            'priority'    => $lastNumber + 1
        ]);

        $woe->save();

        return redirect()->route('site-settings.pages.ways-of-earning.index')->with('success', 'New ways of earning added successfully.');
    }

    public function edit(EarningType $earningType) {
        return view('admin.site-settings.pages.ways-of-earning.edit', [
            "data" => $earningType
        ]);
    }

    public function update(EarningType $earningType, Request $request) {
        $request->validate([
            "woe_title" => "required|max:255",
            "description" => "required",
        ]);

        $earningType->title = $request->woe_title;
        $earningType->description = $request->description;

        $earningType->save();

        return redirect()->route('site-settings.pages.ways-of-earning.index')->with('success', 'Ways of earning updated successfully.');
    }

    public function delete(Request $request) {
        $ids = $request->ids;
        $earningTypes = EarningType::whereIn("id", explode(",", $ids))->delete();

        return redirect()->route('site-settings.pages.ways-of-earning.index')->with('success', 'Ways of earning deleted successfully.');
    }

    public function saveSorting(Request $request)
    {
        $request->validate([
            'ids' => 'required'
        ]);

        $sortOrder = collect($request->ids)->map(function ($record, $key) {
            return (object) [
                'id'    => $record,
                'order' => $key + 1
            ];
        })->keyBy('id');

        \DB::transaction(function () use ($sortOrder) {
            foreach($sortOrder as $item) {
                EarningType::where('id', $item->id)->update([
                    'priority' => $item->order
                ]);
            }
        });

        return response()->json([
            'message' => 'Item sorting successfully updated.'
        ], 200);
    }
}

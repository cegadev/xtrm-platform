<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create');
    }
    
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        Post::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        return redirect()->route('posts.index')->withSuccess('Post successfully created.');
    }
    
    public function update(Post $post, Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $post->update([
            'title' => $request->title,
            'description' => $request->description
        ]);

        return redirect()->route('posts.index')->withSuccess('Post successfully updated.');
    }
}

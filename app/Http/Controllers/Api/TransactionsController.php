<?php

namespace App\Http\Controllers\Api;

use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionsController extends Controller
{
    public function index() {

    }

    /**
     * Transactions list
     *
     * @param Request $request
     * @return void
     */
    public function transactions(Request $request) {
        $transactions = Transaction::when($request->user_id, function($q, $user_id) {
                                        $q->where("user_id", $user_id);
                                    })
                                    ->when($request->member_id, function($q, $member_id) {
                                        $q->where("member_id", $member_id);
                                    })
                                    ->where('resource_id', '<>', 0)
                                    ->latest()
                                    ->with('earnings')
                                    ->with('encashments')
                                    ->get()->map(function($record) {
                                        $notes = "";
                                        $amount = 0;
                                        if ($record->debit > 0 && $record->credit == 0) {
                                            $amount = "+ " . number_format($record->debit, 2);

                                            if ($record->type == "encashment") {
                                                $notes = " - Refunded";
                                            }
                                        }

                                        if ($record->credit > 0 && $record->debit == 0) {
                                            $amount = "(- " . number_format($record->credit, 2) . ")";
                                        }

                                        if (empty($record->earnings) || empty($record->encashments)) {
                                            $transaction_date = $record->created_at;
                                        } else {
                                            $transaction_date = $record->type == "earnings" ? $record->earnings->created_at : $record->encashments->created_at;
                                        }

                                        return (object)[
                                            "id" => sprintf("%08d", $record->id),
                                            "type" => \strtoupper($record->type) . $notes,
                                            "amount" => $amount,
                                            "transaction_date" => $transaction_date->format("Y-m-d h:i A"),
                                        ];
                                    });
        return [
            "data" => $transactions
        ];
    }
}

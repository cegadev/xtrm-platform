<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Review;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function list(Request $request) {
        $products = Product::when($request->keyword, function($q, $keyword) {
                        $q->where('name', 'like', '%'. $keyword .'%');
                    })
                    ->when($request->type, function($q, $type) {
                        $q->where("type", $type);
                    })
                    ->whereNull('disabled_at')
                    ->select('id', 'name', 'code', 'price', 'type')
                    ->orderBy("name")
                    ->get();
                    
        return $products->toJson();
    }

    /**
     * viewProduct
     *
     * @param mixed $productId
     * @return void
     */
    public function viewProduct($productId) {
        $product = Product::with('category')
                            ->with('publishedReviews')
                            ->whereId($productId)->first();
        return $product;
    }

    public function writeReview(Product $product, Request $request) {
        \DB::transaction(function () use ($product, $request){
            $review = new Review([
                "user_id" => $request->user_id,
                "message" => $request->message,
                "rating" => $request->rating,
            ]);
            
            $product->reviews()->save($review);
        });

        return response()->json([
            "status" => "success",
            "message" => "Review successfully created."
        ]);
    }

    /**
     * Create Category
     *
     * @param Request $request
     * @return void
     */
    public function saveCategory(Request $request) {
        $request->validate([
            "category_name" => "required|unique:product_categories,name",
        ]);

        ProductCategory::create([
            "name" => $request->category_name
        ]);

        return response()->json([
            "status" => "success",
            "message" => "Product category successfully created."
        ], 200);
    }
    
    /**
     * Update Category
     *
     * @param Request $request
     * @return void
     */
    public function updateCategory(Request $request) {
        $request->validate([
            "id" => "required",
            "category_name" => "required",
        ]);

        ProductCategory::where("id", $request->id)
        ->update([
            "name" => $request->category_name
        ]);

        return response()->json([
            "status" => "success",
            "message" => "Product category successfully updated."
        ], 200);
    }

    public function deleteCategory(Request $request) {
        $request->validate([
            "id" => "required",
        ]);

        $category = ProductCategory::find($request->id);

        ProductCategory::where("id", $request->id)
        ->update([
            "disabled_at" => Carbon::now(),
            "name" => $category->name . " (DELETED) "
        ]);
    }
}

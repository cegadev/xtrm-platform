<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use App\Models\Earning;
use Carbon\CarbonPeriod;
use App\Models\Membership;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembershipController extends Controller
{
    /**
     * earnings
     *
     * @param Request $request
     * @return void
     */
    public function earnings(Request $request) {
        if ($request->member_id) {
            $membership = Membership::whereId($request->member_id)
                        ->with('earnings')
                        ->first();

            $earnings = $membership->earnings;
        } else {
            $earnings = Earning::all();
        }
        
        $months = [];
        for ($i = 1; $i <= 12; $i++) {
            $months[] = Carbon::parse("2020-" . $i . "-01")->format("Y-M");
        }

        $monthly_earnings = collect($months)->map(function($month) use($earnings) {
            $amount = $earnings->filter(function($earning) use($month) {
                return Carbon::parse($earning->created_at)->format("Y-M") == $month;
            })->sum("amount"); 

            $m = explode("-", $month);

            return [
                "label" => [
                    "month" => $m[1],
                    "year" => $m[0]
                ],
                "amount" => $amount
            ];
        });   

        return collect([
            "monthly_earnings" => $monthly_earnings,
            "yearly_earnings" => []
        ])->toJSON();
    }

    public function genealogy(Request $request) {
        $membership = Membership::where("user_id", $request->user_id)
        ->with('package.settings')
        ->with('user')->get()->first();
        
        /**
         * Get nodes upto 5th level
         */
        
        if ($membership) {
            $nodes = Membership::withDepth()
                                ->with('user')
                                ->with('package.settings')
                                ->having('depth', '<=', 5)
                                ->descendantsOf($membership->id)
                                ->toTree();
            $membership->children = $nodes;
            return $membership;
        } else {
            $nodes = Membership::withDepth()
            ->with('user')
            ->with('package.settings')
            ->get()
                                ->toTree();
            $nodes = collect([
                "member_name" => "MJPremium Company",
                "user" => [
                    "username" => "TOP LEVEL"
                ],
                "children" => $nodes,
            ]);

            return $nodes;
        }
        
    }

    public function earningsType(Request $request) {
        $earnings = Earning::when($request->member_id, function($q, $memberId) {
            $q->where("member_id", $memberId);
        })
        ->get();

        $data = $earnings->groupBy("type")->map(function($records) {
            return $records->sum("amount");
        });


        return [
            "Direct" => $data->get("direct referral") ?? 0,
            "Indirect" => $data->get("indirect referral") ?? 0,
            "Unilevel" => $data->get("unilevel") ?? 0,
        ];
    }
}

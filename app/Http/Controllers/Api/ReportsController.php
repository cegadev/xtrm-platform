<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Product;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Models\CompanyEarning;
use App\Models\EncashmentHistory;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    public function encashmentChart(Request $request) {
        $reports = EncashmentHistory::when($request->member_id, function($q, $member_id){
            $q->where('membership_id', $member_id);
        })->where('status', 'complete')->get();

        $months = collect();
        $rangeStart = Carbon::now()->startOfYear();
        $rangeEnd = Carbon::now()->endOfYear();
        $periods = CarbonPeriod::create($rangeStart, '1 month', $rangeEnd);
        $yearMonths = collect(); 
        foreach($periods as $period) {
            $months->push($period->format("M"));
            $yearMonths->push($period->format("Y-m"));
        }
        
        $monthlyReports = collect();
        $yearMonths->each(function($yearMonth, $key) use (&$monthlyReports, $reports, $months) {
            $report = $reports->filter(function($record) use ($yearMonth) {
                return Carbon::parse($record->approved_at)->format("Y-m") == $yearMonth;
            })->sum("amount");

            $monthlyReports->push([
                "month" => $months[$key],
                "data" => $report
            ]);
        });

        return [
            "months" => $monthlyReports->pluck("month"),
            "values" => $monthlyReports->pluck("data"),
            "raw" => $monthlyReports
        ];
    }

    public function encashmentJsonData(Request $request) {
        $type = $request->type ?? 'requested';

        $dateRangeTypes = [
            'released'  => 'approved_at',
            'requested' => 'requested_at'
        ];

        $reports = EncashmentHistory::when($request->member_id, function($q, $member_id){
            $q->where('membership_id', $member_id);
        })
        ->with('requestedBy')
        ->when($request->date_range, function($q, $date_range) use ($type, $dateRangeTypes) {
            $range = explode("to", $date_range);
            $dateRangeType = $dateRangeTypes[$type] ?? 'approved_at';
            if (count($range) > 1) {
                $start_at = Carbon::parse($range[0])->startOfDay()->toDateTimeString();
                $end_at = Carbon::parse($range[1])->endOfDay()->toDateTimeString();
                $q->whereBetween($dateRangeType, [$start_at, $end_at]);
            } else {
                $q->whereDate($dateRangeType, Carbon::parse($range[0])->toDateString());
            }
        })
        ->where('status', 'complete')
        ->get();

        if ($request->excel) {
            $transformedData = $reports->map(function($record) {
                // return [
                //     sprintf("%08d", $record->id),
                //     $record->requestedBy->first_name . " " . $record->requestedBy->last_name,
                //     number_format($record->amount, 2),
                //     Carbon::parse($record->requested_at)->format("M j, Y - h:i A"),
                //     Carbon::parse($record->approved_at)->format("M j, Y - h:i A"),
                // ];

                return [
                    "reference_number" => "ENC-" . sprintf("%08d", $record->id), 
                    "name" => $record->requestedBy->first_name . " " . $record->requestedBy->last_name, 
                    "amount" => number_format($record->amount, 2), 
                    "amount_released" => number_format($record->amount_released, 2),
                    "service_fee" => number_format($record->service_fee, 2),
                    "tax" => number_format(($record->amount * $record->vat_applied), 2),
                    "transaction_type" => \Str::upper($record->transaction_type),
                    "requested_at" => Carbon::parse($record->requested_at)->format("M j, Y "), 
                    "approved_at" => Carbon::parse($record->approved_at)->format("M j, Y "), 
                ];
            })->toArray();


            return [
                "recordsTotal" => $reports->count(),
                "recordsFiltered" => $reports->count(),
                "data" => $transformedData
            ];
        }

        return $reports;
    }

    public function salesChart(Request $request) {
        $product_id = $request->product_id;
        $products = Product::whereNull('disabled_at')->get();
        $earnings = CompanyEarning::with(['order.product' => function($q) {
            $q->whereNull('disabled_at');
        }])
        ->when($request->product_type, function($q, $product_type) use ($product_id) {
            if ($product_type != "all" && $product_id == "all") {
                $q->whereHas("order.product", function($q) use ($product_type){
                    $q->where("type", $product_type);
                });
            }
        })
        ->when($request->product_id, function($q, $product_id) {
            if ($product_id != "all") {
                $q->whereHas("order.product", function($q) use ($product_id) {
                    $q->where("id", $product_id);
                });
            }
        })
        ->when($request->date_range, function($q, $date_range) {
            $range = explode(" to ", $date_range);
            if (count($range) > 1) {
                $q->whereBetween("created_at", [Carbon::parse($range[0])->startOfDay()->toDateTimeString(), Carbon::parse($range[1])->endOfDay()->toDateTimeString()]);
            } else {
                $q->whereDate("created_at", Carbon::parse($range[0])->toDateString());
            }
        })
        ->get();

        $product_type = $request->product_type ?? "all";

        if ($product_type == "all") {
            $selectedProducts = $products;
        } else {
            $selectedProducts = $products->where("type", $product_type);
        }

        $earningsSummary = $selectedProducts->keyBy("id")->map(function($record, $productId) use ($earnings) {
            $amount = $earnings->where("order.product.id", $productId)->sum("amount");
            return [
                "name" => $record->name,
                "earning" => $amount 
            ];
        })->values();
        

        if ($product_type == "all") {
            $otherEarnings = $earnings->whereNotIn("type", ["package purchase", "product purchase"])->groupBy("type")->map(function($records, $type) {
                return [
                    "name" => $type == "service fee" ? "Processing Fee" : \Str::ucfirst($type),
                    "earning" => $records->sum("amount")
                ];
            })->values();

            $earningsSummary = $earningsSummary->merge($otherEarnings);
        }

        return $earningsSummary;
    }

    public function salesJsonData(Request $request) {
        $product_id = $request->product_id;
        $earnings = CompanyEarning::latest()
                        ->with('order')
                        ->with('transactedBy')
                        ->when($request->product_type, function($q, $product_type) use ($product_id) {
                            if ($product_type != "all" && $product_id == "all") {
                                $q->whereHas("order.product", function($q) use ($product_type){
                                    $q->where("type", $product_type);
                                });
                            }
                        })
                        ->when($request->product_id, function($q, $product_id) {
                            if ($product_id != "all") {
                                $q->whereHas("order.product", function($q) use ($product_id) {
                                    $q->where("id", $product_id);
                                });
                            }
                        })
                        ->when($request->date_range, function($q, $date_range) {
                            $range = explode(" to ", $date_range);
                            if (count($range) > 1) {
                                $q->whereBetween("created_at", [Carbon::parse($range[0])->startOfDay()->toDateTimeString(), Carbon::parse($range[1])->endOfDay()->toDateTimeString()]);
                            } else {
                                $q->whereDate("created_at", Carbon::parse($range[0])->toDateString());
                            }
                        })
                        ->get();

        $earnings = $earnings->map(function($record) {
            return [
                "reference_number" => sprintf("%08d", $record->id),
                "from" => $record->resource_type == "order" ? "Order #". $record->order->id : \Str::ucfirst($record->resource_type),
                "notes" => $record->notes,
                "transacted_by" => $record->transactedBy->first_name . " " . $record->transactedBy->last_name,
                "transaction_date" => $record->created_at->format("M d, Y - h:i A"),
                "amount" => \number_format($record->amount, 2)
            ];
        });

        return [
            "data" => $earnings
        ];
    }
}

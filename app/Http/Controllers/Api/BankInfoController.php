<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\MemberBankInfo;
use App\Http\Controllers\Controller;

class BankInfoController extends Controller
{
    public function index(Request $request) {
        $infos = MemberBankInfo::when($request->member_id, function($q, $member_id) {
            $q->where('member_id', $member_id);
        })
        ->with('member')
        ->when($request->type, function($q, $type) {
            if ($type == "others") {
                $q->where('type', 'other');
            } else {
                $q->where('type', $type);
            }
        })
        ->get()->map(function($record) {
            $record->account_name = $record->account_name ?? $record->member->member_name;
            return $record;
        });

        return $infos;
    }

    public function bankInfo(MemberBankInfo $member_bank_info, Request $request) {
        if (empty($member_bank_info->account_name)) {
            $member_bank_info->account_name = $member_bank_info->member->member_name;
        }

        return $member_bank_info;
    }
}

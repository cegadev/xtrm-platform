<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Earning;
use App\Models\Package;
use App\Models\OrderItem;
use App\Models\Membership;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Models\ActivationCode;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\ReleasePackageHistory;
use App\Http\Helpers\TransactionsHelper;

class ActivationController extends Controller
{    
    /**
     * validateActivation
     *
     * @param  mixed $request
     * @return void
     */
    public function validateActivation(Request $request) {
        
        // validate order
        $order = Order::whereId($request->id)
                        ->where('user_id', $request->user_id)
                        ->where('status', 'released')
                        ->select('id', 'total_activation_left')
                        ->first();
        
        if ($order) {
            if ($order->total_activation_left > 0) {
                return response()->json([
                    'message' => "Can be activated",
                ], 200);
            } else {
                return response()->json([
                    'message' => "No activation left.",
                ], 400);
            }
        } else {
            $data["activation_left"] = "order_not_found";
            return response()->json([
                'message' => "Order not found."
            ], 404);
        }
    }


    public function validateNewUser(Request $request) {
        $username = $request->username;
        $email = $request->email;
        $sponsor = $request->sponsor_username;

        // check if username taken
        $usernameExists = User::where('username', $username)->first();
        if ($request->email) {
            $emailExists = User::where('email', $email)->first();
        } else {
            $emailExists = false;
        }

        if ($request->sponsor_username) {
            $sponsorExists = User::where('username', $sponsor)->first();
        } else {
            $sponsorExists = false;
        }

        $data = [];
        if ($usernameExists || $emailExists) {
            $data["status"] = "failed";
            $data["data"] = [
                "username" => $usernameExists ? "exists" : "true",
                "email" => $emailExists ? "exists" : "true",
                "sponsor" => $sponsorExists && $sponsorExists->role_id == 1 ? "exists" : "true"
            ];
        } else {
            $data["status"] = "success";
        }

        return response()->json($data, 200);
    }

    public function validateActivationCode(Request $request) {
        $order = Order::find($request->order_id);
        $activation_codes = $order->product_codes;

        if ($activation_codes->count() == 0) {
            return response()->json([
                "status" => "success",
                "data" => "valid"
            ], 200);
        } else {
            $given_code = "MJP-" . $request->product_code;

            $all_activation_codes = $activation_codes->pluck("code");
            $activation_code_match = $activation_codes->firstWhere("code", $given_code);
            if ($activation_code_match) {
                if ($activation_code_match->is_activated == false) {
                    return response()->json([
                        "status" => "success",
                        "data" => "valid"
                    ], 200);
                } else {
                    return response()->json([
                        "status" => "failed",
                        "data" => "Product code already used. Please enter another one."
                    ], 400);
                }
            } else {
                return response()->json([
                    "status" => "failed",
                    "data" => "Invalid product code"
                ], 400);
            }
        }
    }

    public function viewHistory(Request $request) {
        $order = $request->order_id;
        $histories = ReleasePackageHistory::where("order_id", $order)
                                            ->with('user.membership')
                                            ->get();
        return $histories->map(function($record) {
            return (object)[
                "member_name" => $record->user->membership->member_name,
                "ago" => $record->user->membership->created_at->diffForHumans()
            ];
        });
    }

    public function registerNewMember(Request $request) {
        $order_item_id = $request->order_item_id;
        $order_item = OrderItem::find($order_item_id);
        
        $loggedUserId = $order_item->user_id;

        if ($order_item->is_activated) {
            return response()->json([
                "status" => "failed",
                "message" => "Action cannot proceed. Order item has already been placed. Please try again."
            ], 405);
        }

        $order_id = $request->order_id;
        $order = $order_item->order;
        
        if ($order->completed_at) {
            return response()->json([
                "status" => "failed",
                "message" => "Action cannot proceed. Order has already been completed."
            ], 405);
        }
        
        $activations_left = $order->items->where('is_activated', false);
        
        $my_activation_keys = ActivationCode::whereStatus('released')
                                            ->whereReleasedTo($loggedUserId)
                                            ->whereProductId($order_item->product_id)
                                            ->whereNull('activated_at')
                                            ->get();

        if ($my_activation_keys->count() == 0) {
            return response()->json([
                "status" => "failed",
                "message" => "Action cannot proceed. No activation keys left."
            ], 405);
        }

        if ($activations_left->count() == 0) {
            return response()->json([
                "status" => "failed",
                "message" => "Action cannot proceed. No registrations left."
            ], 405);
        }

        $request->validate([
            "first_name" => ["required","max:255"],
            "middle_name" => ["required","max:255"],
            "last_name" => ["required", "max:255"],
            "email" => ["nullable", "email", "max:255", "unique:users,email"],
            "username" => ["required", "min:4", "max:255", "unique:users,username"],
            "password" => ["required", "max:255", "min:6"],
            "mobile_number" => ["required", "min:11"],
            "repeat_password" => ["required", "max:255", "same:password", "min:6"],
            "sponsor_username" => ["required", Rule::exists("users", "username")->where(function($q) {
                $q->where("role_id", 1);
            })]
        ]);

        $fullname = $request->first_name . "-" . $request->last_name . "-" . $request->middle_name;
        
        $findDuplicate = User::where(\DB::raw("CONCAT_WS('-', lower(first_name),lower(last_name),lower(middle_name))"), \Str::lower($fullname))->first();

        if ($findDuplicate) {
            return response()->json([
                "status" => "failed",
                "message" => "Can't proceed. This member's name is a duplicate entry."
            ], 405);
        }

        \DB::transaction(function () use ($request, $order, $activations_left, $my_activation_keys, $order_item) {
            // Create new user
            $new_member = new User([
                "first_name"    => $request->first_name,
                "middle_name"   => $request->middle_name ?? null,
                "last_name"     => $request->last_name,
                "email"         => $request->email ?? null,
                "username"      => $request->username,
                "password"      => $request->password,
                "mobile_number" => $request->mobile_number ?? null,
                "password"      => Hash::make($request->password),
                "role_id"       => 1,
            ]);

            $new_member->save();
            
            $parent = $order_item->user;
            
            if ($request->sponsor_username) {
                $parent = User::where('username', $request->sponsor_username)->first();
            }

            $parent_membership = $parent->membership;

            // Save membership info
            $new_member->assignRole('Member');
            
            $membership = new Membership([
                "order_id" => $order_item->order_id,
                "product_id" => $order_item->product_id,
                "member_name" => $new_member->first_name . " " . $new_member->last_name,
                "ancestor_ids" => is_null($parent_membership->ancestor_ids) ? $parent_membership->id : $parent_membership->ancestor_ids . "," . $parent_membership->id,
                "user_id" => $new_member->id,
            ]);

            $membership->appendToNode($parent_membership)->save();

            /**
             * Create Relase of Package logs
             */
            $packageHistory = new ReleasePackageHistory([
                "user_id"   => $new_member->id,
                "status"    => "activated"
            ]);

            $order->package_histories()->save($packageHistory);
            
            // $total_activation_left = $order->total_activation_left - 1;
            // $order->total_activation_left = $total_activation_left;
            $activationKey = $my_activation_keys->first();
            $orderItem = $order_item;
            

            $activationKey->order_id = $order->id;
            $activationKey->activated_at = Carbon::now();
            $activationKey->activated_by = $orderItem->user_id;
            $activationKey->status = "activated";
            $activationKey->registered_to = $new_member->id;
            $activationKey->save();

            $orderItem->is_activated = true;
            $orderItem->activated_by = $orderItem->user_id;
            $orderItem->activated_at = Carbon::now();
            $orderItem->activation_code_id = $activationKey->id;
            $orderItem->save();
            
            
            // check if this order has more activations left, if none left then make orders complete
            if (($activations_left->count() - 1) == 0) {
                $order->completed_at = Carbon::now();
                $order->status = "completed";
            }

            $order->save();

            $isFreeAccount = false;
            if (!$isFreeAccount) {
                /**
                 * Earn direct referral commision
                 */
                $upline_membership = $parent_membership;
                $package = $order->package;

                $direct_referral = TransactionsHelper::earnDirectReferral($upline_membership, $membership, $package, $order, $order_item->user_id);

                if (!$upline_membership->isRoot()) {
                    $indirect_parent = Membership::find($upline_membership->parent_id);

                    TransactionsHelper::irbBonus($indirect_parent, $membership, $package, $order, $direct_referral);

                    // LIC bonus
                    if ($upline_membership->package->enabled_lic) {
                        $lic_earnings = TransactionsHelper::initializeLicBonus($upline_membership);

                        foreach ($lic_earnings as $earning) {
                            TransactionsHelper::licBonus($earning->enabled_lic, $earning->member, $membership, $membership->package, $earning->percent, $membership->order);
                        }
                    }
                }
            }

        });


        return response()->json([
            "status" => "success",
            "message" => "New member successfully registered."
        ], 200);
    }

    /**
     * activateProduct
     *
     * @param Order $order
     * @return void
     */
    public function activateProduct(Request $request) {
        $request->validate([
            "order_id" => "required",
            "user_id"   => "required",
            "quantity" => "required"
        ]);


        $order = Order::find($request->order_id);
        $order_items = $order->items->where('is_activated', false)->where("user_id", $request->user_id);
        $quantity = $request->quantity;

        if ($request->quantity > $order_items->count()) {
            return response()->json([
                "status" => "failed",
                "message" => "Quantity is invalid."
            ], 405);
        }

        $user = User::find($request->user_id);
        
        // check if user has product activation codes remaining
        $my_activation_keys = ActivationCode::whereStatus('released')
                                ->whereReleasedTo($user->id)
                                ->whereType('product')
                                ->whereNull('activated_at')
                                ->get();
        if ($my_activation_keys->count() == 0) {
            return response()->json([
                "status" => "failed",
                "message" => "No activation keys for products available.",
            ], 405);
        } 

        if ($my_activation_keys->count() < $quantity) {
            return response()->json([
                "status" => "failed",
                "message" => "Not enough activation keys."
            ], 405);
        }

        \DB::transaction(function () use ($order, $order_items, $quantity, $user, $my_activation_keys) {
            $for_activation = $order_items->take($quantity)->values();
            
            $me = $user->membership;
            $ancestors_points = collect();
            $ancestors = collect(explode(",", $me->ancestor_ids))->reverse()->values()->take(5);
            $members = collect();
            if (!$me->isRoot() && $ancestors->count()) {

                $members = Membership::whereIn("id", $ancestors)->get()->keyBy("id");

                $ancestors_points = $ancestors->map(function($record, $key) {
                    return (object)[
                        "ancestor" => $record,
                        "points" => 5 - $key,
                    ];
                });
            }


            $now = Carbon::now();
            $today = Carbon::now();
            $valid_date = $now->firstOfMonth()->addMonths(1);
            foreach($for_activation as $key => $order_item) {
                $activation_code = $my_activation_keys->get($key);

                if ($ancestors_points->count()) {
                    $ancestors_points->each(function($record) use ($members, $me, $order, $user) {
                        $member = $members->get($record->ancestor);
                        TransactionsHelper::unilevelBonus($member, $me, $record->points, $order, $user->id);
                    });
                }

                $order_item->is_activated = true;
                $order_item->activated_by = $me->user_id;
                $order_item->activated_at = Carbon::now();
                $order_item->activation_code_id = $activation_code->id;
                $order_item->save();

                $activation_code->activated_at = Carbon::now();
                $activation_code->activated_by = $me->user_id;
                $activation_code->status = "activated";
                $activation_code->order_id = $order->id;
                $activation_code->product_id = $order->product_id;
                $activation_code->save();

                        
                if ($me->isRoot()) {
                    $memberTotalProductActivations = TransactionsHelper::getTotalProductActivationThisMonth($me);
                    if ($memberTotalProductActivations >= 3) {
                        Earning::where("member_id", $me->id)
                                ->whereYear("created_at", $today->year)
                                ->whereMonth("created_at", $today->month)
                                ->whereIn("type", ["indirect referral", "unilevel"])
                                ->whereNull("valid_at")
                                ->update([
                                    'valid_at' => $valid_date
                                ]);

                    }

                    if ($memberTotalProductActivations >= 5) {
                        Earning::where("member_id", $me->id)
                                ->whereYear("created_at", $today->year)
                                ->whereMonth("created_at", $today->month)
                                ->where('type', 'lic')
                                ->whereNull("valid_at")
                                ->update([
                                    'valid_at' => $valid_date
                                ]);
                    }

                }

                // check if has no more activations, mark order as complete
                $remaining = OrderItem::whereOrderId($order_item->order_id)->whereNull('activated_at')->count();
                if ($remaining == 0) {
                    $order_item->order->update([
                        "status" => "completed",
                        "completed_at" => Carbon::now(),
                    ]);
                }


            }

            $total_activation_this_month = TransactionsHelper::getTotalProductActivationThisMonth($me);

            if ($total_activation_this_month >= 3) {
                Earning::whereIn("type", ["unilevel", "indirect referral"])
                        ->whereMonth("created_at", $today->month)
                        ->whereYear("created_at", $today->year)
                        ->whereNull('valid_at')
                        ->where('member_id', $me->id)
                        ->update([
                            "valid_at" => $valid_date
                        ]);
            }
            
            if ($total_activation_this_month >= 5) {
                Earning::where("type", 'lic')
                        ->whereMonth("created_at", $today->month)
                        ->whereYear("created_at", $today->year)
                        ->whereNull('valid_at')
                        ->where('member_id', $me->id)
                        ->update([
                            "valid_at" => $valid_date
                        ]);
            }
        });

        return response()->json([
            "status" => "success",
            "message" => "Activation complete."
        ], 200);
    } 

    private function productActivation($user, $order_item, $activation_code) {
        $unilevelLimit = 5;
        $points = 5;
        $monthlyLimitPerTeam = 10;
        $me = $user->membership;
        $order = $order_item->order;
        $ancestors = collect(explode(",", $me->ancestor_ids))->reverse()->values()->take($unilevelLimit);
        $currentMonth = Carbon::now()->format("Y-m");
        $currentYear = Carbon::now()->format("Y");

        $activation_code = ActivationCode::find($activation_code->id);
        $order_item = OrderItem::find($order_item->id);
        \DB::transaction(function () use ($ancestors, $points, $unilevelLimit, $me, $order, $currentMonth, $currentYear, $monthlyLimitPerTeam, $order_item, $activation_code){
            if (!$me->isRoot() && $ancestors->count()) {
                $members = Membership::whereIn("id", $ancestors)->get();

                    /**
                     * If has ancestors, then give unilevel commission
                     */
                    // set points
                    $ancestors_points = $ancestors->map(function($record, $key) use ($points){
                        return (object)[
                            "ancestor" => $record,
                            "points" => $points - $key
                        ];
                    });
 

                    $earnings = collect();
                    $ancestors_points->each(function($record) use ($members, &$earnings, $me, $order) {
                        $member = $members->get($record->ancestor);

                        TransactionsHelper::unilevelBonus($member, $me, $record->points, $order, auth()->id());
                    });

                    // $earnings->each(function($earning){
                    //     $earning->save();
                    // });

                    // $members->each(function ($member) {
                    //     $member->available_commissions = Helpers::computeCommission($member);
                    //     $member->save();
                    // });

                    


                
            }

            $order_item->is_activated = true;
            $order_item->activated_by = $me->user_id;
            $order_item->activated_at = Carbon::now();
            $order_item->activation_code_id = $activation_code->id;
            $order_item->save();

            $activation_code->activated_at = Carbon::now();
            $activation_code->activated_by = $me->user_id;
            $activation_code->status = "activated";
            $activation_code->order_id = $order->id;
            $activation_code->product_id = $order->product_id;
            $activation_code->save();
        });

    }
}

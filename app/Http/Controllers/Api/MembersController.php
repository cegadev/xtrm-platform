<?php

namespace App\Http\Controllers\Api;

use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    public function list() {
        $members = Member::whereHas('role', function($q) {
                                $q->where('identity', 'member');
                            })
                            ->select('id', 'first_name', 'last_name', 'email', 'username')
                            ->orderBy('id', 'desc')
                            ->get();

        return $members->toJSON();
    }
}

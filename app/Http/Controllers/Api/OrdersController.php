<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\TransferItem;
use Illuminate\Http\Request;
use App\Models\ActivationCode;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{    
    /**
     * getOrder
     *
     * @param  mixed $request
     * @return void
     */
    public function getOrder(Request $request){
        $order = Order::whereId($request->id)
                        ->with('product')
                        ->with('items')
                        ->first();

        $order_items = OrderItem::whereUserId($request->user_id)->whereOrderId($request->id)->where('is_activated', false)->get();

        $keys_available = ActivationCode::whereReleasedTo($request->user_id)
                                        ->whereStatus('released')
                                        ->where(function($q) use ($order) {
                                            if ($order->product->type == "package") {
                                                $q->where('product_id', $order->product_id);
                                            }
                                        })
                                        ->count();
        return collect([
            'id'                => $order->id,
            'product_code'      => $order->product->code,
            'product_name'      => $order->product->name,
            'activations_left'  => $order->items->where('is_activated', false)->count(),
            'keys_available'    => $keys_available,
            'type'              => $order->product->type,
            'product_id'        => $order->product->id,
            'quantity'          => $order->quantity,
            'amount'            => $order->amount,
            'order_items'       => $order_items
        ])->toJSON();
    }
    
    /**
     * availableOrders
     *
     * @param  mixed $request
     * @return void
     */
    public function availableOrders(Request $request) {
        $orders = Order::where('user_id', $request->user_id)
                        ->where('status', 'released')
                        ->select('id', 'user_id')
                        ->get();
                        
        return $orders->toJSON();
    }
    
    /**
     * pendingOrders
     *
     * @param  mixed $request
     * @return void
     */
    public function pendingOrders(Request $request) {
        $orders = Order::where('status', 'pending')
                        ->latest()
                        ->select('id', 'user_id')
                        ->get();
                        
        return $orders->toJSON();
    }

    /**
     * transferItem
     *
     * @param Request $request
     * @return void
     */
    public function transferItem(Request $request) {
        $user_id = $request->user_id ?? null;
        $user = User::find($user_id);
        $order = Order::find($request->order_id);
        $orderItems = $order->items->where('is_activated', false)->where('user_id', $user_id)->values();
        
        // check if user has item to transfer
        if ($orderItems->count() == 0) {
            return \response()->json([
                "status" => "failed",
                "message" => "No items to transfer",
            ], 405);
        }

        $type = $order->product->type;
        $activation_codes_available = ActivationCode::where("type", $type)
                ->where("released_to", $user_id)
                ->where("status", "released")
                ->whereNull("activated_at");


        if ($type == "package") {
            $activation_codes_available = $activation_codes_available->where("product_id", $order->product_id);
        }
        
        $activation_codes_available = $activation_codes_available->get();
        $request->validate([
            "user_id" => ["required"],
            "quantity" => ["required", "numeric", "min:1", "max:" . $activation_codes_available->count()],
            "order_id" => ["required", Rule::exists('orders', 'id')->where(function($q) use ($user_id, $type) {
                if ($type == "product") {
                    $q->where('status', 'received');
                } else {
                    $q->where('status', 'released');
                }
                        })],
            "username" => ["required", Rule::exists('users', 'username')->where(function($q) {
                            $q->where('role_id', 1);
                        }),
                            Rule::notIn([$user->username]) // not self
                        ]
            ], [
                "quantity.max" => "Quantity is invalid, you only have " . $activation_codes_available->count() . " activation code(s) to transfer."
        ]);
        
        
        \DB::transaction(function () use ($request, $orderItems, $activation_codes_available, $order, $user_id) {
            $histories = collect();
            $to = User::whereUsername($request->username)->first();
            for ($i=0; $i < $request->quantity; $i++) { 
                $item = $orderItems->get($i);
                $activation_code = $activation_codes_available->get($i);
                
                $transferHistory = new TransferItem([
                    "order_id" => $order->id,
                    "order_item_id" => $item->id,
                    "product_id" => $order->product_id,
                    "from" => $order->user_id,
                    "to" => $to->id,
                ]);

                $histories->push($transferHistory);

                $item->user_id = $to->id;
                $item->transferred_at = Carbon::now();
                $item->save();

                $activation_code->released_to = $to->id;
                $activation_code->transferred_from = $user_id;
                $activation_code->transferred_at = Carbon::now();
                $activation_code->save();
            }

            $histories->each(function($history) {
                $history->save();
            });

        });

    }
}

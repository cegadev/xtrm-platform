<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Earning;
use App\Models\Package;
use App\Models\Membership;
use Illuminate\Http\Request;
use App\Http\Helpers\Helpers;
use App\Models\ActivationCode;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\MemberActivationCode;

class ActivationCodesController extends Controller
{
    /**
     * validateRegistration
     *
     * @param Request $request
     * @return void
     */
    public function validateRegistration(Request $request) {
        $user_id = $request->user_id;
        $activation_code = $request->activation_code;
        
        $request->validate([
            "user_id" => ["required", Rule::exists('users', 'id')->where(function($q) {
                $q->where('role_id', 1);
            })],
            "activation_code" => ["required", Rule::exists('activation_codes', 'code')->where(function($q) {
                $q->where('status', 'released');
            })]
        ]);
        
        $valid_activation_code = ActivationCode::where("status", "released")->where("code", $activation_code)->first();
            
        if ($valid_activation_code) {
            
            if ($valid_activation_code->activated_at) {
                return response()->json([
                    "status" => "failed",
                    "message" => "Activation code was already activated by another member."
                ], 422);
            }
            
            $user = User::find($user_id);
            
            $this->registerActivationCode2($user, $user->membership, $valid_activation_code);
        }
        
        return response()->json([
            "status" => "success",
            "message" => "Activation Code is valid.",
            "data" => $valid_activation_code,
        ], 200);
    }

    public function registerActivationCode2 ($user, $member, $activation_code) {
        $user_id = $user->id;
        $member_id = $member->id;
        $activation_code_id = $activation_code->id;

        \DB::transaction(function () use ($user_id, $member_id, $activation_code_id, $activation_code) {
            
            MemberActivationCode::create([
                "user_id"               => $user_id,
                "member_id"             => $member_id,
                "activation_code_id"    => $activation_code_id,
            ]);

            $activation_code->activated_at = Carbon::now();
            $activation_code->activated_by = $user_id;
            $activation_code->status = "activated";
            $activation_code->save();
        });

    }

    /**
     * registerActivationCode
     *
     * @param Request $request
     * @return void
     */
    public function registerActivationCode(Request $request) {
        $user_id = $request->user_id;
        $member_id = $request->member_id;
        $activation_code_id = $request->activation_code_id;

        $activation_code = ActivationCode::find($activation_code_id);
        if ($activation_code->activated_at) {
            return response()->json([
                "status" => "failed",
                "message" => "Activation code was already activated by another member."
            ], 422);
        }

        \DB::transaction(function () use ($user_id, $member_id, $activation_code_id, $activation_code) {
            
            MemberActivationCode::create([
                "user_id"               => $user_id,
                "member_id"             => $member_id,
                "activation_code_id"    => $activation_code_id,
            ]);

            $activation_code->activated_at = Carbon::now();
            $activation_code->activated_by = $user_id;
            $activation_code->status = "activated";
            $activation_code->save();
        });

        return response()->json([
            "status" => "success",
            "message" => "Activation code successfully registered to your account.",
        ], 200);
    }

    /**
     * activateItemOfOrder
     *
     * @param Request $request
     * @return void
     */
    public function activateItemOfOrder(Request $request) {
        $order_id = $request->order_id;
        $user_id = $request->user_id;

        $order = Order::find($order_id);
        $user = User::find($user_id);
        $type = $order->product->type;

        $member_activation_codes_available = ActivationCode::where("activated_by", $user_id)
                                                ->where("type", $type)
                                                ->where("status", "activated")
                                                ->whereNull("order_id")
                                                ->first();
        if ($member_activation_codes_available) {
            \DB::transaction(function () use ($order, $member_activation_codes_available, $user) {
                if ($order->product->type == "package") {
                    $order->increment("total_activation_left");
                    $order->save();
                }
                
                $member_activation_codes_available->order_id = $order->id;
                $member_activation_codes_available->registered_at = Carbon::now();
                $member_activation_codes_available->save();

                if ($order->product->type == "product") {
                    // earn unilevel commission
                    $this->activateProduct($user, $order);

                    $total_product_activations = ActivationCode::where("order_id", $order->id)->count();
                    if ($total_product_activations >= $order->quantity) { // activated all
                        $order->status = "completed";
                        $order->completed_at = Carbon::now();
                        $order->save();
                    }
                }
            });

            return response()->json([
                "status" => "success",
                "identity" => "good",
                "message" => "Product successfully Activated. " . ($type == "package" ? "You may now add new member." : "")
            ]);
        } else {
            return response()->json([
                "status" => "failed",
                "identity" => "no-more",
                "message" => "No more available activation codes."
            ], 422);
        }
    }

    private function activateProduct($user, $order) {
        $unilevelLimit = 5;
        $points = 5;
        $monthlyLimitPerTeam = 10;
        $me = $user->membership;

        $ancestors = collect(explode(",", $me->ancestor_ids))->reverse()->values()->take($unilevelLimit);
        $currentMonth = Carbon::now()->format("Y-m");
        $currentYear = Carbon::now()->format("Y");

        if (!$me->isRoot() && $ancestors->count()) {
            \DB::transaction(function () use ($ancestors, $points, $unilevelLimit, $me, $order, $currentMonth, $currentYear, $monthlyLimitPerTeam){
                $canEarn = collect();
                $members = Membership::whereIn("id", $ancestors)
                        ->with('earnings')
                        ->get()->keyBy("id")
                        ->map(function($record, $key) use($me, $currentMonth, $currentYear, $monthlyLimitPerTeam, &$canEarn) {
                            $earnings_this_month_to_this_member = $record->earnings->filter(function($record) use ($me, $currentMonth, $currentYear, $monthlyLimitPerTeam, &$canEarn) {
                                return $record->created_at->format("Y-m") == $currentMonth 
                                        && $record->earned_from_member_id == $me->id 
                                        && $record->type == "unilevel";
                            })->count();


                            $record->current_month_earnings = $record->earnings->filter(function($record) use ($currentMonth) {
                                return $record->created_at->format("Y-m") == $currentMonth;
                            })->sum("amount");
                            
                            $record->current_year_earnings = $record->earnings->filter(function($record) use ($currentYear) {
                                return $record->created_at->format("Y") == $currentYear;
                            })->sum("amount");
                            $canEarn->put($key, $earnings_this_month_to_this_member < $monthlyLimitPerTeam);
                            return $record;
                        });

                /**
                 * If has ancestors, then give unilevel commission
                 */
                // set points
                $ancestors_points = $ancestors->map(function($record, $key) use ($points){
                    return (object)[
                        "ancestor" => $record,
                        "points" => $points - $key
                    ];
                });


                $earnings = collect();
                $ancestors_points->each(function($record) use ($members, &$earnings, $me, $order, $canEarn) {
                    $member = $members->get($record->ancestor);

                    $earning = new Earning;
                    $commission = $record->points;
                    $can_earn = $canEarn->get($member->id) ?? false;
                    if ($can_earn) {
                        $notes = "Earned unilevel commission from member #" . $me->id . ".";
                    } else {
                        $notes = "Can't earn unilevel commission from member #". $me->id . ". Limit reached to earn unilevel commission from this member.";
                        $commission = 0;
                    }

                    $earning->fill([
                        "member_id"                     => $member->id,
                        "type"                          => "unilevel",
                        "amount"                        => $commission,
                        "notes"                         => $notes,
                        "earned_from_member_id"         => $me->id,
                        "product_id"                    => $order->product_id,
                        "order_id"                      => $order->id,
                    ]);
                    $member->current_month_earnings = $member->current_month_earnings + $commission;
                    $member->current_year_earnings = $member->current_year_earnings + $commission;
                    $member->total_earnings = $member->total_earnings + $commission;
                    $earnings->push($earning);
                });

                $earnings->each(function($earning){
                    $earning->save();
                });

                $members->each(function ($member) {
                    $member->available_commissions = Helpers::computeCommission($member);
                    $member->save();
                });

            });
        }
    }

    /**
     * My Codes
     *
     * @param Request $request
     * @return void
     */
    public function myCodes(Request $request) {
        $user_id = $request->user_id;
        $myCodes = ActivationCode::whereStatus('released')->whereReleasedTo($user_id)->get();

        return $myCodes;
    }

    /**
     * Pending Users
     *
     * @param Request $request
     * @return void
     */
    public function pendingUsers(Request $request) {
        $order_items = \DB::table("order_items")
                ->whereNull("activated_at")
                ->select('user_id', 'type', 'product_id')
                ->get();

        $activation_codes = \DB::table("activation_codes")
            ->whereNull("activated_at")
            ->select("released_to as user_id", "type", "product_id")
            ->get();
        
        $packages = Package::whereNull('disabled_at')
            ->where('type', 'package')
            ->select('id', 'name')
            ->get();

        $users = $order_items->pluck("user_id")->unique();
        $users = $users->map(function($user) use ($order_items, $packages, $activation_codes) {
            $user_orders = $order_items->where("user_id", $user);
            $a_codes = $activation_codes->where("user_id", $user);
            $_p = $packages->map(function($p) use ($user_orders, $a_codes) {
                return [
                    "id" => $p->id,
                    "orders" => $user_orders->where("product_id", $p->id)->count(),
                    "released" => $a_codes->where("product_id", $p->id)->count()
                ];
            });
            return [
                "user_id" => $user,
                "product" => [
                    "orders" => $user_orders->where("type", "product")->count(),
                    "released" => $a_codes->where("type", "product")->count(),
                ],
                "packages" => $_p
            ];
        })->keyBy("user_id");

        $order_items = \DB::table("order_items as oi")
                ->whereNull("oi.activated_at")
                ->select('oi.user_id', \DB::raw('count(1) as pending_orders'))
                ->addSelect(\DB::raw('(select count(1) from activation_codes where released_to = oi.user_id and activated_at is null) as total'))
                ->groupBy('oi.user_id');

        $orders = \DB::table(\DB::raw("({$order_items->toSql()}) as sub"))
                        ->whereRaw('pending_orders > total')
                        ->join("users as u", "u.id", "sub.user_id")
                        ->select("u.id", "u.username", \DB::raw("(pending_orders - total) as needed"), "pending_orders", "total")
                        ->get();
        $orders = $orders->map(function($record) use ($users) {
            $record->data = $users->get($record->id);
            return $record;
        });

        return [
            "packages" => $packages,
            "data" => $orders
        ];
    }
}

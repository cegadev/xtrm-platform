<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\EncashmentHistory;
use App\Http\Controllers\Controller;

class EncashmentController extends Controller
{
    /**
     * viewEncashment
     *
     * @param EncashmentHistory $encashmentHistory
     * @return void
     */
    public function viewEncashment(EncashmentHistory $encashmentHistory) {
        $vat = $encashmentHistory->amount * $encashmentHistory->vat_applied;

        $encashmentHistory->reference = "ENC-" . sprintf("%08d", $encashmentHistory->id);
        $encashmentHistory->status = $encashmentHistory->status == "pending" ? "For Approval" : \Str::ucfirst($encashmentHistory->status);
        $encashmentHistory->amount = \number_format($encashmentHistory->amount, 2);
        $encashmentHistory->vat = \number_format($vat, 2);
        $encashmentHistory->service_fee = \number_format($encashmentHistory->service_fee, 2);
        $encashmentHistory->amount_released = \number_format($encashmentHistory->amount_released, 2);
        $encashmentHistory->transaction_type = $encashmentHistory->transaction_type == "cash" ? "Cash" : "Bank transfer";
        return $encashmentHistory;
    }

    public function pendingCount() {
        return [
            "count" => EncashmentHistory::where('status', 'pending')->count()
        ];
    }
}

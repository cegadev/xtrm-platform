<?php 
namespace App\Http\Repository;

use App\Http\Helpers\Helpers;
use App\Models\ActivationCode;

class ActivationCodeRepository 
{
    public function generateCode(string $type, $product_id = null, $quantity = 1, $order_id = null)
    {
        $product_key = "MJP-" . Helpers::generate_license();
        for ($i = 0; $i < $quantity; $i++) {
            $product_code = new ActivationCode([
                "code"          => $product_key,
                "type"          => $type,
                "status"        => "draft",
                "product_id"    => $type == 'package' ? $product_id : null,
                "created_by"    => auth()->id(),
                "order_id"      => $order_id
            ]);

            $product_code->save();
        }
    }
}
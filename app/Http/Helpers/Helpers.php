<?php
namespace App\Http\Helpers;

use Carbon\Carbon;
use App\Models\Earning;
use App\Models\OrderItem;
use App\Models\Membership;
use Illuminate\Support\Facades\Mail;

class Helpers {

    
    /**
     * startsWith
     *
     * @param mixed $string
     * @param mixed $startString
     * @return void
     */
    public static function startsWith($string, $startString) {
        $len = strlen($startString); 
        
        return (substr($string, 0, $len) === $startString); 
    }

    /**
     * endsWith
     *
     * @param mixed $string
     * @param mixed $endString
     * @return void
     */
    public static function endsWith($string, $endString) {
        $len = strlen($endString); 
        if ($len == 0) { 
            return true; 
        } 

        return (substr($string, -$len) === $endString); 
    }

    /**
     * defaultProductImage
     *
     * @return void
     */
    public static function defaultProductImage() {
        return "https://dummyimage.com/600x400/1f1f1f/fff";
    }

    /**
     * generateRating
     *
     * @param mixed $count
     * @return void
     */
    public static function generateRating($count = 5, $withTotal = null, $hasReviews = true) {
        $str = "";
        $cnt = 0;

        for($i = 0; $i < 5; $i++) {
            if ($cnt < $count) {
                $color = "#ffe31a";
            } else {
                $color = "black";
            }

            $str.= "<i class='fa fa-star" . (!$hasReviews ? "-o" : "") . " small' style='color: {$color}'></i>";
            $cnt++;
        }

        if (is_null($withTotal) == false) {

            if ($withTotal == 0) {
                $str .= " <small>No reviews yet</small>";
            } else {
                $strReviews = $withTotal > 1 ? "reviews" : "review";

                $str .= " <small>($withTotal $strReviews)</small>";
            }
        }

        return $str;
    }

    public static function computeCommission ($membership, $update = false) {
        $totalEarnings = $membership->total_earnings;
        $encashedCommissions = $membership->encashment_histories()->where("status", "<>", "declined")->sum("amount");
        $available = $totalEarnings - $encashedCommissions;
        if ($update) {
            $membership->update([
                "available_commissions" => $available
            ]);

                
            return (object) [
                "membership" => $membership,
                "available_commissions" => $available
            ];
        }
        return $available;
    }

    public static function generate_license($suffix = null) {
        // Default tokens contain no "ambiguous" characters: 1,i,0,o
        if(isset($suffix)){
            // Fewer segments if appending suffix
            $num_segments = 3;
            $segment_chars = 6;
        }else{
            $num_segments = 3;
            $segment_chars = 4;
        }
        $tokens = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $license_string = '';
        // Build Default License String
        for ($i = 0; $i < $num_segments; $i++) {
            $segment = '';
            for ($j = 0; $j < $segment_chars; $j++) {
                $segment .= $tokens[rand(0, strlen($tokens)-1)];
            }
            $license_string .= $segment;
            if ($i < ($num_segments - 1)) {
                $license_string .= '';
            }
        }
        // If provided, convert Suffix
        if(isset($suffix)){
            if(is_numeric($suffix)) {   // Userid provided
                $license_string .= '-'.strtoupper(base_convert($suffix,10,36));
            }else{
                $long = sprintf("%u\n", ip2long($suffix),true);
                if($suffix === long2ip($long) ) {
                    $license_string .= '-'.strtoupper(base_convert($long,10,36));
                }else{
                    $license_string .= '-'.strtoupper(str_ireplace(' ','-',$suffix));
                }
            }
        }
        
        return $license_string;
    }

    public static function notifyProductCodes($order) {
        $to = $order->user;
        $data = (object)[
            "name" => $to->first_name,
            "codes" => $order->product_codes->pluck("code"),
            "product_name" => $order->product->name,
            "quantity" => $order->quantity,
            "order_id" => $order->id,
            "total_amount" => $order->amount,
            "activation_url" => route("my-orders.activate.index", $order->id)
        ];

        // Mail::to($to->email)->send(new \App\Mail\ProductActivationCode($data));
    }

    public static function notifyNewOrder($order, $url) {
        // Mail::to("sales@mjpremiumtrading.com")->send(new \App\Mail\NewOrder($order, $url));
    }

    public static function notifyRecievedOrder($order, $url) {
        // Mail::to($order->user->email)->send(new \App\Mail\ReceivedOrder($order, $url));
    } 

    /**
     * createOrderItems
     *
     * @param mixed $order
     * @return void
     */
    public static function createOrderItems($order) {
        $items = collect();
        
        for ($i=0; $i < $order->quantity; $i++) { 
            $items->push(new OrderItem([
                "order_id" => $order->id,
                "product_id" => $order->product_id,
                "type" => $order->product->type,
                "user_id" => $order->user_id,
            ]));
        }

        $items->each(function($item){
            $item->save();
        });
    }

    public static function computeEarnings($member_id) {
        $member = Membership::find($member_id);

        $direct_referral = Earning::where("member_id", $member_id)
                            ->where("type", "direct referral")
                            ->select("amount")
                            ->get()->sum("amount");

        $now = \Carbon\Carbon::now();
        $year = $now->year;
        $lastMonth = $now->firstOfMonth()->subMonth(1)->month;
        $product_activations_last_month_count = $member->user->orderItems()
                                                        ->where("type", "product")
                                                        ->whereNotNull("activated_at")
                                                        ->whereYear("activated_at", "=", $year)
                                                        ->whereMonth("activated_at", "=", $lastMonth)
                                                        ->count();
        $bonus = 0;
        if ($product_activations_last_month_count >= 3) {
            // if has atleast 3 product activation last month
            // add bonus to earnings
            
            $bonus = $bonus + Earning::whereIn("type", ["indirect referral", "unilevel"])
                        ->whereYear("created_at", "=", $year)
                        ->whereMonth("created_at", "=", $lastMonth)
                        ->where('member_id', $member_id)
                        ->sum("amount");
        }
        
        if ($product_activations_last_month_count >= 5) {
            // if has atleast 3 product activation last month
            // add bonus to earnings
            
            $bonus = $bonus + Earning::whereIn("type", ["lic"])
                        ->whereYear("created_at", "=", $year)
                        ->whereMonth("created_at", "=", $lastMonth)
                        ->where('member_id', $member_id)
                        ->sum("amount");
        }
        
        $total_earnings = $direct_referral + $bonus;
        $member->total_earnings = $total_earnings;
        $member->save();
        
        return $total_earnings;
    }

    public static function totalProductActivationThisMonth($member_id) {
        $member = Membership::find($member_id);
        // count total product activation of user this month
        $now = \Carbon\Carbon::now();
        $prod_activated_this_month = OrderItem::whereNotNull("activated_at")
                                    ->where("type", "product")
                                    ->where("activated_by", $member->user_id)
                                    ->whereMonth("activated_at", $now->month)
                                    ->whereYear("activated_at", $now->year)
                                    ->count();

        return $prod_activated_this_month;
    }

    public static function earnReferralCommission($from_id, $swicthToRegular = false) {
        $member = Membership::where("user_id", $from_id)->first();
        
        if ($member->parent_id) {
            $parent = Membership::find($member->parent_id);
            // check if to already earned direct referral commission from member
            // if yes, dont proceed
            $hasEarning = Earning::where("member_id", $parent->id)
                                ->where("type", "direct referral")
                                ->where("earned_from_member_id", $member->id)
                                ->first();
            \DB::transaction(function () use ($member, $swicthToRegular, $parent, $hasEarning) {
                if ($swicthToRegular) {
                    $member->is_free_account = false;
                    $member->save();
                }
                if (!$hasEarning && !$member->is_free_account) {
                    $amount = $member->package->settings->direct_referral_commission;

                    // if parent's package is lower, then earn only  the same amount as parent's package
                    // modify amount only if parent is not top level
                    if (!$parent->top_level) {
                        $parent_package_amount = $parent->package->settings->direct_referral_commission;
                        $amount = $amount > $parent_package_amount ? $parent_package_amount : $amount; 
                    } 

                    $dr_earning = new Earning([
                        "member_id"             => $parent->id,
                        "type"                  => "direct referral",
                        "amount"                => $amount,
                        "notes"                 => "Earned direct referral commision from member " . $member->id . ".",
                        "earned_from_member_id" => $member->id,
                        "order_id"              => $member->order_id, 
                    ]);
                    $dr_earning->save();
                }

                $total_earnings = self::computeEarnings($parent->id);

                $encashments = $parent->encashment_histories()->where("status", "<>", "declined")->sum("amount");
                $parent->total_earnings = $total_earnings;
                $parent->available_commissions = $total_earnings - $encashments;
                $parent->save();

                if (!$parent->top_level && !$member->is_free_account) {
                    // get parent's parent

                    $indirect_parent = Membership::find($parent->parent_id);
                    if ($indirect_parent) {

                        // check if indirect parent already have earnings from member
                        $hasEarning = Earning::where("member_id", $indirect_parent->id)
                                ->where("type", "indirect referral")
                                ->where("earned_from_member_id", $member->id)
                                ->first();

                        if (!$hasEarning) {
                            $dr = $member->package->settings->direct_referral_commission;
                            $bonus = $dr / intval($member->package->settings->indirect_referral_percentage);

                            // if member's package is higher than indirect parent, then the indirect parent will only earn same about his package
                            // except root level
                            if (!$indirect_parent->top_level) {
                                $indirect_parent_package_amount = $indirect_parent->package->settings->direct_referral_commission;
                                if ($dr > $indirect_parent_package_amount) {
                                    $bonus = $indirect_parent_package_amount / intval($indirect_parent->package->settings->direct_referral_commission);
                                }
                            }

                            $indirect_parent_earnings = [
                                "member_id"     => $indirect_parent->id,
                                "type"          => "indirect referral",
                                "amount"        => $bonus ?? 0,
                                "notes"         => "Earned indirect referral commision from member #" . $member->id . ".",
                                "earned_from_member_id" => $member->id,
                                "order_id"      => $member->order_id,
                            ];

                            $indirect_parent_total_product_activation = self::totalProductActivationThisMonth($indirect_parent->id);
                            if ($indirect_parent_total_product_activation >= 3) {
                                $indirect_parent_earnings["valid_at"] = \Carbon\Carbon::now()->firstOfMonth()->addMonths(1);
                                
                                self::validateBonusesThisMonth($indirect_parent->id);
                            }

                            Earning::create($indirect_parent_earnings);

                            
                        }

                        $total_earnings = self::computeEarnings($indirect_parent->id);

                        $encashments = $indirect_parent->encashment_histories()->where("status", "<>", "declined")->sum("amount");
                        $indirect_parent->total_earnings = $total_earnings;
                        $indirect_parent->available_commissions = $total_earnings - $encashments;
                        $indirect_parent->save();
                    }
                }
            });
        }
    }

    public static function validateBonusesThisMonth($member_id) {
        $now = Carbon::now();
        $member = Membership::find($member_id);

        // get all not validated indirect referrals and unilevel this month upon reaching the total number of activated products
        $total_activated = self::totalProductActivationThisMonth($member_id);
        if ($total_activated >= 3) {
            Earning::whereIn("type", ["indirect referral", "unilevel"])
                                ->whereYear("created_at", $now->year)
                                ->whereMonth("created_at", $now->month)
                                ->whereNull("valid_at")
                                ->where("member_id", $member->id)
                                ->update([
                                    "valid_at" => $now->firstOfMonth()->addMonths(1)
                                ]);
        }
    }
}
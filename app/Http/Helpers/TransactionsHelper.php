<?php
namespace App\Http\Helpers;

use App\User;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Earning;
use App\Models\Package;
use App\Models\OrderItem;
use App\Models\Membership;
use App\Models\Transaction;
use App\Models\EncashmentHistory;
use Illuminate\Support\Facades\DB;

class TransactionsHelper {
    /**
     * createTransactionRecord
     *
     * @param Membership $member
     * @param float $amount
     * @param string $type
     * @param int $resource_id
     * @param User $transactedBy
     * @return void
     */
    public static function createTransactionRecord(Membership $member, float $amount, $type = "earnings", $resource_id, User $transactedBy) {

        // get member current wallet balance
        $balance = $member->eWallet();

        $transaction = new Transaction;
        $transaction->member_id     = $member->id;
        $transaction->user_id       = $member->user_id;

        if ($type == "earnings") {
            $transaction->debit     = $amount;
            $transaction->balance   = $balance + $amount;
        } else {
            $transaction->credit    = $amount;
            $transaction->balance   = $balance - $amount;
        }

        $transaction->transacted_by = $transactedBy->id;
        $transaction->resource_id   = $resource_id;
        $transaction->type          = $type;
        $transaction->save();

        $member->available_commissions = $transaction->balance;
        $member->save();

        if ($type == "earnings") {
            $earning = Earning::find($resource_id);
            if ($earning->type == "direct referral") {
                $earning->added_to_wallet_at = Carbon::now();
                $earning->save();
            }
        }
    }

    /**
     * Refund encashment transaction
     *
     * @param Membership $member
     * @param EncashmentHistory $encashment
     * @param User $transactedBy
     * @return void
     */
    public static function refundEncashmentTransaction(Membership $member, EncashmentHistory $encashment, User $transactedBy) {
        $balance = $member->eWallet();

        $transaction = Transaction::where("member_id", $member->id)
                                ->where("type", "encashment")
                                ->where("resource_id", $encashment->id)
                                ->first();

        $new_transaction = new Transaction([
            "member_id" => $member->id,
            "user_id" => $member->user_id,
            "type" => "encashment",
            "debit" => $transaction->credit,
            "credit" => 0,
            "balance" => $balance + $transaction->credit,
            "transacted_by" => $transactedBy->id,
            "resource_id" => $encashment->id,
        ]);

        $new_transaction->save();

        $member->available_commissions = $new_transaction->balance;
        $member->save();

    }

    /**
     * Get direct referral commission amount
     *
     * @param Membership $member
     * @param Membership $earnedFromMember
     * @param Package $activatedPackage
     * @return integer|array
     */
    private static function getDirectReferralCommissionAmount(Membership $member, Membership $earnedFromMember, Package $activatedPackage, $withIndirectPercentage = false)
    {
        // check if new member's package is greater than member
        $memberIsGreater = $member->isRoot() && is_null($member->product_id) ? false : ($activatedPackage->settings->direct_referral_commission > $member->package->settings->direct_referral_commission);
        $direct_referral = $activatedPackage->settings->direct_referral_commission;

        // if new member's package greater than member's package, then let the member earn the same amount as his own package
        if ($memberIsGreater) {
            $direct_referral = $member->package->settings->direct_referral_commission;
        }

        if ($withIndirectPercentage) {
            return [
                $direct_referral,
                $member->isRoot() ? 10 : $member->package->settings->indirect_referral_percentage
            ];
        }

        return $direct_referral;
    }

    /**
     * Get total product activation this month
     *
     * @param Membership $member
     * @return integer
     */
    public static function getTotalProductActivationThisMonth(Membership $member) : int
    {
        // count total product activation of user this month
        $now = Carbon::now();
        $prod_activated_this_month = OrderItem::whereNotNull("activated_at")
                                    ->where("type", "product")
                                    ->where("activated_by", $member->user_id)
                                    ->whereMonth("activated_at", $now->month)
                                    ->whereYear("activated_at", $now->year)
                                    ->count();

        return $prod_activated_this_month;
    }
    /**
     * Get total product activation this month
     *
     * @param Membership $member
     * @return integer
     */
    public static function getTotalProductActivationLastMonth(Membership $member) : int
    {
        // count total product activation of user this month
        $now = Carbon::now()->subMonths(1);
        $prod_activated_last_month = OrderItem::whereNotNull("activated_at")
                                    ->where("type", "product")
                                    ->where("activated_by", $member->user_id)
                                    ->whereMonth("activated_at", $now->month)
                                    ->whereYear("activated_at", $now->year)
                                    ->count();

        return $prod_activated_last_month;
    }

    /**
     * Get total earnings
     *
     * @param Membership $member
     * @return integer
     */
    public static function getTotalEarnings(Membership $member): int
    {
        $zeroResource = Transaction::where("member_id", $member->id)
                                    ->where('type', 'earnings')
                                    ->where("resource_id", 0)
                                    ->get();
        $zeroResource = $zeroResource->sum('credit');

        $total_earnings = Transaction::where("member_id", $member->id)
                                        ->where('type', 'earnings')
                                        ->selectRaw("sum(debit) as amount")
                                        ->where('resource_id', '<>', 0)
                                        ->first()->amount ?? 0;

        return $total_earnings - $zeroResource;
    }

    /**
     * Earn Direct referral
     *
     * @param Membership $member
     * @param Membership $earnedFromMember
     * @param Package $activatedPackage
     * @param Order|null $order
     * @param int $transactedBy
     * @return void
     */
    public static function earnDirectReferral(Membership $member, Membership $earnedFromMember, Package $activatedPackage, $order = null, $transactedBy) {
        $direct_referral = self::getDirectReferralCommissionAmount($member, $earnedFromMember, $activatedPackage);

        $earning = Earning::updateOrCreate([
            "member_id"             => $member->id,
            "type"                  => "direct referral",
            "earned_from_member_id" => $earnedFromMember->id
        ],[
            "order_id"              => !is_null($order) ? $order->id : null,
            "amount"                => $direct_referral,
            "notes"                 => "Earned direct referral commission from member #" . $earnedFromMember->id . ".",
            "valid_at"              => Carbon::now()
        ]);

        $transactedBy = User::find($transactedBy);

        self::createTransactionRecord($member, $direct_referral, "earnings", $earning->id, $transactedBy);

        $earning->update([
            "added_to_wallet_at" => Carbon::now(),
        ]);

        $member->total_earnings = self::getTotalEarnings($member);
        $member->save();

        return $direct_referral;
    }

    /**
     * Earn Indirect referral bonus
     *
     * @param Membership $member
     * @param Membership $earnedFromMember
     * @param Package $activatedPackage
     * @param Order|null $order
     * @return void
     */
    public static function irbBonus(Membership $member, Membership $earnedFromMember, Package $activatedPackage, $order = null, $directCommission = 100) {
        list($direct_referral, $irb_percentage) = self::getDirectReferralCommissionAmount($member, $earnedFromMember, $activatedPackage, true);

        $indirect_referral_bonus = $directCommission / $irb_percentage;

        $limit_per_day = self::getMemberIrbLimit($member, "per_day") ?? 0;
        $limit_per_month = self::getMemberIrbLimit($member, "per_month") ?? 0;
        $this_day_earnings = self::earningsTotalPerDay("irb", $member->id);
        $this_month_earnings = self::earningsTotalPerMonth("irb", $member->id);

        if ($limit_per_month > 0 && $this_month_earnings >= $limit_per_month) { // if has monthly limit, check if exceeds already
            $indirect_referral_bonus = 0;
            $notes = "Limit reached to earn indirect referral bonus this month.";
        } else {
            if ($limit_per_day > 0 && $this_day_earnings >= $limit_per_day) { // if has limit per day, check if exceeds already
                $indirect_referral_bonus = 0;
                $notes = "Limit reached to earn indirect referral bonus this day.";
            } else {
                if ($limit_per_day > 0 && ($this_day_earnings + $indirect_referral_bonus) > $limit_per_day) {
                    $indirect_referral_bonus = $limit_per_day - $this_day_earnings;
                    $notes = "Earned indirect referral bonus from member #" . $earnedFromMember->id . ". Amount adjusted to not exceed limit per day (" . number_format($limit_per_day, 2) . ") .";
                } else {
                    $notes = "Earned indirect referral bonus from member #" . $earnedFromMember->id . ".";
                }
            }
        }

        Earning::updateOrCreate([
            "member_id"             => $member->id,
            "type"                  => "indirect referral",
            "earned_from_member_id" => $earnedFromMember->id
        ],[
            "order_id"              => !is_null($order) ? $order->id : null,
            "amount"                => $indirect_referral_bonus,
            "notes"                 => $notes,
        ]);

        $memberTotalProductActivations = self::getTotalProductActivationThisMonth($member);
        $now = Carbon::now();
        $today = Carbon::now();
        $valid_at = $now->firstOfMonth()->addMonths(1);

        if ($memberTotalProductActivations >= 5) {
            Earning::where("member_id", $member->id)
                    ->whereYear("created_at", $today->year)
                    ->whereMonth("created_at", $today->month)
                    ->whereIn("type", ["indirect referral", "unilevel"])
                    ->whereNull("valid_at")
                    ->update([
                        "valid_at" => $valid_at
                    ]);
        }
    }

    public static function unilevelBonus(Membership $member, Membership $earnedFromMember, $amount, $order, $transactedBy) {
        $now = Carbon::now();
        $notes = "Earned unilevel commission from member #" . $earnedFromMember->id . ".";

        Earning::create([
            "member_id"             => $member->id,
            "type"                  => "unilevel",
            "amount"                => $amount,
            "notes"                 => $notes,
            "earned_from_member_id" => $earnedFromMember->id,
            "product_id"            => $order->product_id,
            "order_id"              => $order->id,
        ]);

        $memberTotalProductActivations = self::getTotalProductActivationThisMonth($member);

        $today = Carbon::now();
        $valid_at = $now->firstOfMonth()->addMonths(1);
        if ($memberTotalProductActivations >= 5) {
            Earning::where("member_id", $member->id)
                    ->whereYear("created_at", $today->year)
                    ->whereMonth("created_at", $today->month)
                    ->whereIn("type", ["indirect referral", "unilevel"])
                    ->whereNull("valid_at")
                    ->update([
                        "valid_at" => $valid_at
                    ]);

        }
    }

    /**
     * Prepare lic bonus calculation
     *
     * @param Membership $member
     * @return Collection
     */
    public static function initializeLicBonus(Membership $member) {
        $parents = $member->ancestors->take(10)->reverse()->values();
        $defaultLevels = collect([
            [
                'level' => [1],
                'percentage' => 10
            ],
            [
                'level' => range(2,3),
                'percentage' => 3
            ],
            [
                'level' => range(4,5),
                'percentage' => 2
            ],
            [
                'level' => range(6,10),
                'percentage' => 1
            ],
        ])->flatMap(function ($record) {
            return collect($record['level'])->map(function ($item) use ($record) {
                return (object) [
                    'level'     => $item,
                    'percent'   => $record['percentage']
                ];
            });
        });

        $records = $parents->map(function ($record, $index) use ($defaultLevels) {
            if (is_null($record->product_id)) {
                $enabled = true;

                $level = $defaultLevels->firstWhere('level', $index + 1);
                $percent = $level->percent;
            } else {
                $enabled = $record->package->enabled_lic && !is_null($record->package->settings->leadership_indirect_commission_percentage);
                $percent = null;

                if ($enabled) {
                    $level = collect(json_decode($record->package->settings->leadership_indirect_commission_percentage))->firstWhere('level', $index + 1);
                    $percent = $level->percent;
                }
            }


            return (object) [
                'member'         => $record,
                'enabled_lic'    => $enabled,
                'percent'        => $percent
            ];
        });

        // remove first member to prevent double indirect earning
        $records->shift();

        return $records->all();
    }


    /**
     * Earn Indirect referral bonus
     *
     * @param Membership $member
     * @param Membership $earnedFromMember
     * @param Package $activatedPackage
     * @param Order|null $order
     * @return void
     */
    public static function licBonus($enabled, Membership $member, Membership $earnedFromMember, Package $activatedPackage, $percentage, $order) {
        if (!$enabled) {
            return false;
        }
        $notes = "Earned leadership indirect commission from member #" . $earnedFromMember->id . ".";
        $direct_referral = self::getDirectReferralCommissionAmount($member, $earnedFromMember, $activatedPackage);
        $lic_bonus = (intval($percentage) / 100) * $direct_referral;

        $limit_per_day = self::getMemberLicLimit($member, "per_day") ?? 0;
        $limit_per_month = self::getMemberLicLimit($member, "per_month") ?? 0;
        $this_day_earnings = self::earningsTotalPerDay("lic", $member->id);
        $this_month_earnings = self::earningsTotalPerMonth("lic", $member->id);

        if ($limit_per_month > 0 && $this_month_earnings >= $limit_per_month) { // if has monthly limit, check if exceeds already
            $lic_bonus = 0;
            $notes = "Limit reached to earn leadership indirect commission this month.";
        } else {
            if ($limit_per_day > 0 && $this_day_earnings >= $limit_per_day) { // if has limit per day, check if exceeds already
                $lic_bonus = 0;
                $notes = "Limit reached to earn leadership indirect commission bonus this day.";
            } else {
                if ($limit_per_day > 0 && ($this_day_earnings + $lic_bonus) > $limit_per_day) {
                    $lic_bonus = $limit_per_day - $this_day_earnings;
                    $notes = "Earned leadership indirect commission from member #" . $earnedFromMember->id . ". Amount adjusted to not exceed limit per day (" . number_format($limit_per_day, 2) . ") .";
                } else {
                    $notes = "Earned leadership indirect commission from member #" . $earnedFromMember->id . ".";
                }
            }
        }

        $earning = Earning::updateOrCreate([
            "member_id"             => $member->id,
            "type"                  => "lic",
            "earned_from_member_id" => $earnedFromMember->id
        ],[
            "order_id"              => !is_null($order) ? $order->id : null,
            "amount"                => $lic_bonus,
            "notes"                 => $notes,
        ]);

        $memberTotalProductActivations = self::getTotalProductActivationThisMonth($member);
        $now = Carbon::now();
        $today = Carbon::now();
        $valid_at = $now->firstOfMonth()->addMonths(1);

        if ($memberTotalProductActivations >= 5) {
            Earning::where("member_id", $member->id)
                    ->whereYear("created_at", $today->year)
                    ->whereMonth("created_at", $today->month)
                    ->whereIn("type", ["lic"])
                    ->whereNull("valid_at")
                    ->update([
                        "valid_at" => $valid_at
                    ]);
        }

        return $earning;
    }

    /**
     * Get earnings total this day
     *
     * @param string $type
     * @param [type] $member_id
     * @return void
     */
    public static function earningsTotalPerDay($type ="irb", $member_id){
        $today = Carbon::today();
        $earnings = Earning::whereDate("created_at", $today)
                        ->where("member_id", $member_id);
        switch($type) {
            case "irb":
                return $earnings->where("type", "indirect referral")->sum("amount");
                break;
            case "unilevel":
                return $earnings->where("type", "unilevel")->sum("amount");
                break;
            case "lic":
                return $earnings->where("type", "lic")->sum("amount");
                break;
            default:
                return 0;
                break;
        }
    }

    /**
     * Get earnings total this month
     *
     * @param string $type
     * @param [type] $member_id
     * @return void
     */
    public static function earningsTotalPerMonth($type ="irb", $member_id){
        $now = Carbon::now();
        $earnings = Earning::whereMonth("created_at", $now->month)
                        ->whereYear("created_at", $now->year)
                        ->where("member_id", $member_id);
        switch($type) {
            case "irb":
                return $earnings->where("type", "indirect referral")->sum("amount");
                break;
            case "unilevel":
                return $earnings->where("type", "unilevel")->sum("amount");
                break;
            default:
                return 0;
                break;
        }
    }

    public static function getMemberIrbLimit(Membership $member, $type = "per_day") {
        if ($member->isRoot()) {
            return 0; // no limit
        }

        $package = $member->package->settings;

        if ($type == "per_day") {
            return $package->irb_limit_per_day;
        } else {
            return $package->irb_limit_per_month;
        }
    }

    public static function getMemberLicLimit(Membership $member, $type = 'per_day') {
        if ($member->isRoot()) {
            return 0; // no limit
        }

        $package = $member->package->settings;

        if ($type == "per_day") {
            return $package->lic_limit_per_day;
        } else {
            return $package->lic_limit_per_month;
        }
    }

    /**
     * Claim bonus
     *
     * @param Membership $membership
     * @return void
     */
    public static function claimBonus(Membership $membership, $transactedBy = 2) {
        // check if already claimed this month
        $now = Carbon::now();

        // check if member has valid bonuses to claim that is not claimed yet
        $secondDayOfThisMonth = $now->firstOfMonth()->addDays(1)->toDateString();
        $not_claimed_bonuses = Earning::where("member_id", $membership->id)
                                ->whereIn("type", ["indirect referral", "unilevel", "lic"])
                                ->whereNull("added_to_wallet_at")
                                ->whereDate("valid_at", "<", $secondDayOfThisMonth)
                                ->get();

        if ($not_claimed_bonuses->count()) {

            $earnings = collect();
            $bonuses = collect();
            $balance = $membership->eWallet();
            $not_claimed_bonuses->each(function($bonus) use (&$earnings, $membership, &$balance, &$bonuses, $now, $transactedBy) {
                $balance = $balance + $bonus->amount;
                $transaction = new Transaction;
                $transaction->fill([
                    "user_id"       => $membership->user_id,
                    "member_id"     => $membership->id,
                    "debit"         => $bonus->amount,
                    "credit"        => 0,
                    "balance"       => $balance,
                    "type"          => "earnings",
                    "resource_id"   => $bonus->id,
                    "transacted_by" => auth()->check() ? auth()->id() : $transactedBy,
                ]);

                $earnings->push($transaction);

                $bonuses->push($bonus->fill([
                    "added_to_wallet_at" => $now
                ]));
            });

            \DB::transaction(function () use ($earnings, $bonuses, $membership, $balance, $not_claimed_bonuses){
                $earnings->each(function($earning) {
                    $earning->save();
                });

                $bonuses->each(function($bonus) {
                    $bonus->save();
                });

                $membership->total_earnings = self::getTotalEarnings($membership);
                $membership->available_commissions = $balance;
                $membership->prev_month_bonus_claimed_amount = $not_claimed_bonuses->sum("amount");
                $membership->prev_month_bonus_claimed_at = Carbon::now();
                $membership->save();
            });
        }
    }
}

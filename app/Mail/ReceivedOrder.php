<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReceivedOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $url;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $url)
    {
        $this->order = $order;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->subject('MJPremium Trading - Received Order Confirmation')
        ->view('mail.received-order');
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductActivationCode extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $codes;
    public $product_name;
    public $quantity;
    public $order_id;
    public $total_amount;
    public $url;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->name = $data->name;
        $this->codes = $data->codes;
        $this->product_name = $data->product_name;
        $this->quantity = $data->quantity;
        $this->order_id = $data->order_id;
        $this->total_amount = $data->total_amount;
        $this->url = $data->activation_url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('noreply@mjpremiumtrading.com')
            ->subject('MJPremium Trading - Order #'. $this->order_id .' Product Activation Codes')
            ->view('mail.product-activation-codes');
    }
}

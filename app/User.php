<?php

namespace App;

use App\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    const __MEMBER__ = 'member';
    const __MANAGER__ = 'manager';
    const __ADMIN__ = 'admin';

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'middle_name', 
        'last_name', 
        'role_id', 
        'username', 
        'email', 
        'mobile_number',
        'password',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    

    // /**
    //  * Get role
    //  *
    //  * @return void
    //  */
    // public function role () {
    //     return $this->belongsTo(Role::class);
    // }

    /**
     * isMember
     *
     * @return boolean
     */
    public function isMember () {
        return $this->roles->first()->identity == self::__MEMBER__;
    }
    
    /**
     * isManager
     *
     * @return boolean
     */
    public function isManager () {
        return $this->roles->first()->identity == self::__MANAGER__;
    }
    
    /**
     * isAdmin
     *
     * @return boolean
     */
    public function isAdmin () {
        return $this->roles->first()->identity == self::__ADMIN__;
    }
    
    /**
     * membership
     *
     * @return void
     */
    public function membership () {
        return $this->hasOne(\App\Models\Membership::class, "user_id");
    }
    
    public function managedBranch() {
        return $this->hasOneThrough(Models\Branch::class, Models\BranchManager::class, "user_id", "id", "id", "branch_id");
    }

    public function orderItems() {
        return $this->hasMany(Models\OrderItem::class, "user_id");
    }

    public function activatedProducts()
    {
        return $this->hasMany(Models\OrderItem::class)->where('type', 'product')->whereNotNull('activated_at');
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $prod_public = "/home2/vpliilmy/public_html/mjpremiumtrading";
        
        if (file_exists($prod_public)) {
            $this->app->bind('path.public', function() use ($prod_public) {
                return $prod_public;
            });
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

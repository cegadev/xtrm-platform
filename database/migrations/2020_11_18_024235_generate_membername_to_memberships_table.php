<?php

use App\Models\Membership;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenerateMembernameToMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $members = Membership::with('user')->get();

        DB::transaction(function () use ($members) {
            $members->each(function($member) {
                $user = $member->user;

                $name = $user->first_name . " " . $user->last_name;

                $member->member_name = $name;
                $member->save();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Membership::whereRaw("1 = 1")->update(["member_name" => null]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIndirectEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $to_update = DB::query()->fromSub(function($query) {
            $query->from("earnings as e")
            ->where("e.type", 'indirect referral')
            ->select("id", "amount", "member_id")
            ->selectRaw("(Select (amount / 10) FROM earnings WHERE TYPE = 'direct referral' AND earned_from_member_id = e.earned_from_member_id) as should_be");
        }, 'tbl_data')
        ->whereRaw('amount <> should_be')
        ->get();

        DB::transaction(function () use ($to_update) {
            $to_update->each(function($data) {
                DB::table("earnings")
                    ->whereId($data->id)
                    ->update([
                        "amount" => $data->should_be,
                    ]);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

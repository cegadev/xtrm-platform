<?php

use App\User;
use App\Models\Membership;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SwitchRegularMemberJcDeleon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::whereUsername("jcdeleon")->first();

        $member = Membership::where("user_id", $user->id)->first();
        Helpers::earnReferralCommission($user->id, true);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

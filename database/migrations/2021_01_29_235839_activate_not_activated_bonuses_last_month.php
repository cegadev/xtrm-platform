<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivateNotActivatedBonusesLastMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $lastMonth = \Carbon\Carbon::now()->subMonths(1);

        $product_activations = \App\Models\OrderItem::whereMonth('activated_at', $lastMonth->month)
        ->whereYear('activated_at', $lastMonth->year)
        ->where('type', 'product')
        ->select('user_id', \DB::raw('count(1) as total_activated'))
        ->groupBy('user_id')
        ->get()
        ->keyBy("user_id")->map(function($record) {
            return $record->total_activated;
        });



        $users = \App\Models\Membership::withCount(['earnings' => function($q) {
            $q->whereIn('type', ['unilevel', 'indirect referral'])
                ->whereNull('valid_at');
        }])->get();

        $ids = $users
        ->where('earnings_count', '>', 0)
        ->map(function($record) use ($product_activations) {
            return (object)[
                "member" => $record->member_name,
                "user_id" => $record->user_id,
                "id" => $record->id,
                "earning_count" => $record->earnings_count,
                "product_activations" => $product_activations->get($record->user_id) ?? 0
            ];
        })
        ->where('product_activations', '>', 0)
        ->values()
        ->pluck("id");

        $firstThisMonth = \Carbon\Carbon::now()->firstOfMonth()->toDateTimeString();
        $earnings = \App\Models\Earning::whereIn("member_id", $ids)
            ->whereMonth("created_at", $lastMonth->month)
            ->whereYear("created_at", $lastMonth->year)
            ->whereNull("valid_at")
            ->update([
                "valid_at" => $firstThisMonth
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersAddFirstMiddleLastName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("middle_name")->after("name")->nullable();
            $table->string("last_name")->after("middle_name")->nullable();
            $table->renameColumn("name", "first_name");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("middle_name");
            $table->dropColumn("last_name");
            $table->renameColumn("first_name", "name");
        });
    }
}

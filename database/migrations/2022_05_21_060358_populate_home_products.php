<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateHomeProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'field' => 'home-block-1',
                'data' => json_encode([
                    'title' => "Our products",
                    "description" => "<p>Extreme Care Products are collection of Premium Quality Shine Protector and Powerful Cleaning Formula for Home and Industrial Use.</p><p>Guaranteed 100% customer satisfaction.</p>",
                    "image" => '/images/products.jpg'
                ]),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ],
            [
                'field' => 'home-block-2',
                'data' => json_encode([
                    'title' => "For Shiny, Mirror-like Finish",
                    "description" => "",
                    "images" => [
                        "/images/the-products-1.png",
                        "/images/the-products-2.png",
                    ]
                ]),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ],
            [
                'field' => 'home-block-3',
                'data' => json_encode([
                    'title' => "For Cleaning & Protection, New-like Finish",
                    "description" => "",
                    "images" => [
                        "/images/the-products-3.png",
                        "/images/the-products-4.png",
                    ]
                ]),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ],
        ];

        app()->db->table('custom_fields')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        app()->db->table('custom_fields')->whereIn('field', ['home-block-1', 'home-block-2', 'home-block-3'])->delete();
    }
}

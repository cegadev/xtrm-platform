<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnclaimBonusOfMemberId100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // claim correct bonuses 
        $now = \Carbon\Carbon::now();

        $lastMonthBonuses = \App\Models\Earning::where("member_id", 100)
        ->whereDate("valid_at", $now->firstOfMonth()->toDateString())
        ->get();


        $member = \App\Models\Membership::find(100);
        $memberLatestBalance = $member->eWallet();

        $transactions = collect();
        $earningsUpdate = collect();
        $balance = $memberLatestBalance;
        $lastMonthBonuses->each(function($record) use (&$balance, $member, &$transactions, &$earningsUpdate) {
            $balance = $balance + $record->amount;

            $transaction = new \App\Models\Transaction;
            $transaction->fill([
                "user_id" => $member->user_id,
                "member_id" => $member->id,
                "debit" => $record->amount,
                "credit" => 0,
                "balance" => $balance,
                "type" => "earnings",
                "resource_id" => $record->id,
                "transacted_by" => 2,
            ]);

            $transactions->push($transaction);

            $earningsUpdate->push($record->fill([
                "added_to_wallet_at" => \Carbon\Carbon::now()->toDateTimeString()
            ]));
        });


        \DB::transaction(function () use ($transactions, $earningsUpdate) {
            $transactions->each(function($transaction) {
                $transaction->save();
            });

            $earningsUpdate->each(function($earning) {
                $earning->save();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

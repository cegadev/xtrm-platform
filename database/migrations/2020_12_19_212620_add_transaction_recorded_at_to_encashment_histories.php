<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionRecordedAtToEncashmentHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('encashment_histories', function (Blueprint $table) {
            $table->timestamp("transaction_recorded_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('encashment_histories', function (Blueprint $table) {
            $table->dropColumn("transaction_recorded_at");
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyMembershipsAddNestedset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::disableForeignKeyConstraints();
        Schema::table('memberships', function (Blueprint $table) {
            $table->dropForeign("memberships_parent_id_foreign");
            $table->dropColumn("parent_id");
        });
        Schema::enableForeignKeyConstraints();
        
        Schema::table('memberships', function (Blueprint $table) {
            $table->nestedSet();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('memberships', function (Blueprint $table) {
            $table->dropNestedSet();
        });
        
        Schema::table('memberships', function (Blueprint $table) {
            $table->unsignedBigInteger("parent_id");
            $table->foreign("parent_id")->references("id")->on("users");
        });
    }
}

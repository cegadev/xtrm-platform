<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEncashmentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encashment_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger("membership_id");
            $table->enum("transaction_type", ["paymaya", "gcash"]);
            $table->float("amount")->default(0);
            $table->float("vat_applied")->default(0);
            $table->float("amount_released")->default(0);
            $table->float("service_fee")->default(0);
            $table->enum("status", ["pending", "complete", "declined"]);
            $table->timestamp("requested_at")->nullable();
            $table->timestamp("approved_at")->nullable();
            $table->timestamp("declined_at")->nullable();
            $table->unsignedInteger("requested_by")->nullable();
            $table->unsignedInteger("approved_by")->nullable();
            $table->unsignedInteger("declined_by")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encashment_histories');
    }
}

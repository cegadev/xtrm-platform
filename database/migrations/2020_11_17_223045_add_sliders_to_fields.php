<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlidersToFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [

                "field" => "slider1",
                "data" => json_encode([
                    "title" => "Slider 1",
                    "sub_title" => "Slider 1 Subtitle",
                    "action" => "#",
                    "image" => "/images/slides/slide1.jpg",
                ])
            ],
            [
                "field" => "slider2",
                "data" => json_encode([
                    "title" => "Slider 2",
                    "sub_title" => "Slider 2 Subtitle",
                    "action" => "#",
                    "image" => "/images/slides/slide2.jpg",
                ])
            ],
            [
                "field" => "slider3",
                "data" => json_encode([
                    "title" => "Slider 3",
                    "sub_title" => "Slider 3 Subtitle",
                    "action" => "#",
                    "image" => "/images/slides/slide3.jpg",
                ])
            ],
        ];

        DB::table("custom_fields")->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table("custom_fields")->whereIn("field", ["slider1", "slider2", "slider3"])->delete();
    }
}

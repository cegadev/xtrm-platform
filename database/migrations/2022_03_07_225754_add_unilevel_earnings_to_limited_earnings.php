<?php

use App\Models\Transaction;
use Illuminate\Support\Facades\Schema;
use App\Http\Helpers\TransactionsHelper;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnilevelEarningsToLimitedEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();


        try {
            $earnings = App\Models\Earning::where('type', 'unilevel')->where('amount', 0)->orderBy('id')->get(); 
            $earningIds = $earnings->pluck('id')->toArray();

            $memberIds = $earnings->pluck('member_id')->unique()->toArray();
            $willEarn = App\Models\Membership::whereIn('id', $memberIds)->get();

            $earnFrom = App\Models\Membership::whereIn('id', $earnings->pluck('earned_from_member_id')->unique()->toArray())->with('ancestors')->get()
            ->keyBy('id')
            ->map(function ($record) {
                return $record->ancestors->pluck('id')->reverse()->values()->take(5)->toArray();
            });

            $earnings->each(function ($earning) use ($willEarn, $earnFrom) {
                $member = $earning->member_id;
                $from   = $earning->earned_from_member_id;

                $parentKey = array_search($member, $earnFrom[$from]);

                $earning->amount  = 5 - $parentKey;
                $earning->notes   = "Earned unilevel commission from member #{$from}.";
                $earning->save();
            });


            unset($memberIds);
            
            // get updated earnings to be transacted
            $earnings = App\Models\Earning::whereIn('id', $earningIds)->whereNotNull('valid_at')->whereNotNull('added_to_wallet_at')->get();

            $memberIds = $earnings->pluck('member_id')->unique()->toArray();
            
            // remove wrong transactions
            App\Models\Transaction::where('type', 'earnings')->whereIn('resource_id', $earnings->pluck('id')->toArray())->delete();
            
            $membersLatestBalance = $willEarn->keyBy('id')->map(function ($record) {
                return $record->eWallet();
            });
            $now = Carbon\Carbon::now();

            foreach ($earnings as $key => $earning)
            {
                $addedAmount = $earning->amount;
                $memberId    = $earning->member_id;
                
                $newBalance = $membersLatestBalance[$memberId] + $addedAmount;

                $membersLatestBalance[$memberId] = $newBalance;
                
                $transaction = new Transaction([
                    'user_id'   => $earning->membership->user_id,
                    'member_id' => $memberId,
                    'debit'     => $addedAmount,
                    'credit'    => 0,
                    'balance'   => $newBalance,
                    'type'      => 'earnings',
                    'resource_id' => $earning->id,
                    'created_at' => $now,
                    'updated_at' => $now
                ]);

                $transaction->save();
            }
        
            // member earnings update (2nd migration)
            $memberEarnings = App\Models\Membership::whereIn('id', $memberIds)->with('earnings')->get();

            $memberEarnings->map(function ($member) use ($now) {
                $earnings = $member->earnings;

                $member->current_month_earnings = $earnings->filter(function ($record) use ($now) {
                    return $record->created_at->format('Y-m') == $now->format('Y-m');
                })->sum('amount');

                $member->current_year_earnings = $earnings->filter(function ($record) use ($now) {
                    return $record->created_at->format('Y') == $now->format('Y');
                })->sum('amount');

                $member->total_earnings = TransactionsHelper::getTotalEarnings($member);
                $member->available_commissions = $member->eWallet();
                $member->save();
            });

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}

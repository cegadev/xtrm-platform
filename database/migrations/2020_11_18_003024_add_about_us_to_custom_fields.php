<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAboutUsToCustomFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                "field" => "about-us",
                "data" => '<div><div class="title">The Company</div><ul class="mt-2"><li>Filipino-Owned Business.</li><li>Started Operating on July 2020.</li><li>Focuses on distributing Extreme Care Products.</li><li>First-ever Car Care products in MLM Industry.</li><li>With Over 17 Stockist (7 City Distributors and 10 Sub-Dealers) in first 2-months of operation, and still counting.</li><li>With Increasing numbers of resellers and distributors nationwide.</li></ul></div>'
            ],
            [
                "field" => "about-us-right-image",
                "data" => '/images/satisfaction.png'
            ],
        ];

        DB::table('custom_fields')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('custom_fields')->whereIn('field', ['about-us', 'about-us-right-image'])->delete();
    }
}

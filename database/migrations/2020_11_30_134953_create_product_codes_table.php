<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("code");
            $table->unsignedBigInteger("order_id");
            $table->unsignedBigInteger("product_id");
            $table->boolean("is_activated")->default(false);
            $table->timestamp("activated_at")->nullable();
            $table->unsignedBigInteger("activated_by")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_codes');
    }
}

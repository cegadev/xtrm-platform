<?php

use App\Models\Membership;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComputeAvailableCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $memberships = Membership::all();
        DB::transaction(function () use ($memberships) {
            foreach($memberships as $member) {
                $commission = Helpers::computeCommission($member);
                $member->update([
                    "available_commissions" => $commission
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

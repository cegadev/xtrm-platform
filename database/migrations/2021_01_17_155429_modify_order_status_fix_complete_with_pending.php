<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyOrderStatusFixCompleteWithPending extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ordersWithIncompleteItems = \App\Models\Order::where("status", "completed")->whereHas("items", function($q) {
            $q->where("is_activated", false);
        })
        ->update([
            "status" => "released",
            "completed_at" => null
        ]);

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMembersPermissionToManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $managerRole = Role::where("name", "Manager")->first();
        $permissions = Permission::where("name", "like", "%members%")->get();

        DB::transaction(function () use($managerRole, $permissions) {
            $permissions->each(function($permission) use($managerRole) {
                $managerRole->givePermissionTo($permission);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $managerRole = Role::where("name", "Manager")->first();
        $permissions = Permission::where("name", "like", "%members%")->get();

        DB::transaction(function () use($managerRole, $permissions) {
            $permissions->each(function($permission) use($managerRole) {
                $managerRole->revokePermissionTo($permission);
            });
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferActivationCodesPreviousTransfers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $transferred_items = \App\Models\TransferItem::whereDate("transferred_at", "<", \Carbon\Carbon::parse("2020-12-18"))->with('product')->get();

        $from = $transferred_items->pluck("from")->unique()->values();
        $activation_codes_available = \App\Models\ActivationCode::whereIn("released_to", $from)
                    ->where("status", "released")
                    ->whereNull("activated_at")
                    ->select("id", "code", "type", "released_to as owner")
                    ->get();
        $activation_codes = $from->map(function($userId) use ($activation_codes_available) {
            return collect([
                "user_id" => $userId,
                "activation_codes" => [
                    "product" => $activation_codes_available->where("owner", $userId)->where("type", "product")->values(),
                    "package" => $activation_codes_available->where("owner", $userId)->where("type", "package")->values(),
                ]
            ]);
        })->keyBy("user_id");
        
        $to = $transferred_items->pluck("to");
        $used_codes = collect();
        $items = $transferred_items->map(function($record) use ($activation_codes, &$used_codes) {
            $activation_code = $activation_codes[$record->from]["activation_codes"][$record->product->type]->filter(function($record) use ($used_codes) {
                return !in_array($record->id, $used_codes->toArray());
            })->first();
            
            if ($activation_code) {
                $record->activation_code = $activation_code->id;
                $used_codes->push($activation_code->id);
            } else {
                $record->activation_code = null;
            }

            return $record;
        })
        ->filter(function($record) {
            return !is_null($record->activation_code);
        })
        ->filter(function($record) {
            return $record->from != $record->to;
        });

        DB::transaction(function () use ($items) {
            $now = \Carbon\Carbon::now();
            $items->each(function($item) use ($now) {
                $activation_code = \App\Models\ActivationCode::find($item->activation_code);
                $activation_code->released_to = $item->to;
                $activation_code->transferred_from = $item->from;
                $activation_code->transferred_at = $now;
                $activation_code->save();
            });
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEarningsEnumTypeAddLic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE earnings MODIFY `type` ENUM('direct referral', 'indirect referral', 'unilevel', 'lic')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE earnings MODIFY `type` ENUM('direct referral', 'indirect referral', 'unilevel')");
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("user_id")->unique();
            $table->unsignedBigInteger("parent_id")->nullable()->comment("The upline who registered his/her account.");
            $table->unsignedBigInteger("order_id")->nullable()->comment("Order id where the package was bought.");
            $table->unsignedBigInteger("product_id")->nullable()->comment("Package Id");
            $table->boolean("disabled")->default(false)->comment("Account may be disabled.");
            $table->boolean("top_level")->default(false)->comment("By default this is false, top level accounts are mostly by admin or owners.");
            $table->timestamp("last_activity_date")->comment("Date/Timestamp where he/she last made some activities.");
            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('parent_id')->references('id')->on('users');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}

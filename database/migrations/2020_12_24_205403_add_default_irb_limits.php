<?php

use App\Models\Package;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultIrbLimits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $eStarter = Package::where("slug", "e-starter")->first();
        $eStarter->settings->update([
            "irb_limit_per_day" => 1000,
            "irb_limit_per_month" => 30000,
        ]);
        
        $eUltra = Package::where("slug", "e-ultra")->first();
        $eUltra->settings->update([
            "irb_limit_per_day" => 1200,
            "irb_limit_per_month" => 36000,
        ]);
        
        $ePremium = Package::where("slug", "e-premium")->first();
        $ePremium->settings->update([
            "irb_limit_per_day" => 1500,
            "irb_limit_per_month" => 45000,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixAddToWalletJan2021 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $startDate = \Carbon\Carbon::parse("2021-01-01");
        $endDate = $startDate->copy()->endOfMonth();


        $order_items = DB::table("order_items")
                        ->whereIsActivated(true)
                        ->whereType('product')
                        ->whereBetween("activated_at", [$startDate->toDateString(), $endDate->toDateString()])
                        ->select("user_id", DB::raw("count(1) as total_activated"))
                        ->groupBy("user_id");
        $records = DB::table('memberships as m')
                        ->joinSub($order_items, 'records', function($join) {
                            $join->on('records.user_id', 'm.user_id');
                        })
                        ->join('earnings as e', 'e.member_id', 'm.id')
                        ->where('records.total_activated', '>=', 3)
                        ->whereDate('e.created_at', '<', $startDate->toDateString())
                        ->whereNull('e.valid_at')
                        ->select('e.*', 'records.*')
                        ->get();

        $earningsIds = $records->pluck("id");
        \DB::table("earnings")->whereIn("id", $earningsIds)->update([
            "valid_at" => $startDate->toDateString()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

<?php

use App\Models\Membership;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValuesAvailableCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $memberships = Membership::all();
        $memberships->each(function($membership) {
            $membership->update([
                "available_commissions" => $membership->total_earnings
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $memberships = Membership::all();
        $memberships->each(function($membership) {
            $membership->update([
                "available_commissions" => 0
            ]);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_earnings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("type")->comment("service_fee, others");
            $table->float("amount");
            $table->unsignedBigInteger("resource_id")->nullable();
            $table->string("resource_type")->comment("encashment")->nullable();
            $table->text("notes")->nullable();
            $table->unsignedBigInteger("transacted_by")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_earnings');
    }
}

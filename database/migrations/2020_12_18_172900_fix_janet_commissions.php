<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixJanetCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::transaction(function () {
            $user = \App\User::whereUsername("janetpaner")->first();
            
            $earnings = \App\Http\Helpers\Helpers::computeEarnings($user->membership->id);
            $encashments = $user->membership->encashment_histories()->whereIn("status", ["pending", "complete"])->sum("amount");
            $available_commissions = $earnings - $encashments;
            $user->membership->update([
                "available_commissions" => $available_commissions
            ]);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

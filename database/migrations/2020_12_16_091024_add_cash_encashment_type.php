<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCashEncashmentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE encashment_histories MODIFY COLUMN transaction_type ENUM('cash','gcash', 'paymaya', 'bdo', 'others') default 'cash'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE encashment_histories MODIFY COLUMN transaction_type ENUM('gcash', 'paymaya', 'bdo', 'others')");

    }
}

<?php

use App\Models\Earning;
use App\Models\Membership;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixJuneBonuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        try {
            $nextMonth = Carbon\Carbon::parse('2022-06-01')->firstOfMonth()->addMonths(1);
            $earnings = Earning::whereDate('created_at', '>=', '2022-06-01')
                                    ->where('type', '<>', 'direct referral')
                                    ->whereNotNull('added_to_wallet_at')
                                    ->has('transaction')
                                    ->with('transaction')
                                    ->get()
                                    ->groupBy('member_id');
            $members = Membership::whereIn('id', $earnings->keys()->toArray())->get()->keyBy('id');
            $records = $earnings->map(function ($records, $memberId) use ($members) {
                $transactions = $records->map(function ($record) {
                    return $record->transaction;
                });

                $totalAdded = $transactions->sum('debit');
                $member = $members[$memberId];

                return (object) [
                    'member'            => $member,
                    'earnings'          => $records,
                    'transactions'      => $transactions,
                    'total_added'       => $totalAdded,
                    'total_earnings'    => $member->total_earnings,
                    'correct_earnings'  => $member->total_earnings - $totalAdded
                ];
            });


            foreach ($records as $record) {
                // Earnings fix correct valid_at
                // Earnings set added_to_wallet as null
                // delete transaction
                // change membership total earnings

                foreach ($record->earnings as $earning) {
                    $earning->update([
                        'valid_at' => $nextMonth,
                        'added_to_wallet_at' => null,
                    ]);
                }
                
                foreach ($record->transactions as $item) {
                    $item->delete();
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

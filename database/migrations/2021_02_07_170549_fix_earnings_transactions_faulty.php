<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixEarningsTransactionsFaulty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $period = \Carbon\Carbon::parse("2020-12-01");
        $startAt = $period->firstOfMonth()->toDateString();
        $endAt = $period->endOfMonth()->toDateString();
        $nextMonth = $period->copy()->addMonths(1)->firstOfMonth()->toDateString();
        // get members with valid_at 2021-01-01
        $members = \App\Models\Membership::whereHas('earnings', function($q) use ($nextMonth) {
            $q->whereDate("valid_at", $nextMonth);
        })->get();
        
        $faultyEarnings = \App\Models\Earning::whereDate("valid_at", $nextMonth)
                            ->with('membership')
                            ->whereIn('type', ['indirect referral', 'unilevel'])
                            ->get();
        $userIds = $faultyEarnings->pluck("membership.user_id")->unique()->values();

        $order_items = \App\Models\OrderItem::whereIn("user_id", $userIds)
                            ->whereType("product")
                            ->whereBetween("activated_at", [$startAt, $endAt])
                            ->get()
                            ->groupBy("user_id")
                            ->map(function($record) {
                                return count($record);
                            })->filter(function($record) {
                                return $record >= 3;
                            });
            $hasActivations = $order_items->keys();
        $hasErrors = collect($userIds)->filter(function($id) use ($hasActivations){
            return !in_array($id, $hasActivations->toArray());
        })->values();
        $toFix = \App\Models\Membership::whereIn("user_id", $hasErrors)
            ->with(['earnings' => function($q) use ($startAt, $endAt, $nextMonth) {
                $q->whereIn("type", ["indirect referral", "unilevel"])
                    ->whereDate("valid_at", $nextMonth);
            }])
            ->get()->map(function($record) {
                $record->deduction = $record->earnings->sum("amount");
                return $record;
            });

        \DB::transaction(function () use ($toFix) {
            $toFix->each(function($record) {
                $balance = $record->eWallet();
                $t = new \App\Models\Transaction([
                    "user_id" => $record->user_id,
                    "member_id" => $record->id,
                    "debit" => 0,
                    "credit" => $record->deduction,
                    "type" => "earnings",
                    "resource_id" => 0,
                    "balance" => $balance - $record->deduction
                ]);

                $record->earnings->each(function($earning) {
                    $earning->fill([
                        "added_to_wallet_at" => null,
                        "valid_at" => null,
                    ]);
                    $earning->save();
                });
                $t->save();
                $record->fill([
                    "total_earnings" => \App\Http\Helpers\TransactionsHelper::getTotalEarnings($record),
                    "available_commissions" => $balance - $record->deduction
                ]);
                unset($record->deduction);

                $record->save();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

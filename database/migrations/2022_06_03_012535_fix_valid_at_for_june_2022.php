<?php

use App\Models\Earning;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixValidAtForJune2022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            $members = Earning::where('type', '<>', 'direct referral')->whereDate('created_at', '>=' , '2022-06-01')->whereNotNull('valid_at')
            ->with('membership')
            ->get()->groupBy('member_id');
            
            foreach ($members as $earnings) {
                $member = $earnings->first()->membership->user()->withCount(['activatedProducts' => function ($query) {
                    $query->whereDate('activated_at', '>=', '2022-06-01')
                            ->whereDate('activated_at', '<=', '2022-06-30');
                }])->first();
                
                $activatedProducts = $member->activated_products_count ?? 0;
                if ($activatedProducts < 3) {
                    foreach ($earnings as $earning) {
                        $earning->update([
                            'valid_at' => null
                        ]);
                    }
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

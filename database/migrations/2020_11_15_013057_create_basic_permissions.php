<?php

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = Carbon\Carbon::now();

        $data = [
            [
                "name"          => "orders.read",
                "guard_name"    => "admin",
                "created_at"    => $now,
                "updated_at"    => $now,
            ],
            [
                "name"          => "orders.write",
                "guard_name"    => "admin",
                "created_at"    => $now,
                "updated_at"    => $now,
            ],
            [
                "name"          => "orders.delete",
                "guard_name"    => "admin",
                "created_at"    => $now,
                "updated_at"    => $now,
            ],
            [
                "name"          => "orders.approve",
                "guard_name"    => "admin",
                "created_at"    => $now,
                "updated_at"    => $now,
            ],
        ];


        DB::transaction(function () use ($data) {
            // update roles add guard name
            $roles = Role::all();
            $roles->each(function($role) {
                $role->update(["guard_name" => "web"]);
            });


            collect($data)->each(function($permission) {
                Permission::create(["name" => $permission["name"]]);
            });

            $roles = Role::whereIn("identity", ["manager", "admin"])->get();
            $permissions = Permission::where('name', 'like', '%orders%')->get();

            $roles->each(function($role) use ($permissions){
                $permissions->each(function($permission) use ($role) {
                    $role->givePermissionTo($permission);
                });
            });
            
            $manager = User::where("username", "manager")->first();
            $manager->assignRole('Manager');

            $admin = User::where("username", "admin")->first();
            $admin->assignRole('Admin');

            $members = User::where("role_id", 1)->get();
            $members->each(function ($member) {
                $member->assignRole('Member');
            });

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table("permissions")->truncate();
        DB::table("model_has_permissions")->truncate();
        Schema::enableForeignKeyConstraints();
    }
}

<?php

use App\Models\Package;
use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateLicValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $lic = [
            [
                'level' => [1],
                'percentage' => 10
            ],
            [
                'level' => range(2,3),
                'percentage' => 3
            ],
            [
                'level' => range(4,5),
                'percentage' => 2
            ],
            [
                'level' => range(6,10),
                'percentage' => 1
            ],
        ];

        $records = [];
        foreach ($lic as $record) {
            foreach ($record['level'] as $level) {
                $records[] = [
                    'level' => $level,
                    'percent' => $record['percentage']
                ];
            }
        }

        $limits = [
            'E-PRM' => [
                'day' => 4000,
                'month' => 120000
            ],
            'E-SP' => [
                'day' => 12000,
                'month' => 360000
            ]  
        ];

        $packages = Package::whereIn('code', ['E-PRM', 'E-SP'])->get();

        $packages->each(function ($package) use ($records, $limits) {
            $package->settings->update([
                'leadership_indirect_commission_percentage' => json_encode($records),
                'lic_limit_per_day'                         => $limits[$package->code]['day'],
                'lic_limit_per_month'                       => $limits[$package->code]['month'],
            ]);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use App\Models\Earning;
use App\Models\Membership;
use App\Models\EncashmentHistory;
use Illuminate\Support\Facades\Schema;
use App\Http\Helpers\TransactionsHelper;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEarningsToWallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::transaction(function () {
            $members = Membership::whereNull("backup")->get();
            $members->each(function($member) {
                $member->update([
                    "backup" => $member->total_earnings
                ]);
            });

            $membersWithEarnings = Earning::where("type", "direct referral")
                ->whereNull("added_to_wallet_at")
                ->with('membership.user')
                ->get()
                ->unique("member_id");

            if ($membersWithEarnings) {
                $membersWithEarnings->each(function($earning_record) {
                    $membership = $earning_record->membership;
        
                    $earnings = Earning::where("type", "direct referral")
                                                ->where("member_id", $membership->id)
                                                ->whereNull("added_to_wallet_at")
                                                ->selectRaw("null as status, 'earnings' as type")
                                                ->addSelect("id", "amount", "created_at", "member_id");
        
                    $transactions = EncashmentHistory::where("membership_id", $membership->id)
                                                ->whereNull("transaction_recorded_at")
                                                ->selectRaw("status, 'encashment' as type")
                                                ->addSelect("id","amount", "created_at", "membership_id as member_id")
                                                ->union($earnings)
                                                ->get();
                    $transactions = $transactions->sortBy("created_at");
                    $balance = 0;
                    $records = [];
                    $record_updates = collect();
                    $transactions->each(function($transaction) use (&$records, $membership, &$balance, &$record_updates) {
                        $record = [
                            "user_id"       => $membership->user_id,
                            "member_id"     => $membership->id,
                            "created_at"    => $transaction->created_at,
                            "updated_at"    => $transaction->created_at ,
                            "type"          => $transaction->type,
                            "resource_id"   => $transaction->id
                        ];
        
                        if ($transaction->type == "encashment" && $transaction->status != "declined") {
                            $balance = $balance - $transaction->amount;
                            $record["credit"] = $transaction->amount;
                            $record["debit"] = 0;
                        } else {
                            $balance = $balance + $transaction->amount;
                            $record["debit"] = $transaction->amount;
                            $record["credit"] = 0;
                        }
                        $record["balance"] = $balance;
        
                        $records[] = $record;
        
                        if ($transaction->type == "earnings") {
                            $earning = Earning::find($transaction->id);
                            $earning->fill([
                                "added_to_wallet_at" => $transaction->created_at,
                                "valid_at"          => $transaction->created_at
                            ]);
                            $record_updates->push($earning);
                        } else {
                            $encashment = EncashmentHistory::find($transaction->id);
                            $encashment->fill([
                                "transaction_recorded_at" => $transaction->created_at
                            ]);
                            $record_updates->push($encashment);
                        }
                    });
                
        
                    DB::table('transactions')->insert($records);
        
                    $record_updates->each(function($record_update) {
                        $record_update->save();
                    });

                    $membership->total_earnings = TransactionsHelper::getTotalEarnings($membership);
                    $membership->available_commissions = DB::table("transactions")
                                                ->where("member_id", $membership->id)
                                                ->selectRaw("sum(debit) - sum(credit) as total")
                                                ->first()
                                                ->total;

                    $membership->save();
                });
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            DB::table("transactions")->truncate();

            Membership::whereNotNull("backup")->update([
                "backup" => null
            ]);
            
            Earning::whereNotNull("added_to_wallet_at")->update([
                "added_to_wallet_at" => null
            ]);

            EncashmentHistory::whereNotNull("transaction_recorded_at")->update([
                "transaction_recorded_at" => null
            ]);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnclaimCurrentMonthBonuses100 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = \Carbon\Carbon::now();

        $currentMonthBonuses = \App\Models\Earning::where("member_id", 100)
        ->whereYear("created_at", $now->year)
        ->whereMonth("created_at", $now->month)
        ->whereIn("type", ["unilevel", "indirect referral"])
        ->whereNotNull("added_to_wallet_at")
        ->with('transaction')
        ->get();
    
        $totalUnclaim = $currentMonthBonuses->sum("amount");
    
        $member = \App\Models\Membership::find(100);
    
        $transaction = new \App\Models\Transaction;
        $transaction->fill([
            "user_id" => $member->user_id,
            "member_id" => $member->id,
            "debit" => 0,
            "credit" => $totalUnclaim,
            "balance" => $member->eWallet() - $totalUnclaim,
            "resource_id" => 0,
            "transacted_by" => 2,
        ]);
        
        $bonuses = collect();
        $currentMonthBonuses->each(function($record) use (&$bonuses) {
            $bonuses->push($record->fill([
                "added_to_wallet_at" => null
            ]));
        });
    
        \DB::transaction(function () use ($bonuses, $transaction) {
            $bonuses->each(function($bonus) {
                $bonus->save();
            });
    
            $transaction->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

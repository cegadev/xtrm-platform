<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixAddToWalletOf131264293 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $period = \Carbon\Carbon::parse("2021-01-01");
        $nextMonth = $period->copy()->addMonths(1)->firstOfMonth()->toDateString();

        $startAt = $period->firstOfMonth()->toDateString();
        $endAt = $period->endOfMonth()->toDateString();
        $earnings = \App\Models\Earning::whereNull("valid_at")
            ->whereBetween("created_at", [$startAt, $endAt])
            ->whereIn('type', ['indirect referral', 'unilevel'])
            ->where("amount", ">", 0)
            ->with('membership')
            ->get()
            ->groupBy('membership.user_id');

        $order_items = \App\Models\OrderItem::whereBetween('activated_at', [$startAt, $endAt])
        ->where('type', 'product')
        ->get()
        ->groupBy('user_id')->filter(function($record) {
            return count($record) >= 3; 
        });

        $members = \App\Models\Membership::whereIn("user_id", $earnings->keys()->values())->get()->map(function($record) use ($earnings, $order_items) {
            return (object)[
                "member" => $record->only("id", "member_name", "user_id"),
                "earnings" => collect($earnings->get($record->user_id))->map(function($r) {
                    return collect($r)->only("id", "amount", "type");
                }),
                "order_items" => collect($order_items->get($record->user_id))->count()
            ];
        })->filter(function($record) {
            return $record->order_items >= 3;
        })->keyBy("member.user_id");
        
        \DB::transaction(function () use ($members, $nextMonth) {
            $members->each(function($member) use ($nextMonth) {
                $member->earnings->each(function($earning) use ($nextMonth) {
                    $e = \App\Models\Earning::find($earning["id"]);
                    $e->valid_at = $nextMonth;
                    $e->save();
                });
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wallet_of_131_264_293', function (Blueprint $table) {
            //
        });
    }
}

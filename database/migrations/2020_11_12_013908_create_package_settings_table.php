<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_settings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger("package_id");
            $table->float("direct_referral_commission")->default(0);
            $table->decimal("indirect_referral_percentage");
            $table->text("notes")->nullable();

            $table->timestamps();

            $table->foreign("package_id")->references("id")->on("products");
        });

        $existingPackages = DB::table('products')->where('type', 'package')->get();
        $now = \Carbon\Carbon::now();
        if ($existingPackages->count()) {
            $settings = [];
            $existingPackages->each(function($package) use(&$settings, $now) {
                $settings[] = [
                    "package_id" => $package->id,
                    "direct_referral_commission" => 0,
                    "indirect_referral_percentage" => 0,
                    "created_at" => $now,
                    "updated_at" => $now
                ];
            });

            DB::table('package_settings')->insert($settings);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_settings');
    }
}

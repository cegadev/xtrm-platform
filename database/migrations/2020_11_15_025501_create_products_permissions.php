<?php

use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = ["products.read", "products.write", "products.delete"];

        DB::transaction(function () use ($permissions) {
            $role = Role::where('name', 'Admin')->first();

            collect($permissions)->each(function ($permission) use ($role) {
                $permission = Permission::create(["name" => $permission]);
                
                $role->givePermissionTo($permission);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = ["products.read", "products.write", "products.delete"];
        $role = Role::where('name', 'Admin')->first();

        DB::transaction(function () use ($permissions, $role) {
            collect($permissions)->each(function ($permission) use ($role) {
                $role->revokePermissionTo($permission);
            });

            DB::table("permissions")->whereIn("name", $permissions)->delete();
        });
    }
}

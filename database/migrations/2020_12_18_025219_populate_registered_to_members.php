<?php

use App\Models\Membership;
use App\Models\ActivationCode;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateRegisteredToMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $registered_activation_codes = ActivationCode::where("status", "activated")
                                    ->where("type", "package")
                                    ->select("id", "order_id")
                                    ->get();

        $members = Membership::whereIn("order_id", $registered_activation_codes->pluck("order_id"))->get();
        $orders = $registered_activation_codes->groupBy("order_id")->map(function($records, $orderId) use ($members) {
            return (object)[
                "activation_codes" => $records,
                "members" => $members->where("order_id", $orderId)->pluck("user_id")
            ];
        });

        \DB::transaction(function () use ($orders){
            $orders->each(function($record) {
                $activation_codes = $record->activation_codes;
                $members = $record->members;
                $activation_codes->each(function($code, $key) use ($members) {
                    $code->update([
                        "registered_to" => $members->get($key)
                    ]);
                });
            });
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ActivationCode::whereRaw("1 = 1")->update([
            "registered_to" => null,
        ]);
    }
}

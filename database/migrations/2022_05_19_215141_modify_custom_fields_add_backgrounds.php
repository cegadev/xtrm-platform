<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCustomFieldsAddBackgrounds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $customFields = [
            [
                'field' => 'home-background-1',
                'data'  =>  json_encode([
                    'url' => 'https://mjpremiumtrading.com/images/slides/slider-1607656223.jpeg',
                    'darken' => true
                ]),
            ],
            [
                'field' => 'home-background-2',
                'data'  =>  json_encode([
                    'url' => 'https://mjpremiumtrading.com/images/slides/slider-1607656254.jpeg',
                    'darken' => true
                ]),
            ],
        ];

        $now = Carbon\Carbon::now();
        $customFields = collect($customFields)->map(function ($record) use ($now) {
            $record['created_at'] = $now;
            $record['updated_at'] = $now;
            return $record;
        })->toArray();

        app()->db->table('custom_fields')->insert($customFields);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        app()->db->table('custom_fields')->whereIn('field', ['home-background-1', 'home-background-2'])->delete();
    }
}

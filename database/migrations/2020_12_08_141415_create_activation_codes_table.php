<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("created_by");
            $table->string("code");
            $table->enum("type", ["product", "package"])->nullable();
            $table->enum("status", ["draft", "released", "activated"])->default('draft');
            $table->unsignedBigInteger("order_id")->nullable();
            $table->unsignedBigInteger("released_to")->nullable();
            $table->timestamp("activated_at")->nullable();
            $table->unsignedBigInteger("activated_by")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_codes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $now = \Carbon\Carbon::now();
        $data = collect(["Category 1", "Category 2"])->map(function($record) use ($now) {
            return [
                "name" => $record,
                "created_at" => $now,
                "updated_at" => $now,
            ];
        })->toArray();

        DB::table("product_categories")->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table("product_categories")->truncate();
    }
}

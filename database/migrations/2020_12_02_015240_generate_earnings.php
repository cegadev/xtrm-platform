<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenerateEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ordersCompleted = \App\Models\Order::whereIn('status', ['completed', 'received'])->with('product')->get();
        $products = $ordersCompleted->where('product.type', 'product');
        $packages = $ordersCompleted->where('product.type', 'package');
        
        $productsList = $products->map(function($record) {
            return [
                "type" => "product purchase",
                "amount" => $record->amount,
                "resource_id" => $record->user_id,
                "resource_type" => "order",
                "notes" => "Product purchase approved.",
                "transacted_by" => $record->released_by,
                "created_at" => $record->released_at,
                "updated_at" => $record->released_at,
                "order_id" => $record->id,
            ];
        });

        $packagesList = $packages->map(function($record) {
            return [
                "type" => "package purchase",
                "amount" => $record->amount,
                "resource_id" => $record->user_id,
                "resource_type" => "order",
                "notes" => "Package purchase approved.",
                "transacted_by" => $record->released_by,
                "created_at" => $record->released_at,
                "updated_at" => $record->released_at,
                "order_id" => $record->id,
            ];
        });

        $orders = $productsList->merge($packagesList)->toArray();
        DB::table("company_earnings")->insert($orders);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table("company_earnings")->whereIn("type", ["product purchase", "package purchase"])->delete();
    }
}

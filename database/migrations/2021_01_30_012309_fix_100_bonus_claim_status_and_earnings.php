<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fix100BonusClaimStatusAndEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = \Carbon\Carbon::now();

        $thisMonthClaimed = \App\Models\Earning::whereNotNull("added_to_wallet_at")
        ->whereDate("valid_at", $now->firstOfMonth()->toDateString())
        ->where("member_id", 100)
        ->whereIn("type", ["unilevel", "indirect referral"])
        ->get();

        $claimed = $thisMonthClaimed->sum("amount");
        $member = \App\Models\Membership::find(100);

        $balance = $member->eWallet();
        $member->prev_month_bonus_claimed_amount = $claimed;
        $member->prev_month_bonus_claimed_at = \Carbon\Carbon::now();
        $member->available_commissions = $balance;
        $member->total_earnings = $balance;
        $member->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

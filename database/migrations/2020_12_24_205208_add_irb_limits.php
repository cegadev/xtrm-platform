<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIrbLimits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_settings', function (Blueprint $table) {
            $table->float("irb_limit_per_day")->default(0);
            $table->float("irb_limit_per_month")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_settings', function (Blueprint $table) {
            $table->dropColumn("irb_limit_per_day");
            $table->dropColumn("irb_limit_per_month");
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBonusClaimedToMemberships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('memberships', function (Blueprint $table) {
            $table->float("prev_month_bonus_claimed_amount")->nullable();
            $table->timestamp("prev_month_bonus_claimed_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('memberships', function (Blueprint $table) {
            $table->dropColumn("prev_month_bonus_claimed_amount");
            $table->dropColumn("prev_month_bonus_claimed_at");
        });
    }
}

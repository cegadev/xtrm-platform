<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPackageSettingsAddLeadershipIndirectCommission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_settings', function (Blueprint $table) {
            $table->text('leadership_indirect_commission_percentage')->nullable()->after('indirect_referral_percentage');
            $table->float('lic_limit_per_day')->default(0);
            $table->float('lic_limit_per_month')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_settings', function (Blueprint $table) {
            $table->dropColumn(['leadership_indirect_commission_percentage', 'lic_limit_per_day', 'lic_limit_per_month']);
        });
    }
}

<?php

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManageEncashment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = Carbon\Carbon::now();

        $permission = Permission::create([
            "name"          => "manage-encashment",
            "guard_name"    => "web",
            "created_at"    => $now,
            "updated_at"    => $now,
        ]);

        $roles = Role::whereIn("identity", ["manager", "admin"])->get();
        $roles->each(function($role) use ($permission) {
            $role->givePermissionTo($permission);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permission = Permission::whereName("manage-encashment")->first();
        $roles = Role::whereIn("identity", ["manager", "admin"])->get();
        $roles->each(function ($role) use ($permission){
            $role->revokePermissionTo($permission);
        });
    }
}

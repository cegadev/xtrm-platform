<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultContactsInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = \Carbon\Carbon::now();
        $data = [
            [
                "field" => "contact-info-emails",
                "data" => "info@mjpremiumtrading.com",
                "created_at" => $now,
                "updated_at" => $now,
            ],
            [
                "field" => "contact-info-mobile-numbers",
                "data" => "0912 345 6789",
                "created_at" => $now,
                "updated_at" => $now,
            ],
            [
                "field" => "contact-info-phone-numbers",
                "data" => "0912 345 6789",
                "created_at" => $now,
                "updated_at" => $now,
            ],
            [
                "field" => "contact-info-address",
                "data" => "Unit 102 Lumera Tower, 2629 Legarda, Sampaloc Manila",
                "created_at" => $now,
                "updated_at" => $now,
            ],
        ];

        DB::table("custom_fields")->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table("custom_fields")->whereIn("field", ["contact-info-emails", "contact-info-mobile-numbers", "contact-info-phone-numbers", "contact-info-address"])->delete();
    }
}

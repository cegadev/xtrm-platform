<?php

use App\Models\Transaction;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixMembersWallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            $membersToFix = Transaction::selectRaw('
                    (sum(debit) - sum(credit)) as available_commissions, 
                    sum(debit) total_earnings, 
                    member_id, 
                    (select total_earnings from memberships where id = transactions.member_id) total_earnings_value,
                    (select available_commissions from memberships where id = transactions.member_id) member_available_commissions')
                        ->groupBy('member_id')
                        ->havingRaw("total_earnings <> total_earnings_value")
                        ->with('membership')
                        ->get()->keyBy('member_id');

            foreach ($membersToFix as $mtf) {
                $mtf->membership->update([
                    'total_earnings'        => $mtf->total_earnings,
                    'available_commissions' => $mtf->available_commissions
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use App\User;
use App\Models\Earning;
use \App\Models\Membership;
use App\Http\Helpers\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyIsValidOfEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            $now = \Carbon\Carbon::now();

            DB::table("earnings")->whereIn("type", ["direct referral"])
                                ->whereMonth("created_at", $now->month)
                                ->update([
                                    "valid_at" => $now
                                ]);

            $memberships = Membership::all();
            $now = \Carbon\Carbon::now();
            $year = $now->year;
            $thisMonth = $now->month;
            foreach($memberships as $membership) {
                $product_activations_this_month_count = $membership->user->orderItems
                                                                ->where("type", "product")
                                                                ->where("activated_at", "<>", null)
                                                                ->filter(function($record) {
                                                                    return \Carbon\Carbon::parse($record->activated_at)->format("Y-m") == \Carbon\Carbon::now()->format("Y-m");
                                                                })
                                                                ->count();
                if ($product_activations_this_month_count >= 3) {
                    Earning::whereIn("type", ["indirect referral", "unilevel"])
                        ->whereYear("created_at", $year)
                        ->whereMonth("created_at", "=", $thisMonth)
                        ->where("member_id", $membership->id)
                        ->update([
                            "valid_at" => \Carbon\Carbon::now()->addMonths(1)->firstOfMonth()
                        ]);
                }

                Helpers::computeEarnings($membership->id);
            }
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

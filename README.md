
# MJPremium Trading

An online Multi level marketing platform for Car cleaning essentials based on Calamba Laguna.
https://mjpremiumtrading.com/
## Features

- Direct and Indirect referral Commission
- Unilevel comission
- Sales and Members performance reports.

## Run Locally

Clone the project

```bash
  git clone https://markangel@bitbucket.org/cegadev/xtrm-platform.git
```

Go to the project directory

```bash
  cd xtrm-platform
```
Install composer dependencies

```bash
  composer install
```

Install npm dependencies

```bash
  npm install
```

Start the server

```bash
  php artisan serve
```

Watch changes on assets
```bash
  npm run watch
```

